package com.kapstone.model;

import java.util.List;

import org.springframework.http.HttpStatus;

import com.kapstone.model.AccessRequest.RequestList;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString (includeFieldNames = true)
@NoArgsConstructor
@AllArgsConstructor
public class SubmitResponse {
	
	public SubmitResponse (String status) {
		this.status = status;
	}
	
	public SubmitResponse (String status, HttpStatus httpStatus) {
		this.status = status;
		this.httpStatus = httpStatus;
	}

	private String status;
	
	private HttpStatus httpStatus;
	
	private List<RequestList> alreadyAssignedList;
	
	private boolean allAlreadyAssigned;
}
