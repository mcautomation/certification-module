package com.kapstone.model;

import java.time.LocalDateTime;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString (includeFieldNames = true)
public class AccessRequest {

	private String action;
	private String startDate;
	private String endDate;
	private List<RequestList> requestList;
	private String requestedBy;
	
	@Getter
	@Setter
	@ToString (includeFieldNames = true)
	public static class RequestList {
		private String comments;
		private LocalDateTime startDate;
		private LocalDateTime endDate;
		private String objectID;
		private String objectName;
		private String objectType;
		private String requestedForDisplayName;
		private long requestedForID;
		private String requestedForUserLogin;
	}
}


