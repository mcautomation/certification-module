package com.kapstone.model;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class TrackRequestResponse implements Serializable{
	
	
	private static final long serialVersionUID = 1L;
	
	private int totalRecords;
	private List<TrackRequest> requests;
	
	
	
	

}
