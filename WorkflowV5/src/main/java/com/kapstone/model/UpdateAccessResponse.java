package com.kapstone.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class UpdateAccessResponse  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String id;
	private String parentReqid;
	private Entity entity;
	private String startDate;
	private String endDate;
	private String action;
	private String requestedBy;
	private String requestedOn;
	private String requestedByEmail;
	private String requestName;
	private String requestedForID;
	private String requestedForDisplayName;
	private String requestedForEmail;
	private String managerID;
	private String managerEmail;
	private String managerName;
	private int totalRequests;
	private String subReqId;
	private String form;
	private String benefeciary;
	private String otherDetails;
	private String status;
	private String lastUpdateDateTime;
	private String comments;
	private String sodViolations;
	private String healthy;
	


}
