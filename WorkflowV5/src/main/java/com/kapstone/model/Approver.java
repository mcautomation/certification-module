package com.kapstone.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class Approver {
	
	private String action;
	private String date;
	private String status;
	private String assignedTo;
	private String comments;

}