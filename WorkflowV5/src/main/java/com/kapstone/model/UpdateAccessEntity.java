package com.kapstone.model;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class UpdateAccessEntity implements Serializable{
	
	
	private static final long serialVersionUID = 1L;
	private String id;
	private String actionComment;
	private String action;

}

