package com.kapstone.model;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString (includeFieldNames = true)
public class AccessResponse {
	private String id;
	private String entityType;
	private String entityName;
	private String displayName;
	
	/* Below fields are not used in this applications.
	 * The fields are retained for consistency at UI
	*/
	private String clientType;
	private boolean isAliasApp;
	private boolean active;
	private boolean isOPCService;
}
