package com.kapstone.model;

import java.time.LocalDateTime;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
public class OIMAccessRequest {

	private List<OIMRequest> requests;

	@Getter
	@Setter
	@ToString(includeFieldNames = true)
	@AllArgsConstructor
	@NoArgsConstructor
	public static class OIMRequest {
		private String reqJustification;
		private List<ReqBeneficiary> reqBeneficiaryList;
		private List<ReqTargetEntity> reqTargetEntities;
	}

	@Getter
	@Setter
	@ToString(includeFieldNames = true)
	@AllArgsConstructor
	@NoArgsConstructor
	public static class ReqBeneficiary implements Comparable<ReqBeneficiary> {
		
		private String id;

		@Override
		public int compareTo(ReqBeneficiary obj) {
			return this.id.compareToIgnoreCase(obj.id);
		}
	}

	@Getter
	@Setter
	@ToString(includeFieldNames = true)
	public static class ReqTargetEntity {
		private String entityId;
		private String entityType;
		private LocalDateTime startDate;
		private LocalDateTime endDate;
	}
}
