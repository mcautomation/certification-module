package com.kapstone.service;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kapstone.model.ApproveAccess;
import com.kapstone.model.ApproveAccessResponse;
import com.kapstone.model.Approver;
import com.kapstone.model.Entity;
import com.kapstone.model.UpdateAccessRequest;

@Service
public class ApproveAccessService {
	
	 @Value("${oim.domain}")
	 private String OIMURL;
	 
	 @Value("${GETAPPROVEACCESSURL}")
	 private String GETAPPROVEACCESSURL;
	 
	 @Value("${GETREQUESTURL}")
	 private String GETREQUESTURL;

	public ApproveAccessResponse getApproveAccess(String authorization) {
		RestTemplate restTemplate = new RestTemplate();
		ApproveAccessResponse approveAccessResponse = new ApproveAccessResponse();
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", authorization);
		@SuppressWarnings({ "rawtypes", "unchecked" })
		HttpEntity request = new HttpEntity(headers);
		
		String url = OIMURL+GETAPPROVEACCESSURL;
		
		ResponseEntity<String> response = restTemplate.exchange(url,HttpMethod.GET,request,String.class);
		if (response.getStatusCode() == HttpStatus.OK) {
			JSONObject jsonResponse = new JSONObject(response.getBody());
		    JSONArray requestsArray = jsonResponse.getJSONArray("requests");
		    List<ApproveAccess> approveAccessList = createApproveAccess(requestsArray);
		    approveAccessResponse.setRequests(approveAccessList);
		    approveAccessResponse.setTotalRecords(approveAccessList.size());
		}
		return approveAccessResponse;
	}
	
	public List<ApproveAccess> getApproveAccesslimit(String authorization) {
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", authorization);
		@SuppressWarnings({ "rawtypes", "unchecked" })
		HttpEntity request = new HttpEntity(headers);
		
		String url = OIMURL+GETAPPROVEACCESSURL;
		
		ResponseEntity<String> response = restTemplate.exchange(url,HttpMethod.GET,request,String.class);
		if (response.getStatusCode() == HttpStatus.OK) {
			JSONObject jsonResponse = new JSONObject(response.getBody());
		    JSONArray requestsArray = jsonResponse.getJSONArray("requests");
		    return createApproveAccess(requestsArray);
		    
		}
		return null;
	}
	
	public List<ApproveAccess> createApproveAccess(JSONArray requestsArray) {
		List<ApproveAccess> approveAccessList = new ArrayList<ApproveAccess>();
		for(int i=0;i<requestsArray.length(); i++) {
			JSONObject request = requestsArray.getJSONObject(i);
			String requestUrl = request.getJSONArray("links").getJSONObject(0).getString("href");
			String authStr = "xelsysadm:Kapstone123";
			String authorization = "Basic " + Base64.getEncoder().encodeToString(authStr.getBytes());
			System.out.println(requestUrl);
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.set("Authorization", authorization);
			@SuppressWarnings({ "rawtypes", "unchecked" })
			HttpEntity httpRequest = new HttpEntity(headers);
			
			ResponseEntity<String> response = restTemplate.exchange(requestUrl,HttpMethod.GET,httpRequest,String.class);
			
			if (response.getStatusCode() == HttpStatus.OK) {
				JSONObject jsonResponse = new JSONObject(response.getBody());
				approveAccessList.add(createRequestBean(jsonResponse, authorization));
			}
		}
		
		return approveAccessList;
	}
	
	public ApproveAccess createRequestBean(JSONObject request, String authorization) {
		System.out.println(request.toString());
		ApproveAccess approveAccess = new ApproveAccess();
		approveAccess.setId(request.getString("id"));
		approveAccess.setParentReqid(request.getString("id"));
		approveAccess.setStartDate(request.getString("reqCreatedOn"));
		//trackRequestResponse.setEndDate(endDate);
		approveAccess.setAction(request.getString("reqType"));
		approveAccess.setRequestName(request.getString("reqType"));
		approveAccess.setLastUpdateDateTime(request.getString("reqModifiedOnDate"));
		if(request.getString("reqStatus").equalsIgnoreCase("Request Completed")) {
			approveAccess.setStatus("Approved");
		}else if(request.getString("reqStatus").equalsIgnoreCase("Request Failed")) {
			approveAccess.setStatus("Rejected");
		}else if(request.getString("reqStatus").equalsIgnoreCase("Request Withdrawn")) {
			approveAccess.setStatus("Withdraw");
		}else {
			approveAccess.setStatus("Pending");
		}
		
		getBenificaryDetails(request, approveAccess, authorization);
		getEntityDetails(request, approveAccess, authorization);
		getRequesterDetails(request, approveAccess, authorization);
		
		   if(request.getString("reqStatus").equalsIgnoreCase("Request Completed") && request.getJSONArray("requestTaskDetails").length() == 0) {
			   List<Approver> approvalActions = new ArrayList<Approver>();
			   Approver approver = new Approver();
				approver.setAction("Assigned to Manager");
				approver.setStatus("APPROVE");
				approver.setAssignedTo(approveAccess.getRequestedBy());
				approver.setDate(request.getString("reqModifiedOnDate").substring(0, 10));
				approvalActions.add(approver);
				approveAccess.setApprovalActions(approvalActions);
		   }else {
			   for(int i = 0; i < request.getJSONArray("requestTaskDetails").length(); i++) {
					JSONObject history = request.getJSONArray("requestTaskDetails").getJSONObject(i).getJSONObject("history");
					List<Approver> approvalActions = new ArrayList<Approver>();
						for(int j = 0; j < history.getJSONArray("previousApprovers").length(); j++) {
							if(!history.getJSONArray("previousApprovers").getJSONObject(j).getString("approvers").isEmpty()) {
								Approver approver = new Approver();
								approver.setAction("Assigned to " + history.getJSONArray("previousApprovers").getJSONObject(j).getString("stage"));
								approver.setStatus(history.getJSONArray("previousApprovers").getJSONObject(j).getString("status"));
								approver.setAssignedTo(history.getJSONArray("previousApprovers").getJSONObject(j).getString("approvers"));
								approver.setDate(request.getString("reqModifiedOnDate").substring(0, 10));
								approvalActions.add(approver);
							}
							
						}
						for(int k = 0; k < history.getJSONArray("currentApprovers").length(); k++) {
							if(!history.getJSONArray("currentApprovers").getJSONObject(k).getString("approvers").isEmpty()) {
								Approver approver = new Approver();
								approver.setAction("Assigned to " + history.getJSONArray("currentApprovers").getJSONObject(k).getString("stage"));
								approver.setStatus(history.getJSONArray("currentApprovers").getJSONObject(k).getString("status"));
								approver.setAssignedTo(history.getJSONArray("currentApprovers").getJSONObject(k).getString("approvers"));
								approver.setDate(request.getString("reqModifiedOnDate").substring(0, 10));
								approvalActions.add(approver);
							}
							
						}
						for(int l = 0; l < history.getJSONArray("futureApprovers").length(); l++) {
							if(!history.getJSONArray("futureApprovers").getJSONObject(l).getString("approvers").isEmpty()) {
								Approver approver = new Approver();
								approver.setAction("Assigned to " + history.getJSONArray("futureApprovers").getJSONObject(l).getString("stage"));
								approver.setStatus(history.getJSONArray("futureApprovers").getJSONObject(l).getString("status"));
								approver.setAssignedTo(history.getJSONArray("futureApprovers").getJSONObject(l).getString("approvers"));
								approver.setDate(request.getString("reqModifiedOnDate").substring(0, 10));
								approvalActions.add(approver);
							}
						}
						approveAccess.setApprovalActions(approvalActions);
				}
		   }
		
		
		return approveAccess;
	}
	
	public void getBenificaryDetails(JSONObject request, ApproveAccess approveAccess,String authorization ) {
		if(request.getJSONArray("reqBeneficiaryList").length() > 0) {
			for(int i=0; i < request.getJSONArray("reqBeneficiaryList").length();i++) {
				if(request.getJSONArray("reqBeneficiaryList").getJSONObject(i).getJSONArray("links").length() > 0) {
					for(int j =0; j < request.getJSONArray("reqBeneficiaryList").getJSONObject(i).getJSONArray("links").length() ;j++) {
						String benificaryUrl = request.getJSONArray("reqBeneficiaryList").getJSONObject(i).getJSONArray("links").getJSONObject(j).getString("href");
						
						RestTemplate restTemplate = new RestTemplate();
						HttpHeaders headers = new HttpHeaders();
						headers.set("Authorization", authorization);
						@SuppressWarnings({ "rawtypes", "unchecked" })
						HttpEntity httpRequest = new HttpEntity(headers);
						
						ResponseEntity<String> response = restTemplate.exchange(benificaryUrl,HttpMethod.GET,httpRequest,String.class);
						if (response.getStatusCode() == HttpStatus.OK) {
							JSONObject jsonResponse = new JSONObject(response.getBody());
							JSONArray userFieldArray = jsonResponse.getJSONArray("fields");
							for(int k =0; k < userFieldArray.length(); k++) {
								JSONObject userField = userFieldArray.getJSONObject(k);
								if(userField.getString("name").equalsIgnoreCase("User Login")) {
									approveAccess.setRequestedForID(userField.getString("value"));
								}
								if(userField.getString("name").equalsIgnoreCase("Email")) {
									approveAccess.setRequestedForEmail(userField.getString("value"));
								}
								if(userField.getString("name").equalsIgnoreCase("Display Name")) {
									approveAccess.setRequestedForDisplayName(userField.getJSONObject("value").getString("base"));
								}
							}
						}
					}
					
				}
			}
		}
		
	}
	
	public void getRequesterDetails(JSONObject request, ApproveAccess approveAccess,String authorization ) {
		String requesterUrl = request.getJSONObject("requester").getJSONObject("link").getString("href");
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", authorization);
		@SuppressWarnings({ "rawtypes", "unchecked" })
		HttpEntity httpRequest = new HttpEntity(headers);
		
		ResponseEntity<String> response = restTemplate.exchange(requesterUrl,HttpMethod.GET,httpRequest,String.class);
		if (response.getStatusCode() == HttpStatus.OK) {
			JSONObject jsonResponse = new JSONObject(response.getBody());
			JSONArray userFieldArray = jsonResponse.getJSONArray("fields");
			for(int i =0; i < userFieldArray.length(); i++) {
				JSONObject userField = userFieldArray.getJSONObject(i);
				if(userField.getString("name").equalsIgnoreCase("User Login")) {
					approveAccess.setRequestedBy(userField.getString("value"));
				}
				if(userField.getString("name").equalsIgnoreCase("Email")) {
					approveAccess.setRequestedByEmail(userField.getString("value"));
				}
			}
		}
	}
	
	public void getEntityDetails(JSONObject request, ApproveAccess approveAccess,String authorization) {
		if(request.getJSONArray("reqTargetEntities").length() > 0) {
			for(int i =0;i<request.getJSONArray("reqTargetEntities").length(); i ++) {
				JSONObject reqTargetEntitie = request.getJSONArray("reqTargetEntities").getJSONObject(i);
				String entityUrl = request.getJSONArray("reqTargetEntities").getJSONObject(i).getJSONArray("links").getJSONObject(0).getString("href");
				
				RestTemplate restTemplate = new RestTemplate();
				HttpHeaders headers = new HttpHeaders();
				headers.set("Authorization", authorization);
				@SuppressWarnings({ "rawtypes", "unchecked" })
				HttpEntity httpRequest = new HttpEntity(headers);
				
				ResponseEntity<String> response = restTemplate.exchange(entityUrl,HttpMethod.GET,httpRequest,String.class);	
				if (response.getStatusCode() == HttpStatus.OK) {
					JSONObject jsonResponse = new JSONObject(response.getBody());
					Entity entity = new Entity();
					entity.setEntityID(reqTargetEntitie.getString("entityId"));
					entity.setEntityType(reqTargetEntitie.getString("entityType"));
					if(reqTargetEntitie.getString("entityType") != null && reqTargetEntitie.getString("entityType").equalsIgnoreCase("Role")) {
						for(int k = 0 ; i < jsonResponse.getJSONArray("fields").length() ; k ++) {
							if(jsonResponse.getJSONArray("fields").getJSONObject(k).getString("name") != null &&
									jsonResponse.getJSONArray("fields").getJSONObject(k).getString("name").equalsIgnoreCase("Role Name")) {
								entity.setEntityName(jsonResponse.getJSONArray("fields").getJSONObject(k).getString("value"));
								break;
							}
						}
						
					}else {
						entity.setEntityName(jsonResponse.getString("name"));
					}
					approveAccess.setEntity(entity);
				}
			}
		}
		
		
	}
	
	public ApproveAccessResponse updateAccessRequest(UpdateAccessRequest updateAccessRequest,String authorization) {
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.set("Authorization", authorization);
		requestHeaders.set("x-requested-by", "1");
		
		String url = OIMURL+GETREQUESTURL;
		
		ObjectMapper objMapper = new ObjectMapper();
		try {
			System.out.println("Json string = " + objMapper.writeValueAsString(updateAccessRequest));
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		HttpEntity<UpdateAccessRequest> entity = new HttpEntity<UpdateAccessRequest>(updateAccessRequest, requestHeaders);
		System.out.println(entity.toString());
		restTemplate.exchange(url,HttpMethod.PUT,entity,String.class);
		
		return getApproveAccess(authorization);
		
	}

}
