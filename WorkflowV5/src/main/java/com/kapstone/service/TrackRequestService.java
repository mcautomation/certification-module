package com.kapstone.service;

import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.kapstone.model.Approver;
import com.kapstone.model.Entity;
import com.kapstone.model.TrackRequest;
import com.kapstone.model.TrackRequestResponse;

@Service
public class TrackRequestService {
	
	@Value("${oim.domain}")
	 private String OIMURL;
	 
	 @Value("${GETREQUESTURL}")
	 private String GETREQUESTURL;
	 
	 @Value("${DELETEREQUESTURL}")
	 private String DELETEREQUESTURL;
	
	public TrackRequestResponse getRequests(String authorization, String userId) {
		TrackRequestResponse trackRequestResponse = new TrackRequestResponse();
		List<TrackRequest> trackRequestlist = new ArrayList<TrackRequest>();
		List<String> listId = new ArrayList<String>();
		getRequestRaisedByMe(authorization).forEach(action ->{
			if(!listId.contains(action.getId())) {
				listId.add(action.getId());
				trackRequestlist.add(action);
			}
			
		});
		getRequestRaisedForMe(authorization, userId).forEach(action ->{
			if(!listId.contains(action.getId())) {
				listId.add(action.getId());
				trackRequestlist.add(action);
			}
		});
		trackRequestResponse.setRequests(trackRequestlist);
		trackRequestResponse.setTotalRecords(trackRequestlist.size());
		return trackRequestResponse;
		
	}
	
	public List<TrackRequest> getNoOfRequests(String authorization, String noOfRequest) {
		List<TrackRequest> trackRequestlist = getRequestRaisedByMe(authorization);
		return trackRequestlist;
	}
	
	
	
	public List<TrackRequest> getRequestRaisedForMe(String authorization,String userId) {
		List<TrackRequest> trackRequestlist = new ArrayList<TrackRequest>();
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", authorization);
		@SuppressWarnings({ "rawtypes", "unchecked" })
		HttpEntity request = new HttpEntity(headers);
		
		String url = OIMURL+GETREQUESTURL+"?limit=50&q=beneficiary eq "+userId;
		
		ResponseEntity<String> response = restTemplate.exchange(url,HttpMethod.GET,request,String.class);
		
		if (response.getStatusCode() == HttpStatus.OK) {
		    JSONObject jsonResponse = new JSONObject(response.getBody());
		    JSONArray requestsArray = jsonResponse.getJSONArray("requests");
		    trackRequestlist = createTrackResponse(requestsArray);
		} else {
		    System.out.println("Request Failed");
		    System.out.println(response.getStatusCode());
		}
		return trackRequestlist;
		
	}
	
	public List<TrackRequest> getRequestRaisedByMe(String authorization) {
		List<TrackRequest> trackRequestlist = new ArrayList<TrackRequest>();
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", authorization);
		@SuppressWarnings({ "rawtypes", "unchecked" })
		HttpEntity request = new HttpEntity(headers);
		
		String url = OIMURL+GETREQUESTURL+"?limit=50";
		
		ResponseEntity<String> response = restTemplate.exchange(url,HttpMethod.GET,request,String.class);
		
		if (response.getStatusCode() == HttpStatus.OK) {
		    JSONObject jsonResponse = new JSONObject(response.getBody());
		    JSONArray requestsArray = jsonResponse.getJSONArray("requests");
		    trackRequestlist =  createTrackResponse(requestsArray);
		} else {
		    System.out.println("Request Failed");
		    System.out.println(response.getStatusCode());
		}
		return trackRequestlist;
		
	}
	
	public List<TrackRequest> createTrackResponse(JSONArray requestsArray) {
		List<TrackRequest> trackRequestlist = new ArrayList<TrackRequest>();
		List<String> urlList = new ArrayList<String>();
		for(int i=0;i<requestsArray.length(); i++) {
			JSONObject request = requestsArray.getJSONObject(i);
			urlList.add(request.getJSONArray("links").getJSONObject(0).getString("href"));
			
		}
		urlList.parallelStream().forEach(url ->{
			String authStr = "xelsysadm:Kapstone123";
			String authorization = "Basic " + Base64.getEncoder().encodeToString(authStr.getBytes());
			System.out.println("Thread : " + Thread.currentThread().getName() + ", value: " +url);
			RestTemplate restTemplate = new RestTemplate();
			HttpHeaders headers = new HttpHeaders();
			headers.set("Authorization", authorization);
			@SuppressWarnings({ "rawtypes", "unchecked" })
			HttpEntity httpRequest = new HttpEntity(headers);
			
			ResponseEntity<String> response = restTemplate.exchange(url,HttpMethod.GET,httpRequest,String.class);
			
			if (response.getStatusCode() == HttpStatus.OK) {
				JSONObject jsonResponse = new JSONObject(response.getBody());
				if(!jsonResponse.getString("reqType").equalsIgnoreCase("Heterogeneous Request")) {
					trackRequestlist.add(createRequestBean(jsonResponse, authorization));	
				}
			}
			
		});
		
		return trackRequestlist;
	}
	
	public TrackRequest createRequestBean(JSONObject request, String authorization) {
		System.out.println(request.toString());
		TrackRequest trackRequest = new TrackRequest();
		trackRequest.setId(request.getString("id"));
		trackRequest.setParentReqid(request.getString("id"));
		if(request.getString("reqCreatedOn") != null) {
			trackRequest.setStartDate(request.getString("reqCreatedOn").substring(0, 10));
			trackRequest.setRequestedOn(request.getString("reqCreatedOn").substring(0, 10));
		}
		//trackRequest.setEndDate(endDate);
		trackRequest.setAction(request.getString("reqType"));
		trackRequest.setRequestName(request.getString("reqType"));
		trackRequest.setLastUpdateDateTime(request.getString("reqModifiedOnDate"));
		if(request.getString("reqStatus").equalsIgnoreCase("Request Completed")) {
			trackRequest.setStatus("Approved");
		}else if(request.getString("reqStatus").equalsIgnoreCase("Request Failed")) {
			trackRequest.setStatus("Rejected");
		}else if(request.getString("reqStatus").equalsIgnoreCase("Request Withdrawn")) {
			trackRequest.setStatus("Withdraw");
		}else {
			trackRequest.setStatus("Pending");
		}
		
		getBenificaryDetails(request, trackRequest, authorization);
		getEntityDetails(request, trackRequest, authorization);
		getRequesterDetails(request, trackRequest, authorization);
		
		
		   if(request.getString("reqStatus").equalsIgnoreCase("Request Completed") && request.getJSONArray("requestTaskDetails").length() == 0) {
			   List<Approver> approvalActions = new ArrayList<Approver>();
			   Approver approver = new Approver();
				approver.setAction("Assigned to Manager");
				approver.setStatus("APPROVE");
				approver.setAssignedTo(trackRequest.getRequestedBy());
				approver.setDate(request.getString("reqModifiedOnDate").substring(0, 10));
				approvalActions.add(approver);
				trackRequest.setApprovalActions(approvalActions);
		   }else {
			   for(int i = 0; i < request.getJSONArray("requestTaskDetails").length(); i++) {
					JSONObject history = request.getJSONArray("requestTaskDetails").getJSONObject(i).getJSONObject("history");
					List<Approver> approvalActions = new ArrayList<Approver>();
						for(int j = 0; j < history.getJSONArray("previousApprovers").length(); j++) {
							if(!history.getJSONArray("previousApprovers").getJSONObject(j).getString("approvers").isEmpty()) {
								Approver approver = new Approver();
								approver.setAction("Assigned to " + history.getJSONArray("previousApprovers").getJSONObject(j).getString("stage"));
								approver.setStatus(history.getJSONArray("previousApprovers").getJSONObject(j).getString("status"));
								approver.setAssignedTo(history.getJSONArray("previousApprovers").getJSONObject(j).getString("approvers"));
								approver.setDate(request.getString("reqModifiedOnDate").substring(0, 10));
								approvalActions.add(approver);
							}
							
						}
						for(int k = 0; k < history.getJSONArray("currentApprovers").length(); k++) {
							if(!history.getJSONArray("currentApprovers").getJSONObject(k).getString("approvers").isEmpty()) {
								Approver approver = new Approver();
								approver.setAction("Assigned to " + history.getJSONArray("currentApprovers").getJSONObject(k).getString("stage"));
								approver.setStatus(history.getJSONArray("currentApprovers").getJSONObject(k).getString("status"));
								approver.setAssignedTo(history.getJSONArray("currentApprovers").getJSONObject(k).getString("approvers"));
								approver.setDate(request.getString("reqModifiedOnDate").substring(0, 10));
								approvalActions.add(approver);
							}
							
						}
						for(int l = 0; l < history.getJSONArray("futureApprovers").length(); l++) {
							if(!history.getJSONArray("futureApprovers").getJSONObject(l).getString("approvers").isEmpty()) {
								Approver approver = new Approver();
								approver.setAction("Assigned to " + history.getJSONArray("futureApprovers").getJSONObject(l).getString("stage"));
								approver.setStatus(history.getJSONArray("futureApprovers").getJSONObject(l).getString("status"));
								approver.setAssignedTo(history.getJSONArray("futureApprovers").getJSONObject(l).getString("approvers"));
								approver.setDate(request.getString("reqModifiedOnDate").substring(0, 10));
								approvalActions.add(approver);
							}
						}
						trackRequest.setApprovalActions(approvalActions);
				}
		   }
		
		return trackRequest;
	}
	
	public void getBenificaryDetails(JSONObject request, TrackRequest trackRequest,String authorization ) {
		String benificaryUrl = request.getJSONArray("reqBeneficiaryList").getJSONObject(0).getJSONArray("links").getJSONObject(0).getString("href");
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", authorization);
		@SuppressWarnings({ "rawtypes", "unchecked" })
		HttpEntity httpRequest = new HttpEntity(headers);
		
		ResponseEntity<String> response = restTemplate.exchange(benificaryUrl,HttpMethod.GET,httpRequest,String.class);
		if (response.getStatusCode() == HttpStatus.OK) {
			JSONObject jsonResponse = new JSONObject(response.getBody());
			JSONArray userFieldArray = jsonResponse.getJSONArray("fields");
			for(int i =0; i < userFieldArray.length(); i++) {
				JSONObject userField = userFieldArray.getJSONObject(i);
				if(userField.getString("name").equalsIgnoreCase("User Login")) {
					trackRequest.setRequestedForID(userField.getString("value"));
				}
				if(userField.getString("name").equalsIgnoreCase("Email")) {
					trackRequest.setRequestedForEmail(userField.getString("value"));
				}
				if(userField.getString("name").equalsIgnoreCase("Display Name")) {
					trackRequest.setRequestedForDisplayName(userField.getJSONObject("value").getString("base"));
				}
			}
		}
	}
	
	public void getRequesterDetails(JSONObject request, TrackRequest trackRequest,String authorization ) {
		String requesterUrl = request.getJSONObject("requester").getJSONObject("link").getString("href");
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", authorization);
		@SuppressWarnings({ "rawtypes", "unchecked" })
		HttpEntity httpRequest = new HttpEntity(headers);
		
		ResponseEntity<String> response = restTemplate.exchange(requesterUrl,HttpMethod.GET,httpRequest,String.class);
		if (response.getStatusCode() == HttpStatus.OK) {
			JSONObject jsonResponse = new JSONObject(response.getBody());
			JSONArray userFieldArray = jsonResponse.getJSONArray("fields");
			for(int i =0; i < userFieldArray.length(); i++) {
				JSONObject userField = userFieldArray.getJSONObject(i);
				if(userField.getString("name").equalsIgnoreCase("User Login")) {
					trackRequest.setRequestedBy(userField.getString("value"));
				}
				if(userField.getString("name").equalsIgnoreCase("Email")) {
					trackRequest.setRequestedByEmail(userField.getString("value"));
				}
			}
		}
	}
	
	public void getEntityDetails(JSONObject request, TrackRequest trackRequest,String authorization) {
		if(request.getJSONArray("reqTargetEntities").length() > 0) {
			for(int i =0;i<request.getJSONArray("reqTargetEntities").length(); i ++) {
				JSONObject reqTargetEntitie = request.getJSONArray("reqTargetEntities").getJSONObject(i);
				String entityUrl = request.getJSONArray("reqTargetEntities").getJSONObject(0).getJSONArray("links").getJSONObject(0).getString("href");
				
				RestTemplate restTemplate = new RestTemplate();
				HttpHeaders headers = new HttpHeaders();
				headers.set("Authorization", authorization);
				@SuppressWarnings({ "rawtypes", "unchecked" })
				HttpEntity httpRequest = new HttpEntity(headers);
				try {
					ResponseEntity<String> response = restTemplate.exchange(entityUrl,HttpMethod.GET,httpRequest,String.class);	
					if (response.getStatusCode() == HttpStatus.OK) {
						JSONObject jsonResponse = new JSONObject(response.getBody());
						Entity entity = new Entity();
						entity.setEntityID(reqTargetEntitie.getString("entityId"));
						entity.setEntityType(reqTargetEntitie.getString("entityType"));
						if(reqTargetEntitie.getString("entityType") != null && reqTargetEntitie.getString("entityType").equalsIgnoreCase("Role")) {
							if(jsonResponse.has("fields")) {
								for(int k = 0 ; i < jsonResponse.getJSONArray("fields").length() ; k ++) {
									if(jsonResponse.getJSONArray("fields").getJSONObject(k).getString("name") != null &&
										jsonResponse.getJSONArray("fields").getJSONObject(k).getString("name").equalsIgnoreCase("Role Name")) {
										entity.setEntityName(jsonResponse.getJSONArray("fields").getJSONObject(k).getString("value"));
										break;
									}
								}
							}
						}else {
							entity.setEntityName(jsonResponse.getString("name"));
						}
						trackRequest.setEntity(entity);
					}
				}catch (HttpClientErrorException.NotFound ex)   {
				    System.out.println(ex.getMessage());
				}
			}
		}
		
	}
	
	public String deleteRequest(String requestId,String authorization) {
		String url = OIMURL+DELETEREQUESTURL + requestId;
		
		RestTemplate restTemplate = new RestTemplate();
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", authorization);
		headers.set("x-requested-by", "1");
		@SuppressWarnings({ "rawtypes", "unchecked" })
		HttpEntity httpRequest = new HttpEntity(headers);
		
		ResponseEntity<String> response = restTemplate.exchange(url,HttpMethod.DELETE,httpRequest,String.class);
		if (response.getStatusCode() == HttpStatus.OK) {
		    String resp = "Successfully deleted";	
		    return resp;
		}
		return "Error while deleting";
		
	}

}
