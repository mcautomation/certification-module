package com.kapstone.service;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

@Component
public class AuthService {
	
	 @Value("${OIM_ENCRYPT_KEY}")
	 private String OIM_ENCRYPT_KEY;
	 
	 @Value("${tokenType}")
	 private String tokenType;
	
	public static String ALGO = "AES";
	public static String DateFormatPattern = "dd-M-yyyy hh:mm:ss";
	
	public String encrypt(String Data, byte[] keyValue) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Key key = generateKey(keyValue);
		Cipher cipher = Cipher.getInstance(ALGO);
		System.out.println(Cipher.getMaxAllowedKeyLength("AES"));
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] encVal = cipher.doFinal(Data.getBytes());
		String encryptedValue = new BASE64Encoder().encode(encVal);
		return encryptedValue;
	}


	public String decrypt(String encryptedData, byte[] keyValue) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IOException, IllegalBlockSizeException, BadPaddingException {
		Key key = generateKey(keyValue);
		Cipher cipher = Cipher.getInstance(ALGO);
		cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedData);
		byte[] decValue = cipher.doFinal(decordedValue);
		String decryptedValue = new String(decValue);
		return decryptedValue;
	}

	private Key generateKey(byte[] keyValue) {
		Key key = new SecretKeySpec(keyValue, ALGO);
		return key;
	}
	
	public String getAuthorizationHeader(String cookies) {
		
        String key = OIM_ENCRYPT_KEY;
		
		if(cookies.contains("Bearer ")) {
		
		String cookie = cookies.split(" ")[1];// splitted Bearer and cookie
		
		byte[] decodedValueByte = Base64.getDecoder().decode(cookie);
		
		String decodedValue = new String(decodedValueByte);
		
		String decryptedValue="";
		try {
			decryptedValue = decrypt(decodedValue, key.getBytes());
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | IOException e) {
			e.printStackTrace();
		}

		String []credentials = decryptedValue.split("#k:p#", 3);
		System.out.println(credentials.toString());
		String loginID = credentials[0];
		String password = credentials[1];
		
		String userBase64String  = Base64.getEncoder().encodeToString((loginID+":"+password).getBytes());

		String oimAuthHeader = tokenType + " " + userBase64String;

		return oimAuthHeader;
		
		}
		return null;
		
	}
}

