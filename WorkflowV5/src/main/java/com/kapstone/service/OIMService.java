package com.kapstone.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kapstone.model.OIMAccessRequest;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Service
public class OIMService {
	
	public static final Logger logger = LoggerFactory.getLogger(OIMService.class);

	@Autowired
	RestTemplate restTemplate;
	
	@Value("${oim.domain}")
	private String oimUrlPrefix;
	
	@Value("${oim.path.catalogs}")
	private String catalogService;
	
	@Value("${oim.path.catalogdetails}")
	private String catalogDetailsService;
	
	@Value("${oim.path.users}")
	private String userDetailsService;
	
	@Value("${oim.path.requests}")
	private String requestService;
	
	
	private static final int LIMIT = 50;

	
	public List<Catalog> retrieveCatalogsFromOIM(String authorization, String queryVal) {
		
		logger.info("Enter retrieveCatalogsFromOIM :: queryVal = " + queryVal);

		List<MediaType> mediaTypeList = new ArrayList<MediaType>();
		mediaTypeList.add(MediaType.APPLICATION_JSON);

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setAccept(mediaTypeList);
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.set("Authorization", authorization);
		requestHeaders.set("x-requested-by", "1");

		HttpEntity<String> requestEntity = new HttpEntity<>(requestHeaders);
		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
			protected boolean hasError(HttpStatus statusCode) {
				return false;
			}
		});

		ResponseEntity<CatalogResponse> responseEntity;
		CatalogResponse response;
		int offset = 1;
		List<Catalog> catalogs = new ArrayList<>();
		String oimUrl = oimUrlPrefix + catalogService;

		long start = System.currentTimeMillis();

		do {
			
			UriComponents builder = UriComponentsBuilder.fromHttpUrl(oimUrl).queryParam("offset", offset)
					.queryParam("limit", LIMIT).query("q={value}").buildAndExpand(queryVal);

			logger.info("builder = " + builder.toUriString());
			
			responseEntity = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, requestEntity,
					CatalogResponse.class);
			response = responseEntity.getBody();

			// logger.trace("Response = " + response);

			catalogs.addAll(response.getCatalogs());

			offset += LIMIT;
			
		} while (response.hasMore);

		long elapsedTime = System.currentTimeMillis() - start;
		logger.info("Get catalogs call to OIM completed in " + elapsedTime + " ms");

		logger.trace("Catalogs = " + catalogs);
		
		return catalogs;
	}

	
	public UserResponse retrieveCatalogsAssignedToUserFromOIM (String authorization, long userKey) {
		
		logger.info("Enter retrieveCatalogsAssignedToUserFromOIM :: userKey = " + userKey);
		
		List<MediaType> mediaTypeList = new ArrayList<MediaType>();
		mediaTypeList.add(MediaType.APPLICATION_JSON);

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setAccept(mediaTypeList);
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.set("Authorization", authorization);
		requestHeaders.set("x-requested-by", "1");

		HttpEntity<String> requestEntity = new HttpEntity<>(requestHeaders);
		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
			protected boolean hasError(HttpStatus statusCode) {
				return false;
			}
		});

		long start = System.currentTimeMillis();
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(oimUrlPrefix + userDetailsService)
				.queryParam("roles", "all").queryParam("accounts", "all").queryParam("entitlements", "all");
		Map<String, String> urlParams = new HashMap<>();
		urlParams.put("uuid", String.valueOf(userKey));
		//logger.debug("retrieveCatalogsAssignedToUserFromOIM :: url = " + builder.toUriString());

		ResponseEntity<UserResponse> responseEntity = restTemplate.exchange(builder.buildAndExpand(urlParams).toUri(),
				HttpMethod.GET, requestEntity, UserResponse.class);
		logger.trace("retrieveCatalogsAssignedToUserFromOIM :: user response = " + responseEntity.getBody());

		long elapsedTime = System.currentTimeMillis() - start;
		logger.info("Get user details call to OIM completed in " + elapsedTime + " ms");

		UserResponse userResponse = responseEntity.getBody();
		
		// Get the role name
		List<Role> roles = userResponse.getRoles();
		if (null != roles) {
			for (Role role : roles) {
				List<Field> fields = role.getFields();
				Field roleNameField = fields.stream().filter(field -> field.getName().equalsIgnoreCase("Role Name"))
						.findAny().orElse(null);
				if (null != roleNameField)
					role.setRoleName(String.valueOf(roleNameField.getValue()));
				Field roleKeyField = fields.stream().filter(field -> field.getName().equalsIgnoreCase("Role Key"))
						.findAny().orElse(null);
				if (null != roleKeyField)
					role.setRoleKey(String.valueOf(roleKeyField.getValue()));
			}
		}

		// Get the name of pending roles
		List<PendingRole> pendingRoles = userResponse.getPendingRoles();
		if (null != pendingRoles) {
			for (PendingRole role : pendingRoles) {
				List<Field> fields = role.getRoleGrantAttributes();
				Field roleNameField = fields.stream().filter(field -> field.getName().equalsIgnoreCase("Role Name"))
						.findAny().orElse(null);
				if (null != roleNameField)
					role.setRoleName(String.valueOf(roleNameField.getValue()));
				Field roleKeyField = fields.stream().filter(field -> field.getName().equalsIgnoreCase("Role Key"))
						.findAny().orElse(null);
				if (null != roleKeyField)
					role.setRoleKey(String.valueOf(roleKeyField.getValue()));
			}
		}

		logger.info("Exit retrieveCatalogsAssignedToUserFromOIM");
		return userResponse;
	}
	
	
	public HttpStatus submitRequesttoOIM (String authorization, OIMAccessRequest oimRequest) {

		logger.info("Enter submitRequesttoOIM: oimRequest = " + oimRequest);

		String url = oimUrlPrefix + requestService;

		List<MediaType> mediaTypeList = new ArrayList<MediaType>();
		mediaTypeList.add(MediaType.APPLICATION_JSON);
		
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setAccept(mediaTypeList);
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.set("Authorization", authorization);
		requestHeaders.set("x-requested-by", "1");
		
		ObjectMapper objMapper = new ObjectMapper();
		try {
			logger.debug("Json string = " + objMapper.writeValueAsString(oimRequest));
		} catch (JsonProcessingException e1) {
			logger.info("Exception in objMapper :: " + e1);
		}

		HttpEntity<OIMAccessRequest> entity = new HttpEntity<>(oimRequest, requestHeaders);
		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
			protected boolean hasError(HttpStatus statusCode) {
				return false;
			}
		});

		ResponseEntity<String> response = null;
		HttpStatus status = null;
		try {

			response = restTemplate.postForEntity(url, entity, String.class);

			if (null != response) {
				logger.debug("submitRequest response: " + response.getBody());
				status = response.getStatusCode();
			}
			else
				logger.info("submitRequest response is null");

		} catch (Exception e) {
			
			logger.error("Generic exception in submitRequest :: " + e + "::" + e.getMessage() + "::" + e.getCause());

			status = HttpStatus.INTERNAL_SERVER_ERROR;
		}

		logger.info("Exit submitRequest");
		return status;
	}
	
	
	public String getEntityKeyFromOIM (String authorization, String entityId) {
		
		logger.info("Enter getEntityKeyFromOIM :: entityId = " + entityId);
		List<MediaType> mediaTypeList = new ArrayList<MediaType>();
		mediaTypeList.add(MediaType.APPLICATION_JSON);

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setAccept(mediaTypeList);
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.set("Authorization", authorization);
		requestHeaders.set("x-requested-by", "1");

		HttpEntity<String> requestEntity = new HttpEntity<>(requestHeaders);
		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
			protected boolean hasError(HttpStatus statusCode) {
				return false;
			}
		});

		long start = System.currentTimeMillis();
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(oimUrlPrefix + catalogDetailsService);
		
		Map<String, String> urlParams = new HashMap<>();
		urlParams.put("id", entityId);
		logger.trace("Url = " + builder.toUriString());

		ResponseEntity<Catalog> responseEntity = restTemplate.exchange(builder.buildAndExpand(urlParams).toUri(),
				HttpMethod.GET, requestEntity, Catalog.class);
		logger.debug("Get entity key response = " + responseEntity.getBody());

		long elapsedTime = System.currentTimeMillis() - start;
		logger.info("Get entity key call to OIM completed in " + elapsedTime + " ms");
		
		return responseEntity.getBody().getEntityKey();
	}
	
		
	@Getter
	@Setter
	@ToString(includeFieldNames = true)
	static class CatalogResponse {
		private int count;
		private boolean hasMore;
		private List<Link> links;
		private List<Catalog> catalogs;
	}

	@Getter
	@Setter
	@ToString(includeFieldNames = true)
	static class Link {
		private String rel;
		private String href;
	}

	@Getter
	@Setter
	@ToString (includeFieldNames = true)
	static public class Catalog {
		private int id;
		private String entityType;
		private String entityName;
		private String entityDisplayName;
		private String entityKey;
	}
	
	@Getter
	@Setter
	@ToString(includeFieldNames = true)
	static class UserResponse {
		private List<Field> fields;
		private List<Role> roles;
		private List<PendingRole> pendingRoles;
		private List<Account> accounts;
		private List<Entitlement> entitlements;
	}

	@Getter
	@Setter
	@ToString(includeFieldNames = true)
	static class Field {
		private String name;
		private Object value;
	}

	@Getter
	@Setter
	@ToString(includeFieldNames = true)
	static class Role {
		private String id;
		private List<Field> fields;
		private String roleName;
		private String roleKey;
	}

	@Getter
	@Setter
	@ToString(includeFieldNames = true)
	static class PendingRole {
		private String roleId;
		private String userId;
		private List<Field> roleGrantAttributes;
		private String roleName;
		private String roleKey;
	}

	@Getter
	@Setter
	@ToString(includeFieldNames = true)
	static class Account {
		private String id;
		private String name;
		private String userId;
		private String appInstanceId;
		private List<Field> fields;
	}

	@Getter
	@Setter
	@ToString(includeFieldNames = true)
	static class Entitlement {
		private String id;
		private String name;
		private String description;
		private String itResourseName;
		private Field applicationInstanceName;
		private int accountKey;
		private ChildFormValues childFormValues;
	}

	@Getter
	@Setter
	static class ChildFormValues {
		private String UD_PRIVILEG_PRIVILEGE;
	}
	
	@Getter
	@Setter
	@ToString(includeFieldNames = true)
	static class UsersResponse {
		private int count;
		private boolean hasMore;
		private List<Link> links;
		private List<User> users;
	}
	
	@Getter
	@Setter
	@ToString(includeFieldNames = true)
	public static class User {
		private List<Field> fields;
		private String userLogin;
		private String userKey;
				
	}
}
