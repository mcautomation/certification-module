package com.kapstone.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.DefaultResponseErrorHandler;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Service
public class UserManagementService {

	public static final Logger logger = LoggerFactory.getLogger(UserManagementService.class);

	@Autowired
	RestTemplate restTemplate;

	@Value("${usrmgmt.domain}")
	private String usrmgmtUrlPrefix;
	
	@Value("${usrmgmt.path.reportees}")
	private String reporteesService;

		public List<DirectReport> retrieveDirectReports(String authorization, String lastLogin, String oimRemoteUser) {

		logger.info("Enter retrieveDirectReports");

		HttpHeaders headers = new HttpHeaders();
		headers.add("Authorization", authorization);
		headers.add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
		headers.add("lastLogin", lastLogin);
		headers.add("oim_remote_user", oimRemoteUser);
		HttpEntity<String> entity = new HttpEntity<>(headers);
		
		String url = usrmgmtUrlPrefix + reporteesService;
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url);

		RestTemplate restTemplate = new RestTemplate();
		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
			protected boolean hasError(HttpStatus statusCode) {
				return false;
			}
		});

		try {
			ResponseEntity<List<DirectReport>> response = restTemplate.exchange(builder.toUriString(), HttpMethod.GET,
					entity, new ParameterizedTypeReference<List<DirectReport>>() {
					});

			if (null == response || null == response.getBody() || response.getBody().isEmpty()) {
				logger.info("retrieveDirectReports :: response is empty");
				return null;
			}
			
			List<DirectReport> directReports = response.getBody();
			logger.trace("directReports = " + directReports);

			return directReports;

		} catch (HttpStatusCodeException e) {
			logger.error("HttpStatusCodeException = " + e.getResponseBodyAsString());
			return null;
		
		} catch (Exception e) {
			logger.error("Exception = " + e);
			return null;
		}
	}

	@Getter
	@Setter
	@ToString(includeFieldNames = true)
	public static class DirectReport {
		private String id;
		private String userName;
		private String displayName;
	}

}
