package com.kapstone.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import com.kapstone.exception.WorkflowException;
import com.kapstone.model.AccessRequest;
import com.kapstone.model.AccessResponse;
import com.kapstone.model.OIMAccessRequest;
import com.kapstone.model.SubmitResponse;
import com.kapstone.model.AccessRequest.RequestList;
import com.kapstone.model.OIMAccessRequest.OIMRequest;
import com.kapstone.model.OIMAccessRequest.ReqBeneficiary;
import com.kapstone.model.OIMAccessRequest.ReqTargetEntity;
import com.kapstone.service.OIMService.Account;
import com.kapstone.service.OIMService.Catalog;
import com.kapstone.service.OIMService.Entitlement;
import com.kapstone.service.OIMService.Role;
import com.kapstone.service.OIMService.UserResponse;
import com.kapstone.service.UserManagementService.DirectReport;


@Service
public class RequestAccessService {

	public static final Logger logger = LoggerFactory.getLogger(RequestAccessService.class);

	private static final String ENTITY_TYPE_APP = "ApplicationInstance";
	private static final String ENTITY_TYPE_ROLE = "Role";
	private static final String ENTITY_TYPE_ENT = "Entitlement";
	
	private static final String OPERATION = "Submit Access Request";
	
	
	@Autowired
	OIMService oimService;
	
	@Autowired
	UserManagementService userMgmtService;
	
	public List<AccessResponse> retrieveAllCatalogs (String authorization) {
		
		logger.debug("Enter retrieveAllCatalogs");
		
		List<Catalog> catalogs = oimService.retrieveCatalogsFromOIM(authorization, null);
		
		logger.debug("Exit retrieveAllCatalogs");
		
		return populateAccessReponse(catalogs);
	}
	
	
	public List<AccessResponse> retrieveItemsToRequestForUser(String authorization, long userKey) {
		
		logger.debug("Enter retrieveItemsToRequestForUser :: userKey = " + userKey);
		
		List<Catalog> catalogs = oimService.retrieveCatalogsFromOIM(authorization, null);
		if (null == catalogs || catalogs.isEmpty())
			return null;

		UserResponse user = oimService.retrieveCatalogsAssignedToUserFromOIM(authorization, userKey);
		if (null == user || ((null == user.getRoles() || user.getRoles().isEmpty())
				&& (null == user.getPendingRoles() || user.getPendingRoles().isEmpty())
				&& (null == user.getEntitlements() || user.getEntitlements().isEmpty())
				&& (null == user.getAccounts() || user.getAccounts().isEmpty()))) {
			logger.debug("User does not have any roles, entitlements or accounts. Returning entire catalog");
			return populateAccessReponse(catalogs);
		}

		// Iterate through catalog and remove items already assigned to user
		List<Catalog> catalogsToRequest = catalogs.stream().filter(Objects::nonNull)
				.filter(catalog -> (null != catalog.getEntityType() && !catalog.getEntityType().isEmpty()))
				.filter(catalog -> !userHasEntityPredicate(	authorization, 
															catalog.getEntityType(), 
															catalog.getId(), 
															catalog.getEntityName(), 
															catalog.getEntityDisplayName(), 
															user))
				.collect(Collectors.toList());

		logger.trace("Catalogs to request = " + catalogsToRequest);
		logger.debug("Exit retrieveItemsToRequestForUser");
		
		return populateAccessReponse (catalogsToRequest);

	}

	
	private List<AccessResponse> populateAccessReponse (List<Catalog> catalogsToRequest) {
		
		logger.debug("Enter populateAccessReponse");
		
		if (null == catalogsToRequest || catalogsToRequest.isEmpty()) {
			logger.info("catalogsToRequest is empty");
			return null;
		}
		
		List<AccessResponse> responseList = catalogsToRequest.stream()
				.filter(Objects::nonNull)
				.map(catalog -> {
					AccessResponse resp = new AccessResponse();
					resp.setId(String.valueOf(catalog.getId()));
					resp.setEntityType(catalog.getEntityType());
					resp.setEntityName(catalog.getEntityName());
					resp.setDisplayName(catalog.getEntityDisplayName());
					return resp;
				}).collect(Collectors.toList());
		
		logger.debug("Exit populateAccessReponse");
		return responseList;
	}
	
	
	public SubmitResponse submitRequest(String authorization, AccessRequest accessRequest) {
		
		logger.debug("Enter submitRequest");
		
		if (null == accessRequest || null == accessRequest.getRequestList()
				|| accessRequest.getRequestList().isEmpty()) {
			logger.error("submitRequest :: Request list is empty. Returning error");
			throw new WorkflowException("Invalid input", OPERATION, HttpStatus.BAD_REQUEST);
		}
			
		SubmitResponse response;
		
		List<RequestList> alreadyAssignedList = new ArrayList<AccessRequest.RequestList>();;
		accessRequest = filterItemsToBeRequested(authorization, accessRequest, alreadyAssignedList);
		
		if (null == accessRequest.getRequestList() || accessRequest.getRequestList().isEmpty()) {
			logger.error("submitRequest :: Updated request list is empty. All itenms are already assigned to user");
			
			response = new SubmitResponse("SUCCESS", HttpStatus.OK);
			response.setAlreadyAssignedList(alreadyAssignedList);
			response.setAllAlreadyAssigned(true);
			return response;
		}
		
		OIMAccessRequest oimRequest = populateOIMAccessRequest(authorization, accessRequest);
		
		HttpStatus httpStatus = oimService.submitRequesttoOIM(authorization, oimRequest);
		String statusStr = (null == httpStatus) ? "FAILED"
				: httpStatus.equals(HttpStatus.OK) ? "SUCCESS" : "FAILED";
		
		response = new SubmitResponse(statusStr, httpStatus);
		response.setAlreadyAssignedList(alreadyAssignedList);
		
		logger.debug("Exit submitRequest: response = " + response);
		return response;
		
	}
	
	
	private AccessRequest filterItemsToBeRequested (String authorization, AccessRequest accessRequest, 
				List<RequestList> alreadyAssignedList) {
		
		logger.debug("Enter filterItemsToBeRequested");
		
		Set<Long> userKeys = accessRequest.getRequestList().stream()
				.map(req -> req.getRequestedForID())
				.distinct()
				.collect(Collectors.toSet());
		logger.debug("filterItemsToBeRequested :: userKeys = " + userKeys);
		
		for (Long userKey : userKeys) {
			
			UserResponse user = oimService.retrieveCatalogsAssignedToUserFromOIM(authorization, userKey);
			
			if (null == user || ((null == user.getRoles() || user.getRoles().isEmpty())
					&& (null == user.getPendingRoles() || user.getPendingRoles().isEmpty())
					&& (null == user.getEntitlements() || user.getEntitlements().isEmpty())
					&& (null == user.getAccounts() || user.getAccounts().isEmpty()))) {
				logger.debug("filterItemsToBeRequested :: User does not have any roles, entitlements or accounts. Returning accessRequest");
				continue;
			}
			
			
			for (Iterator<RequestList> iter = accessRequest.getRequestList().iterator(); iter.hasNext();) {
				
				RequestList request = iter.next();
				if (userKey == request.getRequestedForID()
						&& userHasEntityPredicate(	authorization, 
													request.getObjectType(), 
													Integer.parseInt(request.getObjectID()), 
													request.getObjectName(), 
													request.getObjectName(), 
													user)) {
					iter.remove();
					alreadyAssignedList.add(request);
				}
			}
			
		}
		
		logger.info("Enter filterItemsToBeRequested :: Updated accessRequest = " + accessRequest);
		logger.info("Enter filterItemsToBeRequested :: alreadyAssignedList = " + alreadyAssignedList);
		
		return accessRequest;
	}
	

	private boolean userHasEntityPredicate (String authorization, String entityType, int entityId, 
					String entityName, String entityDisplayName, UserResponse user) {
		if (entityType.equalsIgnoreCase(ENTITY_TYPE_ENT)) {

			return Objects.isNull(user.getEntitlements()) ? false
					: user.getEntitlements().stream().filter(Objects::nonNull)
							.filter(ent -> (null != ent.getName() 
											&& ent.getName().equalsIgnoreCase(entityDisplayName)))
							.count() > 0 ? true : false;
			
		} else if (entityType.equalsIgnoreCase(ENTITY_TYPE_ROLE)) {

			return Objects.isNull(user.getRoles()) ? false
					: (user.getRoles().stream().filter(Objects::nonNull)
							.filter(role -> role.getRoleName().equalsIgnoreCase(entityName))
							.count() > 0
							|| user.getPendingRoles().stream().filter(Objects::nonNull)
									.filter(role -> role.getRoleName()
											.equalsIgnoreCase(entityName))
									.count() > 0) ? true : false;

		} else if (entityType.equalsIgnoreCase(ENTITY_TYPE_APP)) {
			
			String entityKey = oimService.getEntityKeyFromOIM(authorization, String.valueOf(entityId));
			return Objects.isNull(user.getAccounts()) ? false
					: user.getAccounts().stream().filter(Objects::nonNull)
							.filter(account -> null != account.getAppInstanceId()
									&& account.getAppInstanceId().equalsIgnoreCase(entityKey))
							.count() > 0 ? true : false;
		} else
			return false;
	}
	
	
	private OIMAccessRequest populateOIMAccessRequest (String authorization, AccessRequest accessRequest) {
		
		logger.debug("Enter populateOIMAccessRequest");
		OIMAccessRequest oimAccessRequest = new OIMAccessRequest();
		
		List<OIMRequest> requests = new ArrayList<>();
		
		accessRequest.getRequestList().stream().forEach(request -> {
			
			OIMRequest oimRequest = new OIMRequest();
			oimRequest.setReqJustification(request.getComments());
			
			List<ReqBeneficiary> reqBeneficiaryList = new ArrayList<>(Arrays.asList(new ReqBeneficiary
					(String.valueOf(request.getRequestedForID()))));
			
			List<ReqTargetEntity> reqTargetEntities = new ArrayList<> ();
			ReqTargetEntity reqEntity = new ReqTargetEntity();
			reqEntity.setEntityId(oimService.getEntityKeyFromOIM(authorization, request.getObjectID()));
			reqEntity.setEntityType((null != request.getObjectType()) && request.getObjectType().equalsIgnoreCase("ApplicationInstance")
					? "AppInstance" : request.getObjectType());
			reqEntity.setStartDate(request.getStartDate());
			reqEntity.setEndDate(request.getEndDate());
			reqTargetEntities.add(reqEntity);
			
			oimRequest.setReqBeneficiaryList(reqBeneficiaryList);
			oimRequest.setReqTargetEntities(reqTargetEntities);
			
			requests.add(oimRequest);
			
		});
		
		logger.debug("populateOIMAccessRequest :: requests = " + requests);
		
		oimAccessRequest.setRequests(requests);
		return oimAccessRequest;
	}

	
	public List<AccessResponse> cloneUser (String authorization, long userKey) {
		
		logger.debug("Enter cloneUser :: userKey = " + userKey);
		
		UserResponse user = oimService.retrieveCatalogsAssignedToUserFromOIM(authorization, userKey);
		logger.trace("user = " + user);
		
		if (null == user || ((null == user.getRoles() || user.getRoles().isEmpty())
				&& (null == user.getEntitlements() || user.getEntitlements().isEmpty())
				&& (null == user.getAccounts() || user.getAccounts().isEmpty()))) {
			logger.debug("User does not have any roles, entitlements or accounts");
			return null;
		}

		List<Catalog> catalogs = new ArrayList<>();
		
		for (Account account : user.getAccounts()) {
			String queryVal = "entity_type eq " + ENTITY_TYPE_APP + " and entity_key eq " + account.getAppInstanceId();
			List<Catalog> catalogList = oimService.retrieveCatalogsFromOIM (authorization, queryVal);
			if (null != catalogList && !catalogList.isEmpty())
				catalogs.add(catalogList.get(0));
		}

		List<String> roleNames = user.getRoles().stream().map(Role::getRoleName).collect(Collectors.toList());
		List<String> entitlementNames = user.getEntitlements().stream().map(Entitlement::getName).collect(Collectors.toList());
		
		if (null != roleNames && !roleNames.isEmpty()) {
			String queryVal = "entity_type eq " + ENTITY_TYPE_ROLE;
			List<Catalog> catalogList = oimService.retrieveCatalogsFromOIM (authorization, queryVal);
			if (null != catalogList && !catalogList.isEmpty())
				catalogs.addAll(catalogList.stream()
						.filter(catalog -> roleNames.contains(catalog.getEntityName()))
						.collect(Collectors.toList()));
		}
		
		if (null != entitlementNames && !entitlementNames.isEmpty()) {
			String queryVal = "entity_type eq " + ENTITY_TYPE_ENT;
			List<Catalog> catalogList = oimService.retrieveCatalogsFromOIM (authorization, queryVal);
			if (null != catalogList && !catalogList.isEmpty())
				catalogs.addAll(catalogList.stream()
							.filter(catalog -> entitlementNames.contains(catalog.getEntityDisplayName()))
							.collect(Collectors.toList()));
		}
		
		return populateAccessReponse(catalogs);
		
	}
	
	
	public List<AccessResponse> recommendedCatalogs (String authorization, String authForUserMgmt, String lastLogin, String oimRemoteUser, 
			long userKey, int percentage) {
		
		logger.debug("Enter recommendedCatalogs :: userKey = " + userKey + " :: percentage = " + percentage);
		
		List<DirectReport> directReports = userMgmtService.retrieveDirectReports(authForUserMgmt, lastLogin, oimRemoteUser);
		logger.trace("Direct Reports = " + directReports);
		
		if (null == directReports || directReports.isEmpty()) {
			logger.info("No direct reportees");
			return null;
		}
		
		Map<String, Integer> entityCountMap = populateDirectReportEntityCountMap (authorization, directReports);
		
		if (entityCountMap.isEmpty()) {
			logger.info("Could not retrieve direct reportees details");
			return null;
		}
		
		int countOfDirectReports = directReports.size();
		int reqCount = (int)((percentage/100f) * countOfDirectReports);
		logger.debug("countOfDirectReports = " + countOfDirectReports + " :: reqCount = " + reqCount);
		entityCountMap.values().removeIf(value -> value < reqCount);
		
		if (entityCountMap.isEmpty()) {
			logger.info("No entity matching input percentage");
			return null;
		}
		
		List<String> roleNames = new ArrayList<>();
		List<String> entitlementNames = new ArrayList<>();
		List<Catalog> recommendedCatalogs = new ArrayList<>();
		
		for (Map.Entry<String, Integer> entry : entityCountMap.entrySet()) {
			if (entry.getKey().startsWith(ENTITY_TYPE_APP)) {
				String queryVal = "entity_type eq " + ENTITY_TYPE_APP + " and entity_key eq " + entry.getKey().replace(ENTITY_TYPE_APP, "");
				List<Catalog> catalogList = oimService.retrieveCatalogsFromOIM (authorization, queryVal);
				if (null != catalogList && !catalogList.isEmpty())
					recommendedCatalogs.add(catalogList.get(0));
			
			} else if (entry.getKey().startsWith(ENTITY_TYPE_ROLE)) {
				roleNames.add(entry.getKey().replace(ENTITY_TYPE_ROLE, ""));
			
			} else if (entry.getKey().startsWith(ENTITY_TYPE_ENT)) {
				entitlementNames.add(entry.getKey().replace(ENTITY_TYPE_ENT, ""));
			}
		}
			
		if (null != roleNames && !roleNames.isEmpty()) {
			String queryVal = "entity_type eq " + ENTITY_TYPE_ROLE;
			List<Catalog> catalogList = oimService.retrieveCatalogsFromOIM (authorization, queryVal);
			if (null != catalogList && !catalogList.isEmpty())
				recommendedCatalogs.addAll(catalogList.stream()
						.filter(catalog -> roleNames.contains(catalog.getEntityName()))
						.collect(Collectors.toList()));
		}
		
		if (null != entitlementNames && !entitlementNames.isEmpty()) {
			String queryVal = "entity_type eq " + ENTITY_TYPE_ENT;
			List<Catalog> catalogList = oimService.retrieveCatalogsFromOIM (authorization, queryVal);
			if (null != catalogList && !catalogList.isEmpty())
				recommendedCatalogs.addAll(catalogList.stream()
							.filter(catalog -> entitlementNames.contains(catalog.getEntityDisplayName()))
							.collect(Collectors.toList()));
		}
			
		return populateAccessReponse (recommendedCatalogs);

	}
	
	
	Map<String, Integer> populateDirectReportEntityCountMap (String authorization, List<DirectReport> directReports) {
		
		Map<String, Integer> entityCountMap = new HashMap<>();
		
		for (DirectReport directReport : directReports) {
			
			UserResponse user = oimService.retrieveCatalogsAssignedToUserFromOIM(authorization, Long.parseLong(directReport.getId()));
			logger.trace("user = " + user);
			
			if (null == user)
				continue;
			
			if (null != user.getAccounts() && !user.getAccounts().isEmpty())
				user.getAccounts().forEach(account -> {
					String key = ENTITY_TYPE_APP + account.getAppInstanceId();
					entityCountMap.put(key, entityCountMap.getOrDefault(key, 0) + 1);
				});
			
			if (null != user.getRoles() && !user.getRoles().isEmpty())
				user.getRoles().forEach(role -> {
					String key = ENTITY_TYPE_ROLE + role.getRoleName();
					entityCountMap.put(key, entityCountMap.getOrDefault(key, 0) + 1);
				});
			
			if (null != user.getEntitlements() && !user.getEntitlements().isEmpty())
				user.getEntitlements().forEach(ent -> {
					String key = ENTITY_TYPE_ENT + ent.getName();
					entityCountMap.put(key, entityCountMap.getOrDefault(key, 0) + 1);
				});
		}
		
		logger.debug("Entity count map = " + entityCountMap);
		
		return entityCountMap;
	}
}