package com.kapstone.exception;

import java.time.LocalDateTime;
import java.util.Optional;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;


@ControllerAdvice
public class WorkflowExceptionHandler extends ResponseEntityExceptionHandler {
	
	private static final String GENERIC_EXCEPTION_MSG = "An error has occurred";
	
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<ExceptionResponse> handleGenericException(Exception exception, WebRequest request) 
																		throws Exception {
		
		ExceptionResponse exceptionResponse = new ExceptionResponse ();
		exceptionResponse.setTimestamp(LocalDateTime.now());
		exceptionResponse.setMessage(GENERIC_EXCEPTION_MSG);
		
		ResponseEntity<ExceptionResponse> responseEntity = 
				new ResponseEntity<ExceptionResponse> (exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
		
		return responseEntity;
		
	}
	
	@ExceptionHandler(WorkflowException.class)
	public final ResponseEntity<ExceptionResponse> handleContractorManagementException(WorkflowException exception, 
																			WebRequest request) throws Exception {
		
		ExceptionResponse exceptionResponse = new ExceptionResponse ();
		exceptionResponse.setTimestamp(LocalDateTime.now());
		exceptionResponse.setMessage
			(null == exception.getMessage() || exception.getMessage().trim().isEmpty() ? GENERIC_EXCEPTION_MSG : exception.getMessage());
		exceptionResponse.setOperation(exception.getOperation());
		
		ResponseEntity<ExceptionResponse> responseEntity = new ResponseEntity<ExceptionResponse> (exceptionResponse, 
				Optional.ofNullable(exception.getStatus()).isPresent() ? exception.getStatus() : HttpStatus.INTERNAL_SERVER_ERROR);
		
		return responseEntity;
	}
		
	
}
