package com.kapstone.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kapstone.model.TrackRequest;
import com.kapstone.model.TrackRequestResponse;
import com.kapstone.service.AuthService;
import com.kapstone.service.TrackRequestService;



@RestController
@CrossOrigin
public class RequestController {
	
	@Autowired
	TrackRequestService trackRequestService;
	
	@Autowired
	AuthService authService;
	
	
	@RequestMapping(value="/trackRequest/{userId}", method = RequestMethod.OPTIONS)
	public HttpServletResponse getRequestsOptions(HttpServletResponse response) {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD");
		response.addHeader("Access-Control-Allow-Credentials", "true");
		response.addHeader("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin");
		return response;
	}
	
	
	@GetMapping("/trackRequest/{userId}")
	@ResponseBody
	public ResponseEntity<TrackRequestResponse> getRequests(@RequestHeader("Authorization") String authorization,@PathVariable("userId") String userId) {
		
		TrackRequestResponse trackRequestResponse= trackRequestService.getRequests(authService.getAuthorizationHeader(authorization),userId);
		
		return new ResponseEntity<TrackRequestResponse>(trackRequestResponse,HttpStatus.OK);
		
	}
	
	@RequestMapping(value="deleteRequest/{requestId}", method = RequestMethod.OPTIONS)
	public HttpServletResponse deleteRequestOption(HttpServletResponse response) {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD");
		response.addHeader("Access-Control-Allow-Credentials", "true");
		response.addHeader("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin");
		return response;
	}
	
	@PutMapping("deleteRequest/{requestId}")
	@ResponseBody
	public ResponseEntity<String> deleteRequest(@RequestHeader("Authorization") String authorization, @PathVariable("requestId") String requestId){
		
		String response = trackRequestService.deleteRequest(requestId, authService.getAuthorizationHeader(authorization));
		
		return new ResponseEntity<String>(response, HttpStatus.OK);
	}
	
	@GetMapping("/trackRequest/{userId}/{noOfRequests}")
	@ResponseBody
	public ResponseEntity<TrackRequestResponse> getRecentRequests(@RequestHeader("Authorization") String authorization, @PathVariable("noOfRequests") String noOfRequests) {
		
		List<TrackRequest> trackRequestResponselist= trackRequestService.getNoOfRequests(authService.getAuthorizationHeader(authorization), noOfRequests);
		TrackRequestResponse trackRequestResponse = new TrackRequestResponse();
		
		List<TrackRequest> newtrackRequestResponselist = new ArrayList<TrackRequest>();
		int size = trackRequestResponselist.size();
		for(int i = 0; i < Integer.parseInt(noOfRequests) ; i++) {
			if(i< trackRequestResponselist.size()) {
			newtrackRequestResponselist.add(trackRequestResponselist.get(size-1-i));
			}
		}
		
		trackRequestResponse.setRequests(newtrackRequestResponselist);
		trackRequestResponse.setTotalRecords(newtrackRequestResponselist.size());
		
		return new ResponseEntity<TrackRequestResponse>(trackRequestResponse,HttpStatus.OK);
		
	}

}

