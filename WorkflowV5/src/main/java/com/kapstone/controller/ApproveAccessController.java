package com.kapstone.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.kapstone.model.ApproveAccess;
import com.kapstone.model.ApproveAccessResponse;
import com.kapstone.model.UpdateAccessRequest;
import com.kapstone.service.ApproveAccessService;



@RestController
@CrossOrigin
public class ApproveAccessController {
	
	@Autowired
	ApproveAccessService approveAccessService;
	
	@Autowired
	com.kapstone.service.AuthService authService;
	
	
	@RequestMapping(value="/getApproveAccess", method = RequestMethod.OPTIONS)
	public HttpServletResponse getApproveAccessOptions(HttpServletResponse response) {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD");
		response.addHeader("Access-Control-Allow-Credentials", "true");
		response.addHeader("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin");
		return response;
	}

	
	@GetMapping("/getApproveAccess")
	@ResponseBody
	public ResponseEntity<ApproveAccessResponse> getApproveAccess(@RequestHeader("Authorization") String authorization){
		
		ApproveAccessResponse approveAccessResponse = approveAccessService.getApproveAccess(authService.getAuthorizationHeader(authorization));
		
		return new ResponseEntity<ApproveAccessResponse>(approveAccessResponse, HttpStatus.OK);
		
	}
	
	@RequestMapping(value="/updateRequest", method = RequestMethod.OPTIONS)
	public HttpServletResponse updateRequestOptions(HttpServletResponse response) {
		response.addHeader("Access-Control-Allow-Origin", "*");
		response.addHeader("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD");
		response.addHeader("Access-Control-Allow-Credentials", "true");
		response.addHeader("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin");
		return response;
	}
	
	@PutMapping("/updateRequest")
	@ResponseBody
	public ResponseEntity<ApproveAccessResponse>  updateRequest(@RequestHeader("Authorization") String authorization, @RequestBody UpdateAccessRequest updateApproveAccessRequest) throws IOException{
		
          ApproveAccessResponse approveAccessResponse = approveAccessService.updateAccessRequest(updateApproveAccessRequest,authService.getAuthorizationHeader(authorization));
		
		return new ResponseEntity<ApproveAccessResponse>(approveAccessResponse, HttpStatus.OK);
		
	}
	
	@GetMapping("/getApproveAccess/{noOfRequests}")
	@ResponseBody
	public ResponseEntity<ApproveAccessResponse> getRecentApproveAccess(@RequestHeader("Authorization") String authorization, @PathVariable("noOfRequests") String noOfRequests){
		
		List<ApproveAccess> approveAccessResponseList = approveAccessService.getApproveAccesslimit(authService.getAuthorizationHeader(authorization));
		ApproveAccessResponse approveAccessResponse = new ApproveAccessResponse();
		
		List<ApproveAccess> newapproveAccessResponseList = new ArrayList<ApproveAccess>();
		int size = approveAccessResponseList.size();
		for(int i = 0; i < Integer.parseInt(noOfRequests) ; i++) {
			if(i < approveAccessResponseList.size()) {
				newapproveAccessResponseList.add(approveAccessResponseList.get(size-1-i));
			}
		}
		
		approveAccessResponse.setRequests(newapproveAccessResponseList);
	    approveAccessResponse.setTotalRecords(newapproveAccessResponseList.size());
		
		return new ResponseEntity<ApproveAccessResponse>(approveAccessResponse, HttpStatus.OK);
		
	}

}

