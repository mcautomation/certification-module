package com.kapstone.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kapstone.model.AccessRequest;
import com.kapstone.model.AccessResponse;
import com.kapstone.model.SubmitResponse;
import com.kapstone.service.AuthService;
import com.kapstone.service.RequestAccessService;

@RestController
@CrossOrigin
@RequestMapping ("/api/v1")
public class RequestAccessController {
	
	public static final Logger logger = LoggerFactory.getLogger(RequestAccessController.class);
	
	@Autowired
	RequestAccessService service;
	
	@Autowired
	AuthService authService;
	
	@GetMapping(value = "/catalogs", produces = "application/json")
	public ResponseEntity<List<AccessResponse>> retrieveCatalogs 
								(@RequestHeader("Authorization") String authorization) {
		
		logger.info("Enter retrieveCatalogs");
		
		List<AccessResponse> accessResponseList = service.retrieveAllCatalogs
				(authService.getAuthorizationHeader(authorization));
		
		logger.info("Exit retrieveCatalogs");
		logger.trace("retrieveCatalogs :: accessResponseList = " + accessResponseList);
		
		return new ResponseEntity<List<AccessResponse>>(accessResponseList, HttpStatus.OK);
		
	}

	@GetMapping(value = "/catalogs/{userKey}", produces = "application/json")
	public ResponseEntity<List<AccessResponse>> retrieveCatalogsToRequestForUser 
								(@RequestHeader("Authorization") String authorization,
								 @PathVariable("userKey") long userKey, 
								 HttpServletRequest request) {
		
		logger.info("Enter retrieveCatalogsToRequestForUser :: userKey = " + userKey);
		
		List<AccessResponse> accessResponseList = service.retrieveItemsToRequestForUser
				(authService.getAuthorizationHeader(authorization), userKey);
		
		logger.info("Exit retrieveCatalogsToRequestForUser");
		logger.trace("retrieveCatalogsToRequestForUser :: accessResponseList = " + accessResponseList);
		
		return new ResponseEntity<List<AccessResponse>>(accessResponseList, HttpStatus.OK);
		
	}
	
	@PostMapping (value = "/catalogs", 
			consumes = "application/json", produces = "application/json")
	public ResponseEntity<SubmitResponse> submitRequest 
								(@RequestHeader("Authorization") String authorization,
								 @RequestBody AccessRequest accessRequest,
								 HttpServletRequest request) {
	
		logger.info("Enter submitRequest :: accessRequest = " + accessRequest);
		
		SubmitResponse response = service.submitRequest(authService.getAuthorizationHeader(authorization), 
				accessRequest);
		
		logger.info("Exit submitRequest");
		logger.debug("submitRequest :: response = " + response);
		
		return new ResponseEntity<SubmitResponse> (response, null == response.getHttpStatus() 
				? HttpStatus.INTERNAL_SERVER_ERROR : response.getHttpStatus());
		
	}
	
	@GetMapping(value = "/clone/{userKey}", produces = "application/json")
	public ResponseEntity<List<AccessResponse>> cloneUser 
								(@RequestHeader("Authorization") String authorization,
								 @PathVariable("userKey") long userKey, 
								 HttpServletRequest request) {
		
		logger.info("Enter cloneUser :: userKey = " + userKey);
		
		List<AccessResponse> accessResponseList = service.cloneUser
				(authService.getAuthorizationHeader(authorization), userKey);
		
		logger.info("Exit cloneUser");
		logger.trace("cloneUser :: accessResponseList = " + accessResponseList);
		
		return new ResponseEntity<List<AccessResponse>>(accessResponseList, HttpStatus.OK);
		
	}
	
	@GetMapping(value = "/recommendation/{userKey}/{percentage}", produces = "application/json")
	public ResponseEntity<List<AccessResponse>> recommendedCatalogs 
								(@RequestHeader("Authorization") String authorization,
								 @RequestHeader("lastLogin") String lastLogin, 
								 @RequestHeader("oim_remote_user") String oimRemoteUser,
								 @PathVariable("userKey") long userKey,
								 @PathVariable("percentage") int percentage, 
								 HttpServletRequest request) {
		
		logger.info("Enter recommendedCatalogs :: userKey = " + userKey + " :: percentage = " + percentage);
		
		List<AccessResponse> accessResponseList = service.recommendedCatalogs
				(authService.getAuthorizationHeader(authorization), authorization, lastLogin, oimRemoteUser, userKey, percentage);
		
		logger.trace("recommendedCatalogs :: accessResponseList = " + accessResponseList);
		logger.info("Exit recommendedCatalogs");
		
		return new ResponseEntity<List<AccessResponse>>(accessResponseList, HttpStatus.OK);
		
	}
	
	
}