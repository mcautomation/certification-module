package com.kapstone.certification.service;

import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import com.kapstone.certification.bean.ConfidenceLevelData;
import com.kapstone.certification.bean.Login;
import com.kapstone.certification.config.CertificationConfig;
import com.kapstone.certification.oimresponse.certificate.bean.Catalog;
import com.kapstone.certification.oimresponse.certificate.bean.CatalogResponse;
import com.kapstone.certification.oimresponse.certificate.bean.DirectReport;
import com.kapstone.certification.oimresponse.certificate.bean.DirectReportsResponse;
import com.kapstone.certification.oimresponse.certificate.bean.Field;
import com.kapstone.certification.oimresponse.certificate.bean.PendingRole;
import com.kapstone.certification.oimresponse.certificate.bean.Role;
import com.kapstone.certification.oimresponse.certificate.bean.UserResponse;
import com.kapstone.certification.service.util.CertificationUtils;
import com.kapstone.certification.service.util.PropertiesUtils;

@Service
public class ConfidenceService {

	public static final Logger logger = LoggerFactory.getLogger(ConfidenceService.class);

	private static final String ENTITY_TYPE_APP = "ApplicationInstance";
	private static final String ENTITY_TYPE_ROLE = "Role";
	private static final String ENTITY_TYPE_ENT = "Entitlement";
	private static final int LIMIT = 50;

	@Autowired
	private CertificationConfig config;

	@Autowired
	private CertificationUtils utils;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	HttpHeaders requestHeaders;

	@PostConstruct
	public String getAuthHeader() {
		String clientAccess = config.getOim().getAdminUser() + ":" + config.getOim().getAdminPassword();
		String encodedStr = Base64.getEncoder().encodeToString(clientAccess.getBytes());
		return "Basic " + encodedStr;
	}

	public String findConfidenceLevel(String authorization, long userKey, String entityType, String entityName,
			ConfidenceLevelData data, List<DirectReport> directReports, Map<String, Integer> entityCountMap) {
		Login login = utils.getCredentials(authorization);
		return findConfidenceLevel(login, userKey, entityType, entityName, data, directReports, entityCountMap);
	}

	public String findConfidenceLevel(Login login, long userKey, String entityType, String entityName,
			ConfidenceLevelData data, List<DirectReport> directReports, Map<String, Integer> entityCountMap) {

		logger.debug("Enter findConfidenceLevel :: userKey = " + userKey + " :: entityType = " + entityType
				+ " :: entityName = " + entityName);

//		ConfidenceLevelData data = retrieveConfidenceLevelData();
		logger.trace("findConfidenceLevel :: data = " + data);

		String catalogItemRisk = null;
		int entityKey = 0;

		// OIM user API does not include account name. So entity key has to be used for
		// further processing
		if (ENTITY_TYPE_APP.equalsIgnoreCase(entityType)) {
			String queryVal = "entity_type eq " + ENTITY_TYPE_APP + " and entity_name eq "
					+ StringUtils.replace(entityName, " ", "::");

			int catalogId = getCatalogID(login, queryVal);

			Catalog catalog = retrieveCatalogDetailsFromOIM(login, String.valueOf(catalogId));
			if (!ObjectUtils.isEmpty(catalog)
					&& !org.apache.commons.lang3.StringUtils.isBlank(catalog.getEntityKey())) {
				entityKey = Integer.parseInt(catalog.getEntityKey());
				catalogItemRisk = catalog.getItemRisk();
			}

		} else {
			catalogItemRisk = riskAnalysis(login, userKey, entityType, entityKey, entityName);// done performance
		}

		logger.trace("findConfidenceLevel :: catalogItemRisk = " + catalogItemRisk);

		int intCatalogItemRisk = org.apache.commons.lang3.StringUtils.isBlank(catalogItemRisk) ? 0
				: Integer.parseInt(catalogItemRisk);
		String itemRisk = intCatalogItemRisk == 7 ? "high" : intCatalogItemRisk == 5 ? "medium" : "low";

		logger.debug("findConfidenceLevel :: itemRisk = " + itemRisk);

		double percentage = peerAnalysis(login, userKey, entityType, entityKey, entityName,directReports, entityCountMap);

		String confidence;
		confidence = (percentage >= data.getHigh().getPeerAnalysis()) ? "high"
				: (percentage >= data.getMedium().getPeerAnalysis()) ? "medium" : "low";

		String entityConfidence = null;
		if (null != itemRisk) {
			entityConfidence = itemRisk.equalsIgnoreCase(data.getHigh().getEntityRisk()) ? "high"
					: itemRisk.equalsIgnoreCase(data.getMedium().getEntityRisk()) ? "medium" : "low";
		}

		if (null != entityConfidence) {
			confidence = getLower(confidence, entityConfidence);
		}

		logger.debug("findConfidenceLevel :: confidence = " + confidence);
		return confidence;
	}

	private int getCatalogID(Login login, String queryVal) {

		List<Catalog> catalogList = retrieveCatalogsFromOIM(login, queryVal);
		int catalogId = 0;
		if (null != catalogList && !catalogList.isEmpty() && null != catalogList.get(0)) {
			catalogId = catalogList.get(0).getId();
		}

		return catalogId;
	}

	private String getLower(String confidence1, String confidence2) {

		if (null == confidence1 || confidence1.isEmpty())
			return confidence2;

		if (null == confidence2 || confidence2.isEmpty())
			return confidence1;

		String confidence = null;
		if (confidence1.equalsIgnoreCase("low") || confidence2.equalsIgnoreCase("low"))
			confidence = "low";
		else if (confidence1.equalsIgnoreCase("medium") || confidence2.equalsIgnoreCase("medium"))
			confidence = "medium";
		else
			confidence = "high";

		return confidence;
	}

	private double peerAnalysis(Login login, long userKey, String entityType, int entityKey, String entityName, 
			 List<DirectReport> directReports, Map<String, Integer> entityCountMap) {

		logger.debug("Enter peerAnalysis :: userKey = " + userKey + " :: entityType = " + entityType
				+ " :: entityKey = " + entityKey + " :: entityName = " + entityName);

		double percentage = 0;

//		UserResponse user = retrieveCatalogsAssignedToUserFromOIM(userKey, false, false, false);
//
//		if (null == user || user.getManagerKey() == 0) {
//			logger.info("peerAnalysis :: Cannot retrieve manager key");
//			return percentage;
//		}


//		List<DirectReport> directReports = retrieveReporteesFromOIM(user.getManagerKey());
		logger.debug("peerAnalysis :: Direct Reports = " + directReports);

		if (null == directReports || directReports.isEmpty()) {
			logger.info("peerAnalysis :: No direct reportees");
			return percentage;
		}

		int countOfDirectReports = directReports.size();

//		Map<String, Integer> entityCountMap = populateDirectReportOIMEntityCountMap(login, directReports,
//				entityType.equalsIgnoreCase(ENTITY_TYPE_APP) ? true : false,
//				entityType.equalsIgnoreCase(ENTITY_TYPE_ROLE) ? true : false,
//				entityType.equalsIgnoreCase(ENTITY_TYPE_ENT) ? true : false);

		if (!entityCountMap.isEmpty()) {
			String key = entityType.equals(ENTITY_TYPE_APP) ? (ENTITY_TYPE_APP + entityKey)
					: entityType.equals(ENTITY_TYPE_ROLE) ? (ENTITY_TYPE_ROLE + entityName)
							: (ENTITY_TYPE_ENT + entityName);

			int totalCount = entityCountMap.getOrDefault(key, 0);
			percentage = (totalCount * 100.0) / countOfDirectReports;
		}

		logger.debug("peerAnalysis :: percentage = " + percentage);

		return percentage;

	}

	public Map<String, Integer> populateDirectReportOIMEntityCountMap(Login login, List<DirectReport> directReports,
			boolean needApplications, boolean needRoles, boolean needEntitlements) {

		Map<String, Integer> entityCountMap = new HashMap<>();

		for (DirectReport directReport : directReports) {

			UserResponse user = retrieveCatalogsAssignedToUserFromOIM(Long.parseLong(directReport.getId()),
					needApplications, needRoles, needEntitlements);
			logger.trace("populateDirectReportOIMEntityCountMap :: user = " + user);

			if (null == user)
				continue;

			if (null != user.getAccounts() && !user.getAccounts().isEmpty())
				user.getAccounts().forEach(account -> {
					String key = ENTITY_TYPE_APP + account.getAppInstanceId();
					entityCountMap.put(key, entityCountMap.getOrDefault(key, 0) + 1);
				});

			if (null != user.getRoles() && !user.getRoles().isEmpty())
				user.getRoles().forEach(role -> {
					String key = ENTITY_TYPE_ROLE + role.getRoleName();
					entityCountMap.put(key, entityCountMap.getOrDefault(key, 0) + 1);
				});

			if (null != user.getEntitlements() && !user.getEntitlements().isEmpty())
				user.getEntitlements().forEach(ent -> {
					String key = ENTITY_TYPE_ENT + ent.getName();
					entityCountMap.put(key, entityCountMap.getOrDefault(key, 0) + 1);
				});
		}

		logger.debug("populateDirectReportOIMEntityCountMap :: Entity count map = " + entityCountMap);

		return entityCountMap;
	}

	/*
	 * entityKey - should be app instance id for application, not required for role
	 * and entitlement entityName - role name for role and dsplay name for
	 * entitlement. Not required for application
	 */
	private String riskAnalysis(Login login, long userKey, String entityType, int entityKey, String entityName) {

		logger.debug("Enter riskAnalysis :: userKey = " + userKey + " :: entityType = " + entityType
				+ " :: entityKey = " + entityKey + " :: entityName = " + entityName);
		int catalogId = 0;

		if (entityType.equalsIgnoreCase(ENTITY_TYPE_ROLE)
				&& org.apache.commons.lang3.StringUtils.isNotBlank(entityName)) {
			String queryVal = "entity_type eq " + ENTITY_TYPE_ROLE + " and entity_name eq "
					+ StringUtils.replace(entityName, " ", "::");
			catalogId = getCatalogID(login, queryVal);
		} else if (entityType.equalsIgnoreCase(ENTITY_TYPE_ENT)
				&& org.apache.commons.lang3.StringUtils.isNotBlank(entityName)) {
			String queryVal = "entity_type eq " + ENTITY_TYPE_ENT + " and entity_display_name eq "
					+ StringUtils.replace(entityName, " ", "::");
			catalogId = getCatalogID(login, queryVal);
		} else if (entityType.equalsIgnoreCase(ENTITY_TYPE_APP)) {
			String queryVal = "entity_type eq " + ENTITY_TYPE_APP + " and entity_key eq " + entityKey;
			catalogId = getCatalogID(login, queryVal);
		}

		logger.debug("riskAnalysis :: catalogId = " + catalogId);

		Catalog catalog = retrieveCatalogDetailsFromOIM(login, String.valueOf(catalogId));

		return catalog.getItemRisk();

	}

	public ConfidenceLevelData retrieveConfidenceLevelData() {
		PropertiesUtils propUtils = new PropertiesUtils();

		return propUtils.retrieveConfidenceLevelData(config.getCertificationFile());

	}

	public List<Catalog> retrieveCatalogsFromOIM(Login login, String queryVal) {

		logger.info("Enter retrieveCatalogsFromOIM :: queryVal = " + queryVal);

//		Login login = utils.getCredentials(authorization);

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.set("Authorization", utils.getAuthorizationHeader(login));
		requestHeaders.set("x-requested-by", "1");

		HttpEntity<String> requestEntity = new HttpEntity<>(requestHeaders);

//		RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
//		RestTemplate restTemplate = restTemplateBuilder.build();
//		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
//			protected boolean hasError(HttpStatus statusCode) {
//				return false;
//			}
//		});

		ResponseEntity<CatalogResponse> responseEntity;
		CatalogResponse response;
		int offset = 1;
		List<Catalog> catalogs = new ArrayList<>();
		String oimUrl = config.getUrl().getPrefix() + config.getUrl().getCatalogs();

		long start = System.currentTimeMillis();

		do {

			UriComponents builder = UriComponentsBuilder.fromHttpUrl(oimUrl).queryParam("offset", offset)
					.queryParam("limit", LIMIT).query("q={value}").buildAndExpand(queryVal);

			logger.info("builder = " + builder.toUriString());

			responseEntity = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, requestEntity,
					CatalogResponse.class);
			response = responseEntity.getBody();

			// logger.trace("Response = " + response);

			catalogs.addAll(response.getCatalogs());

			offset += LIMIT;

		} while (response.isHasMore());

		long elapsedTime = System.currentTimeMillis() - start;
		logger.info("Get catalogs call to OIM completed in " + elapsedTime + " ms");

		logger.trace("Catalogs = " + catalogs);

		return catalogs;
	}

	public Catalog retrieveCatalogDetailsFromOIM(Login login, String catalogId) {

		logger.info("Enter getEntityKeyFromOIM :: catalogId = " + catalogId);

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.set("Authorization", utils.getAuthorizationHeader(login));
		requestHeaders.set("x-requested-by", "1");

		HttpEntity<String> requestEntity = new HttpEntity<>(requestHeaders);

		long start = System.currentTimeMillis();

		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(config.getUrl().getPrefix() + config.getUrl().getCatalogDetails());

		Map<String, String> urlParams = new HashMap<>();
		urlParams.put("id", catalogId);

		logger.trace("Url = " + builder.toUriString());

		ResponseEntity<Catalog> responseEntity = restTemplate.exchange(builder.buildAndExpand(urlParams).toUri(),
				HttpMethod.GET, requestEntity, Catalog.class);

		logger.debug("Get entity key response = " + responseEntity.getBody());
		long elapsedTime = System.currentTimeMillis() - start;
		logger.info("Get entity key call to OIM completed in " + elapsedTime + " ms");

		return responseEntity.getBody();
	}

	public UserResponse retrieveCatalogsAssignedToUserFromOIM(long userKey, boolean needApplications, boolean needRoles,
			boolean needEntitlements) {

		logger.info("Enter retrieveCatalogsAssignedToUserFromOIM :: userKey = " + userKey + " :: needApplications = "
				+ needApplications + " :: needRoles = " + needRoles + " :: needEntitlements = " + needEntitlements);

		HttpEntity<String> requestEntity = new HttpEntity<>(requestHeaders);


		long start = System.currentTimeMillis();
		UriComponentsBuilder builder = UriComponentsBuilder
				.fromHttpUrl(config.getUrl().getPrefix() + config.getUrl().getUsers());
		if (needApplications)
			builder = builder.queryParam("accounts", "all");
		if (needRoles)
			builder = builder.queryParam("roles", "all");
		if (needEntitlements)
			builder = builder.queryParam("entitlements", "all");

		Map<String, String> urlParams = new HashMap<>();
		urlParams.put("uuid", String.valueOf(userKey));
		ResponseEntity<UserResponse> responseEntity = restTemplate.exchange(builder.buildAndExpand(urlParams).toUri(),
				HttpMethod.GET, requestEntity, UserResponse.class);
		logger.trace("retrieveCatalogsAssignedToUserFromOIM :: user response = " + responseEntity);

		long elapsedTime = System.currentTimeMillis() - start;
		logger.info("Get user details call to OIM completed in " + elapsedTime + " ms");

		UserResponse userResponse = responseEntity.getBody();

		// Get the role name
		List<Role> roles = userResponse.getRoles();
		if (null != roles) {
			for (Role role : roles) {
				List<Field> fields = role.getFields();
				Field roleNameField = fields.stream().filter(field -> field.getName().equalsIgnoreCase("Role Name"))
						.findAny().orElse(null);
				if (null != roleNameField)
					role.setRoleName(String.valueOf(roleNameField.getValue()));
				Field roleKeyField = fields.stream().filter(field -> field.getName().equalsIgnoreCase("Role Key"))
						.findAny().orElse(null);
				if (null != roleKeyField)
					role.setRoleKey(String.valueOf(roleKeyField.getValue()));
			}
		}

		// Get the name of pending roles
		List<PendingRole> pendingRoles = userResponse.getPendingRoles();
		if (null != pendingRoles) {
			for (PendingRole role : pendingRoles) {
				List<Field> fields = role.getRoleGrantAttributes();
				Field roleNameField = fields.stream().filter(field -> field.getName().equalsIgnoreCase("Role Name"))
						.findAny().orElse(null);
				if (null != roleNameField)
					role.setRoleName(String.valueOf(roleNameField.getValue()));
				Field roleKeyField = fields.stream().filter(field -> field.getName().equalsIgnoreCase("Role Key"))
						.findAny().orElse(null);
				if (null != roleKeyField)
					role.setRoleKey(String.valueOf(roleKeyField.getValue()));
			}
		}

		Integer mgrKey = (Integer) userResponse.getFields().stream()
				.filter(field -> field.getName().equals("usr_manager_key")).map(field -> field.getValue()).findFirst()
				.orElse(null);
		if (null != mgrKey)
			userResponse.setManagerKey(mgrKey);

		logger.info("Exit retrieveCatalogsAssignedToUserFromOIM");
		return userResponse;
	}

	public List<DirectReport> retrieveReporteesFromOIM(long userKey) {

		logger.info("Enter retrieveReporteesFromOIM :: userKey = " + userKey);

		HttpEntity<String> requestEntity = new HttpEntity<>(requestHeaders);
//		RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
//		RestTemplate restTemplate = restTemplateBuilder.build();
//		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
//			protected boolean hasError(HttpStatus statusCode) {
//				return false;
//			}
//		});

		ResponseEntity<DirectReportsResponse> responseEntity;
		DirectReportsResponse response;
		int offset = 1;
		List<DirectReport> directReports = new ArrayList<>();
		String oimUrl = config.getUrl().getPrefix() + config.getUrl().getReportees();

		long start = System.currentTimeMillis();

		do {

			UriComponents builder = UriComponentsBuilder.fromHttpUrl(oimUrl).queryParam("offset", offset)
					.queryParam("limit", LIMIT).buildAndExpand(userKey);

			logger.info("builder = " + builder.toUriString());

			responseEntity = restTemplate.exchange(builder.toUriString(), HttpMethod.GET, requestEntity,
					DirectReportsResponse.class);
			response = responseEntity.getBody();

			// logger.trace("Response = " + response);

			directReports.addAll(response.getUsers());

			offset += LIMIT;

		} while (response.isHasMore());

		long elapsedTime = System.currentTimeMillis() - start;
		logger.info("Get reportees call to OIM completed in " + elapsedTime + " ms");

		logger.trace("Reportees = " + directReports);

		return directReports;
	}

	@Bean
	public HttpHeaders requestHeaders() {
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.set("Authorization", getAuthHeader());
		requestHeaders.set("x-requested-by", "1");
		return requestHeaders;
	}

}
