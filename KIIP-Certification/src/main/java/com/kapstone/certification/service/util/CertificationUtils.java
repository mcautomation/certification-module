package com.kapstone.certification.service.util;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.kapstone.certification.bean.Login;
import com.kapstone.certification.config.CertificationConfig;
import com.kapstone.certification.oimrequest.certificate.bean.Field;
import com.kapstone.certification.oimrequest.certificate.bean.LineItemDetail;
import com.kapstone.certification.oimrequest.certificate.bean.LineItemDetails;

import lombok.extern.slf4j.Slf4j;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

@Slf4j
@Component
public class CertificationUtils {
	
	@Autowired
	CertificationConfig config;
	
	public static String ALGO = "AES";
	public static String DateFormatPattern = "dd-M-yyyy hh:mm:ss";
	
	public Login getCredentials(String cookies) 
	{

		Login login = new Login();
		
		String key = "uivj3377kFVvI2dMBE1s0g==";//config.getOim().getEncryptkey();
		
		if(cookies.contains("Bearer ")) {
		
		String cookie = cookies.split(" ")[1];// splitted Bearer and cookie
		
		byte[] decodedValueByte = Base64.getDecoder().decode(cookie);
		
		String decodedValue = new String(decodedValueByte);
		
		String decryptedValue="";
		try {
			decryptedValue = decrypt(decodedValue, key.getBytes());
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | IOException e) {
			e.printStackTrace();
		}

		String []credentials = decryptedValue.split("#k:p#", 3);
		
		String loginID = credentials[0];
		String password = credentials[1];
		String loginDate = credentials[2];

			
		login.setUserName(loginID);
		login.setPassword(password);
		login.setLoginDateAndTime(loginDate);
		
		}

		return login;

	}
	
	public String encrypt(String Data, byte[] keyValue) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Key key = generateKey(keyValue);
		Cipher cipher = Cipher.getInstance(ALGO);
		System.out.println(Cipher.getMaxAllowedKeyLength("AES"));
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] encVal = cipher.doFinal(Data.getBytes());
		String encryptedValue = new BASE64Encoder().encode(encVal);
		return encryptedValue;
	}


	public String decrypt(String encryptedData, byte[] keyValue) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IOException, IllegalBlockSizeException, BadPaddingException {
		Key key = generateKey(keyValue);
		Cipher cipher = Cipher.getInstance(ALGO);
		cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedData);
		byte[] decValue = cipher.doFinal(decordedValue);
		String decryptedValue = new String(decValue);
		return decryptedValue;
	}

	private Key generateKey(byte[] keyValue) {
		Key key = new SecretKeySpec(keyValue, ALGO);
		return key;
	}
	
	public String getAuthorizationHeader(Login login) {
		
		String userBase64String  = Base64.getEncoder().encodeToString((login.getUserName()+":"+login.getPassword()).getBytes());

		String oimAuthHeader = config.getOim().getTokenType() + " " + userBase64String;
//		String oimAuthHeader = "Basic" + " " + userBase64String;

		log.info("oimAuthHeader" + oimAuthHeader);

		return oimAuthHeader;
	}
	
	public static void main(String argsp[])
	{
		CertificationUtils util = new CertificationUtils();
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(DateFormatPattern);
		String loginDateAndTime = simpleDateFormat.format(new Date());
		System.out.println("LOGIN DATE AND TIME= "+loginDateAndTime);
//		String credentials = "TEST081#k:p#Oracle@123#k:p#"+loginDateAndTime;
		String credentials = "xelsysadm#k:p#Kap123$tone#k:p#"+loginDateAndTime;
		
		try {
			String encryptedCredentials = util.encrypt(credentials, "uivj3377kFVvI2dMBE1s0g==".getBytes());
			
			byte[] encodedCredentialsInByte = Base64.getEncoder().encode(encryptedCredentials.getBytes());
			String encodedCredentials = new String(encodedCredentialsInByte);
			
			System.out.println("CertificationUtils.main()"+ encodedCredentials);
			
			Login login = util.getCredentials("Bearer YUwzeWxta0RjcHAxYnY5am5OTW80cmJKVDQ3cktoOGpSK0RXWm84MlN5UXZtenQwSUo2WXg3bFRuakw4VEZmNQ==");

			HttpHeaders headers = new HttpHeaders();
			headers.setContentType(MediaType.APPLICATION_JSON);
			headers.set("Authorization", util.getAuthorizationHeader(login));
			headers.set("X-Response-Time","X-Response-Time: 1000");
//			headers.set("X-Requested-By",login.getUserName());

//			HttpEntity entity = new HttpEntity<>(headers);
			
			LineItemDetails lids = new LineItemDetails();
			
			
			Field f = new Field();
			f.setName("comment");
			f.setName("new comments");
			

			LineItemDetail lid = new LineItemDetail();
			lid.setEntityId(876);
			lid.setEntityType("ACCOUNT");
			lid.setAction("certify");
			lid.setFields(Arrays.asList(f));
			lids.setLineItemDetails(Arrays.asList(lid));
			
			
			HttpEntity entity = new HttpEntity<>(lids, headers);
			
			RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
			RestTemplate restTemplate = restTemplateBuilder.build();
			
			Map<String,Object> variables = new HashMap<>();
			
			variables.put("certificationId","55");
			variables.put("taskId","845fcbaf-d236-45d2-b51b-ccbcc2a90a74");
			variables.put("linItemId","49");

			String url = "http://192.168.1.22:14000/iam/governance/selfservice/api/v1/certifications/{certificationId}/tasks/{taskId}/lineitems/{linItemId}";
//			String url = "http://192.168.1.22:14000/iam/governance/selfservice/api/v1/certifications/55/tasks/845fcbaf-d236-45d2-b51b-ccbcc2a90a74/lineitems/49";
			
			entity = new HttpEntity<>(headers);
			url = "http://192.168.1.22:14000/iam/governance/selfservice/api/v1/accounts/84";

//			ResponseEntity<String> certificationsResponse = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class,variables);
			
//			 ResponseEntity<com.kapstone.certification.oimresponse.accounts.bean.Selfservice> selfserviceData = restTemplate.exchange(url,
//					  HttpMethod.GET, entity, com.kapstone.certification.oimresponse.accounts.bean.Selfservice.class);
//			 
//			 System.out.println("CertificationUtils.main()"+selfserviceData);
//			 
//			final Map map = new HashMap();
//			 
//			 selfserviceData.getBody().getFields().stream().filter(e->e.getName().equals("Normalize Data")).forEach(e->{
//				 
//				 
//				
//				 {
//					 
//					 
//					 
//					 
//				 }
//				 else
//				 {
//					 map.put(e.getName(), e.getValue());
//				 }
//				 
//				 
//				 
//			 });
//			 
//			 System.out.println("CertificationUtils.main()"+map);
			 
			
			
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


}
