package com.kapstone.certification.service;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.PostConstruct;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.util.UriComponentsBuilder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.kapstone.certification.bean.AllUserOfflineActionableInfo;
import com.kapstone.certification.bean.ApplicationInfo;
import com.kapstone.certification.bean.ApplicationProfile;
import com.kapstone.certification.bean.CertificationActionForUserRequest;
import com.kapstone.certification.bean.CertificationActionForUserResponse;
import com.kapstone.certification.bean.CertificationActionRequest;
import com.kapstone.certification.bean.CertificationActionResponse;
import com.kapstone.certification.bean.CertificationInfo;
import com.kapstone.certification.bean.Comment;
import com.kapstone.certification.bean.ConfidenceLevelData;
import com.kapstone.certification.bean.EntitlementCount;
import com.kapstone.certification.bean.EntitlementInfo;
import com.kapstone.certification.bean.EntityAppinstance;
import com.kapstone.certification.bean.EntityEntitlement;
import com.kapstone.certification.bean.Entityrole;
import com.kapstone.certification.bean.GetCertificateUsersResponse;
import com.kapstone.certification.bean.GetCertificationResponse;
import com.kapstone.certification.bean.GetUserDetailsResponse;
import com.kapstone.certification.bean.Login;
import com.kapstone.certification.bean.Members;
import com.kapstone.certification.bean.ReviewerCertificateActionInfo;
import com.kapstone.certification.bean.ReviewerCertificationInfo;
import com.kapstone.certification.bean.Roleinfo;
import com.kapstone.certification.bean.UserCertificationInfo;
import com.kapstone.certification.bean.UserDetails;
import com.kapstone.certification.bean.UserInfo;
import com.kapstone.certification.certificate.signoff.bean.SignOffCertification;
import com.kapstone.certification.certificate.signoff.bean.SignOffCertificationResponse;
import com.kapstone.certification.config.CertificationConfig;
import com.kapstone.certification.oimrequest.certificate.bean.Field;
import com.kapstone.certification.oimrequest.certificate.bean.LineItemDetail;
import com.kapstone.certification.oimrequest.certificate.bean.OIMCertificationActionRequest;
import com.kapstone.certification.oimrequest.certificate.bean.OIMCompleteAndRevokeRequest;
import com.kapstone.certification.oimrequest.certificate.bean.OIMLineItemRequest;
import com.kapstone.certification.oimresponse.certificate.bean.Certification;
import com.kapstone.certification.oimresponse.certificate.bean.CertificationDefinition;
import com.kapstone.certification.oimresponse.certificate.bean.CertificationDefinitions;
import com.kapstone.certification.oimresponse.certificate.bean.CertificationLineItem;
import com.kapstone.certification.oimresponse.certificate.bean.CertificationTaskLineItems;
import com.kapstone.certification.oimresponse.certificate.bean.Certifications;
import com.kapstone.certification.oimresponse.certificate.bean.DCertificationLineItem;
import com.kapstone.certification.oimresponse.certificate.bean.DirectReport;
import com.kapstone.certification.oimresponse.certificate.bean.LineItemDetails;
import com.kapstone.certification.oimresponse.certificate.bean.Link;
import com.kapstone.certification.oimresponse.certificate.bean.OIMCertificationActionResponse;
import com.kapstone.certification.oimresponse.certificate.bean.Selfservice;
import com.kapstone.certification.oimresponse.certificate.bean.UserResponse;
import com.kapstone.certification.service.util.CertificationUtils;
import com.kapstone.certification.uirequest.certificate.bean.UserActionRequestDetails;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class OimCertificatioWrapperService {

	@Autowired
	private CertificationConfig config;

	@Autowired
	private CertificationUtils utils;

	@Autowired
	ConfidenceService confidenceService;

	@Autowired
	private RestTemplate restTemplate;

	private Set<String> allowedCertificationActions;

	private static final String REASSIGN = "reassign";
	private static final String SIGNOFF = "signoff";
	private static final String COMPLETE = "complete";
	private static final String REVOKE = "revoke";

	private Map<String, String> certTypes = new HashMap<>();

	@PostConstruct
	void init() {
		allowedCertificationActions = new HashSet<>();
		allowedCertificationActions.addAll(Arrays.asList(SIGNOFF, REASSIGN, COMPLETE, REVOKE));

		certTypes.put("3", "UserCertification");
		certTypes.put("12", "RoleCertification");
		certTypes.put("16", "ApplicationCertification");
//		certTypes.put(32,"EntitlementCertification");

	}

	// 1st Call
	public List<GetCertificationResponse> getCertifications(String reviewerId, String authorization, Integer offset,
			Integer limit, boolean isSignOffDataRequired) {

		List<GetCertificationResponse> getCertificationResponseList = new ArrayList<GetCertificationResponse>();

		Login login = utils.getCredentials(authorization);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", utils.getAuthorizationHeader(login));
		headers.set("X-Response-Time", "X-Response-Time: 1000");

		HttpEntity entity = new HttpEntity<>(headers);

//		RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
//		RestTemplate restTemplate = restTemplateBuilder.build();

		String url = config.getUrl().getPrefix() + config.getUrl().getCertifications();

		int offSet = offset != null ? offset.intValue() : 1;

		int pagelimit = limit != null ? limit.intValue() : config.getOim().getPagelimit();

		String url1 = url + "?offset=" + offSet + "&limit=" + pagelimit;

		ResponseEntity<Certifications> certificationsResponse = restTemplate.exchange(url1, HttpMethod.GET, entity,
				Certifications.class);

		if (certificationsResponse.getStatusCode() == HttpStatus.OK) {
			List<Certification> certifications = certificationsResponse.getBody().getCertifications();

			if (limit == null) {
				while (certificationsResponse.getBody().getHasMore()) {
					offSet += pagelimit;
					String url2 = url + "?offset=" + offSet + "&limit=" + pagelimit;
					certificationsResponse = restTemplate.exchange(url2, HttpMethod.GET, entity, Certifications.class);

					if (certificationsResponse.getStatusCode() == HttpStatus.OK) {
						certifications.addAll(certificationsResponse.getBody().getCertifications());
					}
					log.info("" + certificationsResponse.getBody().getHasMore());
					log.info("offSet->" + offSet);
				}
			}

			log.info(certificationsResponse.getBody().toString());

			certifications.forEach(certification -> {
				GetCertificationResponse gcr = new GetCertificationResponse();
				gcr.setCertificationId(Integer.parseInt(certification.getId()));
				gcr.setCampaignId(certification.getId());
				gcr.setTaskId(certification.getTaskId()); // added
				gcr.getReviewerCertificationInfo().setCertificationName(certification.getName());
				gcr.getReviewerCertificationInfo().setCertificationType(certification.getType() + "Certification");
				gcr.getReviewerCertificationInfo().setCertificateRequester(null);

				Instant instant = Instant.parse(certification.getCreatedDate());

				gcr.getReviewerCertificationInfo()
						.setCertificationCreatedOn(instant.atZone(ZoneId.systemDefault()).toLocalDate().toString());
				gcr.getReviewerCertificationInfo().setCertificationExpiration(instant.atZone(ZoneId.systemDefault())
						.toLocalDate().plusDays(config.getOim().getCertificationExpirationDays()).toString());
				gcr.getReviewerCertificationInfo().setCertificationSignedOff("NO");
				gcr.getReviewerCertificationInfo().setCertificationSignedOffOn(null);
				gcr.getReviewerCertificationInfo().setCertificationSignedOffBy(null);
				gcr.getReviewerCertificationInfo()
						.setStatus(certification.getState().equals("ASSIGNED") ? "ACTIVE" : "");

				String url2 = config.getUrl().getPrefix() + config.getUrl().getCertificationsInfo();

				ResponseEntity<CertificationDefinitions> certificationsDefResponse = restTemplate.exchange(url2,
						HttpMethod.GET, entity, CertificationDefinitions.class, certification.getId());

//				Predicate<CertificationDefinition> cdPredicate = cd -> cd.getName().equals("percentComplete");	
//								
//				if(certificationsDefResponse.getStatusCode() == HttpStatus.OK)
//				{					
//					Optional<CertificationDefinition> cDef = certificationsDefResponse.getBody().getCertificationDefinitions().stream().filter(cdPredicate).findAny();
//					if(cDef.isPresent())
//					{
//						gcr.getReviewerCertificateActionInfo().setPercentageCompleted((String)cDef.get().getValue());
//					}
//				}

				if (certificationsDefResponse.getStatusCode() == HttpStatus.OK) {
					String[] roles = null;
					String[] accounts = null;
					String[] entitlements = null;

					for (CertificationDefinition nameValuePair : certificationsDefResponse.getBody()
							.getCertificationDefinitions()) {

						if (nameValuePair.getName().equalsIgnoreCase("percentComplete")) {
							gcr.getReviewerCertificateActionInfo()
									.setPercentageCompleted(Double.parseDouble((String) nameValuePair.getValue()));
						} else if (nameValuePair.getName().equalsIgnoreCase("roleAssignments")) {
							roles = (null != nameValuePair.getValue()) ? ((String) nameValuePair.getValue()).split(" ")
									: null;
						} else if (nameValuePair.getName().equalsIgnoreCase("accountAssignments")) {
							accounts = (null != nameValuePair.getValue())
									? ((String) nameValuePair.getValue()).split(" ")
									: null;
						} else if (nameValuePair.getName().equalsIgnoreCase("entitlementAssignments")) {
							entitlements = (null != nameValuePair.getValue())
									? ((String) nameValuePair.getValue()).split(" ")
									: null;
						}
					}

					int totalActionsCompleted = ((null != roles && roles.length == 4) ? Integer.parseInt(roles[0]) : 0)
							+ ((null != accounts && accounts.length == 4) ? Integer.parseInt(accounts[0]) : 0)
							+ ((null != entitlements && entitlements.length == 4) ? Integer.parseInt(entitlements[0])
									: 0);

					int totalActions = ((null != roles && roles.length == 4) ? Integer.parseInt(roles[2]) : 0)
							+ ((null != accounts && accounts.length == 4) ? Integer.parseInt(accounts[2]) : 0)
							+ ((null != entitlements && entitlements.length == 4) ? Integer.parseInt(entitlements[2])
									: 0);

					gcr.getReviewerCertificateActionInfo().setTotalActions(totalActions);
					gcr.getReviewerCertificateActionInfo().setTotalActionsCompleted(totalActionsCompleted);

				}

				getCertificationResponseList.add(gcr);

			});

		}

		if (isSignOffDataRequired) {
			List<SignOffCertification> signOffCertificationList = getSignOffCertifications(authorization, login);
			System.out.println("OimCertificatioWrapperService.getCertifications(sign off certifcation Founds)"
					+ signOffCertificationList.size());
			if (!signOffCertificationList.isEmpty()) {
				getCertificationResponseList.addAll(getSignOffCertificationData(login, signOffCertificationList));
			}
		}

		return getCertificationResponseList;

	}

	// here we need to get data from KIIPModueService
	private List<SignOffCertification> getSignOffCertifications(String authorization, Login login) {
//		Login login = utils.getCredentials(authorization);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", authorization);
		headers.set("X-Response-Time", "X-Response-Time: 1000");
		headers.set("lastLogin", login.getLoginDateAndTime());
		headers.set("oim_remote_user", login.getUserName());

		HttpEntity<String> entity = new HttpEntity<>(headers);

//		RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
//		RestTemplate restTemplate = restTemplateBuilder.build();

		String url = config.getUrl().getUserModulePrefix() + config.getUrl().getSignOffCertification();

		Map<String, Object> variables = new HashMap<>();

		variables.put("userModuleTenentID", config.getOim().getUserModule());
		variables.put("userId", login.getUserName());

		List<SignOffCertification> signOffCertificationList = new ArrayList<>();

		ResponseEntity<SignOffCertification[]> signCertificationsResponse = restTemplate.exchange(url, HttpMethod.GET,
				entity, SignOffCertification[].class, variables);

		if (signCertificationsResponse.getStatusCode() == HttpStatus.OK) {
			return Arrays.asList(signCertificationsResponse.getBody().clone());
		}

		return signOffCertificationList;

	}

	private List<GetCertificationResponse> getSignOffCertificationData(Login login,
			List<SignOffCertification> signOffCertificationList) {
		List<GetCertificationResponse> getCertificationResponseList = new ArrayList<GetCertificationResponse>();

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", utils.getAuthorizationHeader(login));
		headers.set("X-Response-Time", "X-Response-Time: 1000");

		HttpEntity<String> entity = new HttpEntity<>(headers);

//		RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
//		RestTemplate restTemplate = restTemplateBuilder.build();

		String url = config.getUrl().getPrefix() + config.getUrl().getCertificationSignOffInfo();

		signOffCertificationList.stream().forEach(signOffInstance -> {

			Map<String, Object> variables = new HashMap<>();

			variables.put("certificationid", signOffInstance.getId());
			variables.put("taskid", signOffInstance.getTaskId());

			try {
				ResponseEntity<SignOffCertificationResponse> signCertificationsResponse = restTemplate.exchange(url,
						HttpMethod.GET, entity, SignOffCertificationResponse.class, variables);

				if (signCertificationsResponse.getStatusCode() == HttpStatus.OK) {

					SignOffCertificationResponse soCertificationResponseInst = signCertificationsResponse.getBody();

					GetCertificationResponse gcr = new GetCertificationResponse();
					gcr.setCertificationId(
							Integer.parseInt(soCertificationResponseInst.getCertificationInstance().getId()));
					gcr.setCampaignId(soCertificationResponseInst.getCertificationInstance().getId());
					gcr.setTaskId(soCertificationResponseInst.getTaskId()); // added
					gcr.getReviewerCertificationInfo().setCertificationName(soCertificationResponseInst.getTaskName());
					gcr.getReviewerCertificationInfo().setCertificationType(
							certTypes.getOrDefault(signOffInstance.getType(), "UserCertification"));
					// gcr.getReviewerCertificationInfo().setCertificateRequester(null);

					Instant startDateinstant = Instant.parse(soCertificationResponseInst.getStartDate());

					gcr.getReviewerCertificationInfo().setCertificationCreatedOn(
							startDateinstant.atZone(ZoneId.systemDefault()).toLocalDate().toString());

					if (!org.springframework.util.StringUtils.isEmpty(soCertificationResponseInst.getEndDate())) {
						Instant endDateInstant = Instant.parse(soCertificationResponseInst.getEndDate());
						gcr.getReviewerCertificationInfo().setCertificationExpiration(
								endDateInstant.atZone(ZoneId.systemDefault()).toLocalDate().toString());
					}
					gcr.getReviewerCertificationInfo().setCertificationSignedOff("YES");
					gcr.getReviewerCertificationInfo().setCertificationSignedOffOn(null);
					gcr.getReviewerCertificationInfo().setCertificationSignedOffBy(null);
					gcr.getReviewerCertificationInfo().setStatus("CLOSE");

					gcr.getReviewerCertificateActionInfo()
							.setPercentageCompleted(soCertificationResponseInst.getPercentComplete());

					getCertificationResponseList.add(gcr);

				}
			} catch (Exception e) {
				e.printStackTrace();
			}

		});

		return getCertificationResponseList;
	}

	private CertificationDefinitions getCertificationDetails(String certificationId, Login login) {

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", utils.getAuthorizationHeader(login));
		headers.set("X-Response-Time", "X-Response-Time: 1000");

		HttpEntity<String> entity = new HttpEntity<>(headers);

//		RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
//		RestTemplate restTemplate = restTemplateBuilder.build();

		String url = config.getUrl().getPrefix() + config.getUrl().getCertificationsInfo();

		try {
			ResponseEntity<CertificationDefinitions> certificationsResponse = restTemplate.exchange(url, HttpMethod.GET,
					entity, CertificationDefinitions.class, certificationId);
			return certificationsResponse.getBody();

		} catch (HttpStatusCodeException e) {
			System.out.println("getCertificationDetails :: exception body = " + e.getResponseBodyAsString());

		}
		return null;

	}

	public List<DCertificationLineItem> getCertificationTaskLineItems(String certificationId, String taskId,
			Login login) {

//		Login login = utils.getCredentials(authorization);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", utils.getAuthorizationHeader(login));
		headers.set("X-Response-Time", "X-Response-Time: 1000");

		HttpEntity<String> entity = new HttpEntity<>(headers);

//		RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
//		RestTemplate restTemplate = restTemplateBuilder.build();

		ResponseEntity<CertificationTaskLineItems> responseEntity;
		CertificationTaskLineItems response;
		int offset = 1;
		int limit = 50;
		List<DCertificationLineItem> lineItems = new ArrayList<>();
		String url = config.getUrl().getPrefix() + config.getUrl().getCertificationLineItems();

		do {
			UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(url).queryParam("offset", offset)
					.queryParam("limit", limit);
			Map<String, String> urlParams = new HashMap<>();
			urlParams.put("certificationid", String.valueOf(certificationId));
			urlParams.put("taskid", String.valueOf(taskId));

			responseEntity = restTemplate.exchange(builder.buildAndExpand(urlParams).toUri(), HttpMethod.GET, entity,
					CertificationTaskLineItems.class);
			response = responseEntity.getBody();

			// logger.trace("Response = " + response);

			Link next = response.getLinks().stream().filter(link -> link.getRel().equalsIgnoreCase("next")).findAny()
					.orElse(null);
			url = null != next ? next.getHref() : null;

			lineItems.addAll(response.getCertLineItemList());

		} while (response.isHasMore() && null != url);

		return lineItems;

	}

	public String getCertificationStatus(String certificationId, Login login) {
		String status = "";
//		Login login = utils.getCredentials(authorization);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", utils.getAuthorizationHeader(login));
		headers.set("X-Response-Time", "X-Response-Time: 1000");

		HttpEntity<String> entity = new HttpEntity<>(headers);

//		RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
//		RestTemplate restTemplate = restTemplateBuilder.build();

		String url = config.getUrl().getPrefix() + config.getUrl().getCertificationsStatusInfo();

		ResponseEntity<Certifications> certificationsResponse = restTemplate.exchange(url, HttpMethod.GET, entity,
				Certifications.class, certificationId);

		if (certificationsResponse.getStatusCode() == HttpStatus.OK) {
			List<Certification> certifications = certificationsResponse.getBody().getCertifications();

			if (!certifications.isEmpty()) {
				Certification certification = certifications.get(0);

				status = certification.getState().equals("ASSIGNED") ? "ACTIVE" : "";
			}
		}

		return status;

	}

	public GetCertificateUsersResponse getUsersByCertificateId(String certificationId, String taskId,
			String authorization, boolean isSignOffDataRequired) {
		Login login = utils.getCredentials(authorization);

		return getUsersByCertificateId(certificationId, taskId, login, isSignOffDataRequired);
	}

	public GetCertificateUsersResponse getUsersByCertificateId(String certificationId, String taskId, Login login,
			boolean isSignOffDataRequired) {

		System.out.println("In getUsersByCertificateId");
		ReviewerCertificationInfo certificationInfo = new ReviewerCertificationInfo();
		ReviewerCertificateActionInfo certificateActionInfo = new ReviewerCertificateActionInfo();

//		Login login = utils.getCredentials(authorization);

		GetCertificateUsersResponse getCertificateUsersResponse = new GetCertificateUsersResponse();
		getCertificateUsersResponse.setCertificationId(Integer.parseInt(certificationId));
		getCertificateUsersResponse.setCampaignId(certificationId);
		getCertificateUsersResponse.setTaskId(taskId);

		if (!isSignOffDataRequired) {
			certificationInfo.setCertificationSignedOff("NO");
			certificationInfo.setCertificationSignedOffOn(null);
			certificationInfo.setCertificationSignedOffBy(null);

			String[] roles = null;
			String[] accounts = null;
			String[] entitlements = null;

			CertificationDefinitions definitions = getCertificationDetails(certificationId, login);
			if (null == definitions) {
				return getCertificateUsersResponse;
			}

			String status = getCertificationStatus(certificationId, login);
			certificationInfo.setStatus(status);

			for (CertificationDefinition nameValuePair : definitions.getCertificationDefinitions()) {

				if (nameValuePair.getName().equalsIgnoreCase("type")) {
					certificationInfo.setCertificationType((String) nameValuePair.getValue());
				} else if (nameValuePair.getName().equalsIgnoreCase("name")) {
					certificationInfo.setCertificationName((String) nameValuePair.getValue());
				} else if (nameValuePair.getName().equalsIgnoreCase("createdDate")) {

					certificationInfo.setCertificationCreatedOn((String) nameValuePair.getValue());

					Instant instant = Instant.parse((String) nameValuePair.getValue());

					certificationInfo.setCertificationCreatedOn(
							instant.atZone(ZoneId.systemDefault()).toLocalDate().toString());
					certificationInfo.setCertificationExpiration(instant.atZone(ZoneId.systemDefault())
							.toLocalDate().plusDays(config.getOim().getCertificationExpirationDays()).toString());

				}
				// else if (nameValuePair.getName().equalsIgnoreCase("status")) {
				// certificationInfo.setStatus((String) nameValuePair.getValue());
				// }
				else if (nameValuePair.getName().equalsIgnoreCase("percentComplete")) {
					certificateActionInfo
							.setPercentageCompleted((Double.parseDouble((String) nameValuePair.getValue())));
				} else if (nameValuePair.getName().equalsIgnoreCase("roleAssignments")) {
					roles = (null != nameValuePair.getValue()) ? ((String) nameValuePair.getValue()).split(" ") : null;
				} else if (nameValuePair.getName().equalsIgnoreCase("accountAssignments")) {
					accounts = (null != nameValuePair.getValue()) ? ((String) nameValuePair.getValue()).split(" ")
							: null;
				} else if (nameValuePair.getName().equalsIgnoreCase("entitlementAssignments")) {
					entitlements = (null != nameValuePair.getValue()) ? ((String) nameValuePair.getValue()).split(" ")
							: null;
				}
			}

			int totalActionsCompleted = ((null != roles && roles.length == 4) ? Integer.parseInt(roles[0]) : 0)
					+ ((null != accounts && accounts.length == 4) ? Integer.parseInt(accounts[0]) : 0)
					+ ((null != entitlements && entitlements.length == 4) ? Integer.parseInt(entitlements[0]) : 0);

			int totalActions = ((null != roles && roles.length == 4) ? Integer.parseInt(roles[2]) : 0)
					+ ((null != accounts && accounts.length == 4) ? Integer.parseInt(accounts[2]) : 0)
					+ ((null != entitlements && entitlements.length == 4) ? Integer.parseInt(entitlements[2]) : 0);

			certificateActionInfo.setTotalActions(totalActions);
			certificateActionInfo.setTotalActionsCompleted(totalActionsCompleted);
		} else {
			SignOffCertification signInstance = new SignOffCertification();
			signInstance.setId(certificationId);
			signInstance.setTaskId(taskId);

			List<SignOffCertification> signOffCertificationList = new ArrayList<>();
			signOffCertificationList.add(signInstance);

			List<GetCertificationResponse> getCertificationResponseList = getSignOffCertificationData(login,
					Arrays.asList(signInstance));

			if (!getCertificationResponseList.isEmpty()) {
				GetCertificationResponse certificationResponse = getCertificationResponseList.get(0);

				certificationInfo = certificationResponse.getReviewerCertificationInfo();
				certificateActionInfo = certificationResponse.getReviewerCertificateActionInfo();
			}
		}

		List<DCertificationLineItem> lineItems = getCertificationTaskLineItems(certificationId, taskId, login);

		List<Members> members = new ArrayList<>();
		for (DCertificationLineItem lineItem : lineItems) {
			UserInfo user = new UserInfo();
			user.setUserName(lineItem.getUserLogin());
			user.setFirstName(lineItem.getFirstName());
			user.setLastName(lineItem.getLastName());
			user.setEmail(lineItem.getEmail());
			user.setDepartment(lineItem.getDepartment());
			user.setManager(lineItem.getManager());
			user.setUserkey(lineItem.getIamUserId());

			Members member = new Members();
			member.setUserInfo(user);
			member.setNumOfRoles(lineItem.getRoles());
			member.setNumOfApplications(lineItem.getAccounts());
			member.setNumOfEntitlements(lineItem.getEntitlements());
			member.setPercentageCompleted(lineItem.getPercentComplete());
			member.setLineItemId(lineItem.getId());
			member.setLineItemEntityId(lineItem.getEntityId());

			members.add(member);
		}

//		Login login = utils.getCredentials(authorization);
		getCertificateUsersResponse.setReviewerId(login.getUserName());

		getCertificateUsersResponse.setReviewerCertificationInfo(certificationInfo);
		getCertificateUsersResponse.setReviewerCertificateActionInfo(certificateActionInfo);
		getCertificateUsersResponse.setMembers(members);

		return getCertificateUsersResponse;
	}

	public GetUserDetailsResponse getUserDetailsByUserName(String cert_id, String task_id, String id,
			String authorization) {

		GetUserDetailsResponse getUserDetailsResponse = new GetUserDetailsResponse();
		UserDetails userDetails = new UserDetails();

		Login login = utils.getCredentials(authorization);

		// START:PRADIP Changes
		List<DCertificationLineItem> lineItems = getCertificationTaskLineItems(cert_id, task_id, login);
		Predicate<DCertificationLineItem> predicateDCertificationLineItem = pdc -> ("" + pdc.getId()).equals(id);

		Optional<DCertificationLineItem> oplineItem = lineItems.stream().filter(predicateDCertificationLineItem)
				.findAny();
		
		final List<DirectReport> directReports = new ArrayList<>();
		

		if (oplineItem.isPresent()) {

			DCertificationLineItem lineItem = oplineItem.get();

			UserInfo user = new UserInfo();
			user.setUserName(lineItem.getUserLogin());
			user.setFirstName(lineItem.getFirstName());
			user.setLastName(lineItem.getLastName());
			user.setEmail(lineItem.getEmail());
			user.setDepartment(lineItem.getDepartment());
			user.setManager(lineItem.getManager());
			user.setUserkey(lineItem.getIamUserId());
			// Map userInfo = new HashMap<>();
			// userInfo.put("userInfo",user);
			userDetails.setUserInfo(user);

			Map<String, Object> map = getUserDetails(user.getUserkey());
			
			if (map != null && !map.isEmpty()) {
				String departmentName = (String) map.get("DepartmentName");

				user.setDepartment(departmentName);

				String hireDate = (String) map.get("hireDate");

				if (hireDate != null) {
					Instant hireDateinstant = Instant.parse(hireDate);
					user.setHireDate(hireDateinstant.atZone(ZoneId.systemDefault()).toLocalDate().toString());
				}
				
				String mgrKey = (String) map.get("usr_manager_key");
				
				if(StringUtils.isNotBlank(mgrKey))
				{
					List<DirectReport> resDirectReports =  confidenceService.retrieveReporteesFromOIM(Long.valueOf(mgrKey));
					if(resDirectReports!=null && !resDirectReports.isEmpty())
					directReports.addAll(resDirectReports);
				}
			}

			UserCertificationInfo userCertificationInfo = new UserCertificationInfo();

			userCertificationInfo.setNumOfRolesCertified(lineItem.getRoles());
			userCertificationInfo.setNumOfApplicationsCertified(lineItem.getAccounts());
			userCertificationInfo.setNumOfEntitlements(lineItem.getEntitlements());
			userCertificationInfo.setPercentageCompleted(lineItem.getPercentComplete());
			userCertificationInfo.setLineItemId(lineItem.getId());

			getUserDetailsResponse.setUserCertificationInfo(userCertificationInfo);

		}
		// END:PRADIP Changes
		
		Map<String, Integer> entityCountMap = new HashMap<>();
		
		Map<String, Integer>  retentityCountMap = confidenceService.populateDirectReportOIMEntityCountMap(login, directReports,
				true, true, true);
		
		if(retentityCountMap!=null && !retentityCountMap.isEmpty())
		{
			entityCountMap.putAll(retentityCountMap);
		}
		

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", utils.getAuthorizationHeader(login));
		headers.set("X-Response-Time", "X-Response-Time: 1000");

		HttpEntity entity = new HttpEntity<>(headers);

//		RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
//		RestTemplate restTemplate = restTemplateBuilder.build();

		String url = config.getUrl().getPrefix() + config.getUrl().getCertificationsLineItems();
//		String url1 = url.replaceAll("cert_id", cert_id);
//		String url2 = url1.replaceAll("lineid", id);
//		String url3 = url2.replaceAll("task_id", task_id);
//		System.out.println(url3);
		Map<String,Object> variables = new HashMap<>();
		variables.put("cert_id",cert_id);
		variables.put("lineid",id);
		variables.put("task_id",task_id);
		
		ConfidenceLevelData data = confidenceService.retrieveConfidenceLevelData();
		log.trace("findConfidenceLevel :: data = " + data);
		

		ResponseEntity<CertificationLineItem> certificationLineItemResponse = restTemplate.exchange(url,
				HttpMethod.GET, entity, CertificationLineItem.class,variables);

		if (certificationLineItemResponse.getStatusCode() == HttpStatus.OK) {

			log.info(certificationLineItemResponse.getBody().toString());
			System.out.println("Printing response " + certificationLineItemResponse.getBody().toString());

			List<LineItemDetails> certlineItemList = certificationLineItemResponse.getBody().getlineItemDetails();

			EntitlementCount entitlementCount = new EntitlementCount();

			int i = 0;
			int j = 0;
			for (LineItemDetails element : certlineItemList) {

				if (element.getCategory().contains("Entitlement"))

				{
					i++;
					if (element.getAction() != null)
						if (element.getAction() == 1) {
							j++;
						}
				}
			}

			entitlementCount.setEntitlementcount(i);
			entitlementCount.setTotalCertifiedEntitlements(j);

			List<Entityrole> entityroleList = new ArrayList<Entityrole>();

			List<EntityAppinstance> entityAppinstanceList = new ArrayList<EntityAppinstance>();

			userDetails.setFirstTimeUser("YES");// default value per response

			certlineItemList.forEach(entityRolevariable -> { // Looping for entityRoles

				if (entityRolevariable.getCategory().contains("Role")) {
					Entityrole entityrole = new Entityrole();
					Roleinfo roleinfo = new Roleinfo();
					roleinfo.setRoleName(entityRolevariable.getName());
					roleinfo.setRoleDescription(entityRolevariable.getDisplayName());
					roleinfo.setRoleOwner(entityRolevariable.getDataOwnerDisplayName());
					entityrole.setRoleInfo(roleinfo);
					entityrole.setItemType("Role");

					String itemRisk = confidenceService.findConfidenceLevel(login,
							userDetails.getUserInfo().getUserkey(), entityrole.getItemType(),
							roleinfo.getRoleName(), data, directReports, entityCountMap);

					System.out.println("OimCertificatioWrapperService.getUserDetailsByUserName()" + itemRisk);

					entityrole.setItemRisk(itemRisk);

					Comment newComments = new Comment();
					List<Comment> oldComments = new ArrayList<Comment>();
					newComments.setComment("");
					newComments.setCommentedBy("");
					newComments.setCommentedOn("");
					entityrole.setNewComment(newComments);
					entityrole.setOldComments(oldComments);
					entityrole.setRoleEntityId(entityRolevariable.getEntityId());
					if (entityRolevariable.getAction() == null || entityRolevariable.getAction() > 3)
						entityrole.setAction("required");
					else if (entityRolevariable.getAction() == 0)
						entityrole.setAction("rejected");
					else if (entityRolevariable.getAction() == 1)
						entityrole.setAction("certified");
					else if (entityRolevariable.getAction() == 2)
						entityrole.setAction("abstain");
					else if (entityRolevariable.getAction() == 3)
						entityrole.setAction("certifyConditionally");

					entityroleList.add(entityrole);// Adding to list of entityRoles
				}

			});

			certlineItemList.forEach(entityAppinstanceitem -> {

				if (entityAppinstanceitem.getCategory().contains("ApplicationInstance")) {

					EntityAppinstance entityAppinstance = new EntityAppinstance();

					ApplicationInfo applicationInfo = new ApplicationInfo();

					applicationInfo.setApplicationName(entityAppinstanceitem.getEndPointName());

					applicationInfo.setApplicationDescription(entityAppinstanceitem.getDisplayName());

					applicationInfo.setApplicationOwner(entityAppinstanceitem.getDataOwnerDisplayName());

					applicationInfo.setApplicationOwnerId(entityAppinstanceitem.getDataOwnerLogin());

					applicationInfo.setApplicationEntityId(entityAppinstanceitem.getEntityId()); // added by PradipC

					entityAppinstance.setApplicationInfo(applicationInfo);

					entityAppinstance.setItemType(entityAppinstanceitem.getCategory());

//					if (entityAppinstanceitem.getItemRisk() == 3) {
//						entityAppinstance.setItemRisk("Low");
//					}
//
//					if (entityAppinstanceitem.getItemRisk() == 5) {
//						entityAppinstance.setItemRisk("Medium");
//
//					}
//
//					if (entityAppinstanceitem.getItemRisk() == 7)
//
//					{
//
//						entityAppinstance.setItemRisk("High");
//
//					}

					String itemRisk = confidenceService.findConfidenceLevel(login,
							userDetails.getUserInfo().getUserkey(), entityAppinstance.getItemType(),
							applicationInfo.getApplicationName(),data, directReports, entityCountMap);

					System.out.println("OimCertificatioWrapperService.getUserDetailsByUserName()" + itemRisk);

					entityAppinstance.setItemRisk(itemRisk);

					if (entityAppinstanceitem.getAction() == null || entityAppinstanceitem.getAction() > 3)
						entityAppinstance.setAction("required");
					else if (entityAppinstanceitem.getAction() == 0)
						entityAppinstance.setAction("rejected");
					else if (entityAppinstanceitem.getAction() == 1)
						entityAppinstance.setAction("certified");
					else if (entityAppinstanceitem.getAction() == 2)
						entityAppinstance.setAction("abstain");
					else if (entityAppinstanceitem.getAction() == 3)
						entityAppinstance.setAction("certifyConditionally");

					entityAppinstance.setAccountId(entityAppinstanceitem.getAccountName());

					String url4 = config.getUrl().getPrefix() + config.getUrl().getApplicationProfileItems();

					String url5 = url4.replaceAll("iamId", entityAppinstanceitem.getIamId());

					System.out.println("OimCertificatioWrapperService.getUserDetailsByUserName()"
							+ entityAppinstanceitem.getIamId());

					ResponseEntity<Selfservice> selfserviceData = null;
					try {

						HttpHeaders headers2 = new HttpHeaders();
						headers2.setContentType(MediaType.APPLICATION_JSON);
						headers2.set("Authorization", confidenceService.getAuthHeader());
						headers2.set("X-Response-Time", "X-Response-Time: 1000");

						HttpEntity entity2 = new HttpEntity<>(headers2);

						selfserviceData = restTemplate.exchange(url5, HttpMethod.GET, entity2, Selfservice.class);

					} catch (Exception e) {
						e.printStackTrace();
					}

					ApplicationProfile applicationProfile = new ApplicationProfile();
					HashMap<String, String> hmap = new HashMap<String, String>();

					if (selfserviceData != null && selfserviceData.getStatusCode() == HttpStatus.OK) {

						applicationProfile.setIsLinkedAccount("Yes");
						HashMap<String, String> responseMap = new HashMap<>();

						selfserviceData.getBody().getFields().forEach(cd -> {

							// System.out.println(cd.get("name"));
							String str = (String) cd.get("name");
							if (str.equals("Normalize Data")) {
								// List<NormalizedFinalData> listndata = (List<NormalizedFinalData>)
								// cd.get("value");
								String listndata = cd.get("value").toString().replaceAll("\\[|\\]", "");
								listndata = StringUtils.removeEnd(listndata, "}");
								listndata.replace("/[^a-z0-9-]}/g", "");
								String[] listndataArray = listndata.split("},");
								for (int k = 0; k < listndataArray.length; k++) {
									String[] newarray = listndataArray[k].replaceAll("\\{name=", "").split(", value=");
									if (newarray.length > 1) {
										hmap.put(newarray[0].trim(), newarray[1]);
									}

								}
								applicationProfile.setApplicationProfile(hmap);
								// applicationProfile.setIsLinkedAccount("YES");
								System.out.println("Listndata = " + hmap.toString());
							}
						});

					}

					hmap.put("isLinkedAccount", "YES");
					entityAppinstance.setApplicationProfile(hmap);

					entityAppinstance.setTotalEntitlements(entitlementCount.getEntitlementcount());

					entityAppinstance.setTotalCertifiedEntitlements(entitlementCount.getTotalCertifiedEntitlements());

					List<EntityEntitlement> entityEntitlementList = new ArrayList<EntityEntitlement>();

					certlineItemList.forEach(entitysubloopvariable -> {

						if (entitysubloopvariable.getCategory().contains("Entitlement") && (entityAppinstanceitem
								.getEntityId().intValue() == entitysubloopvariable.getAccountId().intValue()))

						{

							EntityEntitlement eachEntityEntitlement = new EntityEntitlement();

							EntitlementInfo entitlementInfo = new EntitlementInfo();

							entitlementInfo.setEntitlementName(entitysubloopvariable.getName());

							entitlementInfo.setEntitlementDescription(entitysubloopvariable.getDisplayName());

							entitlementInfo.setEntitlementOwner(null);

							entitlementInfo.setEntitlementOwnerId(null);

							// entitlementInfo.setEntitlementType(entitysubloopvariable.getCategory());

							entitlementInfo.setEntitlementId(entitysubloopvariable.getEntityId());

							// entitlementInfo.setEntitlementType(entitysubloopvariable.getEntityType());

							eachEntityEntitlement.setEntitlementInfo(entitlementInfo);

							eachEntityEntitlement.setItemType(entitysubloopvariable.getCategory());

							String entityitemRisk = confidenceService.findConfidenceLevel(login,
									userDetails.getUserInfo().getUserkey(), eachEntityEntitlement.getItemType(),
									eachEntityEntitlement.getEntitlementInfo().getEntitlementName(), data, directReports, entityCountMap);

							System.out.println(
									"OimCertificatioWrapperService.getUserDetailsByUserName()" + entityitemRisk);

							eachEntityEntitlement.setItemRisk(entityitemRisk);

//							if (entitysubloopvariable.getItemRisk() == 3)
//
//							{
//
//								eachEntityEntitlement.setItemRisk("Low");
//
//							}
//
//							else if (entitysubloopvariable.getItemRisk() == 5)
//
//							{
//
//								entityAppinstance.setItemRisk("Medium");
//
//							}
//
//							else if (entitysubloopvariable.getItemRisk() == 7)
//
//							{
//
//								entityAppinstance.setItemRisk("High");
//
//							}

							// DECODE(d.certified,NULL,'No Action',0,'Revoked',1,'Certified', 2,'Abstain',
							// 3,'Conditionally Certified', 'Other Value')

							if (entitysubloopvariable.getAction() == null || entitysubloopvariable.getAction() > 3)
								eachEntityEntitlement.setAction("required");
							else if (entitysubloopvariable.getAction() == 0)
								eachEntityEntitlement.setAction("rejected");
							else if (entitysubloopvariable.getAction() == 1)
								eachEntityEntitlement.setAction("certified");
							else if (entitysubloopvariable.getAction() == 2)
								eachEntityEntitlement.setAction("abstain");
							else if (entitysubloopvariable.getAction() == 3)
								eachEntityEntitlement.setAction("certifyConditionally");

							eachEntityEntitlement.setApplication(entitysubloopvariable.getEndPointName());

							Comment newComments = new Comment();

							List<Comment> oldComments = new ArrayList<Comment>();

							newComments.setComment("");

							newComments.setCommentedBy("");

							newComments.setCommentedOn("");

							eachEntityEntitlement.setNewComment(newComments);

							eachEntityEntitlement.setOldComments(oldComments);

							entityEntitlementList.add(eachEntityEntitlement);
						}
					});

					System.out.println(certlineItemList);

					entityAppinstance.setEntityEntitlements(entityEntitlementList);

					entityAppinstanceList.add(entityAppinstance);
				}
			});

			userDetails.setEntityRole(entityroleList);

			userDetails.setEntityAppinstance(entityAppinstanceList);

			getUserDetailsResponse.setUserDetails(userDetails);
		}

		return getUserDetailsResponse;
	}

	public ResponseEntity<Object> updateUserActionDetails(String tenantid, String authorization, String reviewerId,
			String userName, String certificationId, String taskId, String linItemId,
			List<UserActionRequestDetails> userActionRequestDetails) {

		ResponseEntity<String> certificationsResponse = new ResponseEntity<>(HttpStatus.OK);

		Login login = utils.getCredentials(authorization);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", utils.getAuthorizationHeader(login));
		headers.set("X-Response-Time", "X-Response-Time: 1000");
		headers.set("X-Requested-By", login.getUserName());

		com.kapstone.certification.oimrequest.certificate.bean.LineItemDetails lids = new com.kapstone.certification.oimrequest.certificate.bean.LineItemDetails();

		for (UserActionRequestDetails uard : userActionRequestDetails) {

			com.kapstone.certification.oimrequest.certificate.bean.LineItemDetail lid = new com.kapstone.certification.oimrequest.certificate.bean.LineItemDetail();
			lid.setEntityId(uard.getEntityId());
			lid.setEntityType(uard.getEntityType());
			lid.setAction(uard.getAction());
			if (uard.getFields() != null && !uard.getFields().isEmpty()) {
				lid.setFields(uard.getFields());
			}

			certificationsResponse = validateData(lid, "revoke", "comment");
			if (certificationsResponse != null) {
				return new ResponseEntity<Object>(certificationsResponse, certificationsResponse.getStatusCode());
			}
			certificationsResponse = validateData(lid, "certifyConditionally", "endDate");
			if (certificationsResponse != null) {
				return new ResponseEntity<Object>(certificationsResponse, certificationsResponse.getStatusCode());
			}

			lids.getLineItemDetails().add(lid);
		}

		ObjectMapper objMapper = new ObjectMapper();
		try {
			System.out.println("Json req string = " + objMapper.writeValueAsString(lids));
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		HttpEntity entity = new HttpEntity<>(lids, headers);

//		RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
//		RestTemplate restTemplate = restTemplateBuilder.build();

		String url = config.getUrl().getPrefix() + config.getUrl().getCertificationsLineItemUpdate();

		Map<String, Object> variables = new HashMap<>();

		variables.put("certificationId", certificationId);
		variables.put("taskId", taskId);
		variables.put("linItemId", linItemId);

		certificationsResponse = restTemplate.exchange(url, HttpMethod.PUT, entity, String.class, variables);

		return new ResponseEntity<Object>(certificationsResponse, certificationsResponse.getStatusCode());
	}

	private ResponseEntity<String> validateData(LineItemDetail lid, String action, String fieldName) {
		if (lid.getAction().equals(action) && ObjectUtils.isEmpty(lid.getFields())) {
			return new ResponseEntity<>(fieldName + " should not be empty.", HttpStatus.BAD_REQUEST);
		} else if (lid.getAction().equals(action) && !ObjectUtils.isEmpty(lid.getFields())) {
			Predicate<Field> entlFieldPredicate = entlField -> entlField.getName().equals(fieldName);
			Optional<Field> field = lid.getFields().stream().filter(entlFieldPredicate).findAny();

			if (!field.isPresent() || (field.isPresent() && field.get().getValue().isEmpty())) {
				return new ResponseEntity<>(fieldName + " should not be empty.", HttpStatus.BAD_REQUEST);
			}
		}

		return null;
	}

	private String validateData(UserActionRequestDetails lid, String action, String fieldName) {
		if (lid.getAction().equals(action) && ObjectUtils.isEmpty(lid.getFields())) {
			return "For action - " + action + ", " + fieldName + " should not be empty.";
		} else if (lid.getAction().equals(action) && !ObjectUtils.isEmpty(lid.getFields())) {
			Predicate<Field> entlFieldPredicate = entlField -> entlField.getName().equals(fieldName);
			Optional<Field> field = lid.getFields().stream().filter(entlFieldPredicate).findAny();

			if (!field.isPresent() || (field.isPresent() && field.get().getValue().isEmpty())) {
				return "For action - " + action + ", " + fieldName + " should not be empty.";
			}
		}

		return null;
	}
	
	public CertificationActionResponse updateCertificationAction(CertificationActionRequest certActionRequest,
			String authorization) {
		
		Login login = utils.getCredentials(authorization);
		
		return  updateCertificationAction(certActionRequest,login);
		
	}

	private CertificationActionResponse updateCertificationAction(CertificationActionRequest certActionRequest,
			Login login) {

		if (null == certActionRequest.getCertifications() || certActionRequest.getCertifications().isEmpty()
				|| null == certActionRequest.getAction()
				|| !allowedCertificationActions.contains(certActionRequest.getAction().toLowerCase())) {
			System.out.println("updateCertificationAction :: invalid input");
			return null;
		}

		if (certActionRequest.getAction().equalsIgnoreCase(SIGNOFF)
				|| certActionRequest.getAction().equalsIgnoreCase(REASSIGN)) {
			System.out.println("updateCertificationAction :: Calling signOfforReassignCertification");
			return signOfforReassignCertifications(certActionRequest, login);

		} else if (certActionRequest.getAction().equalsIgnoreCase(COMPLETE)) {
			System.out.println("updateCertificationAction :: Calling completeCertifications");
			return completeCertifications(certActionRequest, login);

		} /*
			 * else if (certActionRequest.getAction().equalsIgnoreCase(REVOKE)) {
			 * System.out.
			 * println("updateCertificationAction :: Calling certifyCertifications"); return
			 * revokeCertifications(tenantid, certActionRequest, authorization); }
			 */

		else {
			System.out.println("updateCertificationAction :: Invalid action");
			return null;
		}
	}

	public CertificationActionResponse signOfforReassignCertifications(CertificationActionRequest certActionRequest,
			Login login) {

		if (null == certActionRequest.getFields() || certActionRequest.getFields().isEmpty())
			return null;

//		Login login = utils.getCredentials(authorization);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", utils.getAuthorizationHeader(login));
		headers.set("X-Response-Time", "X-Response-Time: 1000");
		headers.set("X-Requested-By", login.getUserName());

		String url = config.getUrl().getPrefix() + config.getUrl().getCertificationAction();

		OIMCertificationActionRequest oimRequest = populateSignOffOrReassignRequestBody(certActionRequest.getAction(),
				certActionRequest.getFields());
		ObjectMapper objMapper = new ObjectMapper();
		try {
			System.out.println("Json req string = " + objMapper.writeValueAsString(oimRequest));
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		HttpEntity<OIMCertificationActionRequest> entity = new HttpEntity<>(oimRequest, headers);

		List<CertificationInfo> certifications = certActionRequest.getCertifications();

//		RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
//		RestTemplate restTemplate = restTemplateBuilder.build();
//		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
//			protected boolean hasError(HttpStatus statusCode) {
//				return false;
//			}
//		});

		ResponseEntity<OIMCertificationActionResponse> response;

		for (CertificationInfo certification : certifications) {

			if (null == certification)
				continue;

			if (null == certification.getCertificationId() || certification.getCertificationId().trim().isEmpty()
					|| null == certification.getTaskId() || certification.getTaskId().trim().isEmpty()) {
				certification.setStatus("FAILED");
				continue;
			}

			System.out.println("certid = " + certification.getCertificationId());
			System.out.println("taskid = " + certification.getTaskId());
			try {
				response = restTemplate.exchange(url, HttpMethod.PUT, entity, OIMCertificationActionResponse.class,
						certification.getCertificationId(), certification.getTaskId());
				System.out.println("response = " + response);

				certification.setStatus(
						(null != response && null != response.getBody()) ? response.getBody().getStatus() : "FAILED");
			} catch (HttpStatusCodeException e) {
				System.out.println("Exception = " + e.getResponseBodyAsString());
				certification.setStatus(e.getStatusText());
			} catch (Exception e) {

				System.out.println("Exc = " + e);
			}
		}

		CertificationActionResponse resp = new CertificationActionResponse();
		resp.setCertifications(certifications);

		return resp;

	}

	private OIMCertificationActionRequest populateSignOffOrReassignRequestBody(String action,
			List<com.kapstone.certification.uirequest.certificate.bean.Field> reqFields) {
		OIMCertificationActionRequest oimRequest = new OIMCertificationActionRequest();
		List<Field> fields = new ArrayList<>();

		if (action.equalsIgnoreCase(SIGNOFF)) {
			oimRequest.setAction(COMPLETE); // OIM expects action 'complete' for signoff
			Field field = new Field(reqFields.get(0).getName(), reqFields.get(0).getValue());
			fields.add(field);

		} else {
			oimRequest.setAction(action);
			reqFields.forEach(reqField -> {
				Field field = new Field(reqField.getName(), reqField.getValue());
				fields.add(field);
			});
			Field field = new Field("type", "user");
			fields.add(field);
		}
		oimRequest.setFields(fields);

		return oimRequest;

	}

	public CertificationActionResponse completeCertifications(CertificationActionRequest certActionRequest,
			Login login) {

		List<CertificationInfo> certifications = certActionRequest.getCertifications();

		CertificationActionResponse response = new CertificationActionResponse();
		List<OIMLineItemRequest> oimLineItemReqList = new ArrayList<>();

		for (CertificationInfo certification : certifications) {
			String certificationId = certification.getCertificationId();
			String taskId = certification.getTaskId();
			System.out.println("completeCertifications :: certificationId = " + certificationId);
			System.out.println("completeCertifications :: taskId = " + taskId);

			// Call 2nd API to get the line item id
			GetCertificateUsersResponse certUsersResponse = getUsersByCertificateId(certificationId, taskId, login,
					false);

			if (null == certUsersResponse) {
				certification.setStatus("FAILED");
				continue;
			}

			String reviewerId = certUsersResponse.getReviewerId();
			System.out.println("completeCertifications :: reviewerId = " + reviewerId);

			for (Members member : certUsersResponse.getMembers()) {
				long lineItemId = member.getLineItemId();
				long lineItemEntityId = member.getLineItemEntityId();
				System.out.println("completeCertifications :: lineItemId = " + lineItemId);
				System.out.println("completeCertifications :: lineItemEntityId = " + lineItemEntityId);

				OIMLineItemRequest lineItemReq = new OIMLineItemRequest();
				lineItemReq.setAction(COMPLETE);
				lineItemReq.setLineItemId(lineItemEntityId);
				lineItemReq.setComments("Certified");
				oimLineItemReqList.add(lineItemReq);
			}

			OIMCompleteAndRevokeRequest oimRequest = new OIMCompleteAndRevokeRequest();
			oimRequest.setLineItems(oimLineItemReqList);

			HttpStatus httpStatus = completeOrRevokeCertificationInOIM(certificationId, taskId, oimRequest,
				 login);
			certification.setStatus(httpStatus.equals(HttpStatus.OK) ? "SUCCESS" : "FAILED");
		}

		response.setCertifications(certifications);
		return response;
	}

	private HttpStatus completeOrRevokeCertificationInOIM(String certificationId, String taskId,
			OIMCompleteAndRevokeRequest request, Login login) {

//		Login login = utils.getCredentials(authorization);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", utils.getAuthorizationHeader(login));
		headers.set("X-Response-Time", "X-Response-Time: 1000");
		headers.set("X-Requested-By", login.getUserName());

		String url = config.getUrl().getPrefix() + config.getUrl().getCertificationLineItems();

		ObjectMapper objMapper = new ObjectMapper();
		try {
			System.out.println("Json req string = " + objMapper.writeValueAsString(request));
		} catch (JsonProcessingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		HttpEntity<OIMCompleteAndRevokeRequest> entity = new HttpEntity<>(request, headers);

//		RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
//		RestTemplate restTemplate = restTemplateBuilder.build();
//		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
//			protected boolean hasError(HttpStatus statusCode) {
//				return false;
//			}
//		});

		ResponseEntity<OIMCertificationActionResponse> response;

		try {
			response = restTemplate.exchange(url, HttpMethod.PUT, entity, OIMCertificationActionResponse.class,
					certificationId, taskId);
			System.out.println("response = " + response);

			return response.getStatusCode();

		} catch (HttpStatusCodeException e) {
			System.out.println("Exception = " + e.getResponseBodyAsString());

			return e.getStatusCode();

		} catch (Exception e) {

			System.out.println("Exc = " + e);

			return HttpStatus.INTERNAL_SERVER_ERROR;
		}

	}

	public CertificationActionForUserResponse updateCertificationActionForUser(
			CertificationActionForUserRequest certActionRequest, String authorization)
	{
		Login login = utils.getCredentials(authorization);
	
		return  updateCertificationActionForUser(
				 certActionRequest, login) ;
	}
	
	
	public CertificationActionForUserResponse updateCertificationActionForUser(
			CertificationActionForUserRequest certActionRequest, Login login) {

		if (null == certActionRequest.getCertification()
				|| null == certActionRequest.getCertification().getCertificationId()
				|| null == certActionRequest.getCertification().getTaskId() || null == certActionRequest.getAction()
				|| !(COMPLETE.equalsIgnoreCase(certActionRequest.getAction())
						|| REVOKE.equalsIgnoreCase(certActionRequest.getAction()))) {
			System.out.println("updateCertificationActionForUser :: invalid input");
			return null;
		}

		OIMCompleteAndRevokeRequest oimRequest = new OIMCompleteAndRevokeRequest();
		List<OIMLineItemRequest> lineItems = new ArrayList<>();

		certActionRequest.getLineItemEntityId().stream().forEach(entityId -> {

			OIMLineItemRequest lineItem = new OIMLineItemRequest();
			lineItem.setLineItemId(entityId.longValue());
			lineItem.setAction(certActionRequest.getAction());

			lineItems.add(lineItem);
		});

		oimRequest.setLineItems(lineItems);

		HttpStatus httpStatus = completeOrRevokeCertificationInOIM(
				certActionRequest.getCertification().getCertificationId(),
				certActionRequest.getCertification().getTaskId(), oimRequest, login);

		CertificationActionForUserResponse resp = new CertificationActionForUserResponse();
		resp.setCertification(certActionRequest.getCertification());
		resp.getCertification().setStatus(httpStatus.equals(HttpStatus.OK) ? "SUCCESS" : "FAILED");
		resp.setLineItemEntityId(certActionRequest.getLineItemEntityId());

		return resp;
	}

	public List<GetCertificationResponse> getLatestCertifications(String reviewerId, int count, String authorization) {

		List<GetCertificationResponse> certifications = getCertifications(reviewerId, authorization, null, null, false);

		List<GetCertificationResponse> sortedCertifications = new ArrayList<>();

		if (ObjectUtils.isEmpty(certifications)) {
			return sortedCertifications;
		}

		sortedCertifications = certifications.stream().filter(Objects::nonNull)
				.filter(cert -> null != cert.getReviewerCertificationInfo()
						&& null != cert.getReviewerCertificationInfo().getCertificationCreatedOn())
				.sorted((cert1, cert2) -> {
					LocalDateTime date1 = LocalDateTime
							.parse(cert1.getReviewerCertificationInfo().getCertificationCreatedOn());
					LocalDateTime date2 = LocalDateTime
							.parse(cert2.getReviewerCertificationInfo().getCertificationCreatedOn());
					return date2.compareTo(date1);
				}).collect(Collectors.toList());

		sortedCertifications = sortedCertifications.stream().limit(count).collect(Collectors.toList());
		return sortedCertifications;
	}

	public List<AllUserOfflineActionableInfo> getAllUsersActionableDetailsByCertificateId(String tenantID,
			String certificationId, String taskId, String authorization, boolean isSignOffDataRequired) {
		List<AllUserOfflineActionableInfo> allUsersDetailsResponse = new ArrayList<AllUserOfflineActionableInfo>();

		GetCertificateUsersResponse certUsersResponse = getUsersByCertificateId(certificationId, taskId, authorization,
				isSignOffDataRequired);

		for (Members member : certUsersResponse.getMembers()) {
			long lineItemId = member.getLineItemId();
			long lineItemEntityId = member.getLineItemEntityId();
			System.out.println("completeCertifications :: lineItemId = " + lineItemId);
			System.out.println("completeCertifications :: lineItemEntityId = " + lineItemEntityId);

			GetUserDetailsResponse userDetailsResponse = getUserDetailsByUserName(certificationId, taskId,
					String.valueOf(lineItemId), authorization);

			for (Entityrole eachEntityRole : userDetailsResponse.getUserDetails().getEntityRole()) {
				AllUserOfflineActionableInfo userOfflineActionableInfo = new AllUserOfflineActionableInfo();
				userOfflineActionableInfo
						.setCertificationName(certUsersResponse.getReviewerCertificationInfo().getCertificationName());
				userOfflineActionableInfo.setUserName(member.getUserInfo().getUserName());
//	        	userOfflineActionableInfo.setDisplayName((String)userDetailsResponse.getUserDetails().getUserInfo().get("displayName"));
				userOfflineActionableInfo.setRoleName(eachEntityRole.getRoleInfo().getRoleName());
				userOfflineActionableInfo.setApplicationName("");
				userOfflineActionableInfo.setEntitlement("");
				userOfflineActionableInfo.setAction(eachEntityRole.getAction());
				userOfflineActionableInfo.setComments("");

				userOfflineActionableInfo.setRoleId(eachEntityRole.getRoleEntityId());
				userOfflineActionableInfo.setUserId(member.getLineItemId());
				userOfflineActionableInfo.setEndDate("");

				allUsersDetailsResponse.add(userOfflineActionableInfo);
			}

			for (EntityAppinstance eachAppInstance : userDetailsResponse.getUserDetails().getEntityAppinstance()) {

				AllUserOfflineActionableInfo userOfflineActionableInfo_A = new AllUserOfflineActionableInfo();
				userOfflineActionableInfo_A
						.setCertificationName(certUsersResponse.getReviewerCertificationInfo().getCertificationName());
				userOfflineActionableInfo_A.setUserName(member.getUserInfo().getUserName());
//		        userOfflineActionableInfo_A.setDisplayName((String)userDetailsResponse.getUserDetails().getUserInfo().get("displayName"));
				userOfflineActionableInfo_A.setRoleName("");
				userOfflineActionableInfo_A
						.setApplicationName(eachAppInstance.getApplicationInfo().getApplicationName());
				userOfflineActionableInfo_A.setEntitlement("");
				userOfflineActionableInfo_A.setAction(eachAppInstance.getAction());
				userOfflineActionableInfo_A.setComments("");
				userOfflineActionableInfo_A.setApplnId(eachAppInstance.getApplicationInfo().getApplicationEntityId());

				userOfflineActionableInfo_A.setUserId(member.getLineItemId());
				userOfflineActionableInfo_A.setEndDate("");

				allUsersDetailsResponse.add(userOfflineActionableInfo_A);

				for (EntityEntitlement eachEntityEntitlement : eachAppInstance.getEntityEntitlements()) {

					AllUserOfflineActionableInfo userOfflineActionableInfo_E = new AllUserOfflineActionableInfo();
					userOfflineActionableInfo_E.setCertificationName(
							certUsersResponse.getReviewerCertificationInfo().getCertificationName());
					userOfflineActionableInfo_E.setUserName(member.getUserInfo().getUserName());
//		          userOfflineActionableInfo_E.setDisplayName((String)userDetailsResponse.getUserDetails().getUserInfo().get("displayName"));
					userOfflineActionableInfo_E.setRoleName("");
					userOfflineActionableInfo_E
							.setApplicationName(eachAppInstance.getApplicationInfo().getApplicationName());
					userOfflineActionableInfo_E
							.setEntitlement(eachEntityEntitlement.getEntitlementInfo().getEntitlementName());
					userOfflineActionableInfo_E.setAction(eachEntityEntitlement.getAction());
					userOfflineActionableInfo_E.setComments("");
					userOfflineActionableInfo_E
							.setApplnId(eachAppInstance.getApplicationInfo().getApplicationEntityId());
					userOfflineActionableInfo_E
							.setEntlId(eachEntityEntitlement.getEntitlementInfo().getEntitlementId());
					userOfflineActionableInfo_E.setUserId(member.getLineItemId());
					userOfflineActionableInfo_E.setEndDate("");

					allUsersDetailsResponse.add(userOfflineActionableInfo_E);
				}
			}

		}

//		writeCsvFile();

		return allUsersDetailsResponse;
	}

	public Map<String, Object> userOfflineAction(MultipartFile file, String tenantID, String certificationId,
			String taskId, String authorization) {
		Map<String, Object> retmap = new HashMap<String, Object>();

		String message = "Excel file data uploaded sucessfully.";

		InputStream stream;

		Map<Long, List<UserActionRequestDetails>> userAllActions = new HashMap<>();

		try {

			stream = file.getInputStream();
			Workbook workbook = new XSSFWorkbook(stream);
			Sheet sheet = workbook.getSheetAt(0);
			DataFormatter dataFormatter = new DataFormatter();

			for (Row row : sheet) {

				if (row.getRowNum() == 0) {
					continue;
				}

//				ID	UserName	DisplayName	Role ID	Role	Application ID	Application	Entilement ID	Entitlement	Action	Comments
				List<UserActionRequestDetails> usersActionDetails = new ArrayList<>();

				AllUserOfflineActionableInfo usersDetail = new AllUserOfflineActionableInfo();

				usersDetail.setUserId(Long.valueOf(defaultString(dataFormatter.formatCellValue(row.getCell(0)), "0")));
				usersDetail.setUserName(dataFormatter.formatCellValue(row.getCell(1)));
				usersDetail.setDisplayName(dataFormatter.formatCellValue(row.getCell(2)));
				usersDetail.setRoleId(Long.valueOf(defaultString(dataFormatter.formatCellValue(row.getCell(3)), "0")));
				usersDetail.setRoleName(dataFormatter.formatCellValue(row.getCell(4)));
				usersDetail.setApplnId(Long.valueOf(defaultString(dataFormatter.formatCellValue(row.getCell(5)), "0")));
				usersDetail.setApplicationName(dataFormatter.formatCellValue(row.getCell(6)));
				usersDetail.setEntlId(Long.valueOf(defaultString(dataFormatter.formatCellValue(row.getCell(7)), "0")));
				usersDetail.setEntitlement(dataFormatter.formatCellValue(row.getCell(8)));
				usersDetail.setAction(dataFormatter.formatCellValue(row.getCell(9)));
				usersDetail.setComments(dataFormatter.formatCellValue(row.getCell(10)));
				usersDetail.setEndDate(dataFormatter.formatCellValue(row.getCell(11)));

				Field endDate = null;
				if (!org.springframework.util.StringUtils.isEmpty(usersDetail.getEndDate())) {
					LocalDate date = LocalDate.parse(usersDetail.getEndDate());
					System.out.println("OimCertificatioWrapperService.userOfflineAction()"
							+ date.atStartOfDay(ZoneId.systemDefault()).toInstant().toString());
					endDate = new Field("endDate", date.atStartOfDay(ZoneId.systemDefault()).toInstant().toString());

				}
				if (usersDetail.getRoleId() != 0) {
					UserActionRequestDetails role = new UserActionRequestDetails();
					role.setEntityId(Long.valueOf(usersDetail.getRoleId()).intValue());
					role.setEntityType("ROLE");
					role.setAction(usersDetail.getAction());

					Field comment = new Field("comment", usersDetail.getComments());

					if (endDate != null) {
						role.setFields(Arrays.asList(comment, endDate));
					} else {
						role.setFields(Arrays.asList(comment));
					}

					String res = validateData(role, "revoke", "comment");
					if (res != null) {

						retmap.put("message", res);
						retmap.put("status", "failure");
						return retmap;
					}
					res = validateData(role, "certifyConditionally", "endDate");
					if (res != null) {
						retmap.put("message", res);
						retmap.put("status", "failure");
						return retmap;
					}

					usersActionDetails.add(role);

				}

				if (usersDetail.getApplnId() != 0) {
					UserActionRequestDetails account = new UserActionRequestDetails();
					account.setEntityId(Long.valueOf(usersDetail.getApplnId()).intValue());
					account.setEntityType("ACCOUNT");
					account.setAction(usersDetail.getAction());

					Field comment = new Field("comment", usersDetail.getComments());
//					Field endDate = new Field("endDate", date.atStartOfDay(ZoneId.systemDefault()).toInstant().toString() );
					if (endDate != null) {
						account.setFields(Arrays.asList(comment, endDate));
					} else {
						account.setFields(Arrays.asList(comment));
					}

					String res = validateData(account, "revoke", "comment");
					if (res != null) {
						retmap.put("message", res);
						retmap.put("status", "failure");
						return retmap;
					}
					res = validateData(account, "certifyConditionally", "endDate");
					if (res != null) {
						retmap.put("message", res);
						retmap.put("status", "failure");
						return retmap;
					}

					usersActionDetails.add(account);

				}

				if (usersDetail.getEntlId() != 0) {
					UserActionRequestDetails entl = new UserActionRequestDetails();
					entl.setEntityId(Long.valueOf(usersDetail.getEntlId()).intValue());
					entl.setEntityType("Entitlement");
					entl.setAction(usersDetail.getAction());

					Field comment = new Field("comment", usersDetail.getComments());
//					Field endDate = new Field("endDate", date.atStartOfDay(ZoneId.systemDefault()).toInstant().toString() );
//					entl.setFields(Arrays.asList(comment,endDate));
					if (endDate != null) {
						entl.setFields(Arrays.asList(comment, endDate));
					} else {
						entl.setFields(Arrays.asList(comment));
					}

					String res = validateData(entl, "revoke", "comment");
					if (res != null) {
						retmap.put("message", res);
						retmap.put("status", "failure");
						return retmap;
					}
					res = validateData(entl, "certifyConditionally", "endDate");
					if (res != null) {
						retmap.put("message", res);
						retmap.put("status", "failure");
						return retmap;
					}

					usersActionDetails.add(entl);
				}

				if (!usersActionDetails.isEmpty()) {

					if (userAllActions.containsKey(usersDetail.getUserId())) {
						userAllActions.get(usersDetail.getUserId()).addAll(usersActionDetails);
					} else {
						userAllActions.put(usersDetail.getUserId(), usersActionDetails);
					}

				}
			}

			List<CertificationActionResponse> certActionResponse = new ArrayList<>();
			System.out.println("OimCertificatioWrapperService.userOfflineAction()" + userAllActions);

			userAllActions.forEach((key, value) -> {

				List<UserActionRequestDetails> unique = value.stream()
						.collect(collectingAndThen(
								toCollection(() -> new TreeSet<>(comparingInt(UserActionRequestDetails::getEntityId))),
								ArrayList::new));

//				Set<Integer> nameSet = new HashSet<>();
//				List<UserActionRequestDetails> unique2 = value.stream()
//			            .filter(e -> nameSet.add(e.getEntityId().intValue()))
//			            .collect(Collectors.toList());
//				System.out.println("OimCertificatioWrapperService.userOfflineAction()" + unique2);
				System.out.println("OimCertificatioWrapperService.userOfflineAction()" + unique);
				List<CertificationActionResponse> response = updateUserActionDetailsWithSDKService(tenantID,
						authorization, null, null, certificationId, taskId, String.valueOf(key), unique);

				if (response != null && !response.isEmpty()) {
					certActionResponse.addAll(response);
				}

			});
		} catch (IOException e) {
			e.printStackTrace();
		}

		retmap.put("message", message);
		retmap.put("status", "success");
		return retmap;
	}

	public static String defaultString(final String str, final String defaultStr) {
		return StringUtils.isBlank(str) ? defaultStr : str;
	}

	private List<CertificationActionResponse> updateUserActionDetailsWithSDKService(String tenantid,
			String authorization, String reviewerId, String userName, String certificationId, String taskId,
			String linItemId, List<UserActionRequestDetails> userActionRequestDetails) {
		Login login = utils.getCredentials(authorization);

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.set("Authorization", authorization);
		headers.set("X-Response-Time", "X-Response-Time: 1000");
		headers.set("lastLogin", login.getLoginDateAndTime());
		headers.set("oim_remote_user", login.getUserName());

		HttpEntity entity = new HttpEntity<>(userActionRequestDetails, headers);

//		RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
//		RestTemplate restTemplate = restTemplateBuilder.build();

		String url = config.getUrl().getUserModulePrefix() + config.getUrl().getUserCertificationAction();

		Map<String, Object> variables = new HashMap<>();

		variables.put("userModuleTenentID", config.getOim().getUserModule());
		variables.put("userId", login.getUserName());
		variables.put("certificationId", certificationId);
		variables.put("taskId", taskId);
		variables.put("linItemId", linItemId);

		ResponseEntity<List> certActionResponse = restTemplate.exchange(url, HttpMethod.PUT, entity, List.class,
				variables);

		return (List<CertificationActionResponse>) certActionResponse.getBody();

	}

	public Map<String, Object> getUserDetails(long userKey) {

		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.set("Authorization", confidenceService.getAuthHeader());
		requestHeaders.set("x-requested-by", "1");

		HttpEntity<String> requestEntity = new HttpEntity<>(requestHeaders);
//		RestTemplateBuilder restTemplateBuilder = new RestTemplateBuilder();
//		RestTemplate restTemplate = restTemplateBuilder.build();
//		restTemplate.setErrorHandler(new DefaultResponseErrorHandler() {
//			protected boolean hasError(HttpStatus statusCode) {
//				return false;
//			}
//		});

		long start = System.currentTimeMillis();

//		UriComponentsBuilder builder = UriComponentsBuilder
//				.fromHttpUrl(
		String url = config.getUrl().getPrefix() + config.getUrl().getUsers() + "?fields=Hire Date,DepartmentName,usr_manager_key";

//		builder = builder.queryParam("fields", "Hire Date,DepartmentName");

		Map<String, String> urlParams = new HashMap<>();
		urlParams.put("uuid", String.valueOf(userKey));

		ResponseEntity<UserResponse> responseEntity = restTemplate.exchange(url, HttpMethod.GET, requestEntity,
				UserResponse.class, urlParams);

		UserResponse userResponse = responseEntity.getBody();

//		String departmentName = (String) userResponse.getFields().stream()
//				.filter(field -> field.getName().equals("DepartmentName")).map(field -> field.getValue()).findFirst()
//				.orElse(null);
//
		Map<String, Object> map = new HashMap<>();
//
//		map.put("DepartmentName", departmentName);
//
//		String hireDate = (String) userResponse.getFields().stream()
//				.filter(field -> field.getName().equals("Hire Date")).map(field -> field.getValue()).findFirst()
//				.orElse(null);

		userResponse.getFields().forEach(field ->{
			
			if(field.getName().equals("DepartmentName"))
			{
				map.put("DepartmentName", field.getValue());
			}
			else if(	field.getName().equals("Hire Date"))
			{
				map.put("hireDate",  field.getValue());
			}
			else if(field.getName().equals("usr_manager_key"))
			{
				map.put("usr_manager_key", ""+ field.getValue());
			}
		});

		return map;
	}

//	private void writeCsvFile() {
//		final String CSV_LOCATION = "certifications.xlsx"; 
//		  
//        try { 
//  
//            // Creating writer class to generate 
//            // csv file 
//            FileWriter writer = new 
//                       FileWriter(CSV_LOCATION); 
//  
// 
//  
//            // Create Mapping Strategy to arrange the  
//            // column name in order 
//            ColumnPositionMappingStrategy mappingStrategy= 
//                        new ColumnPositionMappingStrategy(); 
//            mappingStrategy.setType(AllUserOfflineActionableInfo.class); 
//           
//  
//            // Arrange column name as provided in below array. 
//            String[] columns = new String[]  
//                    { "userId",	"userName","displayName", "roleId",	 "roleName", "applnId", "applicationName", "entlId", "entitlement",	"action", "comments" }; 
//            mappingStrategy.setColumnMapping(columns); 
//  
//            // Createing StatefulBeanToCsv object 
//            StatefulBeanToCsvBuilder<AllUserOfflineActionableInfo> builder= 
//                        new StatefulBeanToCsvBuilder(writer); 
//            StatefulBeanToCsv beanWriter =  
//          builder.withMappingStrategy(mappingStrategy).build(); 
//  
//            // Write list to StatefulBeanToCsv object 
//            beanWriter.write(allUsersDetailsResponse); 
//  
//            // closing the writer object 
//            writer.close(); 
//        } 
//        catch (Exception e) { 
//            e.printStackTrace(); 
//        } 
//		
//		
//	}

}
