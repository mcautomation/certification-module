package com.kapstone.certification.service.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Properties;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.kapstone.certification.bean.ConfidenceLevelData;
import com.kapstone.certification.bean.ConfidenceLevelData.ConfidenceLevel;


@Component
public class PropertiesUtils {
	 
	private static final Logger logger = LoggerFactory.getLogger(PropertiesUtils.class);
	
	public void updateConfidenceLevelData (double highConfidencePeerAnalysis, double mediumConfidencePeerAnalysis, double lowConfidencePeerAnalysis,
			String highConfidenceRiskLevel, String mediumConfidenceRiskLevel, String lowConfidenceRiskLevel, double threshold, String fileName) 
					throws ConfigurationException, FileNotFoundException {
 
		File propertiesFile = new File(fileName);
		PropertiesConfiguration config = new PropertiesConfiguration(propertiesFile);		
		
		config.setProperty("confidence.high.peer", highConfidencePeerAnalysis);
		config.setProperty("confidence.high.risk", highConfidenceRiskLevel);
		config.setProperty("confidence.medium.peer", mediumConfidencePeerAnalysis);
		config.setProperty("confidence.medium.risk", mediumConfidenceRiskLevel);
		config.setProperty("confidence.low.peer", lowConfidencePeerAnalysis);
		config.setProperty("confidence.low.risk", lowConfidenceRiskLevel);
		config.setProperty("accessadvisor.threshold", threshold);
		
		config.save();
	}
	
	public ConfidenceLevelData retrieveConfidenceLevelData(String fileName) {
		
		FileInputStream file;
		ConfidenceLevelData data = new ConfidenceLevelData();
		
		try {
			Properties prop = new Properties();
			file = new FileInputStream(fileName);
 			prop.load(file);
 		
 			data.setHigh(new ConfidenceLevel(Double.parseDouble(prop.getProperty("confidence.high.peer")),
					prop.getProperty("confidence.high.risk")));
			data.setMedium(new ConfidenceLevel(Double.parseDouble(prop.getProperty("confidence.medium.peer")),
					prop.getProperty("confidence.medium.risk")));
			data.setLow(new ConfidenceLevel(Double.parseDouble(prop.getProperty("confidence.low.peer")),
					prop.getProperty("confidence.low.risk")));
			data.setThreshold(Double.parseDouble(prop.getProperty("accessadvisor.threshold")));
			
			file.close();
		} catch (FileNotFoundException e) {
			logger.error("FileNotFoundException: " + e);
		} catch (Exception e) {
			logger.error("Exception: " + e);
		}
		return data;
	}
 

}