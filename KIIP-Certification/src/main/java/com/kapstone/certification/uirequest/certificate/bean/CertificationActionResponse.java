package com.kapstone.certification.uirequest.certificate.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString (includeFieldNames = true)
public class CertificationActionResponse {

	private long entityId;
	private String entityType;
	private String status;
}
