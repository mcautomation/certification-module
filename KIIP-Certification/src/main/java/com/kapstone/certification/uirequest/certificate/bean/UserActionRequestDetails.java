
package com.kapstone.certification.uirequest.certificate.bean;

import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.kapstone.certification.oimrequest.certificate.bean.Field;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "entityId",
    "entityType",
    "action",
//    "entitlements",
    "fields"
})
public class UserActionRequestDetails {

    @JsonProperty("entityId")
    private Integer entityId;
    @JsonProperty("entityType")
    private String entityType;
    @JsonProperty("action")
    private String action;
//    @JsonProperty("entitlements")
//    private List<Entitlement> entitlements = null;
    
    @JsonProperty("fields")
    private List<Field> fields = null;
    
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("entityId")
    public Integer getEntityId() {
        return entityId;
    }

    @JsonProperty("entityId")
    public void setEntityId(Integer entityId) {
        this.entityId = entityId;
    }

    @JsonProperty("entityType")
    public String getEntityType() {
        return entityType;
    }

    @JsonProperty("entityType")
    public void setEntityType(String entityType) {
        this.entityType = entityType;
    }

    @JsonProperty("action")
    public String getAction() {
        return action;
    }

    @JsonProperty("action")
    public void setAction(String action) {
        this.action = action;
    }
//
//    @JsonProperty("entitlements")
//    public List<Entitlement> getEntitlements() {
//        return entitlements;
//    }
//
//    @JsonProperty("entitlements")
//    public void setEntitlements(List<Entitlement> entitlements) {
//        this.entitlements = entitlements;
//    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }
    
    @JsonProperty("fields")
    public List<Field> getFields() {
        return fields;
    }

    @JsonProperty("fields")
    public void setFields(List<Field> fields) {
        this.fields = fields;
    }
    
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserActionRequestDetails that = (UserActionRequestDetails) o;
        return this.getEntityId().intValue()==that.getEntityId().intValue();
    }

}
