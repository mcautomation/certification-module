package com.kapstone.certification.uirequest.certificate.bean;

import java.util.List;

import com.kapstone.certification.oimrequest.certificate.bean.Field;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString (includeFieldNames = true)
public class CertificationActionRequest {

	private long entityId;
	private String entityType;
	private String action;
	private List<Field> fields;
	private String comment;
	private String endDate;
}
