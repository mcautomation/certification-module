
package com.kapstone.certification.uirequest.certificate.bean;

import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

import lombok.Getter;

@Setter
@Getter
public class UserActionRequestDetailsWithID {

	private Long id;
	private List<UserActionRequestDetails>  userActionRequestDetails = new ArrayList<>();

}
