package com.kapstone.certification.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.kapstone.certification.bean.AllUserOfflineActionableInfo;
import com.kapstone.certification.bean.CertificationActionForUserRequest;
import com.kapstone.certification.bean.CertificationActionForUserResponse;
import com.kapstone.certification.bean.CertificationActionRequest;
import com.kapstone.certification.bean.CertificationActionResponse;
import com.kapstone.certification.bean.GetCertificateUsersResponse;
import com.kapstone.certification.bean.GetCertificationResponse;
import com.kapstone.certification.bean.GetUserDetailsResponse;
import com.kapstone.certification.config.CertificationConfig;
import com.kapstone.certification.service.ConfidenceService;
import com.kapstone.certification.service.OimCertificatioWrapperService;
import com.kapstone.certification.uirequest.certificate.bean.UserActionRequestDetails;

import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/api/v1/{tenantid}", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
public class CertificationController {

	@Autowired
	CertificationConfig config;

	@Autowired
	OimCertificatioWrapperService service;
	
	@Autowired
	ConfidenceService confidenceService;
	
	//1st Call
	@RequestMapping(value = "/getCertifications/{reviewerId}", produces = "application/json", method = { RequestMethod.GET,
			RequestMethod.OPTIONS })
	public ResponseEntity<List<GetCertificationResponse>> getCertifications(HttpServletRequest request,@RequestHeader("Authorization") String authorization,
			@PathVariable("tenantid") String tenantid,
			@PathVariable("reviewerId") String reviewerId,
			@RequestParam(value = "offset", required = false) Integer offset,
			@RequestParam(value = "limit", required = false) Integer limit,
			@RequestParam(value = "isSignOffDataRequired", required = false) boolean isSignOffDataRequired
			) {
		
		List<GetCertificationResponse> getCertificationResponseList = service.getCertifications(reviewerId,authorization,offset,limit,isSignOffDataRequired);
	
		return new ResponseEntity<List<GetCertificationResponse>>(getCertificationResponseList, HttpStatus.OK);

	}
	
	//DEEPA's Code
	//2nd Call
	@RequestMapping(value = "/getCertificateUsers/{certificationId}/{taskId}", produces = "application/json", 
			method = { RequestMethod.GET, RequestMethod.OPTIONS })

	public ResponseEntity<GetCertificateUsersResponse> getUsersByCertificateId
							(HttpServletRequest request, @RequestHeader("Authorization") String authorization,
							@PathVariable("certificationId") String certificationId,
							@PathVariable("taskId") String taskId,
							@RequestParam(value = "isSignOffDataRequired", required = false) boolean isSignOffDataRequired
							){
		
		GetCertificateUsersResponse getCertificateUsersResponse = service.getUsersByCertificateId
							(certificationId, taskId, authorization,isSignOffDataRequired);
		
		return new ResponseEntity<GetCertificateUsersResponse>(getCertificateUsersResponse, HttpStatus.OK);
	}
	
	//Suarabh's Code
	//3rd call	
	@RequestMapping(value = "/getUserDetails/{cert_id}/{task_id}/{id}", produces = "application/json", method = { RequestMethod.GET,
			RequestMethod.OPTIONS })
	public ResponseEntity<GetUserDetailsResponse> getUserDetails(HttpServletRequest request,@RequestHeader("Authorization") String authorization,
			@PathVariable("tenantid") String tenantid, @PathVariable("task_id") String task_id, @PathVariable("cert_id") String cert_id,
			@PathVariable("id") String id){
				
		GetUserDetailsResponse getUserDetailsResponse = service.getUserDetailsByUserName(cert_id, task_id, id, authorization);
		
		return new ResponseEntity<GetUserDetailsResponse>(getUserDetailsResponse, HttpStatus.OK);
		
	}

	
	@RequestMapping(value = "/action/{reviewerId}/{userName}/{certificationId}/{taskId}/{linItemId}", produces = "application/json", method = { RequestMethod.PATCH,RequestMethod.PUT,
			RequestMethod.OPTIONS })
	public ResponseEntity<Object> updateUserActionDetails(HttpServletRequest request,@RequestHeader("Authorization") String authorization,
			@PathVariable(value="tenantid", required = true) String tenantid,
			@PathVariable(value="reviewerId",required = true) String reviewerId,
			@PathVariable(value="userName" ,required = true) String userName,
			@PathVariable(value="certificationId",required = true) String certificationId,
			@PathVariable(value="taskId",required = true) String taskId, 
			@PathVariable(value="linItemId",required = true) String linItemId,  
			@RequestBody List<UserActionRequestDetails> userActionRequestDeatils){
		
		ResponseEntity<Object> response = service.updateUserActionDetails(tenantid,authorization,reviewerId,userName,certificationId,taskId,linItemId, userActionRequestDeatils);
		
		if(response.getStatusCode() == HttpStatus.OK)
		{
			GetUserDetailsResponse getUserDetailsResponse = service.getUserDetailsByUserName(certificationId, taskId, linItemId, authorization);
			return new ResponseEntity<>(getUserDetailsResponse, HttpStatus.OK);
		}
		else
		{
			return response; 
		}
	}
	
	
	/**
	 * Sign off, reassign or complete certifications
	 * @param request
	 * @param authorization
	 * @param tenantid
	 * @param certActionRequest
	 * @return
	 */
	@RequestMapping(value = "/certificationsAction", produces = "application/json", 
			method = { RequestMethod.PUT, RequestMethod.PATCH, RequestMethod.OPTIONS })
	public ResponseEntity<CertificationActionResponse> updateCertificationAction
									(HttpServletRequest request, 
									@RequestHeader("Authorization") String authorization,
									@RequestBody CertificationActionRequest certActionRequest){
	
		CertificationActionResponse response = service.updateCertificationAction(certActionRequest, authorization);
		
		if (null == response)
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		
		return new ResponseEntity<CertificationActionResponse>(response, HttpStatus.OK);
	}
	
	
	/**
	 * Complete or revoke certification at user level
	 * @param request
	 * @param authorization
	 * @param tenantid
	 * @param certActionRequest
	 * @return
	 */
	@RequestMapping(value = "/certificationsAction/user", produces = "application/json", 
			method = { RequestMethod.PUT, RequestMethod.PATCH, RequestMethod.OPTIONS })
	public ResponseEntity<CertificationActionForUserResponse> updateCertificationActionForUser
									(HttpServletRequest request, 
									@RequestHeader("Authorization") String authorization,
									@PathVariable(value="tenantid", required = true) String tenantid,
									@RequestBody CertificationActionForUserRequest certActionRequest){
	
		CertificationActionForUserResponse response = service.updateCertificationActionForUser(certActionRequest, authorization);
		
		if (null == response)
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		
		return new ResponseEntity<CertificationActionForUserResponse>(response, HttpStatus.OK);
	}
	
	/**
	 * Get latest certifications to display on the dashboard
	 * @param request
	 * @param authorization
	 * @param tenantid
	 * @param reviewerId
	 * @param count
	 * @param offset
	 * @param limit
	 * @return
	 */
	@RequestMapping(value = "/getCertifications/{reviewerId}/{count}", produces = "application/json", method = { RequestMethod.GET,
			RequestMethod.OPTIONS })
	public ResponseEntity<List<GetCertificationResponse>> getLatestCertifications
										(HttpServletRequest request,
										@RequestHeader("Authorization") String authorization,
										@PathVariable("tenantid") String tenantid,
										@PathVariable("reviewerId") String reviewerId,
										@PathVariable("count") int count,
										@RequestParam(value = "offset", required = false) Integer offset,
										@RequestParam(value = "limit", required = false) Integer limit
										) {
		
		List<GetCertificationResponse> getCertificationResponseList = service.getLatestCertifications
																				(reviewerId, count, authorization);
	
		return new ResponseEntity<List<GetCertificationResponse>>(getCertificationResponseList, HttpStatus.OK);

	}
	
	@PostMapping("/submitOfflineCertification/{certificationId}/{taskId}")
    @ResponseBody
    public ResponseEntity<Map<String,Object>> userOfflineAction(@RequestParam("file") MultipartFile file,
    		@PathVariable("tenantid") String tenantId,
    		@PathVariable("certificationId") String certificationId,
    		@PathVariable("taskId") String taskId,
    		@RequestHeader("Authorization") String authorization)
	{
		Map<String,Object> retmap = service.userOfflineAction(file, tenantId,certificationId, taskId, authorization);
				
		return new ResponseEntity<Map<String,Object>>(retmap, HttpStatus.OK);
		
	}
	
	@RequestMapping(value = "/getOfflineCertificationDetails/{certificationId}/{taskId}", produces = "application/json", method = {
			RequestMethod.GET, RequestMethod.OPTIONS })
	@ResponseBody
	@ApiOperation("This API lists all Users based on Certification ID")
	public ResponseEntity<List<AllUserOfflineActionableInfo>> getOfflineCertificationDetails(
			@PathVariable("tenantid") String tenantID, @PathVariable("certificationId") String certificationId,
			@PathVariable("taskId") String taskId, @RequestHeader("Authorization") String authorization,
			@RequestParam(value = "isSignOffDataRequired", required = false) boolean isSignOffDataRequired) {
		
		List<AllUserOfflineActionableInfo> getAllUsersActionableDetails = this.service
				.getAllUsersActionableDetailsByCertificateId(tenantID, certificationId, taskId, authorization,
						isSignOffDataRequired);
		return new ResponseEntity<List<AllUserOfflineActionableInfo>>(getAllUsersActionableDetails, HttpStatus.OK);
	}
	
	/*
	 * Temporary API
	 */
	@GetMapping(value = "/checkConfidence/{userKey}", 
			produces = "application/json")
	public ResponseEntity<String> checkConfidence 
					(@RequestHeader("Authorization") String authorization,
					 @PathVariable("userKey") long userKey,
					 @RequestParam(value="entityType", required = true) String entityType,
					 @RequestParam(value="entityName", required = true) String entityName,
					 HttpServletRequest request) {
		
		String res =  confidenceService.findConfidenceLevel(authorization, userKey, entityType, entityName,null,null,null);
		return new ResponseEntity<>(res, HttpStatus.OK);
		
	}

	
}

