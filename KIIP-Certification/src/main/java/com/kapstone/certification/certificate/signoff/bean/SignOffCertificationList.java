package com.kapstone.certification.certificate.signoff.bean;

import java.util.List;

public class SignOffCertificationList {

    private List<SignOffCertification> signOffCertifications;
	
    public List<SignOffCertification> getSignOffCertifications() {
        return signOffCertifications;
    }

    public void setSignOffCertifications(List<SignOffCertification> signOffCertifications) {
        this.signOffCertifications = signOffCertifications;
    }
    
}
