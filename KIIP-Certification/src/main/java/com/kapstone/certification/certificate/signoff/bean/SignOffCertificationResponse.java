
package com.kapstone.certification.certificate.signoff.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "links",
    "taskId",
    "taskName",
    "reviewer",
    "status",
    "certificationInstance",
    "startDate",
    "endDate",
    "certificationState",
    "percentComplete"
})
public class SignOffCertificationResponse {

    @JsonProperty("links")
    private List<Link> links = null;
    @JsonProperty("taskId")
    private String taskId;
    @JsonProperty("taskName")
    private String taskName;
    @JsonProperty("reviewer")
    private Reviewer reviewer;
    @JsonProperty("status")
    private Integer status;
    @JsonProperty("certificationInstance")
    private CertificationInstance certificationInstance;
    @JsonProperty("startDate")
    private String startDate;
    @JsonProperty("endDate")
    private String endDate;
    @JsonProperty("certificationState")
    private String certificationState;
    @JsonProperty("percentComplete")
    private Double percentComplete;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("links")
    public List<Link> getLinks() {
        return links;
    }

    @JsonProperty("links")
    public void setLinks(List<Link> links) {
        this.links = links;
    }

    @JsonProperty("taskId")
    public String getTaskId() {
        return taskId;
    }

    @JsonProperty("taskId")
    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

    @JsonProperty("taskName")
    public String getTaskName() {
        return taskName;
    }

    @JsonProperty("taskName")
    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    @JsonProperty("reviewer")
    public Reviewer getReviewer() {
        return reviewer;
    }

    @JsonProperty("reviewer")
    public void setReviewer(Reviewer reviewer) {
        this.reviewer = reviewer;
    }

    @JsonProperty("status")
    public Integer getStatus() {
        return status;
    }

    @JsonProperty("status")
    public void setStatus(Integer status) {
        this.status = status;
    }

    @JsonProperty("certificationInstance")
    public CertificationInstance getCertificationInstance() {
        return certificationInstance;
    }

    @JsonProperty("certificationInstance")
    public void setCertificationInstance(CertificationInstance certificationInstance) {
        this.certificationInstance = certificationInstance;
    }

    @JsonProperty("startDate")
    public String getStartDate() {
        return startDate;
    }

    @JsonProperty("startDate")
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    @JsonProperty("endDate")
    public String getEndDate() {
        return endDate;
    }

    @JsonProperty("endDate")
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @JsonProperty("certificationState")
    public String getCertificationState() {
        return certificationState;
    }

    @JsonProperty("certificationState")
    public void setCertificationState(String certificationState) {
        this.certificationState = certificationState;
    }

    @JsonProperty("percentComplete")
    public Double getPercentComplete() {
        return percentComplete;
    }

    @JsonProperty("percentComplete")
    public void setPercentComplete(Double percentComplete) {
        this.percentComplete = percentComplete;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
