package com.kapstone.certification.oimresponse.certificate.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OIMCertificationActionResponse {

   private String status;
}
