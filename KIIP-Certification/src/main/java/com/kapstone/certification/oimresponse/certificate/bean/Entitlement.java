package com.kapstone.certification.oimresponse.certificate.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
public class Entitlement {
	private String id;
	private String name;
	private String description;
	private String itResourseName;
	private Field applicationInstanceName;
	private int accountKey;
}