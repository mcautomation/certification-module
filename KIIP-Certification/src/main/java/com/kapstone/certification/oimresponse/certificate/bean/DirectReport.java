package com.kapstone.certification.oimresponse.certificate.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
public class DirectReport {
	private String id;
	private String name;
	private String userLogin;
	private String displayName;
}