package com.kapstone.certification.oimresponse.certificate.bean;

import java.util.HashMap;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.ToString;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
"id",
"entityId",
"itemRisk",
"category",
"entityType",
"lastLogin",
"lastDecision",
"accountName",
"displayName",
"name",
"dataOwnerLogin",
"dataOwnerDisplayName",
"endPointName",
"iamId",
"action",
"accountId"


})
@ToString(includeFieldNames = true)
public class LineItemDetails {

@JsonProperty("id")
    public Integer getId() {
return id;
}
@JsonProperty("id")
public void setId(Integer id) {
this.id = id;
}
@JsonProperty("itemRisk")
public Integer getItemRisk() {
return itemRisk;
}
@JsonProperty("itemRisk")
public void setItemRisk(Integer itemRisk) {
this.itemRisk = itemRisk;
}
@JsonProperty("category")
public String getCategory() {
return category;
}
@JsonProperty("category")
public void setCategory(String category) {
this.category = category;
}
@JsonProperty("lastLogin")
public String getLastLogin() {
return lastLogin;
}
@JsonProperty("lastLogin")
public void setLastLogin(String lastLogin) {
this.lastLogin = lastLogin;
}
@JsonProperty("lastDecision")
public Integer getLastDecision() {
return lastDecision;
}
@JsonProperty("lastDecision")
public void setLastDecision(Integer lastDecision) {
this.lastDecision = lastDecision;
}
@JsonProperty("accountName")
public String getAccountName() {
return accountName;
}
@JsonProperty("accountName")
public void setAccountName(String accountName) {
this.accountName = accountName;
}
@JsonProperty("displayName")
public String getDisplayName() {
return displayName;
}
@JsonProperty("displayName")
public void setDisplayName(String displayName) {
this.displayName = displayName;
}
@JsonProperty("name")
public String getName() {
return name;
}
@JsonProperty("name")
public void setName(String name) {
this.name = name;
}
@JsonProperty("dataOwnerLogin")
public String getDataOwnerLogin() {
return dataOwnerLogin;
}
@JsonProperty("dataOwnerLogin")
public void setDataOwnerLogin(String dataOwnerLogin) {
this.dataOwnerLogin = dataOwnerLogin;
}
@JsonProperty("dataOwnerDisplayName")
public String getDataOwnerDisplayName() {
return dataOwnerDisplayName;
}
@JsonProperty("dataOwnerDisplayName")
public void setDataOwnerDisplayName(String dataOwnerDisplayName) {
this.dataOwnerDisplayName = dataOwnerDisplayName;
}
@JsonProperty("endPointName")
public String getEndPointName() {
return endPointName;
}
@JsonProperty("endPointName")
public void setEndPointName(String endPointName) {
this.endPointName = endPointName;
}
@JsonProperty("iamId")
public String getIamId() {
return iamId;
}
@JsonProperty("iamId")
public void setIamId(String iamId) {
this.iamId = iamId;
}
@JsonAnyGetter
public Map<String, Object> getAdditionalProperties() {
return additionalProperties;
}
@JsonAnySetter
public void setAdditionalProperties(Map<String, Object> additionalProperties) {
this.additionalProperties = additionalProperties;
}

@JsonProperty("id")
    private Integer id;
    @JsonProperty("itemRisk")
    private Integer itemRisk;  
    @JsonProperty("category")
    private String category;
    @JsonProperty("lastLogin")
    private String lastLogin;
    @JsonProperty("lastDecision")
    private Integer lastDecision;
    @JsonProperty("accountName")
    private String accountName;
    @JsonProperty("displayName")
    private String displayName;
    @JsonProperty("name")
    private String name;
    @JsonProperty("dataOwnerLogin")
    private String dataOwnerLogin;
    @JsonProperty("dataOwnerDisplayName")
    private String dataOwnerDisplayName;
    @JsonProperty("endPointName")
    private String endPointName;
    @JsonProperty("iamId")
    private String iamId;
   
    @JsonProperty("entityId")
    private Integer entityId;
    public Integer getEntityId() {
return entityId;
}
    @JsonProperty("entityId")
public void setEntityId(Integer entityId) {
this.entityId = entityId;
}
    @JsonProperty("entityType")
public String getEntityType() {
return entityType;
}
    @JsonProperty("entityType")
public void setEntityType(String entityType) {
this.entityType = entityType;
}

@JsonProperty("entityType")
    private String entityType;
   
@JsonProperty("action")
private Integer action;

@JsonProperty("action")
     public Integer getAction() {
return action;
}
@JsonProperty("action")
public void setAction(Integer action) {
this.action = action;
}
@JsonProperty("accountId")
public Integer getAccountId() {
return accountId;
}
@JsonProperty("accountId")
public void setAccountId(Integer accountId) {
this.accountId = accountId;
}

@JsonProperty("accountId")
private Integer accountId;


@JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();



}