package com.kapstone.certification.oimresponse.certificate.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
public class Field {
	private String name;
	private Object value;
}