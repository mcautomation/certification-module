package com.kapstone.certification.oimresponse.certificate.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
public class Account {
	private String id;
	private String name;
	private String userId;
	private String appInstanceId;
	private List<Field> fields;
}