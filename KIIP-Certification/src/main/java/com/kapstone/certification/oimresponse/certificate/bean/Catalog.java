package com.kapstone.certification.oimresponse.certificate.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString (includeFieldNames = true)
public class Catalog {
	private int id;
	private String entityType;
	private String entityName;
	private String entityDisplayName;
	private String entityDescription;
	private String entityKey;
	private String itemRisk;
	private String isCertifiable;
}