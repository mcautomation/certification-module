package com.kapstone.certification.oimresponse.certificate.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
public class CatalogResponse {
	private int count;
	private boolean hasMore;
	private List<Link> links;
	private List<Catalog> catalogs;
}