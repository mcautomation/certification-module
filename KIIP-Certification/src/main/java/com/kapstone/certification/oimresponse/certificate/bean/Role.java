package com.kapstone.certification.oimresponse.certificate.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
public class Role {
	private String id;
	private List<Field> fields;
	private String roleName;
	private String roleKey;
}
