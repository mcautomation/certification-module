package com.kapstone.certification.oimresponse.certificate.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CertificationTaskLineItems {

	private List<Link> links;
	private int count;
	private boolean hasMore;
	private List<DCertificationLineItem> certLineItemList;
}
