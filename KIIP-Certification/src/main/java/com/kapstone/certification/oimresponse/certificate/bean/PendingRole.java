package com.kapstone.certification.oimresponse.certificate.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
public class PendingRole {
	private String roleId;
	private String userId;
	private List<Field> roleGrantAttributes;
	private String roleName;
	private String roleKey;
}