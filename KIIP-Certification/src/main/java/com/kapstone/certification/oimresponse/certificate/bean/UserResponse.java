package com.kapstone.certification.oimresponse.certificate.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
public class UserResponse {
	private int managerKey;
	private List<Field> fields;
	private List<Role> roles;
	private List<PendingRole> pendingRoles;
	private List<Account> accounts;
	private List<Entitlement> entitlements;
}
