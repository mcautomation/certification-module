
package com.kapstone.certification.oimresponse.certificate.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "links",
    "certificationDefinitions"
})
public class CertificationDefinitions {

    @JsonProperty("links")
    private List<Link> links = null;
    @JsonProperty("certificationDefinitions")
    private List<CertificationDefinition> certificationDefinitions = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("links")
    public List<Link> getLinks() {
        return links;
    }

    @JsonProperty("links")
    public void setLinks(List<Link> links) {
        this.links = links;
    }

    @JsonProperty("certificationDefinitions")
    public List<CertificationDefinition> getCertificationDefinitions() {
        return certificationDefinitions;
    }

    @JsonProperty("certificationDefinitions")
    public void setCertificationDefinitions(List<CertificationDefinition> certificationDefinitions) {
        this.certificationDefinitions = certificationDefinitions;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
