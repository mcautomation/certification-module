package com.kapstone.certification.oimresponse.certificate.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
public class DirectReportsResponse {
	private List<Link> links;
	int count;
	boolean hasMore;
	List<DirectReport> users;
}