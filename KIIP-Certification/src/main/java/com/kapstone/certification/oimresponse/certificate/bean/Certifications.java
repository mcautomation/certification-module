
package com.kapstone.certification.oimresponse.certificate.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "links",
    "count",
    "hasMore",
    "totalResult",
    "certifications"
})
public class Certifications {

    @JsonProperty("links")
    private List<Link> links = null;
    @JsonProperty("count")
    private Integer count;
    @JsonProperty("hasMore")
    private Boolean hasMore;
    @JsonProperty("totalResult")
    private Integer totalResult;
    @JsonProperty("certifications")
    private List<Certification> certifications = null;
    @JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    @JsonProperty("links")
    public List<Link> getLinks() {
        return links;
    }

    @JsonProperty("links")
    public void setLinks(List<Link> links) {
        this.links = links;
    }

    @JsonProperty("count")
    public Integer getCount() {
        return count;
    }

    @JsonProperty("count")
    public void setCount(Integer count) {
        this.count = count;
    }

    @JsonProperty("hasMore")
    public Boolean getHasMore() {
        return hasMore;
    }

    @JsonProperty("hasMore")
    public void setHasMore(Boolean hasMore) {
        this.hasMore = hasMore;
    }

    @JsonProperty("totalResult")
    public Integer getTotalResult() {
        return totalResult;
    }

    @JsonProperty("totalResult")
    public void setTotalResult(Integer totalResult) {
        this.totalResult = totalResult;
    }

    @JsonProperty("certifications")
    public List<Certification> getCertifications() {
        return certifications;
    }

    @JsonProperty("certifications")
    public void setCertifications(List<Certification> certifications) {
        this.certifications = certifications;
    }

    @JsonAnyGetter
    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    @JsonAnySetter
    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

}
