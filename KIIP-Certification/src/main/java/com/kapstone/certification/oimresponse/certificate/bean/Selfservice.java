package com.kapstone.certification.oimresponse.certificate.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.kapstone.certification.bean.NormalizeData;

import lombok.Getter;
import lombok.Setter;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({"fields"})
@Getter
@Setter
public class Selfservice {
	

	@JsonProperty("fields")
    //private List<NormalizeData> fields;	
	
	private List<HashMap<String, Object>> fields;
	
	
	@JsonIgnore
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

	@JsonIgnore
    public Map<String, Object> getAdditionalProperties() {
		return additionalProperties;
	}
	@JsonIgnore
	public void setAdditionalProperties(Map<String, Object> additionalProperties) {
		this.additionalProperties = additionalProperties;
	}
}
