package com.kapstone.certification.oimresponse.certificate.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
public class DCertificationLineItem {
	private long id;
	private long entityId;
	private String userLogin;
	private String firstName;
	private String lastName;
	private String email;
	private String department;
	private String manager;
	private int roles;
	private int accounts;
	private int entitlements;
	private double percentComplete;
	private long iamUserId;
	
}
