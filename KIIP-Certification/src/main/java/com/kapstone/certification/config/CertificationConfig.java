package com.kapstone.certification.config;


import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Configuration
@ConfigurationProperties
@Getter
@Setter
public class CertificationConfig {
	
	private Url url;
	private Oim oim;
	private String certificationFile;
	
	@Getter
	@Setter
	@ToString (includeFieldNames = true)
	public static class Oim {
		
		private String encryptkey;
		private String tokenType;
		private long certificationExpirationDays;
		private int pagelimit;
		private String userModule;
		private String adminUser;
		private String adminPassword;
		
	}
		

	@Getter
	@Setter
	@ToString (includeFieldNames = true)
	public static class Url {
		
		private String prefix;
		private String certifications;
		private String certificationsInfo;	
		
		private String certificationsLineItemUpdate;
		private String certificationsLineItems;
		private String certificationLineItems;
		private String applicationProfileItems;
		private String certificationsStatusInfo;
		private String certificationAction;
		
		private String certificationSignOffInfo;
		
		private String userModulePrefix;
		private String signOffCertification;
		private String userCertificationAction;
		
		private String catalogs;
		private String catalogDetails;
		private String users;
		private String reportees;
	}

	
}
