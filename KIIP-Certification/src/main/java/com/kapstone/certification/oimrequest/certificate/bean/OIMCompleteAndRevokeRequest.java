package com.kapstone.certification.oimrequest.certificate.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OIMCompleteAndRevokeRequest {

   private List<OIMLineItemRequest> lineItems;
}
