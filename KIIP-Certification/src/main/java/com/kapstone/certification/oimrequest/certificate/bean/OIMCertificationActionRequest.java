package com.kapstone.certification.oimrequest.certificate.bean;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OIMCertificationActionRequest {

   private String action;
   
   private List<Field> fields;
}
