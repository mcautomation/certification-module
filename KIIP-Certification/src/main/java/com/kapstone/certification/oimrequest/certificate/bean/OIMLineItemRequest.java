package com.kapstone.certification.oimrequest.certificate.bean;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OIMLineItemRequest {

   private String action;
   private long lineItemId;
   private String comments;
   
}
