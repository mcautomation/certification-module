package com.kapstone.certification.bean;

import java.util.List;

import com.kapstone.certification.uirequest.certificate.bean.Field;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class CertificationActionRequest {
	
	private String action;
	
	private List<CertificationInfo> certifications;
	
	private List<Field> fields;
}
