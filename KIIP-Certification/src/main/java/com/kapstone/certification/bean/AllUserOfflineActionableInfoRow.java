package com.kapstone.certification.bean;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AllUserOfflineActionableInfoRow
{
  private String certificationName;
  private String userName;
  private String displayName;
  private String roleName;
  
  private String applicationName; 
  private String entitlement; 
  private String action; 
  private String comments;
  
  private String userId;
  private String roleId;
  private String applnId;
  private String entlId;
  
}