package com.kapstone.certification.bean;

import java.util.HashMap;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class EntityAppinstance {
	
	private ApplicationInfo applicationInfo;
	private String itemType;
	private String itemRisk;
	private String action;
	private String accountId;
	private List<Comment> oldComments;
	private Comment newComment;
	private int totalEntitlements;
	private int totalCertifiedEntitlements;
	private List<EntityEntitlement> entityEntitlements;
	private HashMap<String, String> applicationProfile;
	

}
