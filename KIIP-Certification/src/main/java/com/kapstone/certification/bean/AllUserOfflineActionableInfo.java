package com.kapstone.certification.bean;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class AllUserOfflineActionableInfo
{
  private String certificationName;
  private String userName;
  private String displayName;
  private String roleName;
  
  private String applicationName; 
  private String entitlement; 
  private String action; 
  private String comments;
  private String endDate;
  
  private long userId;
  private long roleId;
  private long applnId;
  private long entlId;
  
}