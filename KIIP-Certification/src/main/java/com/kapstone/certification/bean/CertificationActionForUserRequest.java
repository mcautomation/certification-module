package com.kapstone.certification.bean;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class CertificationActionForUserRequest {
	
	private String action;
	
	private CertificationInfo certification;
	
	private List<Long> lineItemEntityId;
}
