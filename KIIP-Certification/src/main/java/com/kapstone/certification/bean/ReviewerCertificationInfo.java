package com.kapstone.certification.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter
@NoArgsConstructor
@ToString (includeFieldNames = true)
public class ReviewerCertificationInfo {

	@JsonProperty("certificationType")
	private String certificationType;
	
	@JsonProperty("certificationName")
	private String certificationName;
	
	@JsonProperty("certificateRequester")
	private String certificateRequester;
	
	@JsonProperty("certificationExpiration")
	private String certificationExpiration;
	
	@JsonProperty("certificationCreatedOn")
	private String certificationCreatedOn;
	
	@JsonProperty("certificationSignedOff")
	private String certificationSignedOff;
	
	@JsonProperty("certificationSignedOffOn")
	private String certificationSignedOffOn;
	
	@JsonProperty("certificationSignedOffBy")
	private String certificationSignedOffBy;
	
	@JsonProperty("status")
	private String status;
}

