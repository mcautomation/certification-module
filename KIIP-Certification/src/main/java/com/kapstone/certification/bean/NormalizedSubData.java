package com.kapstone.certification.bean;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class NormalizedSubData {
	@JsonProperty("name")
	private String name;
	@JsonProperty("value")
	private List<String> value;
}
