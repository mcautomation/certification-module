package com.kapstone.certification.bean;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class Reviewer {
	

	private int certificationId;
	private String reviewerId;
	private String campaignId;
	private ReviewerCertificationInfo reviewerCertificationInfo;
	private ReviewerCertificateActionInfo reviewerCertificateActionInfo;
	private List<Members> members;

}
