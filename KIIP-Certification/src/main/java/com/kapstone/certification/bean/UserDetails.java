package com.kapstone.certification.bean;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonInclude;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import com.fasterxml.jackson.annotation.JsonInclude.Include;;

@Getter 
@Setter
@NoArgsConstructor
@JsonInclude(Include.NON_NULL)
public class UserDetails {
	
	//private Map<String, Object> userInfo;
	private UserInfo userInfo;
	private String firstTimeUser;
	private List<Entityrole> entityRole;
	private List<EntityAppinstance> entityAppinstance;
	private String reviewerId;
	
}
