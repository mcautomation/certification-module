package com.kapstone.certification.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class UserCertificationInfo {
	
	private int noOfRoles;
	private int noOfApplications;
	@JsonProperty("numOfEentitlements")
	private int numOfEntitlements;
	private int numOfRolesCertified;
	private int numOfApplicationsCertified;
	private int numOfEentitlementsCertified;
	private int totalActions;
	private int totalActionCompleted;
	private double percentageCompleted;
	private long lineItemId;
	

}
