package com.kapstone.certification.bean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class Roleinfo {
	
	private String roleName;
	private String roleDescription;
	private String roleOwner;
}
