package com.kapstone.certification.bean;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
@JsonInclude(Include.NON_NULL)
public class Login {

	private String userName;
	private String password;
	private String oldPassword;
	private String confirmPassword;
	private String cookies;
	private String loginDateAndTime;
}
