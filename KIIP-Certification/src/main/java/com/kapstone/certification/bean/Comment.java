package com.kapstone.certification.bean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class Comment {

	private String comment;
	private String commentedBy;
	private String commentedOn;
	
}
