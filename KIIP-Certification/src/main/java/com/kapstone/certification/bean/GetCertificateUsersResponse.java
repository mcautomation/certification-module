package com.kapstone.certification.bean;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class GetCertificateUsersResponse implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int certificationId;
	private String reviewerId; 	//DEEPA's Change
	private String campaignId;
	private ReviewerCertificationInfo reviewerCertificationInfo;
	private ReviewerCertificateActionInfo reviewerCertificateActionInfo;
	private List<Members> members;
	private String taskId; //PRADIP Changes

}
