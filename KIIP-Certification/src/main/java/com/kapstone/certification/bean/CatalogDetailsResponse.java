package com.kapstone.certification.bean;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString (includeFieldNames = true)
public class CatalogDetailsResponse {
	
	private String id;
	private String entityType;
	private String entityName;
	private String displayName;
	private String description;
	private String entityRisk;
	private boolean isCertifiable;
	
}
