package com.kapstone.certification.bean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class EntitlementInfo {
	
	private String entitlementName;
	private String entitlementDescription;
	private String entitlementOwnerId;
	private String entitlementOwner;
	private String entitlementGroups;
	private Integer entitlementId;
    //private String entitlementType;
}