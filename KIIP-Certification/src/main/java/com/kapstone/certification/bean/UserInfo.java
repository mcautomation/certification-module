package com.kapstone.certification.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor


public class UserInfo {
	
	@JsonProperty("UserName")
	private String userName;
	
	@JsonProperty("FirstName")
	private String firstName;
	
	@JsonProperty("LastName")
	private String lastName;
	
	@JsonProperty("Email")
	private String email;
	
	@JsonProperty("Department")
	private String department;
	
	@JsonProperty("Manager")
	private String manager;
	
	private String lastLogin;
	
	private long userkey;
	
	private String hireDate;
}
