package com.kapstone.certification.bean;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class EntitlementCount {
	
	private Integer entitlementcount;
	private Integer totalCertifiedEntitlements;

}
