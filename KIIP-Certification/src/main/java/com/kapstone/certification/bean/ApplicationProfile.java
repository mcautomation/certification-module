package com.kapstone.certification.bean;

import java.util.HashMap;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class ApplicationProfile {
	private String isLinkedAccount;
	private HashMap<String, String> applicationProfile;

}
