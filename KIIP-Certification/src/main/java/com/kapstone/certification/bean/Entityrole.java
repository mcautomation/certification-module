package com.kapstone.certification.bean;

import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class Entityrole {

	private Roleinfo roleInfo;
	private String itemType;
	private String itemRisk;
	private String action;
	private List<Comment> oldComments;
	private Comment newComment;
	private int roleEntityId;
}
