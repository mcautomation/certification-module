package com.kapstone.certification.bean;

import java.io.Serializable;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter
@NoArgsConstructor
@ToString(includeFieldNames = true)
public class GetCertificationResponse implements Serializable{
	
	private static final long serialVersionUID = 1L;
	private int certificationId;
	private String campaignId;
	private String taskId; //PRADIP Changes
	private ReviewerCertificationInfo reviewerCertificationInfo = new ReviewerCertificationInfo();
	private ReviewerCertificateActionInfo reviewerCertificateActionInfo = new ReviewerCertificateActionInfo() ;

}
