package com.kapstone.certification.bean;

import java.time.LocalDate;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class ApplicationInfo {

	private String applicationName;
	private String applicationDescription;
	private String applicationOwnerId;
	private String applicationOwner;
	private Integer applicationEntityId;
	private LocalDate lastLogin;
}
