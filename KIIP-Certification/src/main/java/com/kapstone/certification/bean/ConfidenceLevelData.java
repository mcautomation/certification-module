package com.kapstone.certification.bean;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString (includeFieldNames = true)
public class ConfidenceLevelData {
	
	private ConfidenceLevel high;
	private ConfidenceLevel medium;
	private ConfidenceLevel low;
	private double threshold;

	@Getter
	@Setter
	@ToString (includeFieldNames = true)
	@NoArgsConstructor
	@AllArgsConstructor
	public static class ConfidenceLevel {
		private double peerAnalysis;
		private String entityRisk;
	}

}

