package com.kapstone.certification.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter
@NoArgsConstructor
@ToString(includeFieldNames = true)
public class Members {
	
	private UserInfo userInfo;
	
	@JsonProperty("noOfRoles")
	private int numOfRoles;
	
	@JsonProperty("noOfApplications")
	private int numOfApplications;
	
	@JsonProperty("numOfEntitlements")
	private int numOfEntitlements;
	
	@JsonProperty("numOfRolesCertified")
	private int numOfRolesCertified;
	
	@JsonProperty("numOfApplicationsCertified")
	private int numOfApplicationsCertified;
	
	@JsonProperty("numOfEntitlementsCertified")
	private int numOfEntitlementsCertified;
	
	private double percentageCompleted;
	
	private long lineItemId;
	
	private long lineItemEntityId;

}
