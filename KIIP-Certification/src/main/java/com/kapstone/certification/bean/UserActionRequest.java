package com.kapstone.certification.bean;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class UserActionRequest {
	
	private String userName;
	private String campaignId;
	private List<Entityrole> entityRole;
	private List<EntityAppinstance> entityAppinstance;

}
