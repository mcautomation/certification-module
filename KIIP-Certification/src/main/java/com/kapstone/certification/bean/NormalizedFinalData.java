package com.kapstone.certification.bean;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter 
@Setter
@NoArgsConstructor
@ToString(includeFieldNames = true)
public class NormalizedFinalData {
	@JsonProperty("name")
	private String name;
	@JsonProperty("value")
	private String value;
}
