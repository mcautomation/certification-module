package com.kapstone.certification.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class ReviewerCertificateActionInfo {

	@JsonProperty("totalActions")
	private int totalActions;
	
	@JsonProperty("totalActionsCompleted")
	private int totalActionsCompleted;
	
	@JsonProperty("percentageCompleted")
	private double percentageCompleted;
}
