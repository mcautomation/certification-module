package com.kapstone.certification.bean;

import java.util.HashMap;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor
public class EntityEntitlement {
	
	private EntitlementInfo entitlementInfo;
	private String itemType;
	private String itemRisk;
	private String action;
	private String application;
	private List<Comment> oldComments;
	private Comment newComment;
	

}
