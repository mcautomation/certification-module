package com.kapstone.certification.bean;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter 
@Setter
@NoArgsConstructor

public class User {
	

	private int id;
	private String userName;
	private String campaignId;
	private String reviewerId;
	private UserCertificationInfo userCertificationInfo;
	private UserDetails userDetails;
	

}
