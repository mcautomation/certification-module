package com.kp.oim.services;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.security.auth.login.LoginException;

import com.kp.oim.MyAuthException;
import com.kp.oim.constants.OIMConstants;
import com.kp.oim.pojo.CatlogApplication;
import com.kp.oim.pojo.Login;
import com.thortech.xl.client.dataobj.tcDataBaseClient;
import com.thortech.xl.dataaccess.tcDataProvider;
import com.thortech.xl.dataaccess.tcDataSet;
import com.thortech.xl.dataaccess.tcDataSetException;

import Thor.API.Security.XLClientSecurityAssociation;
import oracle.iam.identity.rolemgmt.api.RoleManager;
import oracle.iam.identity.rolemgmt.vo.Role;
import oracle.iam.platform.OIMClient;
import oracle.iam.provisioning.api.ProvisioningService;
import oracle.iam.provisioning.exception.AccountNotFoundException;
import oracle.iam.provisioning.exception.EntitlementNotProvisionedException;
import oracle.iam.provisioning.exception.GenericProvisioningException;
import oracle.iam.provisioning.exception.UserNotFoundException;
import oracle.iam.provisioning.vo.Account;
import oracle.iam.provisioning.vo.ApplicationInstance;
import oracle.iam.provisioning.vo.Entitlement;
import oracle.iam.provisioning.vo.EntitlementInstance;

public class OIMApplicationServices extends OIMConnections {


	/*public List<CatlogApplication> getAllCatlogDetailsofApps_BKP(OIMClient oimClient) throws tcDataSetException  {
		
		List<CatlogApplication> catlogApplicationList = new ArrayList<CatlogApplication>();
		
		tcDataProvider dbProvider = null;
		XLClientSecurityAssociation.setClientHandle(oimClient); 
		dbProvider = new tcDataBaseClient();

		String sqlQuery = "select usr.Usr_key , usr.Usr_login as Usr_login, app.app_instance_name, app.app_instance_display_name, app.CREATE_BY_USER, usr.Usr_Display_Name ,app.app_instance_description  from dev_oim.USR usr\r\n"
				+ "JOIN dev_oim.App_instance app on app.create_by_user = usr.usr_key";

		System.out.println("QUERY = " + sqlQuery);
		tcDataSet usersDataSet = new tcDataSet(); //store result set of query
		usersDataSet.setQuery(dbProvider, sqlQuery); 
		usersDataSet.executeQuery();

		int numRecords = usersDataSet.getTotalRowCount();

		for(int i = 0; i < numRecords; i++){
				usersDataSet.goToRow(i);
				
				CatlogApplication applicationResponse = new CatlogApplication();

				applicationResponse.setApplication(usersDataSet.getString("app_instance_name"));
				applicationResponse.setDescription(usersDataSet.getString("app_instance_description"));
				applicationResponse.setOwnerID(usersDataSet.getString("Usr_key"));
				applicationResponse.setOwnerName(usersDataSet.getString("Usr_Display_Name"));
//				applicationResponse.setRisk("");
				catlogApplicationList.add(applicationResponse);
			}

			return catlogApplicationList;

	
	}

	
	public List<HashMap<String, Object>> getAllCatlogDetailsofApps(OIMClient oimClient) throws tcDataSetException {

		tcDataProvider dbProvider = null;
		XLClientSecurityAssociation.setClientHandle(oimClient);
		dbProvider = new tcDataBaseClient();

		
		
		Properties oimQueryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);
		String sqlQuery = oimQueryProp.getProperty(OIMConstants.GET_CATLOG_DETAILS_OF_APP);
		System.out.println("QUERY = " + sqlQuery);
		tcDataSet usersDataSet = new tcDataSet(); // store result set of query
		usersDataSet.setQuery(dbProvider, sqlQuery);
		usersDataSet.executeQuery();

		Properties appProp = readPropertiesFile(OIMConstants.OIM_APP_ATTR_MAP_PROP);
		
		int numRecords = usersDataSet.getTotalRowCount();
		List<HashMap<String, Object>> applicationList = new ArrayList<HashMap<String, Object>>();
		for (int i = 0; i < numRecords; i++) {
			usersDataSet.goToRow(i);
			HashMap<String, Object> applicationMap = new HashMap<String, Object>();
			for (String appKey : appProp.stringPropertyNames()) {
				if (!appProp.getProperty(appKey).contains(".")) {
					applicationMap.put(appProp.getProperty(appKey),
							usersDataSet.getString(appKey));
				}
			}
			applicationList.add(applicationMap);
		}

		return applicationList;

	}*/
	/*
	private HashMap<String, Object> getEntityProfileInfo(tcDataSet usersDataSet) throws tcDataSetException{

		HashMap<String, Object> userInfo = new HashMap<>();
		Properties oimResponseAttrMappingProp = readPropertiesFile(OIMConstants.OIM_RESPONSE_ATTR_MAP_PROP);

		for(String key : oimResponseAttrMappingProp.stringPropertyNames()) {
			if(usersDataSet.hasColumn(key)) {
				// DATA TYPE CODE IS REMAINING
				userInfo.put(oimResponseAttrMappingProp.getProperty(key), usersDataSet.getString(key));
			}
		}

		return userInfo;

	}*/
	
	
	public List<CatlogApplication> getAllCatlogDetailsofApps_BKP(OIMClient oimClient) throws tcDataSetException {

		List<CatlogApplication> catlogApplicationList = new ArrayList<CatlogApplication>();

		tcDataProvider dbProvider = null;
		XLClientSecurityAssociation.setClientHandle(oimClient);
		dbProvider = new tcDataBaseClient();

		String sqlQuery = "select usr.Usr_key , usr.Usr_login as Usr_login, app.app_instance_name, app.app_instance_display_name, app.CREATE_BY_USER, usr.Usr_Display_Name ,app.app_instance_description  from dev_oim.USR usr\r\n"
				+ "JOIN dev_oim.App_instance app on app.create_by_user = usr.usr_key";

		System.out.println("QUERY = " + sqlQuery);
		tcDataSet usersDataSet = new tcDataSet(); // store result set of query
		usersDataSet.setQuery(dbProvider, sqlQuery);
		usersDataSet.executeQuery();

		int numRecords = usersDataSet.getTotalRowCount();

		for (int i = 0; i < numRecords; i++) {
			usersDataSet.goToRow(i);

			CatlogApplication applicationResponse = new CatlogApplication();

			applicationResponse.setApplication(usersDataSet.getString("app_instance_name"));
			applicationResponse.setDescription(usersDataSet.getString("app_instance_description"));
			applicationResponse.setOwnerID(usersDataSet.getString("Usr_key"));
			applicationResponse.setOwnerName(usersDataSet.getString("Usr_Display_Name"));
//				applicationResponse.setRisk("");
			catlogApplicationList.add(applicationResponse);
		}

		return catlogApplicationList;

	}

	public List<HashMap<String, Object>> getAllCatlogDetailsofApps(OIMClient oimClient) throws tcDataSetException {

		tcDataProvider dbProvider = null;
		XLClientSecurityAssociation.setClientHandle(oimClient);
		dbProvider = new tcDataBaseClient();

		Properties oimQueryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);
		String sqlQuery = oimQueryProp.getProperty(OIMConstants.GET_CATLOG_DETAILS_OF_APP);
		System.out.println("QUERY = " + sqlQuery);
		tcDataSet usersDataSet = new tcDataSet(); // store result set of query
		usersDataSet.setQuery(dbProvider, sqlQuery);
		usersDataSet.executeQuery();

		Properties appProp = readPropertiesFile(OIMConstants.OIM_APP_ATTR_MAP_PROP);

		int numRecords = usersDataSet.getTotalRowCount();
		List<HashMap<String, Object>> applicationList = new ArrayList<HashMap<String, Object>>();
		for (int i = 0; i < numRecords; i++) {
			usersDataSet.goToRow(i);
			HashMap<String, Object> applicationMap = new HashMap<String, Object>();
			for (String appKey : appProp.stringPropertyNames()) {
				if (!appProp.getProperty(appKey).contains(".")) {
					applicationMap.put(appProp.getProperty(appKey), usersDataSet.getString(appKey));
				}
			}
			applicationList.add(applicationMap);
		}

		return applicationList;

	}

	/*
	 * private HashMap<String, Object> getEntityProfileInfo(tcDataSet usersDataSet)
	 * throws tcDataSetException{
	 * 
	 * HashMap<String, Object> userInfo = new HashMap<>(); Properties
	 * oimResponseAttrMappingProp =
	 * readPropertiesFile(OIMConstants.OIM_RESPONSE_ATTR_MAP_PROP);
	 * 
	 * for(String key : oimResponseAttrMappingProp.stringPropertyNames()) {
	 * if(usersDataSet.hasColumn(key)) { // DATA TYPE CODE IS REMAINING
	 * userInfo.put(oimResponseAttrMappingProp.getProperty(key),
	 * usersDataSet.getString(key)); } }
	 * 
	 * return userInfo;
	 * 
	 * }
	 */
	
	/**
	public List<HashMap<String, Object>> getAllApplication(String authUserName, String loginDateAndTime, String cookies,
			String tenantId) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException, IOException, LoginException, tcDataSetException, MyAuthException {

		tcDataProvider dbProvider = null;
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());
		XLClientSecurityAssociation.setClientHandle(oimClient);

		dbProvider = new tcDataBaseClient();

		Properties queryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);
		String sqlQuery = queryProp.getProperty(OIMConstants.GET_ALL_APP);
		System.out.println("QUERY = " + sqlQuery);

		Properties appAttrProp = readPropertiesFile(OIMConstants.OIM_APP_ATTR_MAP_PROP);
		tcDataSet usersDataSet = new tcDataSet(); // store result set of query
		usersDataSet.setQuery(dbProvider, sqlQuery);
		usersDataSet.executeQuery();
		int numRecords = usersDataSet.getTotalRowCount();
		List<HashMap<String, Object>> appList = new ArrayList<HashMap<String, Object>>();
		for (int i = 0; i < numRecords; i++) {
			usersDataSet.goToRow(i);
			HashMap<String, Object> appMap = new HashMap<String, Object>();
			for (String rolesKeys : appAttrProp.stringPropertyNames()) {
				if (usersDataSet.hasColumn(rolesKeys)) {
					if (!appAttrProp.getProperty(rolesKeys).contains(".")) {
						appMap.put(appAttrProp.getProperty(rolesKeys), usersDataSet.getString(rolesKeys));
					}
				}
			}
			appList.add(appMap);
		}

		return appList;

	}
	**/
	
	
	public List<HashMap<String, Object>> getAllApplication(String authUserName, String loginDateAndTime, String cookies,
			String tenantId) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException, IOException, LoginException, tcDataSetException, MyAuthException {

		tcDataProvider dbProvider = null;
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());
		XLClientSecurityAssociation.setClientHandle(oimClient);

		dbProvider = new tcDataBaseClient();

		Properties queryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);
		String sqlQuery = queryProp.getProperty(OIMConstants.GET_ALL_APP);
		System.out.println("QUERY = " + sqlQuery);

		Properties appAttrProp = readPropertiesFile(OIMConstants.OIM_APP_ATTR_MAP_PROP);
		tcDataSet usersDataSet = new tcDataSet(); // store result set of query
		usersDataSet.setQuery(dbProvider, sqlQuery);
		usersDataSet.executeQuery();
		int numRecords = usersDataSet.getTotalRowCount();
		List<HashMap<String, Object>> appList = new ArrayList<HashMap<String, Object>>();
		for (int i = 0; i < numRecords; i++) {
			usersDataSet.goToRow(i);
			HashMap<String, Object> appMap = new HashMap<String, Object>();
			for (String rolesKeys : appAttrProp.stringPropertyNames()) {
				if (usersDataSet.hasColumn(rolesKeys)) {
					if (!appAttrProp.getProperty(rolesKeys).contains(".")) {
						appMap.put(appAttrProp.getProperty(rolesKeys), usersDataSet.getString(rolesKeys));
					}
				}
			}
			appList.add(appMap);
		}

		return appList;

	}
	
	/**
	public List<HashMap<String, Object>> getAllAppEntitlements(String authUserName, String loginDateAndTime,
			String cookies, String tenantId) throws tcDataSetException, InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, LoginException, MyAuthException {

		tcDataProvider dbProvider = null;
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());
		List<HashMap<String, Object>> appList = getAllApp(oimClient);
		XLClientSecurityAssociation.setClientHandle(oimClient);
		dbProvider = new tcDataBaseClient();

		Properties oimQueryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);
		Properties appAttributesProperties = readPropertiesFile(OIMConstants.OIM_APP_ATTR_MAP_PROP);

		List<HashMap<String, Object>> appEntitlementResonse = new ArrayList<HashMap<String, Object>>();
		System.out.println("app list = " + appList);
		for (int j = 0; j < appList.size(); j++) {
			HashMap<String, Object> appObj = appList.get(j);
			System.out.println("APP OBJECT = " + appObj);
			String appName = (String) appObj.get(appAttributesProperties.getProperty("APP_NAME"));
			System.out.println("APP NAME = " + appName);
			String query = oimQueryProp.getProperty(OIMConstants.GET_ALL_APPLICATION_ENTITLEMENTS);
			System.out.println("query before  = " + query);
			
			if (query.contains("{appName}")) {
				query = query.replaceAll("\\{appName\\}", appName);
			}

			System.out.println("QUERY AFTER = " + query);
			tcDataSet usersDataSet = new tcDataSet(); // store result set of query
			usersDataSet.setQuery(dbProvider, query);
			usersDataSet.executeQuery();
			int numRecords = usersDataSet.getTotalRowCount();
			List<HashMap<String, Object>> entilementList = new ArrayList<>();
			HashMap<String, Object> appProfile = new HashMap<String, Object>();
			for (int i = 0; i < numRecords; i++) {
				usersDataSet.goToRow(i);
				HashMap<String, Object> entitlementMap = new HashMap<String, Object>();
				
				for (String appKeys : appAttributesProperties.stringPropertyNames()) {
					if (usersDataSet.hasColumn(appKeys)) {
						String splitedObject[] = appAttributesProperties.getProperty(appKeys).split("\\.");
						//if (splitedObject.length == 1) {
						//	appObj.put(splitedObject[0], usersDataSet.getString(appKeys));
						//} else
						if (splitedObject.length == 2) {
							if (splitedObject[0].equalsIgnoreCase("entitlement")) {
								String subentitlementKey = splitedObject[1];
								entitlementMap.put(subentitlementKey, usersDataSet.getString(appKeys));
							} else if (splitedObject[0].equalsIgnoreCase("applicationProfile")) {
								String subapplicationProfileKey = splitedObject[1];
								appProfile.put(subapplicationProfileKey, usersDataSet.getString(appKeys));
							}
						}

					}
				}
				
				entilementList.add(entitlementMap);
			}
			appObj.put("applicatioProfile", appProfile);
			appObj.put("entilements", entilementList);
			appEntitlementResonse.add(appObj);
		}

		return appEntitlementResonse;
	}

	**/
	
	

	
	
	public List<HashMap<String, Object>> getAllEntilementOfAppByAppName(String authUserName, String loginDateAndTime,
			String cookies, String appName, String tenantId)
			throws tcDataSetException, LoginException, InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, MyAuthException {

		tcDataProvider dbProvider = null;
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());
		List<HashMap<String, Object>> appList = getAppByAppName(appName, oimClient);
		XLClientSecurityAssociation.setClientHandle(oimClient);
		dbProvider = new tcDataBaseClient();

		Properties oimQueryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);
		Properties appAttributesProperties = readPropertiesFile(OIMConstants.OIM_APP_ATTR_MAP_PROP);

		List<HashMap<String, Object>> appEntitlementResonse = new ArrayList<HashMap<String, Object>>();
		System.out.println("app list = " + appList);
		for (int j = 0; j < appList.size(); j++) {
			HashMap<String, Object> appObj = appList.get(j);
			System.out.println("APP OBJECT = " + appObj);
			String appName_ = (String) appObj.get(appAttributesProperties.getProperty("APP_NAME"));
			System.out.println("APP NAME = " + appName_);
			String query = oimQueryProp.getProperty(OIMConstants.GET_ALL_APPLICATION_ENTITLEMENTS);
			System.out.println("query before  = " + query);
			
			if (query.contains("{appName}")) {
				query = query.replaceAll("\\{appName\\}", appName_);
			}

			System.out.println("QUERY AFTER = " + query);
			tcDataSet usersDataSet = new tcDataSet(); // store result set of query
			usersDataSet.setQuery(dbProvider, query);
			usersDataSet.executeQuery();
			int numRecords = usersDataSet.getTotalRowCount();
			List<HashMap<String, Object>> entilementList = new ArrayList<>();
			for (int i = 0; i < numRecords; i++) {
				usersDataSet.goToRow(i);
				HashMap<String, Object> entitlementMap = new HashMap<String, Object>();
				HashMap<String, Object> appProfile = new HashMap<String, Object>();
				for (String appKeys : appAttributesProperties.stringPropertyNames()) {
					if (usersDataSet.hasColumn(appKeys)) {
						String splitedObject[] = appAttributesProperties.getProperty(appKeys).split("\\.");
						if (splitedObject.length == 1) {
							appObj.put(splitedObject[0], usersDataSet.getString(appKeys));
						} else if (splitedObject.length == 2) {
							if (splitedObject[0].equalsIgnoreCase("entitlement")) {
								String subentitlementKey = splitedObject[1];
								entitlementMap.put(subentitlementKey, usersDataSet.getString(appKeys));
							} else if (splitedObject[0].equalsIgnoreCase("applicationProfile")) {
								String subapplicationProfileKey = splitedObject[1];
								appProfile.put(subapplicationProfileKey, usersDataSet.getString(appKeys));
							}
						}

					}
				}
				appObj.put("applicatioProfile", appProfile);
				entilementList.add(entitlementMap);
			}
			appObj.put("entilements", entilementList);
			appEntitlementResonse.add(appObj);
		}

		return appEntitlementResonse;
	}

	private List<HashMap<String, Object>> getAllApp(OIMClient oimClient)
			throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, IOException, LoginException, tcDataSetException {

		tcDataProvider dbProvider = null;
		XLClientSecurityAssociation.setClientHandle(oimClient);
		dbProvider = new tcDataBaseClient();

		Properties queryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);
		String sqlQuery = queryProp.getProperty(OIMConstants.GET_ALL_APP);
		System.out.println("QUERY = " + sqlQuery);

		Properties appAttrProp = readPropertiesFile(OIMConstants.OIM_APP_ATTR_MAP_PROP);
		tcDataSet usersDataSet = new tcDataSet(); // store result set of query
		usersDataSet.setQuery(dbProvider, sqlQuery);
		usersDataSet.executeQuery();
		int numRecords = usersDataSet.getTotalRowCount();
		List<HashMap<String, Object>> appList = new ArrayList<HashMap<String, Object>>();
		for (int i = 0; i < numRecords; i++) {
			usersDataSet.goToRow(i);
			HashMap<String, Object> appMap = new HashMap<String, Object>();
			for (String appKeys : appAttrProp.stringPropertyNames()) {
				if (usersDataSet.hasColumn(appKeys)) {
					if (!appAttrProp.getProperty(appKeys).contains(".")) {
						appMap.put(appAttrProp.getProperty(appKeys), usersDataSet.getString(appKeys));
					}
				}
			}
			appList.add(appMap);
		}

		return appList;

	}
	private List<HashMap<String, Object>> getAppByAppName(String appName,OIMClient oimClient)
			throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, IOException, LoginException, tcDataSetException {

		tcDataProvider dbProvider = null;
		XLClientSecurityAssociation.setClientHandle(oimClient);
		dbProvider = new tcDataBaseClient();

		Properties queryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);
		String sqlQuery = queryProp.getProperty(OIMConstants.GET_CATLOG_DETAILS_OF_APP_BY_APP_NAME);
		
		System.out.println("query before  = " + sqlQuery);
		
		if (sqlQuery.contains("{appName}")) {
			sqlQuery = sqlQuery.replaceAll("\\{appName\\}", appName);
		}

		System.out.println("QUERY AFTER = " + sqlQuery);

		Properties appAttrProp = readPropertiesFile(OIMConstants.OIM_APP_ATTR_MAP_PROP);
		tcDataSet usersDataSet = new tcDataSet(); // store result set of query
		usersDataSet.setQuery(dbProvider, sqlQuery);
		usersDataSet.executeQuery();
		int numRecords = usersDataSet.getTotalRowCount();
		List<HashMap<String, Object>> appList = new ArrayList<HashMap<String, Object>>();
		for (int i = 0; i < numRecords; i++) {
			usersDataSet.goToRow(i);
			HashMap<String, Object> appMap = new HashMap<String, Object>();
			for (String appKeys : appAttrProp.stringPropertyNames()) {
				if (usersDataSet.hasColumn(appKeys)) {
					if (!appAttrProp.getProperty(appKeys).contains(".")) {
						appMap.put(appAttrProp.getProperty(appKeys), usersDataSet.getString(appKeys));
					}
				}
			}
			appList.add(appMap);
		}

		return appList;

	}
	

	/**
	 * 
	 * @param authUserName
	 * @param loginDateAndTime
	 * @param cookies
	 * @param tenantId
	 * @param entityId
	 * @return
	 * @throws tcDataSetException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws IOException
	 * @throws LoginException
	 * @throws MyAuthException
	 */
	public List<HashMap<String, Object>> getAllApplicationsOfUser(String authUserName, String loginDateAndTime,
			String cookies, String tenantId, String entityId)  throws Exception{

		System.out.println("OIMApplicationServices - getAllApplicationsOfUser - entityId - " + entityId);
		
		List<HashMap<String, Object>> applicationsListOfUser = new ArrayList<>();
		
		Properties configProp = readPropertiesFile("config.properties");
		String adminUserName = "xelsysadm";
		String adminPassword = configProp.getProperty(adminUserName);
		
		OIMClient adminOIMClient = null;
		
		try {
		
			adminOIMClient = getConnection(adminUserName, adminPassword);
			ProvisioningService provService = adminOIMClient.getService (ProvisioningService.class);
			List<Account> userAccounts = provService.getAccountsProvisionedToUser(entityId);
			
			for (Account acct : userAccounts) {
				ApplicationInstance appInstance = acct.getAppInstance();
				
				HashMap<String, Object> entityMap = new HashMap<>();
				
				entityMap.put("applicationName", appInstance.getApplicationInstanceName());
				entityMap.put("accountId", acct.getAccountID());
				entityMap.put("accountType", acct.getAccountType());
				entityMap.put("accountDescription", acct.getAccountDescriptiveField());
				entityMap.put("accountStatus", acct.getAccountStatus());
				entityMap.put("provisionedOnDate", acct.getProvisionedOnDate());
				applicationsListOfUser.add(entityMap);
			}		
		} catch (UserNotFoundException | GenericProvisioningException e) {
			// remove printStackTrace and add logger
			e.printStackTrace();
			throw e;
		} catch (LoginException e) {
			// remove printStackTrace and add logger
			e.printStackTrace();
			throw e;
		}finally {
			if(adminOIMClient != null) {
				disconnect(adminOIMClient);
			}
		}
		
		
		return applicationsListOfUser;
	}
	
	
	/**
	 * 
	 * @param entityId
	 * @param accountId
	 * @param accountName
	 * @param applicationName
	 * @return
	 * @throws Exception
	 */
	public String revokeAccountFromUser(String entityId, String accountId, String accountDescription, String applicationName) throws Exception {
		System.out.println("OIMApplicationServices - revokeAccountFromUser - entityId - " + entityId);
		System.out.println("OIMApplicationServices - revokeAccountFromUser - accountDescription - " + accountDescription);
		System.out.println("OIMApplicationServices - revokeAccountFromUser - accountId - " + accountId);
		System.out.println("OIMApplicationServices - revokeAccountFromUser - applicationName - " + applicationName);
		
		Properties configProp = readPropertiesFile("config.properties");
		String adminUserName = "xelsysadm";
		String adminPassword = configProp.getProperty(adminUserName);
		
		OIMClient adminOIMClient = null;
		
		try {
			adminOIMClient = getConnection(adminUserName, adminPassword);
			ProvisioningService provService = adminOIMClient.getService (ProvisioningService.class);
			List<Account> userAccounts = provService.getAccountsProvisionedToUser(entityId);
			
			for (Account acct : userAccounts) {
				if(accountId.equalsIgnoreCase(acct.getAccountID())) {
					System.out.println("OIMApplicationServices - revokeAccountFromUser - revoking accountId - " + accountId);
					provService.revoke(Long.parseLong(acct.getAccountID()));
					System.out.println("OIMApplicationServices - revokeAccountFromUser - revoked accountId - " + accountId);
					break;
				}
			}		
		} catch (UserNotFoundException | GenericProvisioningException e) {
			// remove printStackTrace and add logger
			e.printStackTrace();
			throw e;
		} catch (LoginException e) {
			// remove printStackTrace and add logger
			e.printStackTrace();
			throw e;
		}finally {
			if(adminOIMClient != null) {
				disconnect(adminOIMClient);
			}
		}

		return OIMConstants.SUCCESS;
	}
	
}
