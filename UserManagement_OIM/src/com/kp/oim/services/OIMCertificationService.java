package com.kp.oim.services;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.stream.Collectors;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.security.auth.login.LoginException;

import com.kp.oim.MyAuthException;
import com.kp.oim.constants.OIMConstants;
import com.kp.oim.constants.OIMEntityConstants;
import com.kp.oim.pojo.CertificationActionRequest;
import com.kp.oim.pojo.CertificationActionResponse;
import com.kp.oim.pojo.Field;
import com.kp.oim.pojo.Login;
import com.thortech.xl.client.dataobj.tcDataBaseClient;
import com.thortech.xl.dataaccess.tcDataProvider;
import com.thortech.xl.dataaccess.tcDataSet;

import Thor.API.Security.XLClientSecurityAssociation;
import oracle.iam.certification.api.CertificationService;
import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.SearchKeyNotUniqueException;
import oracle.iam.identity.exception.UserManagerException;
import oracle.iam.platform.OIMClient;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.selfservice.exception.AuthSelfServiceException;

public class OIMCertificationService extends OIMConnections {

	public List<CertificationActionResponse> updateCertificationAction(String authUserName, String loginDateAndTime,
			String cookies, String tenantId, long certificationId, String taskId, long lineItemId,
			List<CertificationActionRequest> request) throws InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, LoginException,
			oracle.iam.selfservice.exception.ValidationFailedException, AccessDeniedException, AuthSelfServiceException,
			NoSuchUserException, SearchKeyNotUniqueException, MyAuthException {

		String userLogin = authUserName;
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
		OIMClient oimClient = getConnection(login.getUserName(),
		login.getPassword());
		
		CertificationService cert = (CertificationService) oimClient.getService(CertificationService.class);

		List<CertificationActionResponse> certActionResponse = new ArrayList<>();

		if (null == request || request.isEmpty())
			return certActionResponse;

		Map<Integer, List<CertificationActionRequest>> entityMap = populateMap(request);
		System.out.println("Entity Map = " + entityMap);

		for (Map.Entry<Integer, List<CertificationActionRequest>> entry : entityMap.entrySet()) {

			System.out.println("Key = " + entry.getKey());
			System.out.println("value = " + entry.getValue());

			if (entry.getKey() == -1) // Invalid option
				continue;

			if (entry.getKey() == 0 || entry.getKey() == 3) { // Comment is mandatory for revoke and certify
																// conditionally

				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
				formatter.setTimeZone(TimeZone.getTimeZone("CST"));

				for (CertificationActionRequest entity : entry.getValue()) {

					if (entry.getKey() == 0 && (null == entity.getComment() || entity.getComment().trim().isEmpty())) {
						System.out.println("Ignoring entity in if: " + entity);

						CertificationActionResponse resp = new CertificationActionResponse();
						resp.setEntityId(entity.getEntityId());
						resp.setEntityType(entity.getEntityType());
						resp.setStatus("FAILED");
						certActionResponse.add(resp);

						continue;

					} else if (entry.getKey() == 3 && null == entity.getEndDate()) {
						System.out.println("Ignoring entity in if: " + entity);

						CertificationActionResponse resp = new CertificationActionResponse();
						resp.setEntityId(entity.getEntityId());
						resp.setEntityType(entity.getEntityType());
						resp.setStatus("FAILED");
						certActionResponse.add(resp);

						continue;
					}

					if (OIMEntityConstants.ENTITY_ROLE.equalsIgnoreCase(entity.getEntityType())) {

						System.out.println("Updating roles in if");

						try {
							cert.certifyUserEntitlements(certificationId, taskId, lineItemId,
									new HashSet<Long>(Arrays.asList(entity.getEntityId())), null, null, entry.getKey(),
									(null != entity.getEndDate()) ? formatter.parse(entity.getEndDate()) : null,
									entity.getComment());

							CertificationActionResponse resp = new CertificationActionResponse();
							resp.setEntityId(entity.getEntityId());
							resp.setEntityType(entity.getEntityType());
							resp.setStatus("SUCCESS");
							certActionResponse.add(resp);

						} catch (Exception e) {
							System.out.println("Exception in if= " + e);

							CertificationActionResponse resp = new CertificationActionResponse();
							resp.setEntityId(entity.getEntityId());
							resp.setEntityType(entity.getEntityType());
							resp.setStatus("FAILED");
							certActionResponse.add(resp);
						}

					} else if (OIMEntityConstants.ENTITY_ACCOUNT.equalsIgnoreCase(entity.getEntityType())) {

						System.out.println("Updating account in if");
						try {
							cert.certifyUserEntitlements(certificationId, taskId, lineItemId, null,
									new HashSet<Long>(Arrays.asList(entity.getEntityId())), null, entry.getKey(),
									(null != entity.getEndDate()) ? formatter.parse(entity.getEndDate()) : null,
									entity.getComment());

							CertificationActionResponse resp = new CertificationActionResponse();
							resp.setEntityId(entity.getEntityId());
							resp.setEntityType(entity.getEntityType());
							resp.setStatus("SUCCESS");
							certActionResponse.add(resp);

						} catch (Exception e) {
							System.out.println("Exception in if = " + e);

							CertificationActionResponse resp = new CertificationActionResponse();
							resp.setEntityId(entity.getEntityId());
							resp.setEntityType(entity.getEntityType());
							resp.setStatus("FAILED");
							certActionResponse.add(resp);
						}

					} else if (OIMEntityConstants.ENTITY_ENTITLEMENT.equalsIgnoreCase(entity.getEntityType())) {

						System.out.println("Updating entitlement in if");
						try {
							cert.certifyUserEntitlements(certificationId, taskId, lineItemId, null, null,
									new HashSet<Long>(Arrays.asList(entity.getEntityId())), entry.getKey(),
									(null != entity.getEndDate()) ? formatter.parse(entity.getEndDate()) : null,
									entity.getComment());

							CertificationActionResponse resp = new CertificationActionResponse();
							resp.setEntityId(entity.getEntityId());
							resp.setEntityType(entity.getEntityType());
							resp.setStatus("SUCCESS");
							certActionResponse.add(resp);

						} catch (Exception e) {
							System.out.println("Exception in if = " + e);

							CertificationActionResponse resp = new CertificationActionResponse();
							resp.setEntityId(entity.getEntityId());
							resp.setEntityType(entity.getEntityType());
							resp.setStatus("FAILED");
							certActionResponse.add(resp);
						}
					}

				}

			} else {

				Set<Long> roleEntityIds = entry.getValue().stream().filter(Objects::nonNull)
						.filter(entity -> OIMEntityConstants.ENTITY_ROLE.equalsIgnoreCase(entity.getEntityType()))
						.map(entity -> entity.getEntityId()).filter(entityId -> entityId != -1)
						.collect(Collectors.toSet());

				Set<Long> accountEntityIds = entry.getValue().stream().filter(Objects::nonNull)
						.filter(entity -> OIMEntityConstants.ENTITY_ACCOUNT.equalsIgnoreCase(entity.getEntityType()))
						.map(entity -> entity.getEntityId()).filter(entityId -> entityId != -1)
						.collect(Collectors.toSet());

				Set<Long> accountAttributeEntityIds = entry.getValue().stream().filter(Objects::nonNull)
						.filter(entity -> OIMEntityConstants.ENTITY_ENTITLEMENT
								.equalsIgnoreCase(entity.getEntityType()))
						.map(entity -> entity.getEntityId()).filter(entityId -> entityId != -1)
						.collect(Collectors.toSet());

				System.out.println("roleEntityIds = " + roleEntityIds + " :: accountEntityIds = " + accountEntityIds
						+ " :: accountAttributeEntityIds = " + accountAttributeEntityIds);

				if (null != roleEntityIds && !roleEntityIds.isEmpty()) {
					System.out.println("Updating roles list in else");
					try {
						cert.certifyUserEntitlements(certificationId, taskId, lineItemId, roleEntityIds, null, null,
								(4 == entry.getKey()) ? null : entry.getKey(), null, null);

						roleEntityIds.forEach(entity -> {
							CertificationActionResponse resp = new CertificationActionResponse();
							resp.setEntityId(entity);
							resp.setEntityType(OIMEntityConstants.ENTITY_ROLE);
							resp.setStatus("SUCCESS");
							certActionResponse.add(resp);
						});

					} catch (Exception e) {
						System.out.println("Exception in else = " + e);

						roleEntityIds.forEach(entity -> {
							CertificationActionResponse resp = new CertificationActionResponse();
							resp.setEntityId(entity);
							resp.setEntityType(OIMEntityConstants.ENTITY_ROLE);
							resp.setStatus("FAILED");
							certActionResponse.add(resp);
						});
					}
				}

				if (null != accountAttributeEntityIds && !accountAttributeEntityIds.isEmpty()) {
					System.out.println("Updating entitlements in else");
					try {
						cert.certifyUserEntitlements(certificationId, taskId, lineItemId, null, null,
								accountAttributeEntityIds, (4 == entry.getKey()) ? null : entry.getKey(), null, null);

						roleEntityIds.forEach(entity -> {
							CertificationActionResponse resp = new CertificationActionResponse();
							resp.setEntityId(entity);
							resp.setEntityType(OIMEntityConstants.ENTITY_ENTITLEMENT);
							resp.setStatus("SUCCESS");
							certActionResponse.add(resp);
						});

					} catch (Exception e) {
						System.out.println("Exception in else = " + e);

						roleEntityIds.forEach(entity -> {
							CertificationActionResponse resp = new CertificationActionResponse();
							resp.setEntityId(entity);
							resp.setEntityType(OIMEntityConstants.ENTITY_ENTITLEMENT);
							resp.setStatus("FAILED");
							certActionResponse.add(resp);
						});
					}
				}

				if (null != accountEntityIds && !accountEntityIds.isEmpty()) {
					System.out.println("Updating accounts in else");
					try {
						cert.certifyUserEntitlements(certificationId, taskId, lineItemId, null, accountEntityIds, null,
								(4 == entry.getKey()) ? null : entry.getKey(), null, null);

						roleEntityIds.forEach(entity -> {
							CertificationActionResponse resp = new CertificationActionResponse();
							resp.setEntityId(entity);
							resp.setEntityType(OIMEntityConstants.ENTITY_ACCOUNT);
							resp.setStatus("SUCCESS");
							certActionResponse.add(resp);
						});
					} catch (Exception e) {
						System.out.println("Exception in else= " + e);

						roleEntityIds.forEach(entity -> {
							CertificationActionResponse resp = new CertificationActionResponse();
							resp.setEntityId(entity);
							resp.setEntityType(OIMEntityConstants.ENTITY_ACCOUNT);
							resp.setStatus("FAILED");
							certActionResponse.add(resp);
						});
					}
				}

			}
		}
		
		return certActionResponse;

	}

	private Map<Integer, List<CertificationActionRequest>> populateMap(List<CertificationActionRequest> request) {

		Map<Integer, List<CertificationActionRequest>> entityMap = new HashMap<>();

		java.util.function.Predicate<CertificationActionRequest> isValidEntity = entity -> entity.getEntityId() != 0
				&& null != entity.getEntityType() && null != entity.getAction();

		// Find the valid entities and populate the map
		request.stream().filter(Objects::nonNull).filter(isValidEntity::test).forEach(entity -> {
			if (null != entity.getFields()) {
				Field commentField = entity
						.getFields().stream().filter(field -> field.getName().equalsIgnoreCase("comment")
								&& null != field.getValue() && !field.getValue().trim().isEmpty())
						.findFirst().orElse(null);
				entity.setComment(null != commentField ? commentField.getValue() : null);

				Field endDateField = entity
						.getFields().stream().filter(field -> field.getName().equalsIgnoreCase("endDate")
								&& null != field.getValue() && !field.getValue().trim().isEmpty())
						.findFirst().orElse(null);
				entity.setEndDate(null != endDateField ? endDateField.getValue() : null);
			}

			entityMap.computeIfAbsent(getActionCodeForValue(entity.getAction()), k -> new ArrayList<>()).add(entity);
		});

		return entityMap;
	}

	private Integer getActionCodeForValue(String actionValue) {

		if (null == actionValue || actionValue.trim().isEmpty())
			return -1;

		Integer actionCode;
		switch (actionValue.toLowerCase()) {
		case "revoke":
			actionCode = 0;
			break;
		case "certify":
			actionCode = 1;
			break;
		case "abstain":
			actionCode = 2;
			break;
		case "certifyconditionally":
			actionCode = 3;
			break;
		case "resetstatus":
			actionCode = 4; // Value to be sent to oim is null
			break;
		default:
			actionCode = -1;
		}

		return actionCode;
	}
	
	public List<HashMap<String, Object>> getSignOffCertifications (String authUserName, String loginDateAndTime, String cookies) 
			throws LoginException, InvalidKeyException, 
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, 
			BadPaddingException, IOException, UserManagerException, AccessDeniedException, MyAuthException{
		
//		Properties queryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);
		
//		String userLogin = authUserName;
//		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
//		OIMClient oimClient = getConnection(login.getUserName(),
//		login.getPassword());
		
		Properties configProp = readPropertiesFile("config.properties");
		String adminUserName = "xelsysadm";
		String adminPassword = configProp.getProperty(adminUserName);
		OIMClient adminOIMClient = getConnection(adminUserName, adminPassword);
		
		tcDataProvider dbProvider = null;
		
		XLClientSecurityAssociation.setClientHandle(adminOIMClient); 
		dbProvider = new tcDataBaseClient();

		String query = OIMConstants.GET_ALL_SIGN_OFF_CERTIFICATION;
		List<HashMap<String, Object>> signOffCertificationsList=new ArrayList<>();	
		

		if(query.contains("{username}") ) {
			query = query.replaceAll("\\{username\\}", authUserName);
		}

		System.out.println("OIMCertificationService.getSignOffCertifications(authUserName)" +authUserName);
		
		System.out.println("OIMCertificationService.getSignOffCertifications(query)" +query);
		
		tcDataSet usersDataSet = new tcDataSet(); //store result set of query
		usersDataSet.setQuery(dbProvider, query); 
		
		try {
		usersDataSet.executeQuery();
		int numRecords = usersDataSet.getTotalRowCount();
		for(int i = 0; i < numRecords; i++){
			usersDataSet.goToRow(i);
			HashMap<String, Object> signOffCertifications = new HashMap<>();
			signOffCertifications.put("name", usersDataSet.getString("TASK_NAME"));
			signOffCertifications.put("id",  usersDataSet.getString("CERT_ID"));
			signOffCertifications.put("taskId",  usersDataSet.getString("TASK_UID"));
			signOffCertifications.put("type",  String.valueOf(usersDataSet.getInt("CERT_TYPE")));

			signOffCertificationsList.add(signOffCertifications);
		}		
		System.out.println("OIMCertificationService.getSignOffCertifications()"
				+signOffCertificationsList);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally
		{
			disconnect(adminOIMClient);
		}
		return signOffCertificationsList;

	}

}
