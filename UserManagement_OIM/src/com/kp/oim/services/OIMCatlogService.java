package com.kp.oim.services;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.security.auth.login.LoginException;

import com.kp.oim.MyAuthException;
import com.kp.oim.pojo.CatlogApplication;
import com.kp.oim.pojo.CatlogDetailsResponse;
import com.kp.oim.pojo.CatlogRole;
import com.kp.oim.pojo.Login;
import com.thortech.xl.dataaccess.tcDataSetException;

import oracle.iam.platform.OIMClient;

public class OIMCatlogService extends OIMConnections{
	
	private OIMRoleServices oimRoleServices;
	private OIMApplicationServices oimApplicationServices;
	
	public OIMCatlogService() {
		oimRoleServices = new OIMRoleServices();
		oimApplicationServices = new OIMApplicationServices();
	}

	public CatlogDetailsResponse getAllCatlogDetails(String authUserName, String loginDateAndTime, String cookies, String tenantId) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, LoginException, tcDataSetException, MyAuthException {
		
		CatlogDetailsResponse allCatlogDetails = new CatlogDetailsResponse();
		
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());
	
		List<HashMap<String, Object>> catlogRoleList = oimRoleServices.getAllCatlogDetailsofRoles(oimClient);
		List<HashMap<String, Object>> catlogApplicationList = oimApplicationServices.getAllCatlogDetailsofApps(oimClient);

		allCatlogDetails.setRoles(catlogRoleList);
		allCatlogDetails.setApplications(catlogApplicationList);

		return allCatlogDetails;
		
	}
	
}
