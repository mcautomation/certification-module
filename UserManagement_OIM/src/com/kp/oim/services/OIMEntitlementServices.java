package com.kp.oim.services;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.security.auth.login.LoginException;

import com.kp.oim.MyAuthException;
import com.kp.oim.constants.OIMConstants;
import com.thortech.xl.dataaccess.tcDataSetException;

import oracle.iam.identity.rolemgmt.api.RoleManager;
import oracle.iam.identity.rolemgmt.vo.Role;
import oracle.iam.platform.OIMClient;
import oracle.iam.provisioning.api.ProvisioningService;
import oracle.iam.provisioning.exception.AccountNotFoundException;
import oracle.iam.provisioning.exception.EntitlementNotProvisionedException;
import oracle.iam.provisioning.exception.GenericProvisioningException;
import oracle.iam.provisioning.exception.UserNotFoundException;
import oracle.iam.provisioning.vo.Entitlement;
import oracle.iam.provisioning.vo.EntitlementInstance;

public class OIMEntitlementServices extends OIMConnections {
	
	
	/**
	 * 
	 * @param authUserName
	 * @param loginDateAndTime
	 * @param cookies
	 * @param tenantId
	 * @param entityId
	 * @return
	 * @throws tcDataSetException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws IOException
	 * @throws LoginException
	 * @throws MyAuthException
	 */
	public List<HashMap<String, Object>> getAllEntitlementsOfUser(String authUserName, String loginDateAndTime,
			String cookies, String tenantId, String entityId) throws Exception {

		System.out.println("OIMApplicationServices - getAllEntitlementsOfUser - entityId- " + entityId);
		
		List<HashMap<String, Object>> entitlementsListForUser = new ArrayList<>();
		
		Properties configProp = readPropertiesFile("config.properties");
		String adminUserName = "xelsysadm";
		String adminPassword = configProp.getProperty(adminUserName);
		
		OIMClient adminOIMClient = getConnection(adminUserName, adminPassword);
		
		try {
			
			ProvisioningService provService = adminOIMClient.getService (ProvisioningService.class);
			List<EntitlementInstance> entitlementsList = provService.getEntitlementsForUser(entityId);
			for (EntitlementInstance entitlementInstance : entitlementsList) {
				Entitlement entitlements = entitlementInstance.getEntitlement();
				
				HashMap<String, Object> entitlementMap = new HashMap<>();
				
				entitlementMap.put("entitlementCode", entitlements.getEntitlementCode());
				entitlementMap.put("entitlementValue", entitlements.getEntitlementValue());
				entitlementMap.put("entitlementKey", entitlements.getEntitlementKey());
				entitlementMap.put("displayName", entitlements.getDisplayName());
				entitlementMap.put("description", entitlements.getDescription());
				entitlementMap.put("type", "Entitlement");
				entitlementMap.put("applicationName", entitlements.getAppInstance().getApplicationInstanceName());
				
				entitlementsListForUser.add(entitlementMap);
			}		
			
			RoleManager selfServiceManager = (RoleManager) adminOIMClient.getService(RoleManager.class);
			List<Role> roles = selfServiceManager.getUserMemberships(entityId, true);
			
			for (Role role : roles) {
				HashMap<String, Object> roleMap = new HashMap<>();
				
				roleMap.put("entitlementCode", role.getAttributes().get("Role Name"));
				roleMap.put("entitlementValue", "");
				roleMap.put("entitlementKey", role.getAttributes().get("Role Key"));
				roleMap.put("displayName", role.getAttributes().get("Role Name"));
				roleMap.put("description", role.getAttributes().get("Role Description"));
				roleMap.put("type", "Role");
				roleMap.put("applicationName", "NA");
				
				entitlementsListForUser.add(roleMap);
			}
			
		} catch (UserNotFoundException | GenericProvisioningException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}finally {
			disconnect(adminOIMClient);
		}
		
		
		return entitlementsListForUser;
	}

	

	/**
	 * 
	 * @param entityId
	 * @param entitlementName
	 * @param entitlementCode
	 * @return
	 * @throws LoginException 
	 */
	public String revokeEntitlementFromUser(String entityId, String entitlementName, String entitlementCode, String type) throws Exception {
		System.out.println("OIMApplicationServices - revokeEntitlementFromUser - entityId - " + entityId);
		System.out.println("OIMApplicationServices - revokeEntitlementFromUser - entitlementName - " + entitlementName);
		System.out.println("OIMApplicationServices - revokeEntitlementFromUser - type - " + type);
		
		Properties configProp = readPropertiesFile("config.properties");
		String adminUserName = "xelsysadm";
		String adminPassword = configProp.getProperty(adminUserName);
		
		OIMClient adminOIMClient = getConnection(adminUserName, adminPassword);
		
		try {
			if("Entitlement".equalsIgnoreCase(type)) {
				ProvisioningService provService = adminOIMClient.getService (ProvisioningService.class);
				List<EntitlementInstance> entitlementsList = provService.getEntitlementsForUser(entityId);
				
				for (EntitlementInstance entitlementInstance : entitlementsList) {
	        		if (entitlementCode.equalsIgnoreCase(entitlementInstance.getEntitlement().getEntitlementCode())) {
	        			System.out.println("OIMApplicationServices - revokeEntitlementFromUser - Revoking entitlement - " + entitlementName);
	        			try { 
	        				provService.revokeEntitlement(entitlementInstance);
	        				System.out.println("OIMApplicationServices - revokeEntitlementFromUser - Revoked entitlement - " + entitlementName);
	        				break;
	                    } catch (oracle.iam.platform.authopss.exception.AccessDeniedException | AccountNotFoundException
	        					| EntitlementNotProvisionedException | GenericProvisioningException e) {
	        				e.printStackTrace();
	        				
	        				throw e;
	        			}
	        		}
				}		
			} else {
				RoleManager roleManager = (RoleManager) adminOIMClient.getService(RoleManager.class);
				List<Role> roles = roleManager.getUserMemberships(entityId, true);
				
				for (Role role : roles) {
					String roleName = (String)role.getAttributes().get("Role Name");
					String roleKey = (String)role.getAttributes().get("Role Key");
					if (roleName.equalsIgnoreCase(entitlementCode)) {
	        			System.out.println("OIMApplicationServices :: revokeEntitlementFromUser :: Revoking Role: " + roleName);
	        			try { 
	        				Set <String> roleKeySet = new HashSet<String>(); 
	        				roleKeySet.add(roleKey);
	        				roleManager.revokeRoleGrants(entityId, roleKeySet);
	        				System.out.println("OIMApplicationServices :: revokeEntitlementFromUser :: Revoked Role: " + roleName);
	        				break;
	                    } catch (oracle.iam.platform.authopss.exception.AccessDeniedException e) {
	        				e.printStackTrace();
	        				
	        				throw e;
	        			}
	        		}
				}
			}
		} catch (UserNotFoundException | GenericProvisioningException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}finally {
			disconnect(adminOIMClient);
		}
		
		
		return OIMConstants.SUCCESS;
		
	}

}
