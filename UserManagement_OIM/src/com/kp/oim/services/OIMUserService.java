package com.kp.oim.services;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.security.auth.login.LoginException;

import com.kp.oim.MyAuthException;
import com.kp.oim.constants.OIMConstants;
import com.kp.oim.constants.OIMEntityConstants;
import com.kp.oim.pojo.Login;
import com.kp.oim.pojo.UserInfo;
import com.thortech.xl.client.dataobj.tcDataBaseClient;
import com.thortech.xl.dataaccess.tcClientDataAccessException;
import com.thortech.xl.dataaccess.tcDataProvider;
import com.thortech.xl.dataaccess.tcDataSet;
import com.thortech.xl.dataaccess.tcDataSetException;
import com.thortech.xl.orb.dataaccess.tcDataAccessException;

import Thor.API.Security.XLClientSecurityAssociation;
import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.SearchKeyNotUniqueException;
import oracle.iam.identity.exception.UserAlreadyExistsException;
import oracle.iam.identity.exception.UserCreateException;
import oracle.iam.identity.exception.UserDeleteException;
import oracle.iam.identity.exception.UserDisableException;
import oracle.iam.identity.exception.UserEnableException;
import oracle.iam.identity.exception.UserLockException;
import oracle.iam.identity.exception.UserManagerException;
import oracle.iam.identity.exception.UserModifyException;
import oracle.iam.identity.exception.UserUnlockException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.api.UserManagerConstants;
import oracle.iam.identity.usermgmt.vo.Proxy;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.identity.usermgmt.vo.UserManagerResult;
import oracle.iam.platform.OIMClient;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.platform.entitymgr.spi.entity.Searchable;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.platform.entitymgr.vo.SearchCriteria.Operator;
import oracle.iam.selfservice.exception.AuthSelfServiceException;
import oracle.iam.selfservice.exception.ChangePasswordException;
import oracle.iam.selfservice.exception.SetChallengeValueException;
import oracle.iam.selfservice.exception.UserAccountDisabledException;
import oracle.iam.selfservice.exception.UserAccountInvalidException;
import oracle.iam.selfservice.exception.UserLookupException;
import oracle.iam.selfservice.self.selfmgmt.api.AuthenticatedSelfService;

import oracle.security.jps.JpsException;
import oracle.security.jps.service.JpsServiceLocator;
import oracle.security.jps.service.credstore.Credential;
import oracle.security.jps.service.credstore.CredentialMap;
import oracle.security.jps.service.credstore.CredentialStore;
import oracle.security.jps.service.credstore.PasswordCredential;


public class OIMUserService extends OIMConnections{

	/*
	 * Authentication operations
	 * */

	public Login login(String loginID, String password) throws LoginException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException {


		System.out.println("OIMService.login()");

		OIMClient oimClient = getConnection(loginID, password);

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(OIMConstants.DateFormatPattern);
		String loginDateAndTime = simpleDateFormat.format(new Date());
		System.out.println("LOGIN DATE AND TIME= "+loginDateAndTime);
		String credentials = loginID+"#k:p#"+password+"#k:p#"+loginDateAndTime;
		Properties prop = readPropertiesFile("credentialstore.properties");
		String key = prop.getProperty("OIM_ENCRYPT_KEY");
		String encryptedCredentials = encrypt(credentials, key.getBytes());

		byte[] encodedCredentialsInByte = Base64.getEncoder().encode(encryptedCredentials.getBytes());
		String encodedCredentials = new String(encodedCredentialsInByte);

		// here to be logic of credential store

		Login login = new Login();
		login.setUserName(loginID);
		login.setCookies(encodedCredentials);
		login.setLoginDateAndTime(loginDateAndTime);

		return login;

	}

	/*
	 * User operations
	 * */

	public HashMap<String, Object> getUserByID(OIMClient oimClient, String userLogin, String tenantId) throws tcDataSetException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, LoginException, IOException, tcDataAccessException, tcClientDataAccessException{
		HashMap<String, Object> userDetails = new HashMap<>();

		tcDataProvider dbProvider = null;

		//Establish connection to OIM Schema through the OIMClient
		XLClientSecurityAssociation.setClientHandle(oimClient); 
		dbProvider = new tcDataBaseClient();

		Properties queryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);
		String query = queryProp.getProperty(OIMConstants.GET_USER_BY_ID);

		if(query.contains("{userLogin}")) {
			query = query.replaceAll("\\{userLogin\\}", userLogin);
		}

		System.out.println("QUERY = "+ query);

		tcDataSet usersDataSet = new tcDataSet(); //store result set of query
		usersDataSet.setQuery(dbProvider, query); 
		usersDataSet.executeQuery();

		int numRecords = usersDataSet.getTotalRowCount();
		for(int i = 0; i < numRecords; i++)
		{
			usersDataSet.goToRow(i);

			userDetails = getUserEntityData(usersDataSet);
			userDetails.put("TenantID", tenantId);

			/*List<EntityAppInstance> entityAppInstatince = getEntityAppInstance(dbProvider, userLogin);
			userDetails.setEntityAppInstance(entityAppInstatince);
			userDetails.setEntityRole(getAllRolesOfUser(userLogin, oimClient));*/

			List<HashMap<String, Object>> entityAppInstatince = getEntityAppInstance(dbProvider, userLogin);
			userDetails.put("Application",entityAppInstatince);
			userDetails.put("Role",getAllRolesOfUser(userLogin, oimClient));

		}

		dbProvider.close();

		return userDetails;
	}

	public HashMap<String, Object> getUserByID(String authUserName, String loginDateAndTime, String cookies, String userLogin, String tenantId) throws InvalidKeyException, 
	NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
	BadPaddingException, IOException, LoginException, tcDataSetException, tcDataAccessException, tcClientDataAccessException, MyAuthException {

		HashMap<String, Object> userDetails = new HashMap<>();

		tcDataProvider dbProvider = null;

		Login login = getCredentials(authUserName, loginDateAndTime, cookies);

		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		//perform the operation of oimClient
		//		User user = oimClient.getService(User.class);

		//Establish connection to OIM Schema through the OIMClient
		XLClientSecurityAssociation.setClientHandle(oimClient); 
		dbProvider = new tcDataBaseClient();

		//Property read for Query
		Properties queryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);

		/*String query = "select a.usr_key, a.usr_first_name,a.usr_middle_name, a.usr_last_name, a.usr_login, a.usr_display_name, a.usr_email, a.usr_status, a.usr_type,a.usr_disabled, a.usr_mobile, a.usr_state, a.usr_street, a.usr_telephone_number, a.usr_emp_no,a.usr_emp_type, a.usr_locked, a.usr_locale, a.usr_start_date, a.usr_end_date, a.usr_country, a.usr_location, a.USR_LAST_SUCCESFUL_LOGIN_DATE, a.USR_DEPT_NO, a.USR_CREATE, a.USR_UPDATE, a.USR_UPDATEBY,\r\n" + 
				"b.usr_login usr_Manager  from dev_oim.usr a left join dev_oim.usr b on a.usr_manager_key=b.usr_key where a.usr_login=\'"+userLogin+"\' AND a.usr_status != 'Deleted'";
		 */
		String query = queryProp.getProperty(OIMConstants.GET_USER_BY_ID);

		if(query.contains("{userLogin}")) {

			query = query.replaceAll("\\{userLogin\\}", userLogin);

		}

		System.out.println("QUERY = "+ query);

		tcDataSet usersDataSet = new tcDataSet(); //store result set of query
		usersDataSet.setQuery(dbProvider, query); 
		usersDataSet.executeQuery();

		int numRecords = usersDataSet.getTotalRowCount();
		for(int i = 0; i < numRecords; i++)
		{
			usersDataSet.goToRow(i);

			userDetails = getUserEntityData(usersDataSet);
			userDetails.put("TenantID", tenantId);
			/*List<EntityAppInstance> entityAppInstatince = getEntityAppInstance(dbProvider, userLogin);
			userDetails.setEntityAppInstance(entityAppInstatince);
			userDetails.setEntityRole(getAllRolesOfUser(userLogin, oimClient));*/

			List<HashMap<String, Object>> entityAppInstatince = getEntityAppInstance(dbProvider, userLogin);
			userDetails.put(OIMEntityConstants.Application.APPLICATION,entityAppInstatince);
			userDetails.put(OIMEntityConstants.Role.ROLE, getAllRolesOfUser(userLogin, oimClient));

		}
		/*int numColumns = usersDataSet.getColumnCount();
		int numRecords = usersDataSet.getTotalRowCount();

		//iterate through each record
		for(int i = 0; i < numRecords; i++)
		{
			usersDataSet.goToRow(i);

			//iterate through each column of a record
			for(int j = 0; j < numColumns; j++)
			{
				String columnName = usersDataSet.getColumnName(j);
				String value = usersDataSet.getString(columnName);
				System.out.println(columnName + " = " + value);
				obj.put(columnName, value);
			}

			System.out.println();
		}*/

		dbProvider.close();
		usersDataSet.cloneRow();
		disconnect(oimClient);

		return userDetails;

	}


	private HashMap<String, Object> getUserEntityData(tcDataSet usersDataSet) throws tcDataSetException{

		String entity = "user";
		String userProfile = "userProfile";

		HashMap<String, Object> userDetails = new HashMap<>();
		HashMap<String, Object> userProfileInfo = new HashMap<>();
		Properties oimUserAttrMappingProp = readPropertiesFile(OIMConstants.OIM_USER_ATTR_MAP_PROP);

		for(String key : oimUserAttrMappingProp.stringPropertyNames()) {
			if(usersDataSet.hasColumn(key)) {
				String splitedKey[] = oimUserAttrMappingProp.getProperty(key).split("\\.");

				if(splitedKey.length == 1) {
					userDetails.put(splitedKey[0], usersDataSet.getString(key));
				}else if(splitedKey.length == 2 && splitedKey[0].equalsIgnoreCase(userProfile)) {
					userProfileInfo.put(splitedKey[1], usersDataSet.getString(key));
				}

			}
		}

		userDetails.put("userProfile", userProfileInfo);
		return userDetails;

	}

	public List<HashMap<String, Object>> getUsers(String authUserName, String loginDateAndTime, String cookies, String tenantId) throws InvalidKeyException, 
	NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
	BadPaddingException, IOException, LoginException, tcDataSetException, MyAuthException {

		List<HashMap<String, Object>> userDetailsList = new ArrayList<>();

		tcDataProvider dbProvider = null;

		Login login = getCredentials(authUserName, loginDateAndTime, cookies);

		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		//Establish connection to OIM Schema through the OIMClient
		XLClientSecurityAssociation.setClientHandle(oimClient); 
		dbProvider = new tcDataBaseClient();

		/*String query ="select a.usr_key, a.usr_first_name,a.usr_middle_name, a.usr_last_name, a.usr_login, a.usr_display_name, a.usr_email, a.usr_status, a.usr_type,a.usr_disabled, a.usr_mobile, a.usr_state, a.usr_street, a.usr_telephone_number, a.usr_emp_no,a.usr_emp_type, a.usr_locked, a.usr_locale, a.usr_start_date, a.usr_end_date, a.usr_country, a.usr_location, a.USR_LAST_SUCCESFUL_LOGIN_DATE, a.USR_DEPT_NO, a.USR_CREATE, a.USR_UPDATE, a.USR_UPDATEBY,\r\n" + 
				"b.usr_login usr_Manager  from dev_oim.usr a left join dev_oim.usr b on a.usr_manager_key=b.usr_key where a.usr_status != 'Deleted'";
		 */
		Properties queryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);
		String query = queryProp.getProperty(OIMConstants.GET_ALL_USERS);
		tcDataSet usersDataSet = new tcDataSet(); //store result set of query
		usersDataSet.setQuery(dbProvider, query); 
		usersDataSet.executeQuery();

		int numRecords = usersDataSet.getTotalRowCount();

		for(int i = 0; i < numRecords; i++){

			usersDataSet.goToRow(i);

			HashMap<String, Object> userDetails = null;
			String userLogin = usersDataSet.hasColumn("USER_LOGIN") ? usersDataSet.getString("USER_LOGIN") : null;
			if(userLogin != null) {
				userDetails = getUserEntityData(usersDataSet);
				userDetails.put("TenenatID", tenantId);
				List<HashMap<String, Object>> entityAppInstatince = getEntityAppInstance(dbProvider, userLogin);
				userDetails.put("Application",entityAppInstatince);
				userDetails.put("Role",getAllRolesOfUser(userLogin, oimClient));
				userDetailsList.add(userDetails);
			}else {
				// remeainign
				System.err.println("Please alias data mapp with Query -- USERLOGIN is not found ");
			}

		}

		disconnect(oimClient);

		return userDetailsList;

	}

	private HashMap<String, Object> getUserRequestDetails(UserInfo userInfo){

		HashMap<String, Object> userAttributes = new HashMap<>();

		if(userInfo.getUserName() != null) {
			userAttributes.put("User Login", userInfo.getUserName());

			if(userInfo.getFirstName() != null) {
				userAttributes.put("First Name", userInfo.getFirstName());
			}
			if(userInfo.getLastName() != null) {
				userAttributes.put("Last Name", userInfo.getLastName());
			}
			if(userInfo.getMiddleName() != null) {
				userAttributes.put("Middle Name", userInfo.getMiddleName());
			}
			if(userInfo.getPassword() != null) {
				userAttributes.put("usr_password", userInfo.getPassword());
			}
			if(userInfo.getRole() != null) {
				userAttributes.put("Role", userInfo.getRole());
			}
			if(userInfo.getActKey() != null) {
				userAttributes.put("act_key", Long.parseLong(userInfo.getActKey()));
			}
			if(userInfo.getEmail() != null) {
				userAttributes.put("Email", userInfo.getEmail());
			}
			if(userInfo.getCountry() != null) {
				userAttributes.put("Country", userInfo.getCountry());
			}
			if(userInfo.getDepartmentNo() != null) {
				userAttributes.put("Department Number", userInfo.getDepartmentNo());
			}
			if(userInfo.getHomePostalAddress() != null) {
				userAttributes.put("Home Postal Address", userInfo.getHomePostalAddress());
			}
			if(userInfo.getPostalAddress()!= null) {
				userAttributes.put("Postal Address", userInfo.getPostalAddress());
			}
			if(userInfo.getTitle() != null) {
				userAttributes.put("Title", userInfo.getTitle());
			}
			if(userInfo.getState() != null) {
				userAttributes.put("State", userInfo.getState());
			}
			if(userInfo.getManager() != null) {
				userAttributes.put("Manager key", userInfo.getManager());
			}
		}else {

			System.out.println("Please fill mandatory details");
		}

		return userAttributes;
	}

	public HashMap<String, Object> createUser(String authUserName, String loginDateAndTime, String cookies, UserInfo userInfo, String tenantId) throws InvalidKeyException, 
	NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
	BadPaddingException, IOException, LoginException, tcDataSetException, ValidationFailedException, UserAlreadyExistsException, UserCreateException, AccessDeniedException, tcDataAccessException, tcClientDataAccessException, MyAuthException {

		HashMap<String, Object> userAttributes = getUserRequestDetails(userInfo);

		Login login = getCredentials(authUserName, loginDateAndTime, cookies);

		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		UserManager userManager = oimClient.getService(UserManager.class);

		User user = new User(userInfo.getUserName(), userAttributes);

		UserManagerResult result = userManager.create(user);

		String userLogin = userInfo.getUserName();

		HashMap<String, Object> userDetails = getUserByID(oimClient, userLogin, tenantId);

		disconnect(oimClient);

		return userDetails;

	}

	private UserInfo getUserProfileDetails(OIMClient oimClient, String userLogin) throws tcDataSetException {

		XLClientSecurityAssociation.setClientHandle(oimClient); 
		tcDataProvider dbProvider = new tcDataBaseClient();

		String query ="select * from dev_oim.usr where usr_login="+"\'"+userLogin+"\' AND usr_status != 'Deleted'"; //Query all OIM users
		tcDataSet usersDataSet = new tcDataSet(); //store result set of query
		usersDataSet.setQuery(dbProvider, query); 
		usersDataSet.executeQuery();

		UserInfo userInfo = new UserInfo();

		/*userInfo.setUserName(usersDataSet.getString("USR_LOGIN"));
		userInfo.setUserId(usersDataSet.getString("USR_KEY"));
		userInfo.setPhoneNumber(usersDataSet.getString("USR_MOBILE"));
		userInfo.setManager(usersDataSet.getString("USR_MANAGER"));
		userInfo.setFirstName(usersDataSet.getString("USR_FIRST_NAME"));
		userInfo.setMiddleName(usersDataSet.getString("USR_MIDDLE_NAME"));
		userInfo.setLastName(usersDataSet.getString("USR_LAST_NAME"));
		userInfo.setLastLogin(usersDataSet.getString("USR_LAST_SUCCESFUL_LOGIN_DATE"));
		userInfo.setEmail(usersDataSet.getString("USR_EMAIL"));
		userInfo.setDepartment(usersDataSet.getString("USR_DEPT_NO"));
		userInfo.setCreateDate(usersDataSet.getString("USR_CREATE"));
		userInfo.setUpdateDate(usersDataSet.getString("USR_UPDATE"));
		userInfo.setUpdatedBy(usersDataSet.getString("USR_UPDATEBY"));

		boolean active = usersDataSet.getString("USR_DISABLED").equalsIgnoreCase("1") ? false : true; 

		boolean lock = usersDataSet.getString("USR_LOCKED").equalsIgnoreCase("1") ? true : false;

		userInfo.setActive(active);
		userInfo.setLock(lock);
		 */

		return userInfo;
	}


	private HashMap<String, Object> getUpdatedRequestForUpdate(UserInfo userInfoRequest) {

		System.out.println("OIMUserService - getUpdatedRequestForUpdate");
		
		HashMap<String, Object> userMap = new HashMap<>();

		//			user.setAttribute("User Login", userLogin);
		String displayName = "";

		if(userInfoRequest.getFirstName() != null) {
			userMap.put("First Name", userInfoRequest.getFirstName());
			displayName = userInfoRequest.getFirstName();
		}
		if(userInfoRequest.getMiddleName() != null) {
			userMap.put("Middle Name", userInfoRequest.getMiddleName());
			displayName = displayName + " "+userInfoRequest.getMiddleName();
		}
		if(userInfoRequest.getLastName() != null) {
			userMap.put("Last Name", userInfoRequest.getLastName());
			displayName = displayName + " "+userInfoRequest.getLastName();
		}
		if(userInfoRequest.getRole() != null) {
			userMap.put("Role", userInfoRequest.getRole());
		}
		if(userInfoRequest.getActKey() != null) {
			userMap.put("act_key", userInfoRequest.getActKey());
		}
		if(userInfoRequest.getEmail() != null) {
			userMap.put("Email", userInfoRequest.getEmail());
		}
		if(userInfoRequest.getCountry() != null) {
			userMap.put("Country", userInfoRequest.getCountry());
		}
		if(userInfoRequest.getDepartmentNo() != null) {
			userMap.put("Department Number", userInfoRequest.getDepartmentNo());
		}
		if(userInfoRequest.getHomePostalAddress() != null) {
			userMap.put("Home Postal Address", userInfoRequest.getHomePostalAddress());
		}
		if(userInfoRequest.getPostalAddress()!= null) {
			userMap.put("Postal Address", userInfoRequest.getPostalAddress());
		}
		if(userInfoRequest.getTitle() != null) {
			userMap.put("Title", userInfoRequest.getTitle());
		}
		if(userInfoRequest.getState() != null) {
			userMap.put("State", userInfoRequest.getState());
		}
		if(userInfoRequest.getManager() != null) {
			userMap.put("Manager key", userInfoRequest.getManager());
		}
		if(userInfoRequest.getDisplayName() != null && !userInfoRequest.getDisplayName().equals("")) {
			userMap.put("Display Name", userInfoRequest.getDisplayName());
		}else {
			userMap.put("Display Name", displayName);
		}

		return userMap;

	}

	public String updateUser(String authUserName, String loginDateAndTime, String cookies, HashMap<String, Object> userInfoRequest, String entityId, String tenantId) 
		throws Exception {

		System.out.println("OIMUserService - updateUser - entityId- " + entityId);
		
		Login login;
		OIMClient oimClient = null;
		UserManagerResult result = null;
		try {
			login = getCredentials(authUserName, loginDateAndTime, cookies);
			oimClient = getConnection(login.getUserName(), login.getPassword());

			UserManager userManager = oimClient.getService(UserManager.class);

			User user = new User(entityId);

			if(userInfoRequest.get("userName") != null) {
				user.setAttribute("User Login", userInfoRequest.get("userName"));
			}
			if(userInfoRequest.get("firstName") != null) {
				user.setAttribute("First Name", userInfoRequest.get("firstName"));
			}
			if(userInfoRequest.get("middleName") != null) {
				user.setAttribute("Middle Name", userInfoRequest.get("middleName"));
			}
			if(userInfoRequest.get("lastName") != null) {
				user.setAttribute("Last Name", userInfoRequest.get("lastName"));
			}
			if(userInfoRequest.get("displayName") != null) {
				user.setAttribute("Display Name", userInfoRequest.get("displayName"));
			}
			if(userInfoRequest.get("email") != null) {
				user.setAttribute("Email", userInfoRequest.get("email"));
			}
			if(userInfoRequest.get("mobile") != null) {
				user.setAttribute("Mobile", userInfoRequest.get("mobile"));
			}
			if(userInfoRequest.get("street") != null) {
				user.setAttribute("Street", userInfoRequest.get("street"));
			}
			if(userInfoRequest.get("state") != null) {
				user.setAttribute("State", userInfoRequest.get("state"));
			}
			if(userInfoRequest.get("postalCode") != null) {
				user.setAttribute("postalCode", userInfoRequest.get("postalCode"));
			}
			if(userInfoRequest.get("country") != null) {
				user.setAttribute("country", userInfoRequest.get("country"));
			}
			
			result = userManager.modify(user);
        
			System.out.println("result.getStatus() -- " + result.getStatus());
		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | IOException | MyAuthException | LoginException | ValidationFailedException 
				| UserModifyException | NoSuchUserException | AccessDeniedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			disconnect(oimClient);
		}
		
		return result.getStatus();
	}

	private HashMap<String, Object> getUserAppEntityData(tcDataSet usersDataSet) throws tcDataSetException{


		String entity = "application";
		String appProfile = "appProfile";

		HashMap<String, Object> application = new HashMap<>();
		HashMap<String, Object> applicationProfile = new HashMap<>();
		Properties oimUserAttrMappingProp = readPropertiesFile(OIMConstants.OIM_USER_ATTR_MAP_PROP);

		for(String key : oimUserAttrMappingProp.stringPropertyNames()) {
			if(usersDataSet.hasColumn(key)) {
				String splitedKey[] = oimUserAttrMappingProp.getProperty(key).split("\\.");

				if(splitedKey.length == 2 && splitedKey[0].equalsIgnoreCase(entity)) {
					application.put(splitedKey[1], usersDataSet.getString(key));
				} else if(splitedKey.length == 3 && splitedKey[0].equalsIgnoreCase(entity))
					applicationProfile.put(splitedKey[2], usersDataSet.getString(key));
			}
		}

		application.put("appProfile", applicationProfile);
		return application;

	}

	private List<HashMap<String, Object>> getEntityAppInstance(tcDataProvider dbProvider, String userLogin) throws tcDataSetException{

		//		List<EntityAppInstance> applicationList = new ArrayList<>();
		List<HashMap<String, Object>> applicationList = new ArrayList<>();

		Properties queryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);

		String query = queryProp.getProperty(OIMConstants.GET_ALL_APPLICATION_BY_USER_ID);

		/*String query ="select usr.Usr_key as Usr_key, usr.Usr_login as Usr_login, app.app_instance_display_name as Application_Instance, usr.Usr_Display_Name as Usr_Display_Name, usr.Usr_email as Usr_email, app.app_instance_name, app.create_by_user, app.app_instance_key from dev_oim.USR usr\r\n" + 
				"JOIN dev_oim.oiu oiu on usr.usr_key=oiu.usr_key\r\n" + 
				"JOIN dev_oim.App_instance app on app.app_instance_key=oiu.app_instance_key\r\n" + 
				"WHERE usr.usr_status='Active' and usr_login=\'"+userLogin+"\'"; 
		 */
		if(query.contains("{userLogin}")) {

			query = query.replaceAll("\\{userLogin\\}", userLogin);

		}

		tcDataSet usersDataSet = new tcDataSet(); //store result set of query
		usersDataSet.setQuery(dbProvider, query); 
		usersDataSet.executeQuery();

		int numRecords = usersDataSet.getTotalRowCount();

		for(int i = 0; i < numRecords; i++){
			usersDataSet.goToRow(i);
			HashMap<String, Object> entityAppInstance = new HashMap<>();

			//			EntityAppInstance entityAppinstance = new EntityAppInstance();


			/*String appInstanceName = usersDataSet.getString("APPLICATION_INSTANCE_NAME");
			String appInstanceKey = usersDataSet.getString("APPLICATION_INSTANCE_KEY");
			entityAppinstance.setAccountId(appInstanceKey);
			entityAppinstance.setApplicationName(appInstanceName);
			entityAppinstance.setUserRef(usersDataSet.getString("APP_CREATED_BY_USER"));
			entityAppinstance.setAccountType(usersDataSet.getString("APP_INSTANCE_TYPE"));*/

			entityAppInstance = getUserAppEntityData(usersDataSet);
			System.out.println("ENTITY APP INSTANCE = "+entityAppInstance);
			String appInstanceKey = (String) entityAppInstance.get("applicationKey");
			//			entityAppinstance.setApplicationProfile(getEntityProfileInfo(usersDataSet));

			List<String> entitlements = getEntitlementsOfApplication(dbProvider, userLogin, appInstanceKey);
			entityAppInstance.put("entitlements", entitlements);
			applicationList.add(entityAppInstance);

		}

		return applicationList;


	}


	private List<String> getEntitlementsOfApplication(tcDataProvider dbProvider, String userLogin, String appInstanceKey) throws tcDataSetException{

		List<String> entitlementList = new ArrayList<String>();

		Properties queryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);

		String query = queryProp.getProperty(OIMConstants.GET_ALL_ENTITLEMENTS_OF_APP_OF_USER);


		/*String query = "select usr.Usr_key as Usr_key, usr.Usr_login as Usr_login,  entlist.ent_display_name as ENTITLEMENTS, usr.Usr_Display_Name as Usr_Display_Name, usr.Usr_email as Usr_email \r\n" + 
				"from dev_oim.USR usr\r\n" + 
				"JOIN dev_oim.ent_assign entassign on entassign.usr_key=usr.usr_key\r\n" + 
				"JOIN dev_oim.oiu oiu on oiu.oiu_key = entassign.oiu_key\r\n" + 
				"JOIN dev_oim.ent_list entlist on entlist.ent_list_key=entassign.ent_list_key\r\n" + 
				"Where usr_login=\'"+userLogin+"\' \r\n" +
				"AND oiu.app_instance_key = \'"+appInstanceKey+"\'"; */ 

		if(query.contains("{userLogin}") && query.contains("{appInstanceKey}")) {
			query = query.replaceAll("\\{userLogin\\}", userLogin).replaceAll("\\{appInstanceKey\\}", appInstanceKey);
		}

		tcDataSet usersDataSet = new tcDataSet(); //store result set of query
		usersDataSet.setQuery(dbProvider, query); 
		usersDataSet.executeQuery();

		int numRecords = usersDataSet.getTotalRowCount();
		for(int i = 0; i < numRecords; i++){
			usersDataSet.goToRow(i);
			entitlementList.add(usersDataSet.getString("ENTITLEMENTS"));
		}

		return entitlementList;

	}


	/*public List<EntityRole> getAllRoles(String authParam) throws InvalidKeyException, NoSuchAlgorithmException,
	NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, LoginException, tcDataSetException {

		// String sqlQuery = "select Role_id, role_name, role_display_name, description
		// from dev_oim.admin_role";
		// String sqlQuery = "select ugp_key,ugp_name, ugp_display_name, ugp_rolename,
		// ugp_description from dev_oim.ugp";
		List<EntityRole> roleList = new ArrayList<EntityRole>();
		tcDataProvider dbProvider = null;
		Login login = getCredentials(authParam);

		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		XLClientSecurityAssociation.setClientHandle(oimClient); 
		dbProvider = new tcDataBaseClient();

		String query ="select * from dev_oim.ugp join dev_oim.usr on ugp.ugp_role_owner_key=usr.usr_key"; //Query all OIM users
		System.out.println("QUERY = "+query);
		tcDataSet usersDataSet = new tcDataSet(); //store result set of query
		usersDataSet.setQuery(dbProvider, query); 
		usersDataSet.executeQuery();

		int numRecords = usersDataSet.getTotalRowCount();

		for(int i = 0; i < numRecords; i++){
			usersDataSet.goToRow(i);

			String ugp_key = usersDataSet.getString("ugp_key");
			String ugp_name = usersDataSet.getString("ugp_name");
			String ugp_display_name = usersDataSet.getString("ugp_display_name");
			String ugp_rolename = usersDataSet.getString("ugp_rolename");
			String ugp_description = usersDataSet.getString("ugp_description");

			System.out.println("ugp_key : " + ugp_key);
			System.out.println("ugp_name :" + ugp_name);
			System.out.println("ugp_display_name : " + ugp_display_name);
			System.out.println("ugp_rolename : " + ugp_rolename);
			System.out.println("ugp_description : " + ugp_description);

			EntityRole response = new EntityRole();
			response.setRoleName(usersDataSet.getString("ugp_rolename"));
			response.setDescription(usersDataSet.getString("ugp_description"));
			response.setOwnerId(usersDataSet.getString("ugp_role_owner_key"));
			response.setOwnerName(usersDataSet.getString("usr_display_name"));
			roleList.add(response);
		}


		return roleList;


	}
	 */

	public List<HashMap<String, Object>> getAllReporteesOfUser(String authUserName, String loginDateAndTime, String cookies, String userLogin, String tenantId) 
			throws LoginException, tcDataSetException, InvalidKeyException, 
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, 
			BadPaddingException, IOException, MyAuthException{

		List<HashMap<String, Object>> reporteesList = new ArrayList<HashMap<String, Object>>();

		Login login = getCredentials(authUserName, loginDateAndTime, cookies);

		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		XLClientSecurityAssociation.setClientHandle(oimClient); 
		tcDataProvider dbProvider = new tcDataBaseClient();

		//		String query = "select a.usr_key,a.usr_first_name,a.usr_last_name,a.usr_login,a.usr_display_name,a.usr_email,b.usr_login, a.usr_status from dev_oim.usr a join dev_oim.usr b on a.usr_manager_key=b.usr_key where b.usr_login=\'"+userLogin+"\'"; 

		Properties queryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);
		String query = queryProp.getProperty(OIMConstants.GET_ALL_REPORTEES_OF_USER);

		if(query.contains("{userLogin}")) {
			query = query.replaceAll("\\{userLogin\\}", userLogin);
		}

		System.out.println("QUERY = "+query);
		tcDataSet usersDataSet = new tcDataSet(); //store result set of query
		usersDataSet.setQuery(dbProvider, query); 
		usersDataSet.executeQuery();

		int numRecords = usersDataSet.getTotalRowCount();

		for(int i = 0; i < numRecords; i++){
			usersDataSet.goToRow(i);

			HashMap<String, Object> userInfo = getUserEntityData(usersDataSet);

			reporteesList.add(userInfo);
		}

		disconnect(oimClient);

		return reporteesList;

	}


	private HashMap<String, Object> getUserRoleEntityData(tcDataSet usersDataSet) throws tcDataSetException{


		String entity = "role";
		String roleProfile = "roleProfile";

		HashMap<String, Object> role = new HashMap<>();
		HashMap<String, Object> roleProfileInfo = new HashMap<>();
		Properties oimUserAttrMappingProp = readPropertiesFile(OIMConstants.OIM_USER_ATTR_MAP_PROP);

		for(String key : oimUserAttrMappingProp.stringPropertyNames()) {
			if(usersDataSet.hasColumn(key)) {
				String splitedKey[] = oimUserAttrMappingProp.getProperty(key).split("\\.");

				if(splitedKey.length == 2 && splitedKey[0].equalsIgnoreCase(entity)) {
					role.put(splitedKey[1], usersDataSet.getString(key));
				} else if(splitedKey.length == 3 && splitedKey[0].equalsIgnoreCase(entity))
					roleProfileInfo.put(splitedKey[2], usersDataSet.getString(key));
			}
		}

		role.put("roleProfile", roleProfileInfo);
		return role;

	}
	private List<HashMap<String, Object>> getAllRolesOfUser(String userLogin, OIMClient oimClient) 
			throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, 
			IllegalBlockSizeException, BadPaddingException, IOException, LoginException, 
			tcDataSetException{

		//		List<EntityRole> roleList = new ArrayList<EntityRole>();
		List<HashMap<String, Object>> roleList = new ArrayList<HashMap<String, Object>>();

		XLClientSecurityAssociation.setClientHandle(oimClient); 
		tcDataProvider dbProvider = new tcDataBaseClient();

		Properties queryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);

		String query = queryProp.getProperty(OIMConstants.GET_ALL_ROLES_OF_USER);

 
		/*String query  = "SELECT UGP.UGP_KEY, UGP.ugp_role_owner_key, UGP.UGP_NAME Role_Name,USR.USR_LOGIN,UGP.UGP_DISPLAY_NAME,UGP.UGP_ROLENAME, UGP.UGP_DESCRIPTION, usr.usr_display_name, usr.usr_email\r\n" + 
				"FROM dev_oim.UGP, dev_oim.USG, dev_oim.USR \r\n" + 
				"WHERE USG.USR_KEY = USR.USR_KEY\r\n" + 
				"AND UGP.UGP_KEY = USG.UGP_KEY\r\n" + 
				"AND USR.USR_LOGIN = \'"+userLogin+"\'";*/
 
		if(query.contains("{userLogin}")) {

			query = query.replaceAll("\\{userLogin\\}", userLogin);

		}

		System.out.println("QUERY = "+query);
		tcDataSet usersDataSet = new tcDataSet(); //store result set of query
		usersDataSet.setQuery(dbProvider, query); 
		usersDataSet.executeQuery();

		int numRecords = usersDataSet.getTotalRowCount();

		for(int i = 0; i < numRecords; i++){
			usersDataSet.goToRow(i);

			HashMap<String, Object> entityRole = getUserRoleEntityData(usersDataSet);

			roleList.add(entityRole);

		}


		return roleList;

	}


	public String changePassword(String authUserName, String loginDateAndTime, String cookies, String userLogin, String tenantId) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, LoginException, NoSuchUserException, UserManagerException, AccessDeniedException, MyAuthException {


		Login login = getCredentials(authUserName, loginDateAndTime, cookies);

		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		UserManager userManager = oimClient.getService(UserManager.class);

		boolean isLogin = true;
		boolean sendNotification = true;

		userManager.changePassword(login.getUserName(), login.getPassword().toCharArray(), isLogin, sendNotification);
		System.out.println("Password changed Successfully!!!");

		disconnect(oimClient);

		return "Password changed Successfully!!!";
	}


	public String enableDisableUser(String authUserName, String loginDateAndTime, 
			String cookies, String userLogin, boolean isEnable, String tenantId) 
					throws ValidationFailedException, UserEnableException, NoSuchUserException,
					AccessDeniedException, InvalidKeyException, NoSuchAlgorithmException,
					NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
					IOException, LoginException, UserDisableException, MyAuthException {


		Login login = getCredentials(authUserName, loginDateAndTime, cookies);

		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		UserManager userManager = oimClient.getService(UserManager.class);

		if(isEnable) {
			userManager.enable(userLogin, true);
			System.out.println("USER ENABLED Successfully !!!");
			disconnect(oimClient);
			return "USER ENABLED Successfully !!!";
		}else {
			userManager.disable(userLogin, true);
			System.out.println("USER DISABLED Successfully!!!");
			disconnect(oimClient);
			return "USER DISABLED Successfully!!!";
		}


	}
	
	/**
	 * Delete user
	 * 
	 * @param authUserName
	 * @param loginDateAndTime
	 * @param cookies
	 * @param userLogin
	 * @param tenantId
	 * @return
	 * @throws ValidationFailedException
	 * @throws NoSuchUserException
	 * @throws AccessDeniedException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws IOException
	 * @throws LoginException
	 * @throws MyAuthException
	 * @throws UserDeleteException
	 */
	public String deleteUser(String authUserName, String loginDateAndTime, 
			String cookies, String userLogin, String tenantId) 
					throws ValidationFailedException,  NoSuchUserException,
					AccessDeniedException, InvalidKeyException, NoSuchAlgorithmException,
					NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
					IOException, LoginException, MyAuthException, UserDeleteException {


		//Delete is only allowed by admin users
		Properties configProp = readPropertiesFile("config.properties");
		String adminUserName = "xelsysadm";
		String adminPassword = configProp.getProperty(adminUserName);

		OIMClient oimClient = getConnection(adminUserName, adminPassword);

		UserManager userManager = oimClient.getService(UserManager.class);

		userManager.delete(userLogin, true);
		System.out.println("USER DELETED Successfully!!!");
		disconnect(oimClient);
		return "USER DELETED Successfully!!!";

	}


	/**
	 * Lock/Unlock user - uses admin access
	 * 
	 * @param authUserName
	 * @param loginDateAndTime
	 * @param cookies
	 * @param userLogin
	 * @param isLock
	 * @param tenantId
	 * @return
	 * @throws ValidationFailedException
	 * @throws UserEnableException
	 * @throws NoSuchUserException
	 * @throws AccessDeniedException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws IOException
	 * @throws LoginException
	 * @throws UserLockException
	 * @throws UserUnlockException
	 * @throws MyAuthException
	 */
	public String lockUnlockUser(String authUserName, String loginDateAndTime, 
			String cookies, String userLogin, boolean isLock, String tenantId) 
					throws ValidationFailedException, UserEnableException, NoSuchUserException,
					AccessDeniedException, InvalidKeyException, NoSuchAlgorithmException,
					NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException,
					IOException, LoginException, UserLockException, UserUnlockException, MyAuthException {


		//Lock/unlock is only allowed by admin users
		Properties configProp = readPropertiesFile("config.properties");
		String adminUserName = "xelsysadm";
		String adminPassword = configProp.getProperty(adminUserName);

		OIMClient adminOIMClient = getConnection(adminUserName, adminPassword);
		
		UserManager userManager = adminOIMClient.getService(UserManager.class);

		if(isLock) {
			userManager.lock(userLogin, true);
			System.out.println("Lock user !!!");
			disconnect(adminOIMClient);
			return "user LOCKED successfully!!!";
		}else {
			userManager.unlock(userLogin, true);
			System.out.println("User unlocked Successfully!!!");
			disconnect(adminOIMClient);
			return "User unlocked Successfully!!!";
		}
	}


	public String resetPassword(String authUserName, String loginDateAndTime,
			String cookies, String tenantId, boolean isAutoGenerated,
			String endUserLogin) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, LoginException, NoSuchUserException, UserManagerException, AccessDeniedException, MyAuthException
	{
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);

		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		UserManager userManager = oimClient.getService(UserManager.class);

		if(isAutoGenerated) {
			userManager.resetPassword(endUserLogin, true, true);
		}else {
			//			userManager.pass
		}
		System.out.println("Password Reset Successfully!!!");

		disconnect(oimClient);

		return "Password Reset Successfully!!!";
	}


	// this method is called in case forget password
	public String resetMyPassword(String authUserName, String loginDateAndTime,
			String cookies, String tenantId, HashMap questionAnsMap, boolean isAutoGenerated) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, LoginException, NoSuchUserException, UserManagerException, AccessDeniedException, MyAuthException
	{
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);

		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		UserManager userManager = oimClient.getService(UserManager.class);

		userManager.resetPassword(login.getUserName(), true, questionAnsMap);

		System.out.println("Password Reset Successfully!!!");

		disconnect(oimClient);

		return "Password Reset Successfully!!!";
	}

	public List<HashMap< String, Object>> getUserProxy(String authUserName, String loginDateAndTime, String cookies,
			String userLogin, String tenantId) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, LoginException, NoSuchUserException, UserManagerException, AccessDeniedException, MyAuthException {

		List<HashMap< String, Object>> listOfUserProxy =  new ArrayList<>();
		System.out.println("getUserProxy() : Start");

		Login login = getCredentials(authUserName, loginDateAndTime, cookies);

		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());


		//get user manager service
		UserManager userManager = oimClient.getService(UserManager.class);

		// get user proxy
		List<Proxy> listOfProxy = userManager.getAllProxies(userLogin, true);

		Properties userAttrProp = readPropertiesFile(OIMConstants.OIM_USER_PROXY_ATTRIBUTE_MAPPING_SELF_SERVICE_PROPERTIES);

		for(Proxy proxy : listOfProxy) {

			HashMap<String, Object> attributes = proxy.getAttributes();

			listOfUserProxy.add(getProfileDetilsResponse(attributes, userAttrProp));

		}

		return listOfUserProxy;
	}


	public String setSecurityQAAdminService(String authUserName, String loginDateAndTime, String cookies,
			String userLogin, String tenantId, HashMap<String, Object> mapQnA) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, LoginException, UserManagerException, AccessDeniedException, MyAuthException {

		Login login = getCredentials(authUserName, loginDateAndTime, cookies);

		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		System.out.println("Setting Challenge Questions ....");

		Boolean bool=true;

		UserManager userManager = oimClient.getService(UserManager.class);

		userManager.setUserChallengeValues(userLogin, bool, mapQnA);
		
		return "Security Q/A successfully updated";
	}

	/*
	 * Self SERVICES
	 * */
	public String changePasswordSelfService(String authUserName, String loginDateAndTime, 
			String cookies, String tenantId, Login requestLogin) throws oracle.iam.selfservice.exception.ValidationFailedException, ChangePasswordException, LoginException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, MyAuthException {


		if(requestLogin != null && requestLogin.getPassword() != null && !requestLogin.getPassword().isEmpty() &&
				requestLogin.getConfirmPassword() != null && !requestLogin.getConfirmPassword().isEmpty() &&
				requestLogin.getOldPassword() != null && !requestLogin.getOldPassword().isEmpty()) {

			Login login = getCredentials(authUserName, loginDateAndTime, cookies);
			OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

			AuthenticatedSelfService authApi = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);

			String oldpwd =requestLogin.getOldPassword();
			String newpwd = requestLogin.getPassword();
			String confirmpwd = requestLogin.getConfirmPassword();
			authApi.changePassword(oldpwd.toCharArray(), newpwd.toCharArray(), confirmpwd.toCharArray());

			System.out.println("successfully password changed");

			return "successfully password changed";
		}else {
			System.out.println("Old Password , New password, Confirm password fields are mandotary...");
			throw new LoginException("Old Password , New password, Confirm password fields are mandotary...");
		}
	}

	public List<HashMap<String, Object>> getProxySelfService(OIMClient oimClient)
	{
		System.out.println("OIMUserService.getProxySelfService()");

		List<HashMap<String, Object>> listOfProxyResponse = new ArrayList<>();

		AuthenticatedSelfService authApi = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);
		List <Proxy > listOfProxy =  authApi.getAllProxies();

		Properties userProxyAttrProp = readPropertiesFile(OIMConstants.OIM_USER_PROXY_ATTRIBUTE_MAPPING_SELF_SERVICE_PROPERTIES);

		for(Proxy proxy : listOfProxy) {
			HashMap<String, Object> attributes =proxy.getAttributes();

			listOfProxyResponse.add(getProfileDetilsResponse(attributes, userProxyAttrProp));
		}

		return listOfProxyResponse;

	}

	public List<HashMap<String, Object>> getProxySelfService(String authUserName, String loginDateAndTime, 
			String cookies) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, LoginException, MyAuthException{

		System.out.println("OIMUserService.getProxySelfService()");
		List<HashMap<String, Object>> listOfProxyResponse = new ArrayList<>();

		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		AuthenticatedSelfService authApi = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);
		List <Proxy > listOfProxy =  authApi.getAllProxies();

		Properties userProxyAttrProp = readPropertiesFile(OIMConstants.OIM_USER_PROXY_ATTRIBUTE_MAPPING_SELF_SERVICE_PROPERTIES);

		for(Proxy proxy : listOfProxy) {
			HashMap<String, Object> attributes =proxy.getAttributes();

			listOfProxyResponse.add(getProfileDetilsResponse(attributes, userProxyAttrProp));
		}

		return listOfProxyResponse;

	}


	public HashMap<String, Object> updateUserSelfService(String authUserName, String loginDateAndTime, String cookies, UserInfo userInfoRequest, 
			String tenantId) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, LoginException, oracle.iam.selfservice.exception.ValidationFailedException, AccessDeniedException, AuthSelfServiceException, NoSuchUserException, SearchKeyNotUniqueException, MyAuthException{

		String userLogin = authUserName;
		
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);

		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		//		UserDetails userDetails = getUserCompleteDetails(oimClient, userLogin);

		AuthenticatedSelfService authApi = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);

		//		User user = new User(userLogin);
		HashMap<String, Object> updateUserMap = getUpdatedRequestForUpdate(userInfoRequest);

		System.out.println("UPDATE REQ = "+updateUserMap);
		
		HashMap<String, Object> updatedUserResponse = authApi.modifyProfileDetails(updateUserMap);

		System.out.println("UPDATED USER = "+updatedUserResponse);
		
		HashMap<String, Object> profileDetails = getProfileDetailsSelfService(oimClient, userLogin);

		return profileDetails;
	}


	public List<HashMap<String, Object>> getAllReporteesOfUserSelfService(String authUserName, String loginDateAndTime, String cookies, String tenantId) 
			throws LoginException, tcDataSetException, InvalidKeyException, 
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, 
			BadPaddingException, IOException, UserManagerException, AccessDeniedException, MyAuthException{

		List<HashMap<String, Object>> reporteesList = new ArrayList<HashMap<String, Object>>();

		Login login = getCredentials(authUserName, loginDateAndTime, cookies);

		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());


		AuthenticatedSelfService authApi = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);
		List<User> list = new ArrayList<User>(); 
		System.out.println("Inside Get Current Proxy");
		//Proxy list=client.getService(Proxy.class);
		Set <String>retAttr= null;
		HashMap <String,Object> map=new HashMap<String,Object>();
		list=authApi.getMyDirects(retAttr, map);

		Properties userAttrProp = readPropertiesFile(OIMConstants.OIM_USER_ATTRIBUTE_MAPPING_SELF_SERVICE_PROPERTIES);

		for(User user : list) {

			HashMap<String, Object> attributes = user.getAttributes();

			reporteesList.add(getProfileDetilsResponse(attributes, userAttrProp));

		}

		disconnect(oimClient);

		return reporteesList;

	}
	
	/**
	 * Searches user based on provided criteria e.g. "First Name", "Last Name", "User Login"
	 * 
	 * @param authUserName
	 * @param loginDateAndTime
	 * @param cookies
	 * @param tenantId
	 * @param criteria
	 * @return
	 * @throws LoginException
	 * @throws tcDataSetException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws IOException
	 * @throws UserManagerException
	 * @throws AccessDeniedException
	 * @throws MyAuthException
	 */
	public List<HashMap<String, Object>> searchUserByCriteria(String authUserName, String loginDateAndTime, String cookies, String tenantId, String searchField, String searchValue) 
			throws LoginException, InvalidKeyException, 
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, 
			BadPaddingException, IOException, UserManagerException, AccessDeniedException, MyAuthException{

		List<HashMap<String, Object>> reporteesList = new ArrayList<HashMap<String, Object>>();

		Login login = getCredentials(authUserName, loginDateAndTime, cookies);

		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());
		UserManager  userManagerAPI  = (UserManager) oimClient.getService(UserManager .class);
		String searchFieldOIM = "First Name";
		
		if ("firstName".equalsIgnoreCase(searchField)) {
			searchFieldOIM = "First Name";
		} else if ("lastName".equalsIgnoreCase(searchField)) {
			searchFieldOIM = "Last Name"; 
		} else if ("userLogin".equalsIgnoreCase(searchField)) {
			searchFieldOIM = "User Login";
		}
			
		SearchCriteria sc = new SearchCriteria(searchFieldOIM, searchValue, Operator.CONTAINS);
		
		List<User> list = userManagerAPI.search(sc, null, null);
		
		System.out.println(list);
		Properties userAttrProp = readPropertiesFile(OIMConstants.OIM_USER_ATTRIBUTE_MAPPING_SELF_SERVICE_PROPERTIES);

		for(User user : list) {
			HashMap<String, Object> attributes = user.getAttributes();
			reporteesList.add(getProfileDetilsResponse(attributes, userAttrProp));
		}

		disconnect(oimClient);
		return reporteesList;

	}
	
	/**
	 * Add proxy to user
	 * 
	 * @param authUserName
	 * @param loginDateAndTime
	 * @param cookies
	 * @param tenantId
	 * @param proxyId
	 * @param startDateStr
	 * @param endDateStr
	 * @throws LoginException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws IOException
	 * @throws UserManagerException
	 * @throws AccessDeniedException
	 * @throws MyAuthException
	 * @throws ParseException
	 */
	public void addProxy(String authUserName, String loginDateAndTime, String cookies, String tenantId, String proxyUserId, String startDateStr, String endDateStr) 
			throws LoginException, InvalidKeyException, 
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, 
			BadPaddingException, IOException, UserManagerException, AccessDeniedException, MyAuthException, ParseException{

		Login login = getCredentials(authUserName, loginDateAndTime, cookies);

		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());
		AuthenticatedSelfService authAPI  = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);
		
		SimpleDateFormat dateFormat = new SimpleDateFormat(OIMConstants.DATE_FORMAT_FROM_UI);
		Date startDate = dateFormat.parse(startDateStr);
		Date endDate = dateFormat.parse(endDateStr);
		
		authAPI.addProxyForUser(proxyUserId, startDate, endDate, true);
		
		disconnect(oimClient);
	}
	
	/**
	 * Remove Proxy for the user
	 * 
	 * @param authUserName
	 * @param loginDateAndTime
	 * @param cookies
	 * @param tenantId
	 * @param proxyId
	 * @throws LoginException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws IOException
	 * @throws UserManagerException
	 * @throws AccessDeniedException
	 * @throws MyAuthException
	 * @throws ParseException
	 */
	public void removeProxy(String authUserName, String loginDateAndTime, String cookies, String tenantId, String proxyId) 
			throws LoginException, InvalidKeyException, 
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, 
			BadPaddingException, IOException, UserManagerException, AccessDeniedException, MyAuthException, ParseException{

		Login login = getCredentials(authUserName, loginDateAndTime, cookies);

		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());
		AuthenticatedSelfService authAPI  = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);
		
		authAPI.removeProxy(proxyId);
		
		disconnect(oimClient);
	}

	public List<HashMap<String, Object>> getAllReporteesOfUserSelfService(OIMClient oimClient, String tenantId) 
			throws LoginException, tcDataSetException, InvalidKeyException, 
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, 
			BadPaddingException, IOException, UserManagerException, AccessDeniedException{

		List<HashMap<String, Object>> reporteesList = new ArrayList<HashMap<String, Object>>();

		AuthenticatedSelfService authApi = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);
		
		Set <String>retAttr= null;
		
		HashMap <String,Object> map=new HashMap<String,Object>();
		
		List<User> userDirectivesList =authApi.getMyDirects(retAttr, map);

		System.out.println(userDirectivesList);

		Properties userAttrProp = readPropertiesFile(OIMConstants.OIM_USER_ATTRIBUTE_MAPPING_SELF_SERVICE_PROPERTIES);

		for(User user : userDirectivesList) {

			HashMap<String, Object> attributes = user.getAttributes();

			reporteesList.add(getProfileDetilsResponse(attributes, userAttrProp));

		}

//		disconnect(oimClient);
		return reporteesList;

	}


	public List<HashMap<String, Object>> getAllProxiesSelfService(String authUserName, String loginDateAndTime, 
			String cookies)  throws AccessDeniedException, NoSuchUserException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, LoginException, MyAuthException{

		List<HashMap<String, Object>> listOfProxy = new ArrayList<>();

		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());
		AuthenticatedSelfService authApi = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);
		List <Proxy > list = new ArrayList<Proxy>(); 
		System.out.println("Inside Get Current Proxy");
		//Proxy list=client.getService(Proxy.class);
		list=authApi.getCurrentProxies();
		System.out.println("Inside Get Proxy2");
		System.out.println(list);

		Properties userProxyAttrProp = readPropertiesFile(OIMConstants.OIM_USER_PROXY_ATTRIBUTE_MAPPING_SELF_SERVICE_PROPERTIES);

		for(Proxy proxy : list) {

			HashMap<String, Object> attributes = proxy.getAttributes();

			listOfProxy.add(getProfileDetilsResponse(attributes, userProxyAttrProp));
		}

		return listOfProxy;

	}

	public List<HashMap<String, Object>> getAllProxiesSelfService(OIMClient oimClient, String tenantId)  throws AccessDeniedException, NoSuchUserException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, LoginException{

		System.out.println("OIMUserService.getAllProxiesSelfService()");

		List<HashMap<String, Object>> listOfProxy = new ArrayList<>();

		AuthenticatedSelfService authApi = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);
		
		List<Proxy> list = authApi.getAllProxies();

		Properties userProxyAttrProp = readPropertiesFile(OIMConstants.OIM_USER_PROXY_ATTRIBUTE_MAPPING_SELF_SERVICE_PROPERTIES);

		for(Proxy proxy : list) {

			HashMap<String, Object> attributes = proxy.getAttributes();

			listOfProxy.add(getProfileDetilsResponse(attributes, userProxyAttrProp));
		}

		return listOfProxy;
	}

	public List<HashMap<String, Object>> getAllProxiesSelfService(AuthenticatedSelfService authenticatedSelfService)  throws AccessDeniedException, NoSuchUserException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, LoginException{

		System.out.println("OIMUserService.getAllProxiesSelfService()");

		List<HashMap<String, Object>> listOfProxy = new ArrayList<>();

		List<Proxy> list = authenticatedSelfService.getAllProxies();

		Properties userProxyAttrProp = readPropertiesFile(OIMConstants.OIM_USER_PROXY_ATTRIBUTE_MAPPING_SELF_SERVICE_PROPERTIES);

		for(Proxy proxy : list) {

			HashMap<String, Object> attributes = proxy.getAttributes();

			listOfProxy.add(getProfileDetilsResponse(attributes, userProxyAttrProp));
		}

		return listOfProxy;
	}



	public List<HashMap<String, Object>> getCurrentProxiesSelfService(String authUserName, String loginDateAndTime, 
			String cookies)  throws AccessDeniedException, NoSuchUserException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, LoginException, MyAuthException{

		System.out.println("OIMUserService.getCurrentProxiesSelfService()");

		List<HashMap<String, Object>> listOfProxy = new ArrayList<>();

		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());
		AuthenticatedSelfService authApi = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);

		List <Proxy > list = authApi.getCurrentProxies();

		Properties userProxyAttrProp = readPropertiesFile(OIMConstants.OIM_USER_PROXY_ATTRIBUTE_MAPPING_SELF_SERVICE_PROPERTIES);

		for(Proxy proxy : list) {

			HashMap<String, Object> attributes = proxy.getAttributes();

			listOfProxy.add(getProfileDetilsResponse(attributes, userProxyAttrProp));
		}

		return listOfProxy;

	}



	public List<HashMap<String, Object>> getPastProxiesSelfService(String authUserName, String loginDateAndTime, 
			String cookies) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, LoginException, MyAuthException
	{
		System.out.println("OIMUserService.getPastProxiesSelfService()");

		List<HashMap<String, Object>> listOfProxy = new ArrayList<>();

		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());
		AuthenticatedSelfService authApi = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);
		List <Proxy > list = authApi.getPastProxies();

		Properties userProxyAttrProp = readPropertiesFile(OIMConstants.OIM_USER_PROXY_ATTRIBUTE_MAPPING_SELF_SERVICE_PROPERTIES);

		for(Proxy proxy : list) {

			HashMap<String, Object> attributes = proxy.getAttributes();

			listOfProxy.add(getProfileDetilsResponse(attributes, userProxyAttrProp));
		}

		return listOfProxy;


	}

	/*public HashMap <String,Object> getChallengesForSelfSelfService(OIMClient oimClient) throws AuthSelfServiceException
	{
		System.out.println("OIMUserService.getChallengesForSelfSelfService()");
		AuthenticatedSelfService authApi = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);
		HashMap <String,Object> mapQA=new HashMap<String,Object>();
		mapQA= authApi.getChallengeValuesForSelf();
		System.out.println(mapQA);
		return mapQA;
	}*/

	public HashMap<String, Object> setChallengeValuesSelfService(String authUserName, String loginDateAndTime, 
			String cookies,  HashMap <String, Object> queAnsMap)throws ValidationFailedException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, LoginException, MyAuthException, AuthSelfServiceException
	{
		System.out.println("OIMUserService.setChallengeValuesSelfService()");
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());
		AuthenticatedSelfService authSelfService = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);
		authSelfService.setChallengeValues(queAnsMap);
		
		HashMap<String, Object> response = getChallengesForSelfSelfService(oimClient);
		System.out.println("RESPOSNE = "+response);
		return response;
	}

	
	
	public String setChallengeValuesSelfService(OIMClient oimClient,  HashMap <String, Object> queAnsMap)throws ValidationFailedException, SetChallengeValueException,oracle.iam.selfservice.exception.ValidationFailedException
	{
		System.out.println("OIMUserService.setChallengeValuesSelfService()");
		AuthenticatedSelfService authSelfService = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);
		authSelfService.setChallengeValues(queAnsMap);
		System.out.println("Security Questions and Answere successfully updatede....");
		String response = "Security Questions and Answere successfully updatede....";
		return response;
	}

	public HashMap<String, Object> getChallengesForSelfSelfService(OIMClient oimClient) throws AuthSelfServiceException{
		System.out.println("OIMUserService.getChallengesQuestionAndAns()");
		AuthenticatedSelfService authSelfService = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);
		HashMap<String, Object> response = authSelfService.getChallengeValuesForSelf();
		return response;
	}
	
	public List<String> getUserDefinedChallengeQuestionsSelfService(OIMClient oimClient, 
			String userLogin) throws UserAccountDisabledException, UserAccountInvalidException,AuthSelfServiceException
	{
		System.out.println("OIMUserService.getUserDefinedChallengeQuestionsSelfService()");
		AuthenticatedSelfService authApi = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);
		String []challengeQ=authApi.getUserDefinedChallengeQuestions(userLogin);
		System.out.println("challenge QA = "+challengeQ);
		return Arrays.asList(challengeQ);
	}


	public HashMap<String, Object> getProfileDetailsSelfService(String authUserName, String loginDateAndTime, 
			String cookies) throws UserLookupException,NoSuchUserException,SearchKeyNotUniqueException,
	oracle.iam.selfservice.exception.UserLookupException, LoginException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, MyAuthException
	{
		System.out.println("OIMUserService.getProfileDetailsSelfService()");
		
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		AuthenticatedSelfService authenticationSelfService = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);

		User user =  authenticationSelfService.getProfileDetails(null);

		Properties userAttrProp = readPropertiesFile(OIMConstants.OIM_USER_ATTRIBUTE_MAPPING_SELF_SERVICE_PROPERTIES);
		
		HashMap<String , Object> userAttributes = user.getAttributes();

		HashMap<String , Object> userProfileDetails = getProfileDetilsResponse(userAttributes, userAttrProp);

		return userProfileDetails;
	}
	

	public HashMap<String, Object> getProfileDetailsSelfService(OIMClient oimClient, String userLogin) throws UserLookupException,NoSuchUserException,SearchKeyNotUniqueException,
	oracle.iam.selfservice.exception.UserLookupException
	{
		System.out.println("OIMUserService.getProfileDetailsSelfService()");
		
		AuthenticatedSelfService authenticationSelfService = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);

		User user =  authenticationSelfService.getProfileDetails(null);

		Properties userAttrProp = readPropertiesFile(OIMConstants.OIM_USER_ATTRIBUTE_MAPPING_SELF_SERVICE_PROPERTIES);
		
		HashMap<String , Object> userAttributes = user.getAttributes();
		
		for(String key : userAttributes.keySet()) {
			System.out.println("Key : = "+key+" ::: values = "+userAttributes.get(key));
		}
		
		HashMap<String , Object> userProfileDetails = getProfileDetilsResponse(userAttributes, userAttrProp);

		return userProfileDetails;
	}

	public HashMap<String, Object> getMyProfileAndReporteesAndProxyDetailsService(String authUserName, String loginDateAndTime, 
			String cookies, String tenantId) throws UserLookupException,SearchKeyNotUniqueException,
	oracle.iam.selfservice.exception.UserLookupException, LoginException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, UserManagerException, AccessDeniedException, tcDataSetException, MyAuthException
	{

		HashMap<String, Object> myProfileDetails = new HashMap<>();
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		AuthenticatedSelfService selfServiceManager = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);

		UserManager userManager = oimClient.getService(UserManager.class);

		User selfServiceUser =  selfServiceManager.getProfileDetails(null);

		HashMap<String , Object> mapAttr = selfServiceUser.getAttributes();

		Properties userAttrProp = readPropertiesFile(OIMConstants.OIM_USER_ATTRIBUTE_MAPPING_SELF_SERVICE_PROPERTIES);

		myProfileDetails.put("profile", getProfileDetilsResponse(mapAttr, userAttrProp));

		//logic for get admin credentials from credentials store
		myProfileDetails.put("Direct Reportiees", getAllReporteesOfUserSelfService(oimClient, tenantId));

		myProfileDetails.put("Proxy", getAllProxiesSelfService(oimClient, tenantId));

		return myProfileDetails;
	}


	private HashMap<String, Object> getProfileDetilsResponse(HashMap<String, Object> attributes, Properties prop){

		HashMap<String, Object> profileDetails = new HashMap<>();

		for(String key : prop.stringPropertyNames()) {

			String attributeName = prop.getProperty(key).trim();
			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(OIMConstants.DateFormatPattern_forUi);
			if(attributes.containsKey(attributeName)) {
				if (attributeName.equalsIgnoreCase("Start Date") || attributeName.equalsIgnoreCase("Hire Date") 
						|| attributeName.equalsIgnoreCase("pxd_create") || attributeName.equalsIgnoreCase("pxd_update")) { 
					if(attributes.get(attributeName) != null) {
						profileDetails.put(key, simpleDateFormat.format((Date)attributes.get(attributeName)));
					}
				} else {
					profileDetails.put(key, attributes.get(attributeName));
				}
			}

		}


		return profileDetails;
	}

	
	private HashMap<String, Object> getProfileDetilsResponseForUserProfile(HashMap<String, Object> attributes, Properties prop){

		HashMap<String, Object> profileDetails = new HashMap<>();

		for(String key : prop.stringPropertyNames()) {

			String attributeName = prop.getProperty(key).trim();

			SimpleDateFormat simpleDateFormat = new SimpleDateFormat(OIMConstants.DateFormatPattern_forUi);
			if(attributes.containsKey(attributeName)) {
				if (attributeName.equalsIgnoreCase("Start Date") || attributeName.equalsIgnoreCase("Hire Date") 
						|| attributeName.equalsIgnoreCase("pxd_create") || attributeName.equalsIgnoreCase("pxd_update")) {
					if(attributes.get(attributeName) != null) {
						profileDetails.put(key, simpleDateFormat.format((Date)attributes.get(attributeName)));
					}
				} else if (attributeName.equalsIgnoreCase("Display Name")) {
					HashMap displayNameMap = (HashMap)attributes.get(attributeName);
					System.out.println("displayNameMap == "  + displayNameMap);
					System.out.println("displayName == " + displayNameMap.get("base"));
					profileDetails.put(key, displayNameMap.get("base"));
				} else {
					profileDetails.put(key, attributes.get(attributeName));
				}
			}

		}


		return profileDetails;
	}
	
	
	public HashMap<String, Object> getMyInfoCompleteDetailsWithAppAndRoleInfo(String authUserName,
			String loginDateAndTime, String cookies, String tenantId) throws Exception
	{

		HashMap<String, Object> myProfileDetails = new HashMap<>();
		Login login;
		try {
			login = getCredentials(authUserName, loginDateAndTime, cookies);

			OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

			AuthenticatedSelfService selfServiceManager = (AuthenticatedSelfService) oimClient
					.getService(AuthenticatedSelfService.class);

			User selfServiceUser = selfServiceManager.getProfileDetails(null);

			HashMap<String, Object> mapAttr = selfServiceUser.getAttributes();

			Properties userAttrProp = readPropertiesFile(
					OIMConstants.OIM_USER_ATTRIBUTE_MAPPING_SELF_SERVICE_PROPERTIES);

			myProfileDetails.put(OIMEntityConstants.PROFILE,
					getProfileDetilsResponseForUserProfile(mapAttr, userAttrProp));

			// logic for get admin credentials from credentials store
			myProfileDetails.put(OIMEntityConstants.User.DIRECT_REPORTEES,
					getAllReporteesOfUserSelfService(oimClient, tenantId));

			myProfileDetails.put(OIMEntityConstants.User.PROXY, getAllProxiesSelfService(oimClient, tenantId));

			disconnect(oimClient);

			OIMApplicationServices oimApplicationServices = new OIMApplicationServices();
			OIMEntitlementServices oimEntitlementServices = new OIMEntitlementServices();
			List<HashMap<String, Object>> applicationsList = oimApplicationServices.getAllApplicationsOfUser(
					authUserName, loginDateAndTime, cookies, tenantId, selfServiceUser.getEntityId());
			List<HashMap<String, Object>> entitlementList = oimEntitlementServices.getAllEntitlementsOfUser(
					authUserName, loginDateAndTime, cookies, tenantId, selfServiceUser.getEntityId());

			myProfileDetails.put(OIMEntityConstants.Application.APPLICATION, applicationsList);
			myProfileDetails.put(OIMEntityConstants.Entitlement.ENTITLEMENT, entitlementList);

		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException 
				| BadPaddingException | IOException | MyAuthException | LoginException | UserManagerException | AccessDeniedException | tcDataSetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw e;
		}

		return myProfileDetails;
	}
	
	
	/**
	 * Get admin details from credential store
	 * @return
	 * @throws JpsException
	 */
	public String getAdminCredentials() throws JpsException {
        CredentialStore cs = JpsServiceLocator.getServiceLocator().lookup(CredentialStore.class);
        CredentialMap cmap = cs.getCredentialMap("oim");
        Credential cred = cmap.getCredential("sysadmin");   
        
        String username = null;
        
       String password = null;
       String response = "";
        if ((cred instanceof PasswordCredential)) {
            PasswordCredential pcred = (PasswordCredential)cred;
            char[] p = pcred.getPassword();
            username = pcred.getName();
            response = response + "Username = " + username;
            password = new String(p);
            String description = pcred.getDescription();
            response = response + ", Password = " + password+ ", Description ="+description;
            System.out.println("Complete Credential = " + response);
            
        }
        
        return response;
	}


	public List<HashMap<String, Object>> getAllUsers(String authUserName, String loginDateAndTime, String cookies) 
			throws LoginException, InvalidKeyException, 
			NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, 
			BadPaddingException, IOException, UserManagerException, AccessDeniedException, MyAuthException{

		//List<HashMap<String, Object>> reporteesList = new ArrayList<HashMap<String, Object>>();

		//Login login = getCredentials(authUserName, loginDateAndTime, cookies);

		Properties configProp = readPropertiesFile("config.properties");
		String adminUserName = "xelsysadm";
		String adminPassword = configProp.getProperty(adminUserName);
		
		OIMClient oimClient = getConnection(adminUserName, adminPassword);
		UserManager  userManagerAPI  = (UserManager) oimClient.getService(UserManager .class);
			
		List<HashMap<String, Object>> reporteesList = new ArrayList<HashMap<String, Object>>();
		//SearchCriteria sc = new SearchCriteria("User Login", null, null);
		
		int start = 0;
        int interval = 1000;
        List<User> users = null;
        SearchCriteria search = null;
        
        do {

            start += interval;
            HashMap<String, Object> configParams = new HashMap<String, Object>();
            configParams.put(oracle.iam.provisioning.vo.ApplicationInstance.SORTORDER, Searchable.SortOrder.ASCENDING);
            configParams.put(oracle.iam.provisioning.vo.ApplicationInstance.SORTEDBY, UserManagerConstants.AttributeName.USER_LOGIN.getId());
            configParams.put(oracle.iam.provisioning.vo.ApplicationInstance.STARTROW, start - (interval - 1));
            configParams.put(oracle.iam.provisioning.vo.ApplicationInstance.ENDROW, start);
            users = null;
            search = new SearchCriteria("User Login", null, null);
            users = userManagerAPI.search(search, null, configParams);
            Properties userAttrProp = readPropertiesFile(OIMConstants.OIM_USER_ATTRIBUTE_MAPPING_SELF_SERVICE_PROPERTIES);
    		for(User user : users) {
    			System.out.println("Enployee Type -- " + user.getEmployeeType());
    			HashMap<String, Object> attributes = user.getAttributes();
    			System.out.println("attributes -- " + attributes);
    			reporteesList.add(getProfileDetilsResponse(attributes, userAttrProp));
    		}
            

            } while (!users.isEmpty()); 
		
		disconnect(oimClient);
		return reporteesList;

	}

}



