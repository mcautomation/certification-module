package com.kp.oim.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.Hashtable;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import javax.security.auth.login.LoginException;
import javax.servlet.ServletContext;

import com.kp.oim.MyAuthException;
import com.kp.oim.constants.OIMConstants;
import com.kp.oim.pojo.Login;

import oracle.iam.platform.OIMClient;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

public class OIMConnections {

	public Login getCredentials(String userID, String loginDateAndTime, String cookies) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, MyAuthException {

		Properties prop = readPropertiesFile("credentialstore.properties");
		String key = prop.getProperty("OIM_ENCRYPT_KEY");
		
		if(cookies.contains("Bearer ")) {
		
		String cookie = cookies.split(" ")[1];// splitted Bearer and cookie
		
		byte[] decodedValueByte = Base64.getDecoder().decode(cookie);
		
		String decodedValue = new String(decodedValueByte);
		
		String decryptedValue = decrypt(decodedValue, key.getBytes());

		

		
		//		System.out.println("decodedValue : "+decodedValue);

		String []credentials = decryptedValue.split("#k:p#", 3);
		
		String loginID = credentials[0];
		String password = credentials[1];
		String loginDate = credentials[2];
		System.out.println("LOGIN DATE = "+ loginDateAndTime);
		System.out.println("DATE LOGIN = "+loginDate);
		if(loginID.equals(userID) && loginDate.equals(loginDateAndTime)) {
			Login login = new Login();

			login.setUserName(loginID);
			login.setPassword(password);
			login.setLoginDateAndTime(loginDate);
			
			
			return login;
		}else {
			throw new InvalidKeyException("Credentials are not match...");
		}
		
		}else {
			throw new MyAuthException("Please Provide Authorization Header Parameter in Bearer format");
		}
		

	}
	
	
	public OIMClient getConnection(String loginID, String password) throws LoginException {

		Properties prop = readPropertiesFile("config.properties");
		String oimURL = prop.getProperty("OIM_CONNECTION_URL");
		String oimPORT = prop.getProperty("OIM_CONNECTION_PORT");
		String oimAuthFileURL = prop.getProperty("OIM_AUTH_FILE_LOCATION");

		oimURL = oimURL + ":" + oimPORT;

		System.out.println("oimURL = "+oimURL);
		System.out.println("oimPORT = "+oimPORT);
		System.out.println("oimAuthFileURL = "+oimAuthFileURL);
		System.out.println("USERNAME = "+loginID);
//		System.out.println("PASSWORD = "+password);
		Hashtable env = new Hashtable<Object, Object>();

		env.put("java.naming.factory.initial", "weblogic.jndi.WLInitialContextFactory");
		env.put("java.naming.provider.url", oimURL);
		System.setProperty("java.security.auth.login.config", oimAuthFileURL);
		System.setProperty("APPSERVER_TYPE", "wls");

		OIMClient oimClient = new OIMClient(env);
		oimClient.login(loginID, password.toCharArray());
		System.out.println("Connected");

		return oimClient;

	}

	
	public Properties readPropertiesFile_BLK(String fileName){
		FileInputStream fis = null;
		Properties prop = null;
		try {
			//			fis = new FileInputStream(fileName);
			// create Properties class object
			prop = new Properties();
			// load properties file into it
			//			prop.load(fis);
			String filePathName = "D:\\WORKSPACE\\DYNAMIC_PROJECT_WORKSPACE\\KPIIP_OIM\\WebContent\\"+fileName;
			prop.load(new FileInputStream(new File(filePathName)));

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			/*try {
				fis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		}

		return prop;
	}

	public Properties readPropertiesFile(String fileName){
		FileInputStream fis = null;
		Properties prop = null;
		try {
			ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
			InputStream inputStream = classLoader.getResourceAsStream(fileName);
			prop = new Properties();
			prop.load(inputStream);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			/*try {
				fis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}*/
		}

		return prop;
	}
	
	 
	

	//	private static final String ALGO = "AES";
	//	private static byte[] keyValue = null;

	/*public DecryptTextUtility(String strkeyValue) {
		super();
		this.keyValue=strkeyValue.getBytes();
	}
	 */

	/*public void destroy() {
		this.keyValue=null;
	}
	 */

	public String encrypt(String Data, byte[] keyValue) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
		Key key = generateKey(keyValue);
		Cipher cipher = Cipher.getInstance(OIMConstants.ALGO);
		System.out.println(Cipher.getMaxAllowedKeyLength("AES"));
		cipher.init(Cipher.ENCRYPT_MODE, key);
		byte[] encVal = cipher.doFinal(Data.getBytes());
		String encryptedValue = new BASE64Encoder().encode(encVal);
		return encryptedValue;
	}


	public String decrypt(String encryptedData, byte[] keyValue) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IOException, IllegalBlockSizeException, BadPaddingException {
		Key key = generateKey(keyValue);
		Cipher cipher = Cipher.getInstance(OIMConstants.ALGO);
		cipher.init(Cipher.DECRYPT_MODE, key);
		byte[] decordedValue = new BASE64Decoder().decodeBuffer(encryptedData);
		byte[] decValue = cipher.doFinal(decordedValue);
		String decryptedValue = new String(decValue);
		return decryptedValue;
	}

	private Key generateKey(byte[] keyValue) {
		Key key = new SecretKeySpec(keyValue, OIMConstants.ALGO);
		return key;
	}


	public void disconnect(OIMClient oimClient) {
		oimClient.logout();
	}
	
}
