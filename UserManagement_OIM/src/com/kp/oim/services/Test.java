package com.kp.oim.services;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.security.auth.login.LoginException;

import com.kp.oim.constants.OIMConstants;
import com.thortech.xl.dataaccess.tcClientDataAccessException;
import com.thortech.xl.dataaccess.tcDataSetException;
import com.thortech.xl.orb.dataaccess.tcDataAccessException;

import oracle.iam.identity.exception.UserManagerException;
import oracle.iam.identity.usermgmt.vo.Proxy;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.OIMClient;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.selfservice.exception.AuthSelfServiceException;
import oracle.iam.selfservice.exception.UserLookupException;
import oracle.iam.selfservice.self.selfmgmt.api.AuthenticatedSelfService;

public class Test extends OIMConnections{

	public static void main(String[] args) throws LoginException, UserManagerException, AccessDeniedException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, tcDataSetException, IOException, tcDataAccessException, tcClientDataAccessException, AuthSelfServiceException {

		//		System.out.println("hello");

		Date now = new Date();

		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(OIMConstants.DateFormatPattern);
		//		String date = simpleDateFormat.format(now, stringBuffer, new FieldPosition(0));

		String date = simpleDateFormat.format(now);

		//		System.out.println("DATE = "+ date);


		DateTimeFormatter dtf = DateTimeFormatter.ofPattern(OIMConstants.DateFormatPattern);  
		LocalDateTime lnow = LocalDateTime.now();  
		//		   System.out.println(dtf.format(lnow));  


		String userName = "xelsysadm";
		String password = "pass:##:word";
		String loginDate = "24-12-2020 03:12";

		String credentials = userName + "#k:p#" + password + "#k:p#" + loginDate;

		//		System.out.println("Credentials = "+ credentials);

		String splitCredentials[] = credentials.split("#k:p#", 3);

		//		System.out.println("LENGTH = "+splitCredentials.length);

		for(String c : splitCredentials) {
			//			System.out.println(c);
		}



		Test t = new Test();
		//		t.m1("Jayesh");
		//		t.m2();
		//		t.checkProxy();
		//		t.getUserDetails();
		//		t.getAllReportees();
//		t.m3();
//		t.getAllProxy();
//		t.list();
		t.m4();
	}


	public void m1(String userId) {

		String s = "select * from users where usrid = {userID}";

		System.out.println("BEFORE REPLACE = "+s);

		s = s.replaceAll("\\{userID\\}", userId);

		System.out.println("AFTER REPLACE = "+s);
	}


	public void m2() {

		String auth = "Bearer def938tudlgij98q4hd==";
		String authSplit[] = auth.split(" ");

		for(String s : authSplit) {
			System.out.println(s);
		}
	}

	public void checkProxy() throws LoginException {

		Hashtable env = new Hashtable<Object, Object>();

		env.put("java.naming.factory.initial", "weblogic.jndi.WLInitialContextFactory");
		env.put("java.naming.provider.url", "t3://192.168.1.155:14000");
		//		env.put("java.security.auth.login.config", "D:\\OIM_Config\\authwl.conf");
		//		env.put("APPSERVER_TYPE", "wls");

		System.setProperty("java.security.auth.login.config", "D:\\OIM_Config\\authwl.conf");
		System.setProperty("APPSERVER_TYPE", "wls");

		OIMClient oimClient = new OIMClient(env);
		oimClient.login("xelsysadm", "Oracle123".toCharArray());
		System.out.println("Connected");

		AuthenticatedSelfService authApi = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);
		List <Proxy > list = new ArrayList<Proxy>(); 
		System.out.println("Inside Get Proxy");
		//Proxy list=client.getService(Proxy.class);
		list=authApi.getAllProxies();
		System.out.println("Inside Get Proxy2");
		System.out.println(list);

		for(Proxy proxy : list) {
			System.out.println("ATTRIBUTES = "+ proxy.getAttributes());
			System.out.println("________________________");
		}

	}

	public List<HashMap<String, Object>> getAllProxy() throws LoginException{
		Hashtable env = new Hashtable<Object, Object>();

		env.put("java.naming.factory.initial", "weblogic.jndi.WLInitialContextFactory");
		env.put("java.naming.provider.url", "t3://192.168.1.155:14000");

		System.setProperty("java.security.auth.login.config", "D:\\OIM_Config\\authwl.conf");
		System.setProperty("APPSERVER_TYPE", "wls");

		OIMClient oimClient = new OIMClient(env);
		oimClient.login("JAYESHS", "Oracle123".toCharArray());
		System.out.println("Connected");

		AuthenticatedSelfService authApi = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);
		
		List <Proxy > list = authApi.getAllProxies();
		List<HashMap<String, Object>> returnList = new ArrayList<>();
		
		for(Proxy proxy : list) {
			HashMap<String, Object> attributes =proxy.getAttributes();
			Set<String> keys = attributes.keySet();
			System.out.println("Attributes = "+ attributes.toString());
			for(String k : keys) {
				System.out.println("KEY = "+k);
			}
			System.out.println("---------------------------------------");
			returnList.add(attributes);
		}

		return returnList;
	}
	
	public void getUserDetails() throws LoginException, UserLookupException {

		Hashtable env = new Hashtable<Object, Object>();

		env.put("java.naming.factory.initial", "weblogic.jndi.WLInitialContextFactory");
		env.put("java.naming.provider.url", "t3://192.168.1.155:14000");

		System.setProperty("java.security.auth.login.config", "D:\\OIM_Config\\authwl.conf");
		System.setProperty("APPSERVER_TYPE", "wls");

		OIMClient oimClient = new OIMClient(env);
		oimClient.login("xelsysadm", "Oracle123".toCharArray());
		System.out.println("Connected");

		AuthenticatedSelfService authApi = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);

		User user =  authApi.getProfileDetails(null);

		HashMap<String , Object> mapAttr = user.getAttributes();

		for(String key : mapAttr.keySet()) {
			System.out.println("Key = "+key);
			System.out.println("Values = "+mapAttr.get(key));
			System.out.println("________________________________________________");
		}


		System.out.println("Last getProfileDetails");

	}


	public void getAllReportees() throws UserManagerException, AccessDeniedException, LoginException {
		Hashtable env = new Hashtable<Object, Object>();

		env.put("java.naming.factory.initial", "weblogic.jndi.WLInitialContextFactory");
		env.put("java.naming.provider.url", "t3://192.168.1.155:14000");

		System.setProperty("java.security.auth.login.config", "D:\\OIM_Config\\authwl.conf");
		System.setProperty("APPSERVER_TYPE", "wls");

		OIMClient oimClient = new OIMClient(env);
		oimClient.login("xelsysadm", "Oracle123".toCharArray());
		System.out.println("Connected");

		AuthenticatedSelfService authApi = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);

		List<User> list = new ArrayList<User>(); 
		System.out.println("Inside Get Current Proxy");
		//Proxy list=client.getService(Proxy.class);
		Set <String>retAttr= null;
		HashMap <String,Object> map=new HashMap<String,Object>();
		list=authApi.getMyDirects(retAttr, map);

		System.out.println(list);

		for(User user : list) {
			HashMap<String , Object> attributes = user.getAttributes();
			for(String key : attributes.keySet()) {
				System.out.println("Key = "+key);
				System.out.println("Values = "+attributes.get(key));
				System.out.println("\n");
			}

			System.out.println("------------------------------------------");
		}
	}

	public void m3() {

		String s = null;
		s = "";
		System.out.println("Length = "+s.length());
		if(s != null) {
			if(s.isEmpty()) {
				System.out.println("String is empty");
			}else{
				System.out.println("String is not empty");
			}
		}else {
			System.out.println("S is NULL....");
		}
	}
	
	
	public void list() {
		
		List<String> list = new ArrayList<>();
		
		System.out.println("List = "+list);
		
		for(String s : list) {
			System.out.println("s = "+s);
		}
		
	}

	public void m4() throws LoginException, UserManagerException, InvalidKeyException, AccessDeniedException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, tcDataSetException, IOException, tcDataAccessException, tcClientDataAccessException, AuthSelfServiceException {
		
		String userLogin = "JAYESHS";
		String adminLogin = "xelsysadm";
		String adminPassword = "Oracle123";
		
		Hashtable env = new Hashtable<Object, Object>();

		env.put("java.naming.factory.initial", "weblogic.jndi.WLInitialContextFactory");
		env.put("java.naming.provider.url", "t3://192.168.1.155:14000");

		System.setProperty("java.security.auth.login.config", "D:\\OIM_Config\\authwl.conf");
		System.setProperty("APPSERVER_TYPE", "wls");

		OIMClient oimClient = new OIMClient(env);
		oimClient.login("JAYESHS", adminPassword.toCharArray());
		System.out.println("Connected");

//		AuthenticatedSelfService authApi = (AuthenticatedSelfService) oimClient.getService(AuthenticatedSelfService.class);

		OIMUserService service = new OIMUserService();
//		List<HashMap<String, Object>> reporteesList = service.getAllReporteesOfUserSelfService(oimClient, "");
//		System.out.println("REPORTEES LIST = "+reporteesList);
		
//		List<HashMap<String, Object>> proxyList = service.getAllProxiesSelfService(oimClient, "");
//		System.out.println("PROXY LIST = "+proxyList);

//		service.disconnect(oimClient);
		
//		OIMClient adminOIMClient = new OIMClient(env);
//		adminOIMClient.login(adminLogin, adminPassword.toCharArray());
//		System.out.println("ADMIN CLIENT Connected");
		
//		HashMap<String, Object> userDetails = service.getUserByID(adminOIMClient, userLogin, "");
//		System.out.println("USER DETAILS = "+userDetails);
		
		HashMap<String, Object> resposne = service.getChallengesForSelfSelfService(oimClient);
	
		System.out.println("Response = "+resposne);
		
	}
}

