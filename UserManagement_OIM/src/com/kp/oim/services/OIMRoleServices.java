package com.kp.oim.services;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.security.auth.login.LoginException;

import com.kp.oim.MyAuthException;
import com.kp.oim.constants.OIMConstants;
import com.kp.oim.pojo.CatlogRole;
import com.kp.oim.pojo.EntityRole;
import com.kp.oim.pojo.Login;
import com.kp.oim.pojo.Member;
import com.kp.oim.pojo.RoleMembers;
import com.thortech.xl.client.dataobj.tcDataBaseClient;
import com.thortech.xl.dataaccess.tcDataProvider;
import com.thortech.xl.dataaccess.tcDataSet;
import com.thortech.xl.dataaccess.tcDataSetException;
import com.thortech.xl.vo.AccessPolicyResourceData;
import com.thortech.xl.vo.PolicyChildTableRecord;

import Thor.API.Exceptions.tcAPIException;
import Thor.API.Exceptions.tcPolicyNotFoundException;
import Thor.API.Operations.tcAccessPolicyOperationsIntf;
import Thor.API.Security.XLClientSecurityAssociation;
import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.UserLookupException;
import oracle.iam.identity.exception.UserManagerException;
import oracle.iam.identity.exception.UserMembershipException;
import oracle.iam.identity.rolemgmt.api.RoleManager;
import oracle.iam.identity.rolemgmt.api.RoleManagerConstants;
import oracle.iam.identity.rolemgmt.api.RoleManagerConstants.RoleAttributeName;
import oracle.iam.identity.rolemgmt.vo.Role;
import oracle.iam.identity.usermgmt.api.UserManager;
import oracle.iam.identity.usermgmt.vo.User;
import oracle.iam.platform.OIMClient;
import oracle.iam.platform.authopss.vo.AdminRole;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.platform.entitymgr.vo.SearchCriteria;
import oracle.iam.platformservice.api.AdminRoleService;

public class OIMRoleServices extends OIMConnections{

	/**
	 * 
	 * @param authUserName
	 * @param loginDateAndTime
	 * @param cookies
	 * @param tenantId
	 * @param entityId
	 * @return
	 * @throws tcDataSetException
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws IOException
	 * @throws LoginException
	 * @throws MyAuthException
	 */
	public List<HashMap<String, Object>> getUserRoleGrants(String authUserName, String loginDateAndTime,
			String cookies, String tenantId, String entityId) throws tcDataSetException, InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, LoginException, MyAuthException {

		System.out.println("OIMRoleServices - getUserRoleGrants - entityId - " + entityId);
		
		List<HashMap<String, Object>> roleListOfUser = new ArrayList<>();
		
		//Properties configProp = readPropertiesFile("config.properties");
		//String adminUserName = "xelsysadm";
		//String adminPassword = configProp.getProperty(adminUserName);
		
		//OIMClient adminOIMClient = getConnection(adminUserName, adminPassword);
		
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());
		
			//ProvisioningService provService = adminOIMClient.getService (ProvisioningService.class);
			//List<Account> userAccounts = provService.getAccountsProvisionedToUser(entityId);
			
		//SearchCriteria criteria = new SearchCriteria(RoleAttributeName.KEY.NAME.getId(), roleName, SearchCriteria.Operator.EQUAL);
		
		//SearchCriteria criteria = new SearchCriteria(RoleGrantAttributeName.USER_KEY.getId(), entityId, SearchCriteria.Operator.EQUAL);
		
			RoleManager selfServiceManager = (RoleManager) oimClient.getService(RoleManager.class);
			List<Role> roles = null;
			try {
				roles = selfServiceManager.getUserMemberships(entityId, true);
				
				for (Role role : roles) {
					HashMap<String, Object> entityMap = new HashMap<>();
					
					entityMap.put("roleName", role.getAttributes().get("Role Name"));
					entityMap.put("roleDescription", role.getAttributes().get("Role Description"));
					entityMap.put("roleKey", role.getAttributes().get("Role Key"));
					entityMap.put("roleDisplayName", role.getAttributes().get("Role Display Name"));
					entityMap.put("type", "Role");
					
					roleListOfUser.add(entityMap);
				}
				
			} catch (AccessDeniedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (UserMembershipException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally {
				disconnect(oimClient);
				//disconnect(adminOIMClient);
			}
			return roleListOfUser;
			
			//roleListOfUser.add(roles);
	}

	
	public List<EntityRole> getAllRoles_BKP(String authUserName, String loginDateAndTime, String cookies) throws InvalidKeyException, NoSuchAlgorithmException,
	NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, LoginException, tcDataSetException, MyAuthException {

		// String sqlQuery = "select Role_id, role_name, role_display_name, description
		// from dev_oim.admin_role";
		// String sqlQuery = "select ugp_key,ugp_name, ugp_display_name, ugp_rolename,
		// ugp_description from dev_oim.ugp";
		List<EntityRole> roleList = new ArrayList<EntityRole>();
		tcDataProvider dbProvider = null;
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);

		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		XLClientSecurityAssociation.setClientHandle(oimClient); 
		dbProvider = new tcDataBaseClient();

		String query ="select * from dev_oim.ugp join dev_oim.usr on ugp.ugp_role_owner_key=usr.usr_key"; //Query all OIM users
		System.out.println("QUERY = "+query);
		tcDataSet usersDataSet = new tcDataSet(); //store result set of query
		usersDataSet.setQuery(dbProvider, query); 
		usersDataSet.executeQuery();

		int numRecords = usersDataSet.getTotalRowCount();

		for(int i = 0; i < numRecords; i++){
			usersDataSet.goToRow(i);

			String ugp_key = usersDataSet.getString("ugp_key");
			String ugp_name = usersDataSet.getString("ugp_name");
			String ugp_display_name = usersDataSet.getString("ugp_display_name");
			String ugp_rolename = usersDataSet.getString("ugp_rolename");
			String ugp_description = usersDataSet.getString("ugp_description");

			System.out.println("ugp_key : " + ugp_key);
			System.out.println("ugp_name :" + ugp_name);
			System.out.println("ugp_display_name : " + ugp_display_name);
			System.out.println("ugp_rolename : " + ugp_rolename);
			System.out.println("ugp_description : " + ugp_description);

			EntityRole response = new EntityRole();
			response.setRoleName(usersDataSet.getString("ugp_rolename"));
			response.setDescription(usersDataSet.getString("ugp_description"));
			response.setOwnerId(usersDataSet.getString("ugp_role_owner_key"));
			response.setOwnerName(usersDataSet.getString("usr_display_name"));
			roleList.add(response);
		}


		return roleList;


	}


	public List<CatlogRole> getAllCatlogDetailsofRoles_BKP(OIMClient oimClient) throws tcDataSetException {

		// String sqlQuery = "select Role_id, role_name, role_display_name, description
		// from dev_oim.admin_role";
		// String sqlQuery = "select ugp_key,ugp_name, ugp_display_name, ugp_rolename,
		// ugp_description from dev_oim.ugp";

		List<CatlogRole> catlogRoleList = new ArrayList<CatlogRole>();

		tcDataProvider dbProvider = null;
		XLClientSecurityAssociation.setClientHandle(oimClient); 
		dbProvider = new tcDataBaseClient();
		String sqlQuery = "select ugp.ugp_key, ugp.ugp_name, ugp.ugp_display_name, ugp.ugp_rolename, ugp.ugp_description, ugp.UGP_ROLE_OWNER_KEY , usr.usr_display_name from dev_oim.ugp join dev_oim.usr on ugp.ugp_role_owner_key=usr.usr_key";
		System.out.println("QUERY = " + sqlQuery);
		tcDataSet usersDataSet = new tcDataSet(); //store result set of query
		usersDataSet.setQuery(dbProvider, sqlQuery); 
		usersDataSet.executeQuery();

		int numRecords = usersDataSet.getTotalRowCount();

		for(int i = 0; i < numRecords; i++){
			usersDataSet.goToRow(i);

			CatlogRole response  = new CatlogRole();
			response.setRoleName(usersDataSet.getString("UGP_ROLENAME"));
			response.setDescription(usersDataSet.getString("UGP_DESCRIPTION"));
			response.setOwner(usersDataSet.getString("UGP_ROLE_OWNER_KEY"));
			response.setOwnerName(usersDataSet.getString("UGP_DISPLAY_NAME"));

			catlogRoleList.add(response);

		}

		return catlogRoleList;

	}

	public List<RoleMembers> getAllMemberOfRoles_BKP(String authUserName, String loginDateAndTime, String cookies) throws tcDataSetException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, LoginException, MyAuthException {


		tcDataProvider dbProvider = null;
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);

		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		XLClientSecurityAssociation.setClientHandle(oimClient); 
		dbProvider = new tcDataBaseClient();

		String query = "SELECT UGP.UGP_NAME Role_Name, ugp.ugp_description Role_Description, ugp.ugp_role_owner_key Role_Owner_key, u.usr_display_name Owner_Display_Nmae,USR.USR_LOGIN, usr.usr_display_name,usr.usr_status,usr.usr_emp_type,usr.usr_first_name,usr.usr_last_name,usr.usr_manager_key, usermanger.usr_login usr_manager,usermanger.usr_display_name,usr.usr_email "
				+ "FROM dev_oim.UGP, dev_oim.USG, dev_oim.USR , dev_oim.USR u,dev_oim.USR usermanger "
				+ "WHERE USG.USR_KEY = USR.USR_KEY " + "AND UGP.UGP_KEY = USG.UGP_KEY "
				+ "And ugp_role_owner_key=u.usr_key" + "And usr.usr_manager_key=usermanger.usr_key";

		System.out.println("QUERY = "+query);
		tcDataSet usersDataSet = new tcDataSet(); //store result set of query
		usersDataSet.setQuery(dbProvider, query); 
		usersDataSet.executeQuery();

		int numRecords = usersDataSet.getTotalRowCount();

		List<RoleMembers> roleMembersResponseList =  new ArrayList<>();
		List<Member> memberList = new ArrayList<>();

		for(int i = 0; i < numRecords; i++){
			usersDataSet.goToRow(i);
			RoleMembers roleMembersResponse = new RoleMembers();

			roleMembersResponse.setRoleName(usersDataSet.getString("UGP_NAME"));
			roleMembersResponse.setDescription(usersDataSet.getString("ugp_description"));
			roleMembersResponse.setId(usersDataSet.getString("UGP_KEY"));
			//				roleMembersResponse.setItemRisk("");
			roleMembersResponse.setOwnerID(usersDataSet.getString("ugp_role_owner_key"));
			roleMembersResponse.setOwnerName(usersDataSet.getString("usr_display_name"));
			roleMembersResponse.setTenantID("");

			Member member = new Member();

			member.setActive(usersDataSet.getString("usr_status"));
			member.setCountryWork(usersDataSet.getString(""));
			member.setDisplayName(usersDataSet.getString("usr_display_name"));
			member.setDivision(usersDataSet.getString(""));
			member.setEmail(usersDataSet.getString("usr_email"));
			member.setEmployeeNumber(usersDataSet.getString(""));
			member.setFirstName(usersDataSet.getString("usr_first_name"));
			member.setId(usersDataSet.getString("USR_KEY"));
			member.setLastName(usersDataSet.getString("usr_last_name"));
			member.setLocalityWork(usersDataSet.getString(""));
			member.setManager(usersDataSet.getString("usr_manager"));
			member.setManagerDisplayName("usr_display_name");
			member.setMiddleName(usersDataSet.getString(""));
			member.setNickName(usersDataSet.getString(""));
			member.setOrganization(usersDataSet.getString(""));
			//				member.setPostalCodeWork("");
			//				member.setRegionWork("");
			member.setStreetAddressWork("");
			member.setTenantID("");
			member.setTitle("");
			member.setUserName("USR_LOGIN");
			member.setUserType("usr_emp_type");
			memberList.add(member);
			roleMembersResponse.setMembers(memberList);
			roleMembersResponseList.add(roleMembersResponse);
		}

		return roleMembersResponseList;


	}


	public RoleMembers getAllMembersOfRoleByRoleName_BKP(String authUserName, String loginDateAndTime, String cookies, String roleName) throws tcDataSetException, LoginException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, MyAuthException {


		tcDataProvider dbProvider = null;
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);

		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		XLClientSecurityAssociation.setClientHandle(oimClient); 
		dbProvider = new tcDataBaseClient();

		String query = "SELECT UGP.UGP_NAME Role_Name, ugp.ugp_description Role_Description, ugp.ugp_role_owner_key Role_Owner_key, u.usr_display_name Owner_Display_Nmae,USR.USR_LOGIN, usr.usr_display_name,usr.usr_status,usr.usr_emp_type,usr.usr_first_name,usr.usr_last_name,usr.usr_manager_key, usermanger.usr_login usr_manager,usermanger.usr_display_name,usr.usr_email "
				+ "FROM dev_oim.UGP, dev_oim.USG, dev_oim.USR , dev_oim.USR u,dev_oim.USR usermanger "
				+ "WHERE USG.USR_KEY = USR.USR_KEY " + "AND UGP.UGP_KEY = USG.UGP_KEY "
				+ "And ugp_role_owner_key=u.usr_key" + "And usr.usr_manager_key=usermanger.usr_key"+"And UGP.UGP_NAME ="+roleName;

		System.out.println("QUERY = "+query);
		tcDataSet usersDataSet = new tcDataSet(); //store result set of query
		usersDataSet.setQuery(dbProvider, query); 
		usersDataSet.executeQuery();

		int numRecords = usersDataSet.getTotalRowCount();

		RoleMembers roleMembersResponse =  new RoleMembers();
		List<Member> memberList = new ArrayList<>();

		for(int i = 0; i < numRecords; i++){
			usersDataSet.goToRow(i);
			roleMembersResponse.setRoleName(usersDataSet.getString("UGP_NAME"));
			roleMembersResponse.setDescription(usersDataSet.getString("ugp_description"));
			roleMembersResponse.setId(usersDataSet.getString("UGP_KEY"));
			roleMembersResponse.setItemRisk("");
			roleMembersResponse.setOwnerID(usersDataSet.getString("ugp_role_owner_key"));
			roleMembersResponse.setOwnerName(usersDataSet.getString(usersDataSet.getString("usr_display_name")));
			roleMembersResponse.setTenantID("");

			Member member = new Member();

			member.setActive(usersDataSet.getString("usr_status"));
			member.setCountryWork(usersDataSet.getString(""));
			member.setDisplayName(usersDataSet.getString("usr_display_name"));
			member.setDivision(usersDataSet.getString(""));
			member.setEmail(usersDataSet.getString("usr_email"));
			member.setEmployeeNumber(usersDataSet.getString(""));
			member.setFirstName(usersDataSet.getString("usr_first_name"));
			member.setId(usersDataSet.getString("USR_KEY"));
			member.setLastName(usersDataSet.getString("usr_last_name"));
			member.setLocalityWork(usersDataSet.getString(""));
			member.setManager(usersDataSet.getString("usr_manager"));
			member.setManagerDisplayName(usersDataSet.getString("usr_display_name"));
			//		member.setMiddleName(usersDataSet.getString(""));
			//		member.setNickName(usersDataSet.getString(""));
			//		member.setOrganization(usersDataSet.getString(""));
			//		member.setPostalCodeWork("");
			//		member.setRegionWork("");
			//		member.setStreetAddressWork("");
			//		member.setTenantID("");
			//		member.setTitle("");
			member.setUserName(usersDataSet.getString("USR_LOGIN"));
			member.setUserType(usersDataSet.getString("usr_emp_type"));
			memberList.add(member);
			roleMembersResponse.setMembers(memberList);

		}
		return roleMembersResponse;

	}


	/*UPDATED CODE V2*/

	public List<HashMap<String, Object>> getAllRoles(String authUserName, String loginDateAndTime, String cookies, String tenantId)
			throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, IOException, LoginException, tcDataSetException, MyAuthException {

		tcDataProvider dbProvider = null;
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());
		XLClientSecurityAssociation.setClientHandle(oimClient);

		dbProvider = new tcDataBaseClient();

		Properties queryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);
		String sqlQuery = queryProp.getProperty(OIMConstants.GET_ALL_ROLES);
		System.out.println("QUERY = " + sqlQuery);

		Properties roleAttrProp = readPropertiesFile(OIMConstants.OIM_ROLE_ATTR_MAP_PROP);
		tcDataSet usersDataSet = new tcDataSet(); // store result set of query
		usersDataSet.setQuery(dbProvider, sqlQuery);
		usersDataSet.executeQuery();
		int numRecords = usersDataSet.getTotalRowCount();
		List<HashMap<String, Object>> roleList = new ArrayList<HashMap<String, Object>>();
		for (int i = 0; i < numRecords; i++) {
			usersDataSet.goToRow(i);
			HashMap<String, Object> roleMap = new HashMap<String, Object>();
			for (String rolesKeys : roleAttrProp.stringPropertyNames()) {
				if (usersDataSet.hasColumn(rolesKeys)) {
					if (!roleAttrProp.getProperty(rolesKeys).contains(".")) {
						roleMap.put(roleAttrProp.getProperty(rolesKeys), usersDataSet.getString(rolesKeys));
					}
				}
			}
			roleList.add(roleMap);
		}

		return roleList;

	}

	public List<HashMap<String, Object>> getAllCatlogDetailsofRoles(OIMClient oimClient)
			throws tcDataSetException {

		tcDataProvider dbProvider = null;
		XLClientSecurityAssociation.setClientHandle(oimClient);
		dbProvider = new tcDataBaseClient();
		Properties queryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);
		String sqlQuery = queryProp.getProperty(OIMConstants.GET_ALL_ROLE_CATLOG_DETAILS);
		System.out.println("QUERY = " + sqlQuery);

		Properties roleAttrProp = readPropertiesFile(OIMConstants.OIM_ROLE_ATTR_MAP_PROP);
		tcDataSet usersDataSet = new tcDataSet(); // store result set of query
		usersDataSet.setQuery(dbProvider, sqlQuery);
		usersDataSet.executeQuery();
		int numRecords = usersDataSet.getTotalRowCount();
		List<HashMap<String, Object>> roleList = new ArrayList<HashMap<String, Object>>();
		for (int i = 0; i < numRecords; i++) {
			usersDataSet.goToRow(i);
			HashMap<String, Object> roleMap = new HashMap<String, Object>();
			for (String rolesKeys : roleAttrProp.stringPropertyNames()) {
				if (usersDataSet.hasColumn(rolesKeys)) {
					if (!roleAttrProp.getProperty(rolesKeys).contains(".")) {
						roleMap.put(roleAttrProp.getProperty(rolesKeys), usersDataSet.getString(rolesKeys));
					}
				}
			}
			roleList.add(roleMap);
		}

		return roleList;

	}

	public List<HashMap<String, Object>> getAllMemberOfRoles(String authUserName, String loginDateAndTime, String cookies, String tenantId)
			throws tcDataSetException, InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException, IOException, LoginException, MyAuthException {

		tcDataProvider dbProvider = null;
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());
		List<HashMap<String, Object>> rolesList = getAllRoles(oimClient);
		XLClientSecurityAssociation.setClientHandle(oimClient);
		dbProvider = new tcDataBaseClient();

		Properties oimQueryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);
		Properties roleAttributesProperties = readPropertiesFile(OIMConstants.OIM_ROLE_ATTR_MAP_PROP);

		List<HashMap<String, Object>> roleMemberResonse = new ArrayList<HashMap<String, Object>>();
		System.out.println("Role list = "+rolesList);
		for (int j = 0; j < rolesList.size(); j++) {
			HashMap<String, Object> roleObj = rolesList.get(j);
			System.out.println("ROLE OBJECT = "+roleObj);
			String roleName = (String) roleObj.get(roleAttributesProperties.getProperty("ROLE_NAME"));
			System.out.println("ROLE NAME = "+roleName);
			String query = oimQueryProp.getProperty(OIMConstants.GET_ALL_ROLE_MEMBERS);
			System.out.println("query before  = "+query);
			if (query.contains("{roleName}")) {
				query = query.replaceAll("\\{roleName\\}", roleName);
			}

			System.out.println("QUERY AFTER = " + query);
			tcDataSet usersDataSet = new tcDataSet(); // store result set of query
			usersDataSet.setQuery(dbProvider, query);
			usersDataSet.executeQuery();
			int numRecords = usersDataSet.getTotalRowCount();
			List<HashMap<String, Object>> memberList = new ArrayList<>();
			for (int i = 0; i < numRecords; i++) {
				usersDataSet.goToRow(i);
				HashMap<String, Object> membersMap = new HashMap<String, Object>();
				for (String rolesKeys : roleAttributesProperties.stringPropertyNames()) {
					if (usersDataSet.hasColumn(rolesKeys)) {
						if (roleAttributesProperties.getProperty(rolesKeys).contains("members.")) {
							String subMemberKey = roleAttributesProperties.getProperty(rolesKeys).split("\\.")[1];
							membersMap.put(subMemberKey, usersDataSet.getString(rolesKeys));
						}
					}
				}
				memberList.add(membersMap);
			}
			roleObj.put("members", memberList);
			roleMemberResonse.add(roleObj);
		}
		
		return roleMemberResonse;
	}



	public List<HashMap<String, Object>> getAllMembersOfRoleByRoleName(String authUserName, String loginDateAndTime, String cookies, String roleName, String tenantId)
			throws tcDataSetException, LoginException, InvalidKeyException, NoSuchAlgorithmException,
			NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, MyAuthException {

		tcDataProvider dbProvider = null;
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		XLClientSecurityAssociation.setClientHandle(oimClient);
		dbProvider = new tcDataBaseClient();

		Properties oimQueryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);
		Properties roleAttributesProperties = readPropertiesFile(OIMConstants.OIM_ROLE_ATTR_MAP_PROP);

		List<HashMap<String, Object>> roleMemberResonse = new ArrayList<HashMap<String, Object>>();

		String sqlquery = oimQueryProp.getProperty(OIMConstants.GET_ALL_ROLE_MEMBER_OF_ROLE);

		if (sqlquery.contains("{roleName}")) {
			sqlquery = sqlquery.replaceAll("\\{roleName\\}", roleName);
		}

		System.out.println("QUERY = " + sqlquery);
		tcDataSet usersDataSet = new tcDataSet(); // store result set of query
		usersDataSet.setQuery(dbProvider, sqlquery);
		usersDataSet.executeQuery();
		int numRecords = usersDataSet.getTotalRowCount();
		
		List<HashMap<String, Object>> memberList = new ArrayList<>();
		HashMap<String, Object> roleMap = new HashMap<String, Object>();
		for (int i = 0; i < numRecords; i++) {
			usersDataSet.goToRow(i);
			
			HashMap<String, Object> membersMap = new HashMap<String, Object>();
			for (String rolesKeys : roleAttributesProperties.stringPropertyNames()) {
				if (usersDataSet.hasColumn(rolesKeys)) {
					
					String splitedObject[] = roleAttributesProperties.getProperty(rolesKeys).split("\\.");
					
					if(splitedObject.length == 1) {
						roleMap.put(splitedObject[0], usersDataSet.getString(rolesKeys));
					}else if(splitedObject.length == 2) {
						if(splitedObject[0].equalsIgnoreCase("members")) {
							String subMemberKey = splitedObject[1];
							membersMap.put(subMemberKey, usersDataSet.getString(rolesKeys));
						}
					}
					
				}
			}
			memberList.add(membersMap);
			
		}
		roleMap.put("members", memberList);
		roleMemberResonse.add(roleMap);
		return roleMemberResonse;
	}


	private List<HashMap<String, Object>> getAllRoles(OIMClient oimClient)
			throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException,
			BadPaddingException, IOException, LoginException, tcDataSetException {

		tcDataProvider dbProvider = null;
		XLClientSecurityAssociation.setClientHandle(oimClient);
		dbProvider = new tcDataBaseClient();

		Properties queryProp = readPropertiesFile(OIMConstants.OIM_QUERY_PROP_FILE_NAME);
		String sqlQuery = queryProp.getProperty(OIMConstants.GET_ALL_ROLES);
		System.out.println("QUERY = " + sqlQuery);

		Properties roleAttrProp = readPropertiesFile(OIMConstants.OIM_ROLE_ATTR_MAP_PROP);
		tcDataSet usersDataSet = new tcDataSet(); // store result set of query
		usersDataSet.setQuery(dbProvider, sqlQuery);
		usersDataSet.executeQuery();
		int numRecords = usersDataSet.getTotalRowCount();
		List<HashMap<String, Object>> roleList = new ArrayList<HashMap<String, Object>>();
		for (int i = 0; i < numRecords; i++) {
			usersDataSet.goToRow(i);
			HashMap<String, Object> roleMap = new HashMap<String, Object>();
			for (String rolesKeys : roleAttrProp.stringPropertyNames()) {
				if (usersDataSet.hasColumn(rolesKeys)) {
					if (!roleAttrProp.getProperty(rolesKeys).contains(".")) {
						roleMap.put(roleAttrProp.getProperty(rolesKeys), usersDataSet.getString(rolesKeys));
					}
				}
			}
			roleList.add(roleMap);
		}

		return roleList;

	}
	
	/**
	 * Get Admin roles
	 * 
	 * @param authUserName
	 * @param loginDateAndTime
	 * @param cookies
	 * @param tenantId
	 * @return
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws IOException
	 * @throws LoginException
	 * @throws MyAuthException
	 * @throws NoSuchUserException
	 * @throws UserLookupException
	 * @throws AccessDeniedException
	 */
	public Map<String, Object> getUserAdminRoles(String authUserName, String loginDateAndTime, String cookies,
			String tenantId)
			throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException, IOException, LoginException, MyAuthException, NoSuchUserException, UserLookupException, AccessDeniedException {

		System.out.println("OIMRoleServices - getUserAdminRoles - ");

		Map<String, Object> roleListOfUser = new HashMap<>();
		
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		AdminRoleService adminRoleService = (AdminRoleService) oimClient.getService(AdminRoleService.class);
		UserManager userManager = (UserManager) oimClient.getService(UserManager.class);

		User userDetails = userManager.getDetails(login.getUserName(), null, true);
		List<AdminRole> roles = adminRoleService.getAdminRolesForUser(userDetails.getId(), new HashMap());

		List<String> adminRoles = new ArrayList<>();
		adminRoles.add("Employee");

		for (AdminRole role : roles) {
			adminRoles.add(role.getRoleDisplayName());
		}
		
		roleListOfUser.put("userAdminRole", adminRoles);

		disconnect(oimClient);
		return roleListOfUser;

	}

	/**
	 * Get operation role
	 * 
	 * @param authUserName
	 * @param loginDateAndTime
	 * @param cookies
	 * @param tenantId
	 * @return
	 * @throws InvalidKeyException
	 * @throws NoSuchAlgorithmException
	 * @throws NoSuchPaddingException
	 * @throws IllegalBlockSizeException
	 * @throws BadPaddingException
	 * @throws IOException
	 * @throws LoginException
	 * @throws MyAuthException
	 * @throws NoSuchUserException
	 * @throws UserLookupException
	 * @throws AccessDeniedException
	 */
	public Map<String, Object> getOperationalRole(String authUserName, String loginDateAndTime, String cookies,
			String tenantId)
			throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException,
			IllegalBlockSizeException, BadPaddingException, IOException, LoginException, MyAuthException, NoSuchUserException, UserLookupException, AccessDeniedException {

		System.out.println("OIMRoleServices - getUserAdminRoles - ");

		Map<String, Object> roleListOfUser = new HashMap<>();
		
		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		AdminRoleService adminRoleService = (AdminRoleService) oimClient.getService(AdminRoleService.class);
		UserManager userManager = (UserManager) oimClient.getService(UserManager.class);

		User userDetails = userManager.getDetails(login.getUserName(), null, true);
		List<AdminRole> roles = adminRoleService.getAdminRolesForUser(userDetails.getId(), new HashMap());

		String operationalRole = "";

		//Check HelpDesk Role
		for (AdminRole role : roles) {
			if ("HelpDesk".equalsIgnoreCase(role.getRoleDisplayName())) {
				operationalRole = "HelpDesk";
			}
		}
		
		//Check Manager role
		if (operationalRole.isEmpty()) {
			OIMUserService userService = new OIMUserService();
			try {
				List<HashMap<String, Object>> reportees = userService.getAllReporteesOfUserSelfService(authUserName, loginDateAndTime, cookies, tenantId);
				
				if (!reportees.isEmpty()) {
					operationalRole = "Manager";
				}
			} catch (UserManagerException | tcDataSetException e) {
				e.printStackTrace();
			}
		}
		
		//If no role - add Employee
		if (operationalRole.isEmpty()) {
			operationalRole = "Employee";
		}
		
		roleListOfUser.put("userOperationalRole", operationalRole);

		disconnect(oimClient);
		return roleListOfUser;
	}
	
	public Map<String, List<String>> retrieveAccessPolicyDetailsForRole(String authUserName, String loginDateAndTime, String cookies,
			String tenantId, String roleName) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, IOException, MyAuthException, LoginException {

		Login login = getCredentials(authUserName, loginDateAndTime, cookies);
		OIMClient oimClient = getConnection(login.getUserName(), login.getPassword());

		Map<String, List<String>> map = null;
		
		String accessPolicyKey = retrieveAccessPolicyKey(oimClient, roleName);
		if (null != accessPolicyKey)
			map = getAppsAndEntitlementsForAccessPolicy(oimClient, Integer.parseInt(accessPolicyKey));

		return map;

	}

	
	private String retrieveAccessPolicyKey(OIMClient oimClient, String roleName) {

		try {
			RoleManager roleManager = oimClient.getService(RoleManager.class);

			SearchCriteria criteria = new SearchCriteria(RoleAttributeName.NAME.getId(), roleName,
					SearchCriteria.Operator.EQUAL);

			List<Role> roleList = roleManager.search(criteria,
					new HashSet<>(Arrays.asList(RoleManagerConstants.ACCESS_POLICIES)), null);
			System.out.println("roleList = " + roleList);
			if (null == roleList || roleList.isEmpty())
				return null;

			List<Integer> accessPolicies = (List<Integer>) roleList.get(0).getAttribute("Access Policies");
			if (null == accessPolicies || accessPolicies.isEmpty())
				return null;

			String accessPolicyKey = String.valueOf(accessPolicies.get(0));
			System.out.println("accessPolicyKey = " + accessPolicyKey);

			return accessPolicyKey;

		} catch (Exception e) {
			System.out.println("Exception = " + e);
			return null;
		}

	}

	
	private Map<String, List<String>> getAppsAndEntitlementsForAccessPolicy(OIMClient oimClient, int accessPolicyKey) {

		tcAccessPolicyOperationsIntf moAccesspolicyutility = oimClient.getService(tcAccessPolicyOperationsIntf.class);
		Map<String, List<String>> applEntMap = new HashMap<>();

		try {
			AccessPolicyResourceData[] result = moAccesspolicyutility.getDataSpecifiedForObjects(accessPolicyKey);
			
			if (result == null || result.length == 0) {
				System.out.println("Result is null");
				return applEntMap;
			}

			for (int i = 0; i < result.length; i++) {
				
				if (result[i] == null)
					continue;
				
				String application = result[i].getObjectName();
				System.out.println("application = " + application);

				if (result[i].getChildTableRecords() == null || result[i].getChildTableRecords().isEmpty()) {
					applEntMap.put(application, null);
					continue;
				}
					
				ArrayList<PolicyChildTableRecord> childTableRecords = (ArrayList<PolicyChildTableRecord>) result[i]
						.getChildTableRecords().get(0);

				List<String> entitlements = new ArrayList<String>();
				if (null != childTableRecords) {
					childTableRecords.forEach(childTableRecord -> {
						HashMap<String, String> map = childTableRecord.getRecordData();
						entitlements.addAll(map.values());
					});
				}
				
				applEntMap.put(application, entitlements);
			}
			
			System.out.println("applEntMap = " + applEntMap);

		} catch (tcAPIException | tcPolicyNotFoundException e) {
			// TODO Auto-generated catch bloc
			e.printStackTrace();
		}
		
		return applEntMap;
	}
}
