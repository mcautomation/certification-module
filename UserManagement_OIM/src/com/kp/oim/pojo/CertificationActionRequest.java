package com.kp.oim.pojo;

import java.time.LocalDateTime;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString (includeFieldNames = true)
public class CertificationActionRequest {

	private long entityId;
	private String entityType;
	private String action;
	private List<Field> fields;
	private String comment;
	private String endDate;
}
