package com.kp.oim.pojo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Field {

	private String name;
	private String value;
}
