
package com.kp.oim.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
public class UserInfo {
	
	private String id;
	private String firstName;
    private String middleName;
    private String lastName;
    private String userName;
    private String displayName;
    private String actKey;
    private String password;
    private String role;
    private String email;
    private String departmentNo;
    private String homePostalAddress;
    private String postalAddress;
    private String title;
    private String manager;
    private String managerLogin;
    private String managerFirstName;
    private String managerLastName;
    private String managerDisplayName;
    private String organization;
    private String mobile;
    private String street;
    private String state;
    private String postalCode;
    private String country;
}
