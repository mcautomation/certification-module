package com.kp.oim.pojo;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
public class EntityRole {


	private String roleName;

//	@JsonProperty("Description")
	private String description;

//	@JsonProperty("owner")
	private String ownerId;

//	@JsonProperty("ownerName")
	private String ownerName;

//	@JsonProperty("Risk")
	private String risk;


}
