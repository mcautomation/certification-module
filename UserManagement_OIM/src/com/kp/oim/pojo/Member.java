
package com.kp.oim.pojo;

import java.util.HashMap;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
public class Member {

    private String tenantID;
    private String userName;
    private String firstName;
    private String lastName;
    private String email;
    private String manager;
    private String id;
    private String displayName;
    private String nickName;
    private String middleName;
    private String countryWork;
    private String postalCodeWork;
    private String regionWork;
    private String streetAddressWork;
    private String localityWork;
    private String userType;
    private String title;
    private String employeeNumber;
    private String organization;
    private String active;
    private String division;
    private String managerDisplayName;

}
