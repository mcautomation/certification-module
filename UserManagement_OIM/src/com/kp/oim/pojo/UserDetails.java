
package com.kp.oim.pojo;

import java.util.HashMap;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
//import org.codehaus.jackson.annotate.JsonPropertyOrder;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
//@JsonPropertyOrder({"userInfo","entityRole","entityAppInstance"})
public class UserDetails {

	@JsonProperty("userInfo")
    private HashMap<String, Object> userInfo;
//    private String firstTimeUser;
	@JsonProperty("entityRole")
    private List<EntityRole> entityRole;
	
	@JsonProperty("entityAppInstance")
    private List<EntityAppInstance> entityAppInstance;
//    private Map<String, Object> additionalProperties = new HashMap<String, Object>();
    
}
