package com.kp.oim.pojo;

import org.codehaus.jackson.annotate.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
public class CatlogRole {

	@JsonProperty("roleName")
	private String roleName;

	@JsonProperty("description")
	private String description;

	@JsonProperty("owner")
	private String owner;

	@JsonProperty("ownerName")
	private String ownerName;

	@JsonProperty("Risk")
	private String risk;

}
