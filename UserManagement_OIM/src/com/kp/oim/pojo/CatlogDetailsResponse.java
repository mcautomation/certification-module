package com.kp.oim.pojo;

import java.util.HashMap;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
public class CatlogDetailsResponse {

	private List<HashMap<String, Object>> roles;

	private List<HashMap<String, Object>> applications ;

}
