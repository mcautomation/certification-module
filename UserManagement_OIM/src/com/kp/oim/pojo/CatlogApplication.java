package com.kp.oim.pojo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
public class CatlogApplication {

	private String application;
	private String description;
	private String ownerID;
	private String ownerName;
	private String risk;

}
