
package com.kp.oim.pojo;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
public class ApplicationInfo {

    private String applicationName;
    private String applicationDescription;
    private String applicationOwnerId;
    private String applicationOwner;
    private String lastLogin;

}
