
package com.kp.oim.pojo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString(includeFieldNames = true)
public class RoleMembers {

    private String tenantID;
    private String roleName;
    private String description;
    private String ownerID;
    private String ownerName;
    private String itemRisk;
    private String id;
    private List<Member> members;
   
}
