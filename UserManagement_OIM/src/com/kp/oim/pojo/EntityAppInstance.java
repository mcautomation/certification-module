
package com.kp.oim.pojo;

import java.util.HashMap;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

//import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Getter
@Setter
@ToString(includeFieldNames = true)
public class EntityAppInstance {

	
	@JsonProperty("Application Name")
	private String applicationName;
	
	@JsonProperty("Account ID")
	private String accountId;
	
	@JsonProperty("Account Type")
	private String accountType;
	
	@JsonProperty("Application Reference")
	private String applicationRef;
	
	@JsonProperty("User Reference")
	private String userRef;
	
	@JsonProperty("lastLogin")
	private String lastLogin;
	
	@JsonProperty("ApplicationProfile")
	private HashMap<String, Object> applicationProfile;

    private List<String> entityEntitlements ;

}
