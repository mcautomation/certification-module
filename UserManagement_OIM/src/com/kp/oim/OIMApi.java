package com.kp.oim;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.security.auth.login.LoginException;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.OPTIONS;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.kp.oim.constants.OIMConstants;
import com.kp.oim.pojo.CatlogDetailsResponse;
import com.kp.oim.pojo.CertificationActionRequest;
import com.kp.oim.pojo.CertificationActionResponse;
import com.kp.oim.pojo.ErrorMessage;
import com.kp.oim.pojo.Login;
import com.kp.oim.pojo.UserInfo;
import com.kp.oim.services.OIMApplicationServices;
import com.kp.oim.services.OIMCatlogService;
import com.kp.oim.services.OIMCertificationService;
import com.kp.oim.services.OIMEntitlementServices;
import com.kp.oim.services.OIMRoleServices;
import com.kp.oim.services.OIMUserService;
import com.thortech.xl.dataaccess.tcClientDataAccessException;
import com.thortech.xl.dataaccess.tcDataSetException;
import com.thortech.xl.orb.dataaccess.tcDataAccessException;

import oracle.iam.identity.exception.NoSuchUserException;
import oracle.iam.identity.exception.SearchKeyNotUniqueException;
import oracle.iam.identity.exception.UserAlreadyExistsException;
import oracle.iam.identity.exception.UserCreateException;
import oracle.iam.identity.exception.UserDisableException;
import oracle.iam.identity.exception.UserEnableException;
import oracle.iam.identity.exception.UserLockException;
import oracle.iam.identity.exception.UserManagerException;
import oracle.iam.identity.exception.UserModifyException;
import oracle.iam.identity.exception.UserUnlockException;
import oracle.iam.identity.exception.ValidationFailedException;
import oracle.iam.platform.authz.exception.AccessDeniedException;
import oracle.iam.selfservice.exception.AuthSelfServiceException;
import oracle.iam.selfservice.exception.ChangePasswordException;
import oracle.iam.selfservice.exception.UserLookupException;

@Path("/api/v1/{TenantID}")
public class OIMApi {

	private OIMUserService oimService;

	private OIMRoleServices oimRoleService;

	private OIMApplicationServices oimApplicationServices;
	
	private OIMEntitlementServices oimEntitlementService;

	private OIMCatlogService oimCatlogService;
	
	private OIMCertificationService oimCertService;

	public OIMApi() {

		oimService = new OIMUserService();
		oimRoleService = new OIMRoleServices();
		oimApplicationServices = new OIMApplicationServices();
		oimCatlogService = new OIMCatlogService();
		oimEntitlementService = new OIMEntitlementServices();
		oimCertService = new OIMCertificationService();
	}

	@OPTIONS
	@Path("/")
	public Response setOptions() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}
	
	@OPTIONS
	@Path("/login")
	public Response setOptionsLogin() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}
	
	
	@OPTIONS
	@Path("/Users/selfservice/reportees")
	public Response setOptionsGetAllReporteesSelfService() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}
	

	
	@POST
//	@OPTIONS
	@Path("/login")
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(@PathParam("TenantID") String tenantId, Login request) {

		System.out.println("Request = "+request);

		Response response;
		try {
			String loginID = request.getUserName();
			String password = request.getPassword();
			Login loginResponse = oimService.login(loginID, password);



			response = Response.status(201)
					.entity(loginResponse)
					.header("Access-Control-Allow-Origin", "*")
					//					.cookie(new NewCookie("Auth", loginID + password))
					.build();


		} catch (LoginException | InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());
			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();


		}

		return response;

	}

	@OPTIONS
	@Path("/Users")
	public Response setOptionsGetUsers() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}
	
	@GET
	@Path("/Users")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUsers(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies, @PathParam("TenantID") String tenantId) {

		Response response;

		try {

			List<HashMap<String, Object>> userDetailsList = oimService.getUsers(authUserName, loginDateAndTime, cookies, tenantId);

			response = Response.status(201)
					.entity(userDetailsList)
					.header("Access-Control-Allow-Origin", "*")
					.build();


		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException 
				| IllegalBlockSizeException | BadPaddingException | LoginException 
				| IOException | tcDataSetException | MyAuthException  e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}



	@GET
	@Path("/Users/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getUser(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId,
			@PathParam("id") String endUserLogin) {

		Response response;


		try {

			HashMap<String, Object> userDetails = oimService.getUserByID(authUserName, loginDateAndTime, cookies, endUserLogin, tenantId);

			response = Response.status(201)
					.entity(userDetails)
					.header("Access-Control-Allow-Origin", "*")
					.build();


		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException 
				| IllegalBlockSizeException | BadPaddingException | LoginException 
				| IOException | tcDataSetException | tcDataAccessException | tcClientDataAccessException | MyAuthException  e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}

	@POST
	@Path("/Users")
	@Produces(MediaType.APPLICATION_JSON)
	public Response createUser(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,  @PathParam("TenantID") String tenantId,
			UserInfo userInfo) {
		Response response;


		try {

			HashMap<String, Object> createdUserResponse = oimService.createUser(authUserName, loginDateAndTime, cookies, userInfo, tenantId);
			response = Response.status(201)
					.entity(createdUserResponse)
					.header("Access-Control-Allow-Origin", "*")
					.build();


		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException 
				| IllegalBlockSizeException | BadPaddingException | LoginException 
				| IOException | tcDataSetException | ValidationFailedException | UserAlreadyExistsException | UserCreateException | AccessDeniedException | tcDataAccessException | tcClientDataAccessException | MyAuthException  e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}

	
	@OPTIONS
	@Path("/Users/{id}")
	public Response setOptionsUpdateUser() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin,id,enable")
				.build();
		
		return response;
	}

	
	@PUT
	@Path("/Users/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateUser(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,  @PathParam("TenantID") String tenantId,
			@PathParam("id") String entityId,  HashMap<String, Object> userInfo) {

		Response response;

		try {

			String updatedUserResponse = oimService.updateUser(authUserName, loginDateAndTime, cookies, userInfo, entityId, tenantId);

			response = Response.status(201)
					.entity(updatedUserResponse)
					.header("Access-Control-Allow-Origin", "*")
					.build();

		} catch (Exception  e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}


	@GET
	@Produces("application/json")
	@Path("/reportees/{userLogin}")
	public Response getAllReportees(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,  @PathParam("TenantID") String tenantId,
			@PathParam("userLogin") String userLogin){

		Response response = null;

		try {

			List<HashMap<String, Object>> reporteesList =  oimService.getAllReporteesOfUser(authUserName, loginDateAndTime, cookies, userLogin, tenantId);
			response =  Response.status(201)
					.entity(reporteesList)
					.header("Access-Control-Allow-Origin", "*")
					.build();

		} catch (InvalidKeyException | LoginException | NoSuchAlgorithmException 
				| NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException 
				| tcDataSetException | IOException | MyAuthException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;


	}

	@POST
	@Path("/Users/{id}/changepassword")
	public Response changePassword(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId,
			@PathParam("id") String endUserLogin) {

		Response response;


		try {

			String changedPasswordResponse = oimService.changePassword(authUserName, loginDateAndTime, cookies, endUserLogin, tenantId);

			response = Response.status(201)
					.entity(changedPasswordResponse)
					.header("Access-Control-Allow-Origin", "*")
					.build();


		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException 
				| IllegalBlockSizeException | BadPaddingException | LoginException 
				| IOException | UserManagerException | AccessDeniedException | MyAuthException  e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}
	
	@OPTIONS
	@Path("/Users/{id}/resetpassword")
	public Response setresetPasswordOptions() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}

	@POST
	@Path("/Users/{id}/resetpassword")
	public Response resetPassword(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId,
			@PathParam("id") String endUserLogin,
			@QueryParam("isAutoGenerated") boolean isAutoGenerated) {

		Response response;


		try {

			String resetPasswordResponse = oimService.resetPassword(authUserName, loginDateAndTime, cookies, tenantId, isAutoGenerated, endUserLogin);

			response = Response.status(201)
					.entity(resetPasswordResponse)
					.header("Access-Control-Allow-Origin", "*")
					.build();


		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException 
				| IllegalBlockSizeException | BadPaddingException | LoginException 
				| IOException | UserManagerException | AccessDeniedException | MyAuthException  e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}

	
	@OPTIONS
	@Path("/Users/{id}/enabledisable")
	public Response setOptionsEnableDisableUser() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin,id,enable")
				.build();
		
		return response;
	}
	
	@POST
	@Path("/Users/{id}/enabledisable")
	public Response enableDisableUser(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId,
			@PathParam("id") String endUserLogin,
			@QueryParam("enable") boolean isEnable) {

		Response response;


		try {

			System.out.println("is Enable = "+isEnable);

			String res = oimService.enableDisableUser(authUserName, loginDateAndTime,
					cookies, endUserLogin, isEnable, tenantId);

			response = Response.status(201)
					.entity(res)
					.header("Access-Control-Allow-Origin", "*")
					.build();


		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException 
				| IllegalBlockSizeException | BadPaddingException | LoginException 
				| IOException | UserManagerException | AccessDeniedException | MyAuthException  e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}
	
	@OPTIONS
	@Path("/Users/{id}/delete")
	public Response setOptionsDeleteUser() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin,id,enable")
				.build();
		
		return response;
	}
	
	@POST
	@Path("/Users/{id}/delete")
	public Response deleteUser(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId,
			@PathParam("id") String endUserLogin) {

		Response response;

		try {

			String res = oimService.deleteUser(authUserName, loginDateAndTime,
					cookies, endUserLogin, tenantId);

			response = Response.status(201)
					.entity(res)
					.header("Access-Control-Allow-Origin", "*")
					.build();


		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException 
				| IllegalBlockSizeException | BadPaddingException | LoginException 
				| IOException | AccessDeniedException | MyAuthException | UserManagerException  e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}

	@OPTIONS
	@Path("/Users/{id}/lockunlock")
	public Response setOptionsLockUnlockUser() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin,id,isLock")
				.build();
		
		return response;
	}

	@POST
	@Path("/Users/{id}/lockunlock")
	public Response lockUnlockUser(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId,
			@PathParam("id") String endUserLogin,
			@QueryParam("lock") boolean isLock) {

		Response response;


		try {
			System.out.println("isLock = "+isLock);
			String res = oimService.lockUnlockUser(authUserName, loginDateAndTime,
					cookies, endUserLogin, isLock, tenantId);

			response = Response.status(201)
					.entity(res)
					.header("Access-Control-Allow-Origin", "*")
					.build();


		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException 
				| IllegalBlockSizeException | BadPaddingException | LoginException 
				| IOException | UserManagerException | AccessDeniedException | MyAuthException  e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}


	/**
	 * ROLES
	 * */

	@GET
	@Produces("application/json")
	@Path("/Roles")
	public Response getRoles(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId){

		Response response = null;

		try {

			List<HashMap<String, Object>> listOfRoleResponse = oimRoleService.getAllRoles(authUserName, loginDateAndTime, cookies, tenantId);
			response =  Response.status(201)
					.entity(listOfRoleResponse)
					.header("Access-Control-Allow-Origin", "*")
					.build();

		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | LoginException | IOException | tcDataSetException | MyAuthException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;
	}

	/*
	 * CATLOG ITEM
	 * */

	/*@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/getAllCatlogDetails")
	public Response getAllCatlogDetails_BKP(@HeaderParam("userName") String authUserName,
			@HeaderParam("loginDateAndTime") String loginDateAndTime,
			@HeaderParam("cookies") String cookies)  {

		Response response = null;

		try {

			CatlogDetailsResponse allCatlogResponse = oimCatlogService.getAllCatlogDetails(authUserName, loginDateAndTime, cookies);
			response =  Response.status(201)
					.entity(allCatlogResponse)
					.header("Access-Control-Allow-Origin", "*")
					.build();

		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException 
				| IllegalBlockSizeException | BadPaddingException | LoginException 
				| IOException | tcDataSetException  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;
	}
	 */
	/*
	 * GET ALL ROLES MEMBERS
	 * */

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/Roles/getAllRoleMembers")
	public Response getAllRoleMembers(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId)  {

		Response response = null;

		try {
			List<HashMap<String , Object>> roleMembersList = oimRoleService.getAllMemberOfRoles(authUserName, loginDateAndTime, cookies, tenantId);
			//			List<RoleMembers> roleMembersList = oimRoleService.getAllMemberOfRoles(authUserName, loginDateAndTime, cookies);
			response =  Response.status(201)
					.entity(roleMembersList)
					.header("Access-Control-Allow-Origin", "*")
					.build();

		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException 
				| IllegalBlockSizeException | BadPaddingException | LoginException 
				| IOException | tcDataSetException | MyAuthException  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/Roles/getAllRoleMembers/{roleName}")
	public Response getAllMemberOfARole(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies, @PathParam("TenantID") String tenantId,
			@PathParam("roleName") String roleName)  {

		Response response = null;

		try {

			List<HashMap<String,Object>> roleMembers = 
					oimRoleService.getAllMembersOfRoleByRoleName(authUserName, loginDateAndTime, cookies, roleName, tenantId);
			//			RoleMembers roleMembers = oimRoleService.getAllMembersOfRoleByRoleName(authUserName, loginDateAndTime, cookies, roleName);

			response =  Response.status(201)
					.entity(roleMembers)
					.header("Access-Control-Allow-Origin", "*")
					.build();

		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException 
				| IllegalBlockSizeException | BadPaddingException | LoginException 
				| IOException | tcDataSetException | MyAuthException  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;
	}


	/*
	 * Get catlog details
	 * */

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/getAllCatlogDetails")
	public Response getAllCatlogDetails(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies, @PathParam("TenantID") String tenantId) {

		Response response = null;

		try {

			CatlogDetailsResponse allCatlogResponse = oimCatlogService.getAllCatlogDetails(authUserName, loginDateAndTime, cookies, tenantId);
			response =  Response.status(201)
					.entity(allCatlogResponse)
					.header("Access-Control-Allow-Origin", "*")
					.build();

		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException
				| IllegalBlockSizeException | BadPaddingException | LoginException
				| IOException | tcDataSetException | MyAuthException  e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;
	}

/**
	@GET
	@Produces("application/json")
	@Path("/App")
	public Response getAllApplications(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime, @HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId) {

		Response response = null;

		try {

			List<HashMap<String, Object>> listOfRoleResponse = oimApplicationServices.getAllApplication(authUserName,
					loginDateAndTime, cookies, tenantId);
			response = Response.status(201).entity(listOfRoleResponse).header("Access-Control-Allow-Origin", "*")
					.build();

		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | LoginException | IOException | tcDataSetException | MyAuthException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500).entity(errorMessage).header("Access-Control-Allow-Origin", "*").build();
		}

		return response;
	}

**/
	/**
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/App/entitlements")
	public Response getAllEntitmentOfApps(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime, @HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId) {

		Response response = null;

		try {
			List<HashMap<String, Object>> roleMembersList = oimApplicationServices.getAllAppEntitlements(authUserName,
					loginDateAndTime, cookies, tenantId);
			response = Response.status(201).entity(roleMembersList).header("Access-Control-Allow-Origin", "*").build();

		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | LoginException | IOException | tcDataSetException | MyAuthException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500).entity(errorMessage).header("Access-Control-Allow-Origin", "*").build();
		}

		return response;
	}
	**/
	

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/App/{appName}/appentitlements")
	public Response getAllEntitlementOfApp(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime, @HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId, @PathParam("appName") String appName) {

		Response response = null;

		try {

			List<HashMap<String, Object>> roleMembers = oimApplicationServices
					.getAllEntilementOfAppByAppName(authUserName, loginDateAndTime, cookies, appName, tenantId);

			response = Response.status(201).entity(roleMembers).header("Access-Control-Allow-Origin", "*").build();

		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException
				| BadPaddingException | LoginException | IOException | tcDataSetException | MyAuthException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500).entity(errorMessage).header("Access-Control-Allow-Origin", "*").build();
		}

		return response;
	}
	
	
	@POST
	@Path("/Users/{endUserLogin}/setSecurityQA")
	public Response setSecurityQAAdminService(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,  @PathParam("TenantID") String tenantId,
			@PathParam("endUserLogin") String endUserLogin,
			HashMap<String, Object> setSecurityQA) {
		
		Response response;
		try {

			String qAResponse = oimService.setSecurityQAAdminService(authUserName, loginDateAndTime, cookies, endUserLogin, tenantId, setSecurityQA);
			response = Response.status(201)
					.entity(qAResponse)
					.header("Access-Control-Allow-Origin", "*")
					.build();


		} catch (InvalidKeyException | UserManagerException | AccessDeniedException | NoSuchAlgorithmException 
				| NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException 
				| LoginException | IOException | MyAuthException   e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}
	
	
	private void selfServicesStarted() {
		System.out.println("");
	}
	
	/**
	 * SELF SERVICE API
	 * */

	
	@GET
	@Path("/Users/selfService/myprofiledetails")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getMyProfileDetails(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId) {

		Response response;


		try {

			HashMap<String, Object> userDetails = oimService.getProfileDetailsSelfService(authUserName, loginDateAndTime, cookies);

			response = Response.status(201)
					.entity(userDetails)
					.header("Access-Control-Allow-Origin", "*")
					.build();


		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException 
				| IllegalBlockSizeException | BadPaddingException | LoginException 
				| IOException | NoSuchUserException | SearchKeyNotUniqueException | UserLookupException | MyAuthException  e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}
	
	@POST
	@Path("/Users/selfService/changepassword")
	public Response changePasswordSelfService(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId,
			Login loginInfo) {

		Response response;


		try {

			String changedPasswordResponse = oimService.changePasswordSelfService(authUserName, loginDateAndTime, cookies, tenantId, loginInfo);

			response = Response.status(201)
					.entity(changedPasswordResponse)
					.header("Access-Control-Allow-Origin", "*")
					.build();


		} catch (InvalidKeyException | NoSuchAlgorithmException | NoSuchPaddingException 
				| IllegalBlockSizeException | BadPaddingException | LoginException 
				| IOException | oracle.iam.selfservice.exception.ValidationFailedException | ChangePasswordException | MyAuthException  e) {

			e.printStackTrace();

			/*ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());*/

			String errorMessage = e.getMessage();
			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}


	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/Users/selfService/setSecurityQA")
	public Response setSecurityQASelfService(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,  @PathParam("TenantID") String tenantId,
			HashMap<String, Object> setSecurityQA) {
	
		Response response;


		try {

			HashMap<String, Object> qAResponse = oimService.setChallengeValuesSelfService(authUserName, loginDateAndTime, cookies, setSecurityQA);
			response = Response.status(201)
					.entity(qAResponse)
					.header("Access-Control-Allow-Origin", "*")
					.build();


		} catch (InvalidKeyException | ValidationFailedException | NoSuchAlgorithmException | NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException 
				| LoginException | IOException | MyAuthException | AuthSelfServiceException   e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());
			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/Users/selfservice/getProxy")
	public Response getProxy(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId) {

		Response response;
		try {

			List<HashMap<String, Object>> userDetails = oimService.getProxySelfService(authUserName, loginDateAndTime, cookies);

			response = Response.status(201)
					.entity(userDetails)
					.header("Access-Control-Allow-Origin", "*")
					.build();

		} catch (InvalidKeyException | AccessDeniedException | NoSuchAlgorithmException | NoSuchPaddingException 
				| IllegalBlockSizeException | BadPaddingException | LoginException 
				| IOException | MyAuthException e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}
	
	@OPTIONS
	@Path("/Users/selfservice/getProxy")
	public Response setOptionsGetProxy() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/Users/selfservice/searchuser")
	public Response searchUser(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId,
			@QueryParam("searchField") String searchField,
			@QueryParam("searchValue") String searchValue) {

		Response response;
		try {

			List<HashMap<String, Object>> userDetails = oimService.searchUserByCriteria(authUserName, loginDateAndTime, cookies, tenantId, searchField, searchValue);

			response = Response.status(201)
					.entity(userDetails)
					.header("Access-Control-Allow-Origin", "*")
					.build();

		} catch (InvalidKeyException | AccessDeniedException | NoSuchAlgorithmException | NoSuchPaddingException 
				| IllegalBlockSizeException | BadPaddingException | LoginException 
				| IOException | MyAuthException | UserManagerException e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}
	
	@OPTIONS
	@Path("/Users/selfservice/searchuser")
	public Response setOptionsSearchUser() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/Users/selfservice/addProxy")
	public Response addProxy(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId,
			@QueryParam("proxyUserId") String proxyUserId,
			@QueryParam("startDate") String startDate,
			@QueryParam("endDate") String endDate) {

		Response response;
		try {

			oimService.addProxy(authUserName, loginDateAndTime, cookies, tenantId, proxyUserId, startDate, endDate);

			response = Response.status(201)
					.entity("Success")
					.header("Access-Control-Allow-Origin", "*")
					.build();

		} catch (InvalidKeyException | AccessDeniedException | NoSuchAlgorithmException | NoSuchPaddingException 
				| IllegalBlockSizeException | BadPaddingException | LoginException 
				| IOException | MyAuthException | UserManagerException | ParseException e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}
	
	@OPTIONS
	@Path("/Users/selfservice/addProxy")
	public Response setOptionsAddProxy() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}
	
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/Users/selfservice/removeProxy")
	public Response removeProxy(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId,
			@QueryParam("proxyId") String proxyId) {

		Response response;
		try {

			oimService.removeProxy(authUserName, loginDateAndTime, cookies, tenantId, proxyId);

			response = Response.status(201)
					.entity("Success")
					.header("Access-Control-Allow-Origin", "*")
					.build();

		} catch (InvalidKeyException | AccessDeniedException | NoSuchAlgorithmException | NoSuchPaddingException 
				| IllegalBlockSizeException | BadPaddingException | LoginException 
				| IOException | MyAuthException | UserManagerException | ParseException e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}
	
	@OPTIONS
	@Path("/Users/selfservice/removeProxy")
	public Response setOptionsRemoveProxy() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/Users/selfservice/profileCompleteDetails")
	public Response getMyCompleteInformationUser(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId) {

		Response response;


		try {

			HashMap<String, Object> userDetails = oimService.getMyInfoCompleteDetailsWithAppAndRoleInfo(authUserName, loginDateAndTime, cookies, tenantId);

			response = Response.status(201)
					.entity(userDetails)
					.header("Access-Control-Allow-Origin", "*")
					.build();


		} catch (Exception e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}
	
	@OPTIONS
	@Path("/Users/selfservice/profileCompleteDetails")
	public Response setOptionsGetMyCompleteInformationUser() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}
	
	@PUT
	@Path("/Users/selfservice/updateUser")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateUserSelfService(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId,
			UserInfo userInfoRequest) {

		Response response;

		try {

			HashMap<String, Object> updatedUserResponse = oimService.updateUserSelfService(authUserName, loginDateAndTime, cookies, userInfoRequest, tenantId);

			response = Response.status(201)
					.entity(updatedUserResponse)
					.header("Access-Control-Allow-Origin", "*")
					.build();

		} catch (InvalidKeyException | NoSuchUserException | SearchKeyNotUniqueException 
				| AccessDeniedException | AuthSelfServiceException | NoSuchAlgorithmException 
				| NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException 
				| LoginException | IOException | MyAuthException  e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}
	
	@GET
	@Produces("application/json")
	@Path("/Users/selfservice/reportees")
	public Response getAllReporteesSelfService(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId){

		Response response = null;

		try {

			List<HashMap<String, Object>> reporteesList =  oimService.getAllReporteesOfUserSelfService(authUserName, loginDateAndTime, cookies, tenantId);
			response =  Response.status(201)
					.entity(reporteesList)
					.header("Access-Control-Allow-Origin", "*")
					.build();

		} catch (InvalidKeyException | UserManagerException | AccessDeniedException 
				| LoginException | NoSuchAlgorithmException | NoSuchPaddingException 
				| IllegalBlockSizeException | BadPaddingException | tcDataSetException 
				| IOException | MyAuthException  e) {
			
			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;


	}
	
	
	@OPTIONS
	@Path("/Users/{id}/applications")
	public Response setOptionsGetAllApplications() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}
	
	@GET
	@Produces("application/json")
	@Path("/Users/{id}/applications")
	public Response getAllApplications(@PathParam("id") String entityId, 
			@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime, @HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId) {

		Response response = null;

		try {
  
			List<HashMap<String, Object>> listOfApplications = oimApplicationServices.getAllApplicationsOfUser(authUserName,
					loginDateAndTime, cookies, tenantId, entityId);
			response = Response.status(201).entity(listOfApplications).header("Access-Control-Allow-Origin", "*")
					.build();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500).entity(errorMessage).header("Access-Control-Allow-Origin", "*").build();
		}

		return response;
	}	
	

	@OPTIONS
	@Path("/Users/{id}/entitlements")
	public Response setOptionsGetAllEntitlementsOfUser() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}
	
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/Users/{id}/entitlements")
	public Response getAllEntitlementsOfUser(@PathParam("id") String entityId,
			@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime, @HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId) {

		Response response = null;

		System.out.println("getAllEntitlementsForUser... rest service..");
		
		try {
			List<HashMap<String, Object>> roleMembersList = oimEntitlementService.getAllEntitlementsOfUser(authUserName,
					loginDateAndTime, cookies, tenantId, entityId);
			response = Response.status(201).entity(roleMembersList).header("Access-Control-Allow-Origin", "*").build();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500).entity(errorMessage).header("Access-Control-Allow-Origin", "*").build();
		} 

		return response;
	}

	
	@OPTIONS
	@Path("/Users/{id}/entitlements/revoke")
	public Response setOptionsRevokeEntitlementFromUser() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}
	

	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/Users/{id}/entitlements/revoke")
	public Response revokeEntitlementFromUser(@PathParam("id") String entityId,
			@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime, @HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId,
			@QueryParam("entitlementName") String entitlementName, @QueryParam("entitlementCode") String entitlementCode, @QueryParam("type") String type) {

		Response response = null;

		System.out.println("revokeEntitlementFromUser... rest service..");
		
		try {
			String status = oimEntitlementService.revokeEntitlementFromUser(entityId, entitlementName, entitlementCode, type);
			if(OIMConstants.SUCCESS.equalsIgnoreCase(status)) {
				response = Response.status(200).entity(status).header("Access-Control-Allow-Origin", "*").build();	
			}else {
				response = Response.status(500).entity(status).header("Access-Control-Allow-Origin", "*").build();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500).entity(errorMessage).header("Access-Control-Allow-Origin", "*").build();
		}

		return response;
	}
	

	@OPTIONS
	@Path("/Users/{id}/applications/revoke")
	public Response setOptionsRevokeApplicationFromUser() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}
	
	
	@PUT
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/Users/{id}/applications/revoke")
	public Response revokeApplicationFromUser(@PathParam("id") String entityId,
			@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime, @HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId,
			@QueryParam("accountId") String accountId, 
			@QueryParam("accountDescription") String accountDescription, 
			@QueryParam("applicationName") String applicationName) {

		Response response = null;

		System.out.println("revokeApplicationFromUser... rest service..");
		
		try {
			String status = oimApplicationServices.revokeAccountFromUser(entityId, accountId, accountDescription, applicationName);
			
			response = Response.status(200)
					.entity(status)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500).entity(errorMessage).header("Access-Control-Allow-Origin", "*").build();
		}

		return response;
	}

	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/Users/selfservice/getUserAdminRoles")
	public Response getUserAdminRoles(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId) {

		Response response;
		try {
			Map<String, Object> userAdminRole = oimRoleService.getUserAdminRoles(authUserName, loginDateAndTime, cookies, tenantId);
			response = Response.status(201)
					.entity(userAdminRole)
					.header("Access-Control-Allow-Origin", "*")
					.build();

		} catch (Exception e) {
			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}
		return response;
	}
	
	@OPTIONS
	@Path("/Users/selfservice/getUserAdminRoles")
	public Response setOptionsGetUserAdminRoles() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}

	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/Users/selfservice/getOperationalRole")
	public Response getOperationalRole(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId) {

		Response response;
		try {
			Map<String, Object> userAdminRole = oimRoleService.getOperationalRole(authUserName, loginDateAndTime, cookies, tenantId);
			response = Response.status(201)
					.entity(userAdminRole)
					.header("Access-Control-Allow-Origin", "*")
					.build();

		} catch (Exception e) {
			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}
		return response;
	}
	
	@OPTIONS
	@Path("/Users/selfservice/getOperationalRole")
	public Response setOptionsGetOperationalRole() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}
	

	@OPTIONS
	@Path("/Users/selfservice/getAdminDetails")
	public Response setOptionsGetAdminDetails() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}
	
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/Users/selfservice/getAdminDetails")
	public Response getAdminDetails(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId) {

		Response response;
		try { 
			String adminCred = oimService.getAdminCredentials();
			response = Response.status(201)
					.entity(adminCred)
					.header("Access-Control-Allow-Origin", "*")
					.build();

		} catch (Exception e) {
			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}
		return response;
	}
	

	@OPTIONS
	@Path("/Users/getallusers")
	public Response setOptionsGetAllUsers() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}
	

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/Users/getallusers")
	public Response getAllUsers(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId) {

		System.out.println("getAllUsers... rest service..");
		
		Response response;
		try {

			List<HashMap<String, Object>> userDetails = oimService.getAllUsers(authUserName, loginDateAndTime, cookies);

			response = Response.status(200)
					.entity(userDetails)
					.header("Access-Control-Allow-Origin", "*")
					.build();

		} catch (InvalidKeyException | AccessDeniedException | NoSuchAlgorithmException | NoSuchPaddingException 
				| IllegalBlockSizeException | BadPaddingException | LoginException 
				| IOException | MyAuthException | UserManagerException e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}
	
	
	@OPTIONS
	@Path("/certificationAction/{certificationId}/{taskId}/{lineItemId}")
	public Response setOptionsUpdateCertificationAction() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}
	
	
	@PUT
	@Path("/certificationAction/{certificationId}/{taskId}/{lineItemId}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response updateCertificationAction(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId,
			@PathParam("certificationId") long certificationId,
			@PathParam("taskId") String taskId,
			@PathParam("lineItemId") long lineItemId,
			List<CertificationActionRequest> request) {

		Response response;

		try {

			List<CertificationActionResponse> certActionResponse = oimCertService.updateCertificationAction
					(authUserName, loginDateAndTime, cookies, tenantId, certificationId, taskId, lineItemId, request);

			response = Response.ok()
					.header("Access-Control-Allow-Origin", "*")
					.entity(certActionResponse)
					.build();

		} catch (InvalidKeyException | NoSuchUserException | SearchKeyNotUniqueException 
				| AccessDeniedException | AuthSelfServiceException | NoSuchAlgorithmException 
				| NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException 
				| LoginException | IOException | MyAuthException  e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}
	
	@OPTIONS
	@Path("/accessPolicyDetails/{roleName}")
	public Response setOptionsRetrieveAccessPolicyDetailsForRole() {
		Response response = Response.status(201)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}
	
	@OPTIONS
	@Path("/getSignOffCertifications")
	public Response getSignOffCertifications() {
		Response response = Response.status(200)
				.header("Access-Control-Allow-Origin", "*")
				.header("Access-Control-Allow-Methods", "POST, GET, PUT, UPDATE, DELETE, OPTIONS, HEAD")
				.header("Access-Control-Allow-Credentials", "true")
				.header("Access-Control-Allow-Headers", "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization,oim_remote_user,lastLogin")
				.build();
		
		return response;
	}
	

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getSignOffCertifications")
	public Response getSignOffCertifications(@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId) {

		System.out.println("getSignOffCertifications... rest service..");
		
		OIMCertificationService oimCertificationService=new OIMCertificationService();
		Response response;
		try {
			
			List<HashMap<String, Object>> signoffDetails =oimCertificationService.getSignOffCertifications(authUserName, loginDateAndTime, cookies);

			response = Response.status(200)
					.entity(signoffDetails)
					.header("Access-Control-Allow-Origin", "*")
					.build();

		} catch (InvalidKeyException | AccessDeniedException | NoSuchAlgorithmException | NoSuchPaddingException 
				| IllegalBlockSizeException | BadPaddingException | LoginException 
				| IOException | MyAuthException | UserManagerException e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}
	
	@GET
	@Path("/roleDetails/{roleName}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response retrieveAccessPolicyDetailsForRole (@HeaderParam("oim_remote_user") String authUserName,
			@HeaderParam("lastLogin") String loginDateAndTime,
			@HeaderParam("Authorization") String cookies,
			@PathParam("TenantID") String tenantId,
			@PathParam("roleName") String roleName) {

		Response response;

		try {
			
			Map<String, List<String>> map = oimRoleService.retrieveAccessPolicyDetailsForRole(authUserName, loginDateAndTime, cookies, tenantId, roleName);

			response = Response.ok()
					.header("Access-Control-Allow-Origin", "*")
					.entity(map)
					.build();

		} catch (InvalidKeyException | AccessDeniedException | NoSuchAlgorithmException 
				| NoSuchPaddingException | IllegalBlockSizeException | BadPaddingException 
				| LoginException | IOException | MyAuthException  e) {

			e.printStackTrace();

			ErrorMessage errorMessage = new ErrorMessage();
			errorMessage.setStatus("error");
			errorMessage.setMessage(e.getMessage());

			response = Response.status(500)
					.entity(errorMessage)
					.header("Access-Control-Allow-Origin", "*")
					.build();
		}

		return response;

	}

}
