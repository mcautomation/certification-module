package com.kp.oim.constants;

public class OIMConstants {

	public static String ALGO = "AES";
	
	public static final String SUCCESS = "success";
	public static final String FAILED = "failed";
	
	public static final String GET_ALL_SIGN_OFF_CERTIFICATION = "select CERT_ID,TASK_UID,TASK_NAME,CERT_CERTS.CERT_TYPE AS CERT_TYPE from cert_task_info  JOIN CERT_CERTS ON (cert_task_info.CERT_ID=CERT_CERTS.ID) WHERE TASK_END_DATE IS NOT NULL AND TASK_STATUS =2 AND  CERT_CERTS.CERT_STATE ='3' AND UPDATEUSER = '{username}'";
	public static String DateFormatPattern = "dd-M-yyyy hh:mm:ss";
	
	public static String DATE_FORMAT_FROM_UI = "MM/dd/yyyy";
	
	public static String DateFormatPattern_forUi = "yyyy-MM-dd";
	
	

	public static String GET_CATLOG_DETAILS_OF_APP ="GET_CATLOG_DETAILS_OF_APP";

	
	//Properties constants
	public static String OIM_QUERY_PROP_FILE_NAME = "oim_query.properties";
	
	public static String OIM_USER_ATTR_MAP_PROP = "oim_user_attribute_mapping.properties";
	
	public static String OIM_APP_ATTR_MAP_PROP = "oim_application_attribute_mapping.properties";
	
	public static String OIM_ROLE_ATTR_MAP_PROP = "oim_role_attribute_mapping.properties";
	
	public static String OIM_ENTL_ATTR_MAP_PROP = "oim_entitlement_attribute_mapping.properties";
	
	//Query Constant
	public static String GET_USER_BY_ID = "GET_USER_BY_ID";
	
	public static String GET_ALL_USERS = "GET_ALL_USERS";
	
	public static String GET_ALL_APPLICATION_BY_USER_ID = "GET_ALL_APPLICATION_BY_USER_ID";
	
	public static String GET_ALL_ENTITLEMENTS_OF_APP_OF_USER = "GET_ALL_ENTITLEMENT_OF_APP_OF_USER";
	
	public static String GET_ALL_ROLES_OF_USER = "GET_ALL_ROLES_OF_USER";
	
	public static String GET_ALL_REPORTEES_OF_USER = "GET_ALL_REPORTEES_OF_USER";
	
	public static String GET_ALL_ROLES = "GET_ALL_ROLES";
	
	public static String GET_ALL_ROLE_CATLOG_DETAILS = "GET_ALL_ROLE_CATLOG_DETAILS";
	
	public static String GET_ALL_ROLE_MEMBERS = "GET_ALL_ROLE_MEMBERS";
	
	public static String GET_ALL_ROLE_MEMBER_OF_ROLE = "GET_ALL_ROLE_MEMBER_OF_ROLE";
	
	public static String GET_CATLOG_DETAILS_OF_APP_BY_APP_NAME = "GET_CATLOG_DETAILS_OF_APP_BY_APP_NAME";
	
	public static String GET_ALL_APPLICATION_ENTITLEMENTS = "GET_ALL_APPLICATION_ENTITLEMENTS";
	
	public static String GET_ALL_APP = "GET_ALL_APP";
	
	public static String OIM_USER_ATTRIBUTE_MAPPING_SELF_SERVICE_PROPERTIES = "oim_user_attribute_mapping_self_service.properties";
	
	public static String OIM_USER_PROXY_ATTRIBUTE_MAPPING_SELF_SERVICE_PROPERTIES = "oim_user_proxy_attribute_mapping_self_service.properties";
}
