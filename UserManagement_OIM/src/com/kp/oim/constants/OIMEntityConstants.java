package com.kp.oim.constants;

public class OIMEntityConstants {

	public static String PROFILE = "profile";
	
	public static final String ENTITY_ACCOUNT = "Account";
	public static final String ENTITY_ENTITLEMENT = "Entitlement";
	public static final String ENTITY_ROLE = "Role";
	 
	public static class Application{
		public static String APPLICATION = "Application";
	}
	
	public static class Role{
		public static String ROLE = "Role";
	}
	
	public static class User{
		public static String DIRECT_REPORTEES = "Direct Reportiees";
		public static String PROXY = "Proxy";
	}
	
	public static class Entitlement {
		public static String ENTITLEMENT = "Entitlement";
	}
	
	
			
}
